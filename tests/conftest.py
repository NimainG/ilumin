
import pytest
from app import core
from app.core import greenhouse
from app.models import BaseModel, Greenhouse, Screenhouse,Shadenet,Shield,Auth,User,Module
from app.models import UserRealm,UserInvite,Role,UserRole,IghUser,CustomerUser,UserConfig,ModuleFeature
from app.models import ModulePermission,Farm,FarmActions,FarmReportActions,Customer,CustomerFarm
from app.models import CustomerGreenhouse,CustomerFarmGreenhouse,CustomerShield,CustomerGreenhouseShield
from app.models import CustomerExternalShield,CustomerScreenhouse,CustomerGreenhouseScreenhouse,CustomerShadenet
from app.models import CustomerGreenhouseShadenet,CustomerSubscription,SaleType,Subscription,SubscriptionParticular
from app.models import Partner,Commodity,Supplier,SupplierCommodities,Supply,SupplyComponents,SupplyPhoto,SupplyMovement
from app.models import SupplyApplicationMethod,Pest,PestPhoto,Disease,DiseasePhoto,GreenhouseType,ShieldType,Plants,Variety
from app.models import ProductionStage,Planting,PlantingProductionStage,PlantingCrops,ShieldPlantingSetting,WaterTankSetting
from app.models import Co2Setting,AirTemperatureSetting,AirHumiditySetting,LightSetting,SoilMoistureSetting,SoilTemperatureSetting
from app.models import NitrogenSetting,PhosphorusSetting,PotassiumSetting,Issue,Maintenance,MaintenancePhoto,PestDisease,AgronomistVisit
from app.models import ShieldMaintenance,ShieldMaintenanceIssue,GreenhouseMaintenance,GreenhouseMaintenanceIssue,ScreenhouseMaintenance
from app.models import ScreenhouseMaintenanceIssue,ShadenetMaintenance,ShadenetMaintenanceIssue,TraceabilityReportRequest,TraceabilityReportCrop
from app.models import OtherMaintenanceReport,MaintenanceAssignment,MaintenanceClosure,Alert,SystemAlert,ShieldAlert,UserAlert,AlertConfig
import uuid

@pytest.fixture(scope='module')
def add_greenhouse():

    new_greenhouse = Greenhouse()
    return new_greenhouse


@pytest.fixture(scope='module')
def add_screenhouse():
    new_screenhouse = Screenhouse()
    return new_screenhouse

@pytest.fixture(scope='module')
def add_shadenets():
    new_shadenets = Shadenet()
    return new_shadenets

@pytest.fixture(scope='module')
def add_shield():
    new_shield = Shield()
    return new_shield

@pytest.fixture(scope='module')
def add_auth():
    new_auth = Auth()
    return new_auth

@pytest.fixture(scope='module')
def add_user():
    new_user = User()
    return new_user
@pytest.fixture(scope='module')
def add_realm():
    new_realm = UserRealm()
    return new_realm
@pytest.fixture(scope='module')
def add_invite():
    new_invite = UserInvite()
    return new_invite

@pytest.fixture(scope='module')
def add_role():
    new_role = Role()
    return new_role
@pytest.fixture(scope='module')
def add_user_role():
    new_user_role = UserRole()
    return new_user_role
@pytest.fixture(scope='module')
def add_igh_user():
    new_igh_user = IghUser()
    return new_igh_user

@pytest.fixture(scope='module')
def add_customer_user():
    new_customer_user = CustomerUser()
    return new_customer_user
@pytest.fixture(scope='module')
def add_userconfig():
    new_userconfig = UserConfig()
    return new_userconfig
@pytest.fixture(scope='module')
def add_module():
    new_module = Module()
    return new_module
@pytest.fixture(scope='module')
def add_module_feature():
    new_module_feature = ModuleFeature()
    return new_module_feature
@pytest.fixture(scope='module')
def add_module_permission():
    new_module_permission = ModulePermission()
    return new_module_permission
@pytest.fixture(scope='module')
def add_farm():
    new_farm = Farm()
    return new_farm

@pytest.fixture(scope='module')
def add_farm_actions():
    new_farm_actions = FarmActions()
    return new_farm_actions

@pytest.fixture(scope='module')
def add_farm_report():
    new_farm_report = FarmReportActions()
    return new_farm_report
@pytest.fixture(scope='module')
def add_customer():
    new_customer = Customer()
    return new_customer
@pytest.fixture(scope='module')
def add_customer_farm():
    new_customer_farm = CustomerFarm()
    return new_customer_farm
@pytest.fixture(scope='module')
def add_customer_greenhouse():
    new_customer_greenhouse = CustomerGreenhouse()
    return new_customer_greenhouse
@pytest.fixture(scope='module')
def add_customer_farm_greenhouse():
    new_customer_farm_greenhouse = CustomerFarmGreenhouse()
    return new_customer_farm_greenhouse
@pytest.fixture(scope='module')
def add_customer_shield():
    new_customer_shield = CustomerShield()
    return new_customer_shield
@pytest.fixture(scope='module')
def add_customer_greenhouse_shield():
    new_customer_greenhouse_shield = CustomerGreenhouseShield()
    return new_customer_greenhouse_shield
@pytest.fixture(scope='module')
def add_customer_external_shield():
    new_customer_external_shield = CustomerExternalShield()
    return new_customer_external_shield
@pytest.fixture(scope='module')
def add_customer_screenhouse():
    new_customer_screenhouse = CustomerScreenhouse()
    return new_customer_screenhouse

@pytest.fixture(scope='module')
def add_customer_greenhouse_screenhouse():
    new_customer_greenhouse_screenhouse = CustomerGreenhouseScreenhouse()
    return new_customer_greenhouse_screenhouse
@pytest.fixture(scope='module')
def add_customer_shadnet():
    new_customer_shadnet = CustomerShadenet()
    return new_customer_shadnet

@pytest.fixture(scope='module')
def add_customer_greenhouse_shadenet():
    new_customer_greenhouse_shadenet = CustomerGreenhouseShadenet()
    return new_customer_greenhouse_shadenet
@pytest.fixture(scope='module')
def add_customer_subscription():
    new_customer_subscription = CustomerSubscription()
    return new_customer_subscription
@pytest.fixture(scope='module')
def add_saletype():
    new_saletype = SaleType()
    return new_saletype
@pytest.fixture(scope='module')
def add_subscription():
    new_subscription = Subscription()
    return new_subscription
@pytest.fixture(scope='module')
def add_subscription_particular():
    new_subscription_particular = SubscriptionParticular()
    return new_subscription_particular











# @pytest.fixture(scope='module')
# def add_igh_user():
#     new_igh_user = IghUser()
#     return new_igh_user

# @pytest.fixture(scope='module')
# def add_customer_user():
#     new_customer_user = CustomerUser()
#     return new_customer_user
# @pytest.fixture(scope='module')
# def add_userconfig():
#     new_userconfig = UserConfig()
#     return new_userconfig
# @pytest.fixture(scope='module')
# def add_module():
#     new_module = Module()
#     return new_module
# @pytest.fixture(scope='module')
# def add_module_feature():
#     new_module_feature = ModuleFeature()
#     return new_module_feature
# @pytest.fixture(scope='module')
# def add_module_permission():
#     new_module_permission = ModulePermission()
#     return new_module_permission
# @pytest.fixture(scope='module')
# def add_farm():
#     new_farm = Farm()
#     return new_farm

# @pytest.fixture(scope='module')
# def add_farm_actions():
#     new_farm_actions = FarmActions()
#     return new_farm_actions

# @pytest.fixture(scope='module')
# def add_farm_report():
#     new_farm_report = FarmReportActions()
#     return new_farm_report
# @pytest.fixture(scope='module')
# def add_customer():
#     new_customer = Customer()
#     return new_customer
# @pytest.fixture(scope='module')
# def add_customer_farm():
#     new_customer_farm = CustomerFarm()
#     return new_customer_farm
# @pytest.fixture(scope='module')
# def add_customer_greenhouse():
#     new_customer_greenhouse = CustomerGreenhouse()
#     return new_customer_greenhouse
# @pytest.fixture(scope='module')
# def add_customer_farm_greenhouse():
#     new_customer_farm_greenhouse = CustomerFarmGreenhouse()
#     return new_customer_farm_greenhouse
# @pytest.fixture(scope='module')
# def add_customer_shield():
#     new_customer_shield = CustomerShield()
#     return new_customer_shield
# @pytest.fixture(scope='module')
# def add_customer_greenhouse_shield():
#     new_customer_greenhouse_shield = CustomerGreenhouseShield()
#     return new_customer_greenhouse_shield
# @pytest.fixture(scope='module')
# def add_customer_external_shield():
#     new_customer_external_shield = CustomerExternalShield()
#     return new_customer_external_shield
# @pytest.fixture(scope='module')
# def add_customer_screenhouse():
#     new_customer_screenhouse = CustomerScreenhouse()
#     return new_customer_screenhouse




    