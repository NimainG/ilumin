import pytest
from app import core
from app.core import farm
from app.core.farm import reportaction 
from app.models import Greenhouse
from app.models import Screenhouse
from app.core import screenhouse
import uuid
from tests.conftest import add_greenhouse
from tests.conftest import add_screenhouse,add_shadenets,add_auth,add_user,add_realm,add_role
from tests.conftest import add_user_role,add_igh_user,add_customer_user,add_userconfig,add_module
from tests.conftest import add_module_feature,add_module_permission,add_farm,add_farm_actions,add_farm_report
from tests.conftest import add_customer,add_customer_farm,add_customer_external_shield,add_customer_farm_greenhouse
from tests.conftest import add_customer_shield,add_customer_screenhouse,add_customer_greenhouse_screenhouse,add_customer_shadnet
from tests.conftest import add_customer_greenhouse_shadenet,add_customer_subscription,add_saletype,add_subscription




def test_add_greenhouse(add_greenhouse):

    assert add_greenhouse.uid == None
    assert add_greenhouse.status == None
    assert add_greenhouse.greenhouse_ref == None
    assert add_greenhouse.size == None
    assert add_greenhouse.info_link == None
    assert add_greenhouse.greenhouse_type_id == None
    assert add_greenhouse.creator_id == None

def test_add_screenhouse(add_screenhouse):
    assert add_screenhouse.uid == None
    assert add_screenhouse.assigned == None
    assert add_screenhouse.status == None
    assert add_screenhouse.screenhouse_ref == None
    assert add_screenhouse.size == None
    assert add_screenhouse.info_link == None
    assert add_screenhouse.creator_id == None

def test_add_shadenets(add_shadenets):

    assert add_shadenets.uid == None
    assert add_shadenets.assigned == None
    assert add_shadenets.status == None
    assert add_shadenets.shadenet_ref == None
    assert add_shadenets.size == None
    assert add_shadenets.info_link == None
    assert add_shadenets.creator_id == None

def test_add_shield(add_shield):

    assert add_shield.uid == None
    assert add_shield.serial == None
    assert add_shield.status == None
    assert add_shield.greenhouse_id == None
    assert add_shield.assigned == None
    assert add_shield.mac_address == None
    assert add_shield.type_id == None
    assert add_shield.creator_id == None

def test_add_auth(add_auth):

    assert add_auth.uid == None
    assert add_auth.username == None
    assert add_auth.status == None
    assert add_auth.user_id == None
    assert add_auth.password == None
    assert add_auth.activation_code == None
    assert add_auth.reset_code == None
    assert add_auth.creator_id == None

def test_add_user(add_user):

    assert add_user.uid == None
    assert add_user.email_address == None
    assert add_user.status == None
    assert add_user.phone_number == None
    assert add_user.first_name == None
    assert add_user.last_name == None
    assert add_user.avatar_media_id == None
    assert add_user.creator_id == None

def test_add_realm(add_realm):

    assert add_realm.uid == None
    assert add_realm.realm == None
    assert add_realm.status == None
    assert add_realm.user_id == None
    assert add_realm.creator_id == None

def test_add_invite(add_invite):

    assert add_invite.uid == None
    assert add_invite.accept == None
    assert add_invite.status == None
    assert add_invite.igh == None
    assert add_invite.customer_id == None
    assert add_invite.partner_id == None
    assert add_invite.user_id == None
    assert add_invite.creator_id == None


def test_add_role(add_role):

    assert add_role.uid == None
    assert add_role.name == None
    assert add_role.status == None
    assert add_role.description == None
    assert add_role.customer_id == None
    assert add_role.partner_id == None
    assert add_role.user_realm == None
    assert add_role.super_admin == None
    assert add_role.creator_id == None


def test_add_user_role(add_user_role):

    assert add_user_role.uid == None
    assert add_user_role.user_id == None
    assert add_user_role.status == None
    assert add_user_role.role_id == None
    assert add_user_role.user_realm == None
    assert add_user_role.creator_id == None


def test_add_igh_user(add_igh_user):

    assert add_igh_user.uid == None
    assert add_igh_user.user_id == None
    assert add_igh_user.status == None
    assert add_igh_user.id_number == None
    assert add_igh_user.employee_id == None
    assert add_igh_user.creator_id == None
    assert add_igh_user.rank == None

def test_add_customer_user(add_customer_user):

    assert add_customer_user.uid == None
    assert add_customer_user.user_id == None
    assert add_customer_user.status == None
    assert add_customer_user.customer_id == None
    assert add_customer_user.employee_id == None
    assert add_customer_user.creator_id == None


def test_add_userconfig(add_userconfig):

    assert add_userconfig.uid == None
    assert add_userconfig.temp_metric == None
    assert add_userconfig.status == None
    assert add_userconfig.liquid_metric == None
    assert add_userconfig.length_metric == None
    assert add_userconfig.user_id == None
    assert add_userconfig.creator_id == None


def test_add_module(add_module):

    assert add_module.uid == None
    assert add_module.status == None
    assert add_module.name == None
    assert add_module.user_realm == None
    assert add_module.icon_media_id == None
    assert add_module.creator_id == None


def test_add_module_feature(add_module_feature):

    assert add_module_feature.uid == None
    assert add_module_feature.status == None
    assert add_module_feature.name == None
    assert add_module_feature.code == None
    assert add_module_feature.module_id == None
    assert add_module_feature.creator_id == None


def test_add_module_permission(add_module_permission):

    assert add_module_permission.uid == None
    assert add_module_permission.status == None
    assert add_module_permission.role_id == None
    assert add_module_permission.module_feature_id == None
    assert add_module_permission.creator_id == None


def test_add_farm(add_farm):

    assert add_farm.uid == None
    assert add_farm.status == None
    assert add_farm.farm_ref == None
    assert add_farm.name == None
    assert add_farm.address == None
    assert add_farm.state == None
    assert add_farm.city == None
    assert add_farm.pincode == None
    assert add_farm.latitude == None
    assert add_farm.longitude == None
    assert add_farm.area == None
    assert add_farm.creator_id == None


def test_add_farm_actions(add_farm_actions):

    assert add_farm_actions.uid == None
    assert add_farm_actions.status == None
    assert add_farm_actions.customer_id == None
    assert add_farm_actions.current_firm_uid == None
    assert add_farm_actions.creator_id == None




def test_add_farm_report(add_farm_report):

    assert add_farm_report.uid == None
    assert add_farm_report.status == None
    assert add_farm_report.customer_id == None
    assert add_farm_report.current_firm_uid == None
    assert add_farm_report.description == None
    assert add_farm_report.creator_id == None

def test_add_customer(add_customer):

    assert add_customer.uid == None
    assert add_customer.status == None
    assert add_customer.billing_address == None
    assert add_customer.shipping_address == None
    assert add_customer.igh_contact_id == None
    assert add_customer.contact_person_id == None
    assert add_customer.creator_id == None


def test_add_customer_farm(add_customer_farm):

    assert add_customer_farm.uid == None
    assert add_customer_farm.status == None
    assert add_customer_farm.farm_id == None
    assert add_customer_farm.customer_id == None
    assert add_customer_farm.creator_id == None

def test_add_customer_external_shield(add_customer_external_shield):

    assert add_customer_external_shield.uid == None
    assert add_customer_external_shield.status == None
    assert add_customer_external_shield.latitude == None
    assert add_customer_external_shield.longitude == None
    assert add_customer_external_shield.growth_area == None
    assert add_customer_external_shield.soil_type_id == None
    assert add_customer_external_shield.customer_shield_id == None
    assert add_customer_external_shield.creator_id == None


def test_add_customer_farm_greenhouse(add_customer_farm_greenhouse):

    assert add_customer_farm_greenhouse.uid == None
    assert add_customer_farm_greenhouse.status == None
    assert add_customer_farm_greenhouse.name == None
    assert add_customer_farm_greenhouse.customer_farm_id == None
    assert add_customer_farm_greenhouse.customer_greenhouse_id == None
    assert add_customer_farm_greenhouse.creator_id == None


def test_add_customer_shield(add_customer_shield):

    assert add_customer_shield.uid == None
    assert add_customer_shield.status == None
    assert add_customer_shield.shield_id == None
    assert add_customer_shield.customer_id == None
    assert add_customer_shield.creator_id == None

def test_add_customer_screenhouse(add_customer_screenhouse):

    assert add_customer_screenhouse.uid == None
    assert add_customer_screenhouse.status == None
    assert add_customer_screenhouse.screenhouse_id == None
    assert add_customer_screenhouse.customer_id == None
    assert add_customer_screenhouse.creator_id == None





def test_add_customer_greenhouse_screenhouse(add_customer_greenhouse_screenhouse):

    assert add_customer_greenhouse_screenhouse.uid == None
    assert add_customer_greenhouse_screenhouse.status == None
    assert add_customer_greenhouse_screenhouse.customer_farm_greenhouse_id == None
    assert add_customer_greenhouse_screenhouse.customer_screenhouse_id == None
    assert add_customer_greenhouse_screenhouse.creator_id == None


def test_add_customer_shadnet(add_customer_shadnet):

    assert add_customer_shadnet.uid == None
    assert add_customer_shadnet.status == None
    assert add_customer_shadnet.shadenet_id == None
    assert add_customer_shadnet.customer_id == None
    assert add_customer_shadnet.creator_id == None


def test_add_customer_greenhouse_shadenet(add_customer_greenhouse_shadenet):

    assert add_customer_greenhouse_shadenet.uid == None
    assert add_customer_greenhouse_shadenet.status == None
    assert add_customer_greenhouse_shadenet.customer_farm_greenhouse_id == None
    assert add_customer_greenhouse_shadenet.customer_shadenet_id == None
    assert add_customer_greenhouse_shadenet.creator_id == None




def test_add_customer_subscription(add_customer_subscription):

    assert add_customer_subscription.uid == None
    assert add_customer_subscription.status == None
    assert add_customer_subscription.starts == None
    assert add_customer_subscription.ends == None
    assert add_customer_subscription.is_active == None
    assert add_customer_subscription.invoice_id == None
    assert add_customer_subscription.sale_type_id == None
    assert add_customer_subscription.subscription_id == None
    assert add_customer_subscription.customer_id == None
    assert add_customer_subscription.creator_id == None




def test_add_subscription_particular(add_subscription_particular):

    assert add_subscription_particular.uid == None
    assert add_subscription_particular.status == None
    assert add_subscription_particular.item == None
    assert add_subscription_particular.cost == None
    assert add_subscription_particular.subscription_id == None
    assert add_subscription_particular.creator_id == None
     


def test_add_subscription(add_subscription):

    assert add_subscription.uid == None
    assert add_subscription.status == None
    assert add_subscription.name == None
    assert add_subscription.cylce == None
    assert add_subscription.amount == None
    assert add_subscription.customer_id == None
    assert add_subscription.creator_id == None





def test_add_saletype(add_saletype):

    assert add_saletype.uid == None
    assert add_saletype.status == None
    assert add_saletype.name == None
    assert add_saletype.description == None
    assert add_saletype.creator_id == None




 

    

     

   

    


   

 
   




 
    





     

  
  
    


     
   


    




     


   
 








                               





    
    

   


    

    
