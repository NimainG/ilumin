from urllib import response

import pytest
from app import create_app
import os
from app.igh import dashboard
from flask import Flask
from app.igh.admin import admin
from app.igh.customer import customer
from app.igh.admin.inventory import igh_inventory
from app.igh.admin.customers import igh_customers
from app.config import activateConfig
from app.models import db
from app.igh.admin.maintenance import igh_maintenance
from app.igh.admin.reports import igh_reports
from app.igh.admin.settings import igh_settings
from app.igh.customer.farms import customer_farms
from app.igh.customer.support import customer_support
from app.igh.customer.alerts import customer_alerts
from app.igh.customer.reports import customer_reports
from app.igh.customer.inventory import customer_inventory
from app.igh.customer.settings import customer_settings

@pytest.fixture
def test_dashboard_app():
    dashboard_app = create_app(os.getenv('ENV') or 'production')
    return dashboard_app

def test_app():

    app = Flask(__name__)
    app.register_blueprint(admin, url_prefix="/admin/")
    web = app.test_client()
    rv = web.get('/admin/')

    
def test_customer_app():

    app = Flask(__name__)
    app.register_blueprint(customer, url_prefix="/customer/")
    web = app.test_client()
    rv = web.get('/customer/')
    print(rv.data)

def test_admin_inventory():
    app = Flask(__name__)
    app.register_blueprint(igh_inventory, url_prefix="/admin/inventory")
    web = app.test_client()
    rv = web.get('/admin/inventory')
    print(rv.data)


def test_admin_maintain():
    app = Flask(__name__)
    app.register_blueprint(igh_maintenance, url_prefix="/admin/maintenance")
    web = app.test_client()
    rv = web.get('/admin/maintenance')
    print(rv.data)

def test_admin_customers():
    app = Flask(__name__)
    app.register_blueprint(igh_customers, url_prefix="/admin/customers")
    web = app.test_client()
    rv = web.get('/admin/customers')
    print(rv.data)


def test_admin_reports():
    app = Flask(__name__)
    app.register_blueprint(igh_reports, url_prefix="/admin/reports")
    web = app.test_client()
    rv = web.get('/admin/reports')
    print(rv.data)

def test_admin_settings():
    app = Flask(__name__)
    app.register_blueprint(igh_settings, url_prefix="/admin/settings")
    web = app.test_client()
    rv = web.get('/admin/settings')
    print(rv.data)

def test_customer_farms():
    app = Flask(__name__)
    app.register_blueprint(customer_farms, url_prefix="/customer/farms")
    web = app.test_client()
    rv = web.get('/customer/farms')
    print(rv.data)

def test_customer_support():
    app = Flask(__name__)
    app.register_blueprint(customer_support, url_prefix="/customer/support")
    web = app.test_client()
    rv = web.get('/customer/support')
    print(rv.data)

def test_customer_alerts():
    app = Flask(__name__)
    app.register_blueprint(customer_alerts, url_prefix="/customer/alerts")
    web = app.test_client()
    rv = web.get('/customer/alerts')
    print(rv.data)

def test_customer_reports():
    app = Flask(__name__)
    app.register_blueprint(customer_reports, url_prefix="/customer/reports")
    web = app.test_client()
    rv = web.get('/customer/reports')
    print(rv.data)


def test_customer_inventory():
    app = Flask(__name__)
    app.register_blueprint(customer_inventory, url_prefix="/customer/inventory")
    web = app.test_client()
    rv = web.get('/customer/inventory')
    print(rv.data)

def test_customer_setings():
    app = Flask(__name__)
    app.register_blueprint(customer_settings, url_prefix="/customer/settings")
    web = app.test_client()
    rv = web.get('/customer/settings')
    print(rv.data)




 
   

   

    
    

  
    

   

   
   

   


