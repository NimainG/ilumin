from __future__ import print_function

import os
import json
import requests
import uuid
import random
import africastalking

from flask import Flask, request, Response, jsonify
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from multiprocessing import Process
from sqlalchemy import func
from os import path

from datetime import datetime
from calendar import monthrange
from dateutil.relativedelta import relativedelta
from sqlalchemy import extract, Integer

from config import activateConfig

from models import db, Customer, User, Role, UserRole, Greenhouse, \
    CustomerGreenhouse, Farm, CustomerDevice, Device, DeviceDataBatch, Service, \
    CustomerUser, DeviceData

load_dotenv()

active_config = activateConfig('production')

class SMS:
    def __init__(self):
        self.username = active_config.AT_USERNAME
        self.api_key = active_config.AT_API_KEY

        africastalking.initialize(self.username, self.api_key)

        self.sms = africastalking.SMS

def batch_exists(date_time_created, batch, source):
    status = False

    _batch_exist = db\
        .session\
        .query(DeviceDataBatch, DeviceData)\
        .join(DeviceData, DeviceData.batch_id == DeviceDataBatch.id)\
        .filter(DeviceDataBatch.batch == batch)\
        .filter(DeviceDataBatch.data_source == source)\
        .filter(DeviceDataBatch.created_at == date_time_created)\
        .filter(DeviceDataBatch.status > active_config.STATUS_DELETED)\
        .first()
    if _batch_exist:
        status = True

    return status

def save_batch(date_time_created, batch, meta_data, source):
    _attached_device = None
    if 'id' in batch:
        _attached_device = db\
            .session\
            .query(Device, CustomerDevice)\
            .outerjoin(CustomerDevice, CustomerDevice.device_id == Device.id)\
            .filter(Device.serial == batch['id'])\
            .filter(Device.status > active_config.STATUS_DELETED)\
            .first()

    _data_batch = DeviceDataBatch(
        uid=uuid.uuid4(),
        revision=0,
        status=active_config.STATUS_ACTIVE,
        created_at=date_time_created,
        batch=batch,
        meta_data=meta_data,
        data_source=source
    )
    db.session.add(_data_batch)
    db.session.commit()

    if _data_batch:
        customer_id = None
        device_id = None
        if _attached_device and _attached_device.CustomerDevice.customer_id:
            customer_id = _attached_device.CustomerDevice.customer_id
            device_id = _attached_device.Device.id

            _device_data = DeviceData(
                uid=uuid.uuid4(),
                revision=0,
                status=active_config.STATUS_ACTIVE,
                created_at=date_time_created,
                device_id=device_id,
                customer_id=customer_id,
                batch_id=_data_batch.id,
            )
            db.session.add(_device_data)
            db.session.commit()

        print("Message id: " + str(meta_data['message_id']) + " saved!")
    else:
        print("Message id: " + str(meta_data['message_id']) + " FAILED!")

def import_data():
    app = Flask(__name__)
    app.config.from_object(activateConfig(active_config))
    app.app_context().push()
    db.init_app(app)

    at_sms = SMS()

    last_received_id = 0;
    _device_data_batch = DeviceDataBatch\
        .query\
        .filter(DeviceDataBatch.data_source == active_config.DATA_AT)\
        .filter(DeviceDataBatch.status > active_config.STATUS_DELETED)\
        .order_by(DeviceDataBatch.meta_data['message_id'].astext.cast(Integer).desc())\
        .limit(1)\
        .all()
    if len(_device_data_batch) > 0:
        last_received_id = _device_data_batch[0].meta_data['message_id']

    try:
        data_count = 0
        while True:
            MessageData = at_sms.sms.fetch_messages(last_received_id)
            messages = MessageData['SMSMessageData']['Messages']
            if len(messages) == 0:
                break
            k = 0
            for message in messages:
                to_ingest = {}
                _data = message['text'].split(",")
                j = 0;
                for _d in _data:
                    _k_v = _d.strip().split("=")
                    if len(_k_v) > 1:
                        to_ingest[_k_v[0].strip().lower()] = _k_v[1].strip()
                    else:
                        to_ingest["data_" + str(k) + "_" + str(j)] = _d.strip()

                    j += 1

                if 'id' in to_ingest and (to_ingest['id'] == 'IGH01' or to_ingest['id'] == 'NDANAI'):
                    to_ingest['id'] = 'IGH45RT578UGF43B'

                if 'npk' not in to_ingest:
                    to_ingest['npk'] = [
                            0,
                            0,
                            0
                        ]

                if 'sp1id' not in to_ingest:
                    to_ingest['sp1id'] = 'ighsp001'

                if 'sp2id' not in to_ingest:
                    to_ingest['sp2id'] = 'ighsp002'

                if 'sp1bv' not in to_ingest:
                    to_ingest['sp1bv'] = 0

                if 'sp2bv' not in to_ingest:
                    to_ingest['sp2bv'] = 0

                if 'vp' not in to_ingest:
                    to_ingest['vp'] = 0

                if 'ssig' not in to_ingest:
                    to_ingest['ssig'] = 0

                if 'csig' not in to_ingest:
                    to_ingest['csig'] = 0

                if 'smsc' not in to_ingest:
                    to_ingest['smsc'] = 0

                if not batch_exists(message['date'], to_ingest, active_config.DATA_AT):
                    meta_data = {
                        "message_id": message['id'],
                        "from": message['from'],
                        "to": message['to'],
                        "linkId": message['linkId']
                    }

                    print("Saving message id: " + str(message['id']))
                    save_batch(message['date'], to_ingest, meta_data, active_config.DATA_AT)
                    data_count += 1

                last_received_id = int(message['id'])

                k += 1

        print("DONE! Imported: " + str(data_count) + " messages from AfricasTalking")

    except Exception as e:
        print ('Encountered an error while fetching: %s' % str(e))

if __name__ == '__main__':
    p1 = Process(target=import_data)
    p1.start()
