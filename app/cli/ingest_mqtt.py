import os
import json
import requests
import uuid
import random
import time
import numpy as np
import struct
import binascii
import codecs
import paho.mqtt.client as mqtt

from flask import Flask, request, Response, jsonify
from multiprocessing import Process
from dotenv import load_dotenv

from config import activateConfig

load_dotenv()

active_config = activateConfig(os.getenv('ENV') or 'production')

app_root = os.path.dirname(os.path.abspath(__file__))
upload_folder = os.path.join(app_root, '../messages/')

def on_connect(client, userdata, flags, rc):
    client.subscribe("ngao")

def on_message(client, userdata, msg):
    message_time = str(int(time.time()))

    message_file = open(upload_folder + message_time, "wb")
    message_file.write(msg.payload)
    message_file.close()

def ingest_data():
    app = Flask(__name__)
    app.config.from_object(activateConfig(active_config))
    app.app_context().push()

    if not os.path.exists(upload_folder):
        os.makedirs(upload_folder)

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.username_pw_set('shields', '940610b43b1')
    client.connect("localhost")
    client.loop_forever()

def read_data():
    # payload_parser = MessageParser("1605541989")
    message_file = open(upload_folder + "1605618566", "r")
    byte = message_file.read(1)
    while byte:
        print(byte)
        byte = message_file.read(1)

    message_file.close()
    # message_file.write(msg.payload)
    # read_data = binascii.b2a_base64(message_file.read()).decode('utf-8')
    # print(read_data)
    # print(struct.unpack('<i',message_file.read(4)))

    
   


if __name__ == '__main__':
    p1 = Process(target=read_data)
    p1.start()
