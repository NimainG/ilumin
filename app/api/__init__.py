
from app.core import alert
from flask import Blueprint
from app.core import user
from app.core.user import invites
from app.core.user import auth
from app.core.user import realms
from app.core.user import configs
from app.core.user import user_roles
from app.core.user import igh
from app.core.user import customers
from app.core.user import partners

from re import DEBUG
from app import *

from app.helper import validateEmailAddress, valid_phone_number, send_account_activation_mail

api = Blueprint('dashboard', __name__, template_folder='templates', static_folder='static',  static_url_path='/igh/static')
api.config = {}

load_dotenv()
