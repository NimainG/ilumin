import os
import logging
import flask_sqlalchemy
from flask import render_template
from flask import Flask
from dotenv import load_dotenv

from app.models import db
from app.config import activateConfig

from flask_mail import Mail
from re import DEBUG
from app import *
from flask import Blueprint
from flask_wtf.csrf import CSRFProtect
from flask_mail_sendgrid import MailSendGrid

app = Flask(__name__)
app.config[DEBUG]=True
app.config['SECRET_KEY'] = 'top-secret!'
app.config['MAIL_SERVER'] = 'smtp.sendgrid.net'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_SENDGRID_API_KEY'] = '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'
app.config['MAIL_USERNAME'] = 'apikey'
app.config['MAIL_PASSWORD'] = "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"
app.config['MAIL_DEFAULT_SENDER'] = "taita.ngetich@synnefa.io"
# mail = Mail(app)
mail = MailSendGrid(app)

csrf = CSRFProtect()


# app = Flask(__name__)
# app.config['SECRET_KEY'] = 'top-secret!'
# app.config['MAIL_SERVER'] = 'smtp.sendgrid.net'
# app.config['MAIL_PORT'] = 587
# app.config['MAIL_USE_TLS'] = True
# app.config['MAIL_USERNAME'] = 'apikey'
# app.config['MAIL_PASSWORD'] = os.environ.get('SENDGRID_API_KEY')
# app.config['MAIL_DEFAULT_SENDER'] = "taita.ngetich@synnefa.io"
# mail = Mail(app)


# app = Flask(__name__)
# app.config[DEBUG]=True
# app.config['MAIL_SERVER'] = 'smtp.mailgun.org' 
# app.config['MAIL_PORT'] =587
# app.config['MAIL_USERNAME'] = 'postmaster@sandbox45a4ff91749947279f7a38d895b6b901.mailgun.org'
# app.config['MAIL_PASSWORD'] = '467cb6f4b24e848fda60f89ced311e3f-7b8c9ba8-00718567'
# app.config['MAIL_DEFAULT_SENDER']='nimain@sinsignal.com'
# app.config['MAIL_TLS'] = False
# app.config['MAIL_SSL'] = False
# app.config['MAIL_DEBUG'] = True
# app.config['MAIL_ASCII_ATTACHMENTS']=False
# mail = Mail(app) # Instance

load_dotenv()


# app = Flask(__name__, static_folder=<path>)
def create_app(config_name):
    igh = Flask(__name__)
    
    igh.config.from_object(activateConfig(config_name))
    igh.app_context().push()
    igh.config['JSON_SORT_KEYS'] = False
    igh.config['SECRET_KEY'] = os.urandom(24)
# this is important or wont work
    igh.config['SESSION_COOKIE_NAME'] = "my_session"
    db.init_app(igh)
    # csrf.init_app(igh)
    db.create_all()

    # @igh.route('/', methods=['GET'])
    # def index():
   
    #  return render_template(
    #     'admin/main.html',
    #      ADMIN_PORTAL=True,
    #     ADMIN_PORTAL_DASHBOARD_ACTIVE='active',
        
    #     )

    

    from app.igh import dashboard
    igh.register_blueprint(dashboard)

    #from app.igh.api import api
    #igh.register_blueprint(admin, url_prefix="/api/")

    from app.igh.admin import admin
    igh.register_blueprint(admin, url_prefix="/admin/")

    from app.igh.admin.inventory import igh_inventory
    igh.register_blueprint(igh_inventory, url_prefix="/admin/inventory")

    from app.igh.admin.resources import igh_resources
    igh.register_blueprint(igh_resources, url_prefix="/admin/resources")

    from app.igh.admin.customers import igh_customers
    igh.register_blueprint(igh_customers, url_prefix="/admin/customers")

    from app.igh.admin.maintenance import igh_maintenance
    igh.register_blueprint(igh_maintenance, url_prefix="/admin/maintenance")

    from app.igh.admin.alerts import igh_alerts
    igh.register_blueprint(igh_alerts, url_prefix="/admin/alerts")

    from app.igh.admin.reports import igh_reports
    igh.register_blueprint(igh_reports, url_prefix="/admin/reports")

    from app.igh.admin.settings import igh_settings
    igh.register_blueprint(igh_settings, url_prefix="/admin/settings")

    from app.igh.customer import customer
    igh.register_blueprint(customer, url_prefix="/customer/")

    from app.igh.customer.farms import customer_farms
    igh.register_blueprint(customer_farms, url_prefix="/customer/farms")

    from app.igh.customer.support import customer_support
    igh.register_blueprint(customer_support, url_prefix="/customer/support")

    from app.igh.customer.alerts import customer_alerts
    igh.register_blueprint(customer_alerts, url_prefix="/customer/alerts")

    from app.igh.customer.reports import customer_reports
    igh.register_blueprint(customer_reports, url_prefix="/customer/reports")

    from app.igh.customer.inventory import customer_inventory
    igh.register_blueprint(customer_inventory, url_prefix="/customer/inventory")

    from app.igh.customer.settings import customer_settings
    igh.register_blueprint(customer_settings, url_prefix="/customer/settings")


    # from app.igh.customer.greenhouses import customer_greenhouses
    # igh.register_blueprint(customer_greenhouses, url_prefix="/customer/greenhouses")
    #
    # from app.igh.customer.devices import customer_devices
    # igh.register_blueprint(customer_devices, url_prefix="/customer/devices")
    #
    # from app.igh.customer.reports import customer_reports
    # igh.register_blueprint(customer_reports, url_prefix="/customer/reports")
    #
    # from app.igh.customer.settings import customer_settings
    # igh.register_blueprint(customer_settings, url_prefix="/customer/settings")

    return igh
