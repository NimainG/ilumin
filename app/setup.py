import os
import logging
import uuid
import secrets
import flask_sqlalchemy
import json

from flask import Flask
from datetime import datetime, timedelta
from dotenv import load_dotenv
from werkzeug.security import generate_password_hash

from app.models import db
from app.core import user
from app.core.user import roles
from app.core.user import user_roles

from app.core import module
from app.core.module import features
from app.core.module import permissions

from app.core import media


from app.config import activateConfig

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

class InitData():

    role_id = None

    def role(self):
        # Default roles
        super_admin_role = roles.fetch_by_name("Super Admin", True).get_json()
        if not super_admin_role:
            default_roles = [
                {
                    "name": "Super Admin",
                    "description": "Super Admin",
                    "user_realm": config.USER_IGH,
                    "super_admin": 1,
                }
            ]
            for _role in default_roles:
                _add_role = roles.add_new(
                    json.dumps(_role),
                    True
                )
                if _add_role:
                    if _role['user_realm'] == config.USER_IGH:
                        self.role_id = _add_role[0].get_json()['id']

    def user(self):
        # Super User
        super_user = user.fetch_by_email("nimain@live.com").get_json()
        if not super_user:
            _user_data = {
                "email_address": "nimain@live.com",
                "first_name": "Super",
                "last_name": "Admin",
                "phone_number": "+918368746404",
                "user_realm": config.USER_IGH,
                "role_id": self.role_id
            }

            _add_user = user.add_new(
                json.dumps(_user_data),
                True
            )

    def modules(self):
        # Default modules
        super_admin_role = roles.fetch_by_name("Super Admin", True).get_json()
        if super_admin_role:
            user_in_role = user_roles.fetch_by_role(super_admin_role['real_id']).get_json()
            if user_in_role:
                default_modules = [
                    #  Admin
                    {
                        "name": "Summary actions",
                        "icon": "user-summary.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Agronomist",
                                "code": "10011",
                            },
                            {
                                "name": "Sales",
                                "code": "10012",
                            },
                        ]
                    },
                    {
                        "name": "Inventory section",
                        "icon": "user-inventory.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Adding actions",
                                "code": "10021",
                            },
                            {
                                "name": "Deleting actions",
                                "code": "10022",
                            },
                        ]
                    },
                    {
                        "name": "Customer section",
                        "icon": "user-customer.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Adding actions",
                                "code": "10031",
                            },
                            {
                                "name": "Deleting actions",
                                "code": "10032",
                            },
                        ]
                    },
                    {
                        "name": "Maintenance section",
                        "icon": "user-maintenance.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Adding actions",
                                "code": "10041",
                            },
                            {
                                "name": "Deleting actions",
                                "code": "10042",
                            },
                        ]
                    },
                    {
                        "name": "Alert list",
                        "icon": "user-alert.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Email subscription",
                                "code": "10051",
                            },
                            {
                                "name": "SMS subscription",
                                "code": "10052",
                            },
                        ]
                    },
                    {
                        "name": "Reports section",
                        "icon": "user-report.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Daily work report",
                                "code": "10061",
                            },
                            {
                                "name": "Scoring reports",
                                "code": "10062",
                            },
                            {
                                "name": "Deleting reports",
                                "code": "10063",
                            },
                        ]
                    },
                    {
                        "name": "Traceabilty record section",
                        "icon": "user-traceability.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Creating traceabilty reports",
                                "code": "10071",
                            },
                        ]
                    },
                    {
                        "name": "User accounts section",
                        "icon": "user-account.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Adding users",
                                "code": "10081",
                            },
                            {
                                "name": "Accepting invites",
                                "code": "10082",
                            },
                            {
                                "name": "Deactivating users",
                                "code": "10083",
                            },
                        ]
                    },
                    {
                        "name": "User role section",
                        "icon": "user-role.svg",
                        "user_realm": config.USER_IGH,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Editing roles",
                                "code": "10091",
                            },
                        ]
                    },

                    #  Customers
                    {
                        "name": "Summary actions",
                        "icon": "user-summary.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Greenhouse details",
                                "code": "20011",
                            },
                            {
                                "name": "Device details",
                                "code": "20012",
                            },
                        ]
                    },
                    {
                        "name": "Maintenance section",
                        "icon": "user-maintenance.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Adding actions",
                                "code": "20021",
                            },
                            {
                                "name": "Deleting actions",
                                "code": "20022",
                            },
                        ]
                    },
                    {
                        "name": "Alert list",
                        "icon": "user-alert.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Email subscription",
                                "code": "20031",
                            },
                            {
                                "name": "SMS subscription",
                                "code": "20032",
                            },
                        ]
                    },
                    {
                        "name": "Reports section",
                        "icon": "user-report.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Daily work report",
                                "code": "20041",
                            },
                            {
                                "name": "Scoring reports",
                                "code": "20042",
                            },
                            {
                                "name": "Deleting reports",
                                "code": "20043",
                            },
                        ]
                    },
                    {
                        "name": "Product Inventory section",
                        "icon": "user-customer-product.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Activating products",
                                "code": "20051",
                            },
                            {
                                "name": "Deactivating products",
                                "code": "20052",
                            },
                            {
                                "name": "Deleting reports",
                                "code": "20043",
                            },
                        ]
                    },
                    {
                        "name": "Activated products",
                        "icon": "user-customer-product.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Deactivating products",
                                "code": "20061",
                            },
                            {
                                "name": "Editing products",
                                "code": "20062",
                            },
                        ]
                    },
                    {
                        "name": "Fertilizers & agrochemicals",
                        "icon": "user-customer-product.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Reordering products",
                                "code": "20071",
                            },
                            {
                                "name": "Editing products",
                                "code": "20072",
                            },
                        ]
                    },
                    {
                        "name": "User accounts section",
                        "icon": "user-customer-product.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Adding users",
                                "code": "20081",
                            },
                            {
                                "name": "Accepting invites",
                                "code": "20082",
                            },
                            {
                                "name": "Deactivating users",
                                "code": "20083",
                            },
                        ]
                    },
                    {
                        "name": "User role section",
                        "icon": "user-role.svg",
                        "user_realm": config.USER_CUSTOMER,
                        "creator_id": user_in_role[0]["user"]["id"],
                        "features": [
                            {
                                "name": "Editing roles",
                                "code": "20091",
                            },
                        ]
                    },
                ]
                for _module in default_modules:
                    _module_exists = module.fetch_by_name_realm(_module["name"], _module["user_realm"]).get_json()
                    if not _module_exists:
                        _media = media.add_new(
                            json.dumps({
                                "file_url": "icons/" + _module["icon"],
                                "caption": _module["name"] + " icon",
                                "creator_id": _module["creator_id"]
                            }),
                            True
                        )[0].get_json()
                        if "id" in _media:
                            _module["icon_media_id"] = _media["id"]

                        _add_module = module.add_new(
                            json.dumps(_module),
                            True
                        )
                        if _add_module:
                            for _feature in _module["features"]:
                                _module_obj = _add_module[0].get_json()
                                if "id" in _module_obj:
                                    _feature["module_id"] = _module_obj["id"]
                                    _feature["creator_id"] = user_in_role[0]["user"]["id"]
                                    _add_feature = features.add_new(
                                        json.dumps(_feature),
                                        True
                                    )
                                    if _add_feature:
                                        _new_permission = {
                                            "role_id": super_admin_role["id"],
                                            "module_feature_id": _add_feature[0].get_json()["id"],
                                            "creator_id": user_in_role[0]["user"]["id"]
                                        }
                                        _add_permission = permissions.add_new(
                                            json.dumps(_new_permission),
                                            True
                                        )

def create_app(config_name):
    igh = Flask(__name__)
    igh.config.from_object(activateConfig(config_name))
    igh.app_context().push()
    igh.config['JSON_SORT_KEYS'] = False

    db.init_app(igh)

    init_data = InitData()
    init_data.role()
    init_data.user()
    init_data.modules()

    return igh

create_app(config)
