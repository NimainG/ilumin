from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid
from werkzeug.exceptions import HTTPException
from werkzeug.security import check_password_hash, generate_password_hash

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from app.core import mail as mails
from app.core.user import logs
from app.config import activateConfig
from app import *
from app.core import alert, customer
from app.core import user
from app.core.user import invites
from app.core.user import auth
from app.core.user import realms
from app.core.user import configs
from app.core.user import user_roles
from app.core.user import igh
from app.core.user import roles
from app.core.user import customers
from app.core.user import partners
from app.core import customer as customer_user


from app.core.user import customers as _customer
from app.core import customer as customerfy
from app.core.customer import farms as _farms
from app.core import farm as ownfarms
from app.core.farm import actions


from app.helper import validateEmailAddress, valid_phone_number, send_account_activation_mail

dashboard = Blueprint('dashboard', __name__, template_folder='templates',
                      static_folder='static',  static_url_path='/igh/static')
dashboard.config = {}
load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def session_setup(user_id):
    _active_user = user.fetch_one(user_id, True).get_json()
    # print("active user",_active_user)
    if _active_user:
        # _active_user = user.fetch_by_id(user_id).get_json()
        _active_realm = realms.fetch_by_user(
            _active_user["real_id"]).get_json()
        _user_configs = configs.fetch_by_user(
            _active_user["real_id"]).get_json()
        _user_role = user_roles.fetch_by_user(
            _active_user["real_id"]).get_json()
        _alert_config = alert.fetch_alert_config(
            _active_user["real_id"])[0].get_json()
        # print('user session1',_active_realm)
        # print('user session2',_user_configs)
        # print('user session3',_user_role)
        # print('user session4',_alert_config)
        _staff_info = {}
        if _active_realm[0]['realm'] == config.USER_IGH:
            _staff_info = igh.fetch_by_user(_active_user["real_id"]).get_json()
            # print('user session5',_staff_info)
        if _active_realm[0]['realm'] == config.USER_CUSTOMER:
            _staff_info = customers.fetch_by_user(
                _active_user["real_id"]).get_json()
            # print('user session6',_staff_info)
        if _active_realm[0]['realm'] == config.USER_PARTNER:
            _staff_info = partners.fetch_by_user(
                _active_user["real_id"]).get_json()
            # print('user session7',_staff_info)
        if _active_user and _active_realm and _user_configs and _user_role:
            session["profile"] = {
                "user": _active_user,
                "realm": realms.realm_name(_active_realm[0]['realm']),
                "configs": _user_configs,
                "role": _user_role,
                "staff_info": _staff_info,
                "alert_config": _alert_config
            }
        # print('user session8',session["profile"])


def portal_check(session, portal=None):

    if portal is not None:
        known_realm = ""
        if "profile" in session:
            if 'realm' in session['profile'] and session['profile']['realm'] != "":
                known_realm = ""
                if session['profile']['realm'].split(" ")[0] == 'IGH':
                    known_realm = 'admin'

                if session['profile']['realm'].split(" ")[0] == 'Customer':
                    known_realm = 'customer'

                if session['profile']['realm'].split(" ")[0] == 'Partner':
                    known_realm = 'partner'

        if portal != known_realm:
            return True, redirect("/" + known_realm)

        return False, False

    return True, redirect("/login")


@dashboard.route('/', methods=['POST', 'GET'])
def main():
    if "profile" not in session:
        return redirect("/login")

    if 'realm' in session['profile'] and session['profile']['realm'] != "" and session['profile']['user']['status'] == "Active" and session['profile']['role']['role']['status']=="Active":
        if session['profile']['realm'] == 'IGH':
            
            return redirect("/admin")

    if session['profile']['realm'] == 'Customer' and session['profile']['role']['role']['status']=="Active":
      

         _this_org = session["profile"]["user"]["real_id"]

         print("This Org",_this_org)
       
         if _this_org != None:
          _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()

          print("Customer Real ID",_cutomer_real_id)
     
          if _cutomer_real_id != {} :
           cutomers_uid = _cutomer_real_id['customer']['id']
           _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
           _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
           if _farm_action_id == {} :
        
            return redirect("/customer/farms/addfirstfarm")

           else:
            return redirect("/customer/")

    return render_template('error.html')


@dashboard.route('/login', methods=['POST', 'GET'])
def login():
    action_response = {}
    _posted = request.form
    if len(_posted) > 0:
        if 'username' in _posted and _posted['username'].strip() != "" and \
                'password' in _posted and _posted['password'].strip() != "":
            _user = auth.fetch_by_username(_posted['username']).get_json()
            print("Auth user",_user)
            if _user and _user["user"] != {} and _user['password'] != None:
                if check_password_hash(_user['password'], _posted['password'].strip()):
                    session_setup(_user["user"]["id"])

                    return redirect("/")

            action_response = {
                "type": "danger",
                "message": "Invalid credentials supplied"
            }

        else:
            action_response = {
                "type": "warning",
                "message": "Email address and password pair required to login"
            }

    return render_template(
        'login.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date": str(datetime.now().year)}
    )


@dashboard.route('/request-invite', methods=['POST', 'GET'])
def request_invite():
    action_response = {}
    _posted = request.form

    if len(_posted) > 0:
        if 'fullname' in _posted and _posted['fullname'].strip() != "" and \
                'email_address' in _posted and validateEmailAddress(_posted['email_address'].strip()) != "":
         name = _posted['fullname']
         email = _posted['email_address']
         first, *last = name.split()
         fname = "{first}".format(first=first)
         lname = "{last}".format(last=" ".join(last))
        #  print("FNAME,LNAME,EMAIL",fname,lname,email)

         exists = auth.fetch_by_username(email).get_json()
         if exists:
                action_response = {
                    "type": "danger",
                    "message": "Email address already in use, please use a different one."
                }
         else:
                phone = None          
                _role_id = None
                _default_role = roles.fetch_by_name("Customer User").get_json()
                if _default_role:
                    _role_id = _default_role['id']
                    print("Role id ", _role_id)
                new_user = user.add_new(
                    json.dumps({
                        "email_address": email,
                        "first_name": fname,
                        "last_name": lname,
                        "phone_number": phone,
                        "user_realm": config.USER_CUSTOMER,
                        "role_id": _role_id
                    })
                )
                print(str(new_user[0].get_json()))

                if new_user:

                    _user_id = user.fetch_by_email(email).get_json()['id']
                    user_real = user.fetch_one(_user_id,True).get_json()['real_id']
                    billing = None
                    shipping = None
                    igh_contact = None
                    _name = _posted['fullname']

                    new_customer = customer_user.add_new(
                        json.dumps({
                                   "status" : config.STATUS_INACTIVE,
                                   "name": _name,
                                   "billing_address": billing,
                                   "shipping_address": shipping,
                                   "igh_contact_id": igh_contact,
                                   "creator_id": _user_id,
                                   "contact_person_id": _user_id

                                   })
                    )

                    print(str(new_customer[0].get_json()))

                    if new_customer:
                        _user_real_id = user.fetch_one(_user_id, True).get_json()['real_id']
                        new_customery_id = customer_user.fetch_by_user(
                            _user_real_id).get_json()['main_id']
                       
                    #    _new_cusreal_id = customer_user.fetch_one(new_customery_id,True).get_json()['real_id']

                        new_customer_id = customers.add_new(
                            json.dumps({
                                "employee_id": user_real,
                                "customer_id": new_customery_id,
                                "user_id": _user_id,
                                "creator_id": _user_id
                            })
                        )
                        print(str(new_customer_id[0].get_json()))
                        if new_customer_id:
                            action_response = {
                                  "type": "success",
                                  "message": "Please click on the link in your email to activate your account."
            }
        else:
            action_response = {
                "type": "warning",
                "message": "Full name,email address and Role are required to invite request."
            }
        


    return render_template(
        'request-invite.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date": str(datetime.now().year)}
    )


@dashboard.route('/invite/<invite_id>', methods=['POST', 'GET'])
def invite(invite_id):
    action_response = {}
    _posted = request.form

    invite_accepted = 0
    invitation = invites.fetch_one(invite_id, True).get_json()
    user_email_details = invitation['user']['email_address']
    if not invitation:
        action_response = {
            "type": "danger",
            "message": "Invite link is invalid."
        }
    else:
        invite_accepted = invitation['accept']

        if invitation['accept'] == 1:
            action_response = {
                "type": "warning",
                "message": "Invite has already been accepted."
            }
        else:
            if len(_posted) > 0:
                # if 'username' in _posted and validateEmailAddress(_posted['username'].strip()) and \
                if 'password' in _posted and _posted['password'].strip() != "" and \
                        'password2' in _posted and _posted['password2'].strip() != "":

                    if _posted['password'].strip() == _posted['password2'].strip():
                        _user = user.fetch_one(
                            invitation['user']['id'], True).get_json()
                        _user_real = _user['real_id']
                        if _user:
                            _user_auth = auth.fetch_by_user(
                                _user['real_id']).get_json()
                            if _user_auth:
                                if _user_auth['username'] == user_email_details:
                                    _auth_accept = auth.edit(
                                        _user_auth['id'],
                                        json.dumps({
                                            "password": _posted['password'].strip(),
                                            "activation_code": "-",
                                            "reset_code": "-",
                                        }),
                                        True
                                    )
                                    print("Auth accept",_auth_accept[1])
                                    if _auth_accept[1] == 200:
                                        
                                        c_a_d = customer_user.fetch_by_user(_user['real_id']).get_json()
                                        if c_a_d != {}:
                                            c_a_id = c_a_d['id']
                                            customer_id_a = customer_user.activate(c_a_id)
                                        invites.edit(
                                            invitation['id'],
                                            json.dumps(
                                                {
                                                    "accept": 1
                                                }
                                            )
                                        )

                                        _user_log = logs.add_new(
                                                   json.dumps({
                                                      "action": "Accepted a new User",
                                                      "ref": invitation['user']['first_name'],
                                                      "ref_id": _user_real,
                                                      
                                                      "creator_id" : invitation['user']['id']
                                                    }),
                                                    
                                                    )
                                        print(str(_user_log[0].get_json()))
                                        action_response = {
                                            "type": "success",
                                            "message": "Invite accepted and account activated, you may now login"
                                        }
                                        return redirect(url_for('dashboard.login'))
                                    else:
                                        action_response = {
                                            "type": "danger",
                                            "message": "An error occured, try again later."
                                        }
                                else:
                                    action_response = {
                                        "type": "danger",
                                        "message": "Email address provided does not match that in the invite."
                                    }
                            else:
                                action_response = {
                                    "type": "danger",
                                    "message": "Invitation is invalid."
                                }
                        else:
                            action_response = {
                                "type": "danger",
                                "message": "User is invalid."
                            }
                    else:
                        action_response = {
                            "type": "danger",
                            "message": "Passwords provided do not match"
                        }
                else:
                    action_response = {
                        "type": "warning",
                        "message": "Email and password required to activate"
                    }

    return render_template(
        'invite.html',
        ACCEPTED=invite_accepted,
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date": str(datetime.now().year)}
    )


@dashboard.route('/register', methods=['POST', 'GET'])
def register():
    action_response = {}
    _posted = request.form
    if len(_posted) > 0:
        if 'first_name' in _posted and _posted['first_name'].strip() != "" and \
            'last_name' in _posted and _posted['last_name'].strip() != "" \
                'email_address' in _posted and validateEmailAddress(_posted['email_address'].strip()) != "":

            exists = auth.fetch_by_username(
                _posted['email_address']).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Email address already in use, please use a different one."
                }
            else:
                _role_id = None
                _default_role = roles.fetch_by_name("Customer User").get_json()
                if _default_role:
                    _role_id = _default_role['id']
                    print("Role id ", _role_id)
                new_user = user.add_new(
                    json.dumps({
                        "email_address": _posted['email_address'].strip(),
                        "first_name": _posted['first_name'].strip(),
                        "last_name": _posted['last_name'].strip(),
                        "phone_number": _posted['phone_number'].strip(),
                        "user_realm": config.USER_CUSTOMER,
                        "role_id": _role_id
                    })
                )
                print(str(new_user[0].get_json()))

                if new_user:

                    _user_id = user.fetch_by_email(
                        _posted['email_address'].strip()).get_json()['id']
                    billing = None
                    shipping = None
                    igh_contact = None
                    _name = _posted['first_name'].strip()

                    new_customer = customer_user.add_new(
                        json.dumps({
                                   "name": _name,
                                   "billing_address": billing,
                                   "shipping_address": shipping,
                                   "igh_contact_id": igh_contact,
                                   "creator_id": _user_id,
                                   "contact_person_id": _user_id

                                   })
                    )

                    print(str(new_customer[0].get_json()))

                    if new_customer:
                        _user_real_id = user.fetch_one(
                            _user_id, True).get_json()['real_id']
                        new_customery_id = customer_user.fetch_by_user(
                            _user_real_id).get_json()['main_id']
                        print("New customery ID", new_customery_id)
                    #    _new_cusreal_id = customer_user.fetch_one(new_customery_id,True).get_json()['real_id']

                        new_customer_id = customers.add_new(
                            json.dumps({
                                "employee_id": "",
                                "customer_id": new_customery_id,
                                "user_id": _user_id,
                                "creator_id": _user_id
                            })
                        )
                        print(str(new_customer_id[0].get_json()))
        else:
            action_response = {
                "type": "warning",
                "message": "First name, last name, phone number and email address are required to register."
            }

    return render_template(
        'register.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date": str(datetime.now().year)}
    )


@dashboard.route('/logout', methods=['POST', 'GET'])
def logout():
    # del session['profile']
    session.clear()
    return redirect("/")

# @dashboard.route('/activate', methods=['POST', 'GET'])
# @dashboard.route('/activate/<activation_code>', methods=['POST', 'GET'])
# def activate(activation_code=None):
#     action_response = {}
#     _posted = request.form
#
#     activation_viable = Auth\
#         .query\
#         .filter(Auth.activation_code == activation_code)\
#         .first()
#
#     if not activation_viable:
#         action_response = {
#             "type": "danger",
#             "message": "Activation code is invalid."
#         }
#
#     else:
#         if len(_posted) > 0:
#             if 'username' in _posted and validateEmailAddress(_posted['username'].strip()) and \
#                 'password' in _posted and _posted['password'].strip() != "" and \
#                 'password2' in _posted and _posted['password2'].strip() != "":
#
#                 if _posted['password'].strip() == _posted['password2'].strip():
#                     activation = auth_activate(activation_code, _posted['username'], _posted['password'])
#
#                     if activation:
#                         action_response = {
#                             "type": "success",
#                             "message": "Account activated, you may login"
#                         }
#                     else:
#                         action_response = {
#                             "type": "danger",
#                             "message": "An error occured, try again later."
#                         }
#                 else:
#                     action_response = {
#                         "type": "danger",
#                         "message": "Passwords provided do not match"
#                     }
#             else:
#                 action_response = {
#                     "type": "warning",
#                     "message": "Email and password required to activate"
#                 }
#
#     return render_template(
#         'activate.html',
#         POSTED=_posted,
#         FORM_ACTION_RESPONSE=action_response,
#         DEVICES_ACTIVE='active',
#         DEVICES_LIST='active',
#         data={"date":str(datetime.now().year)}
#     )


@dashboard.route('/reset-request', methods=['POST', 'GET'])
def reset_request():
    action_response = {}
    _posted = request.form
    if len(_posted) > 0:
        if 'username' in _posted and validateEmailAddress(_posted['username'].strip()):
            activation_code = str(uuid.uuid4()).replace("-", "")
            _auth_data = {}
            print("username", _posted['username'].strip())
            username = _posted['username'].strip()
            _auth_data = auth.fetch_by_username(username).get_json()
            print("Auth data", _auth_data)
            if _auth_data != {}:
                _auth_id = _auth_data['id']
                if _auth_data['user'] != {}:
                 _name = _auth_data['user']['first_name']
                 _user_id = _auth_data['user']['id']
                 _user_real = user.fetch_one(
                    _user_id, True).get_json()['real_id']
                 edit_auth = auth.edit(
                    _auth_id,
                    json.dumps({

                        "activation_code": activation_code

                    }),
                    True
                 )
                 invite = invites.fetch_by_user(_user_real).get_json()
                # invite_id = invite['id']
                # print("invites", invite)

                 if edit_auth:
                    # print(_name,_invite_org)
                    _invite_mail = {
                        "to": username,
                        "subject": "FarmCloud Reset Password",
                        "reset": {
                            "names": _name,
                            "code": str(activation_code),
                            #  "org": _invite_org,
                        },
                    }
                    # print(_invite_mail)
                    mails.password_reset_request(

                        json.dumps(_invite_mail)
                    )
                    action_response = {
                        "type": "Sucess",
                        "message": "Please check your email address"
                    }
                 else:
                    action_response = {
                        "type": "danger",
                        "message": "Invalid email address supplied"
                    }

            elif _auth_data == {}:
                action_response = {
                    "type": "danger",
                    "message": "Invalid email address supplied"
                }
            else:
                action_response = {
                    "type": "danger",
                    "message": "Invalid email address supplied"
                }

        else:
            action_response = {
                "type": "warning",
                "message": "Email address is required"
            }

    return render_template(
        'reset-request.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date": str(datetime.now().year)}
    )


@dashboard.route('/reset/<reset_code>', methods=['POST', 'GET'])
def reset(reset_code):
    action_response = {}
    email_id = ''
    get_user_details = auth.fetch_by_activation_code(reset_code).get_json()
    if get_user_details != {} :
       email_id = get_user_details['username']
    _posted = request.form
    
    if len(_posted) > 0 :
        # if 'email_address' in _posted and validateEmailAddress(_posted['email_address'].strip()) and \
        if 'password' in _posted and _posted['password'].strip() != "" and \
                'password2' in _posted and _posted['password2'].strip() != "":
         if get_user_details != {}:
            if _posted['password'].strip() == _posted['password2'].strip():

                _user1 = user.fetch_by_email(email_id).get_json()
                _user = user.fetch_one(_user1['id'], True).get_json()
                if _user:
                    _user_auth = auth.fetch_by_user(_user['real_id']).get_json()
                    print("userss", _user_auth)
                    if _user_auth:
                        if _user_auth['user']['email_address'] == email_id:
                            _auth_accept = auth.edit(
                                _user_auth['id'],
                                json.dumps({
                                    "password": _posted['password'].strip(),
                                    "activation_code": "-",
                                    "reset_code": "-",
                                }),
                                True
                            )
                            if _auth_accept:
                               
                               action_response = {
                                "type": "success",
                                "message": "Password reset, you may now login."
                            }
                            return redirect(url_for('dashboard.login'))
                # if auth_reset_password(_posted['email_address'], _posted['password'], reset_code):
                #     action_response = {
                #         "type": "success",
                #         "message": "Password reset, you may now login."
                #     }
                else:
                    action_response = {
                        "type": "danger",
                        "message": "An error occured, try again later."
                    }
            else:
                action_response = {
                    "type": "danger",
                    "message": "Passwords provided do not match"
                }
         else:
            action_response = {
                "type": "warning",
                "message": "This link is used or expired"
            }
        else:
            action_response = {
                "type": "warning",
                "message": "Email address and password required to reset"
            }
    

    return render_template(
        'reset.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date": str(datetime.now().year)}
    )
