from functools import wraps
import json
import os
import urllib.parse
import requests
import importlib
import uuid
import math
from time import sleep
# from werkzeug import secure_filename
from werkzeug.exceptions import HTTPException
from flask import Flask, flash, redirect, url_for
from werkzeug.utils import secure_filename
from werkzeug.exceptions import HTTPException
from app.core import media
from app.core.media import files
from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from app.core import maintenance
from app.core.user import igh
from app.core import customer
from app.core import customer as real_customer
from app.core.maintenance import agronomist_visit, assignment
from app.core.maintenance import shield_issues
from app.core.maintenance import greenhouse_issues
from app.core.maintenance import greenhouses as _greenhouse_issuebefore
from app.core.maintenance import screenhouse_issues
from app.core.maintenance import shadenet_issues
from app.core.maintenance import traceability_report_requests
from app.core.maintenance import others as other_request
from app.core.user import customers as cuser
from app.core.maintenance import traceability_report_crops
from app.core.planting import crops
from app.core.user import igh_users
from app.core.maintenance import pests_diseases as pnd
from app.core.maintenance import issues
from app.core.maintenance import closure
from app.core import pest as pesto
from app.core import disease
from werkzeug.wrappers import response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from app.core.customer import shields
from app.core.planting import plants
from app.core.planting import varieties
from app.core.customer import greenhouses as g_house
from app.core.customer import screenhouses as s_house
from app.core.customer import shadenets as sh_net
from app.models import GreenhouseMaintenance
from app.core import supply
from app.core.supply import components
from app.core.customer import farms as _farms
from app.config import activateConfig
from app.core.planting import production_stages
from app.core import user
from app.igh import portal_check
from app.igh.admin import contact_person

from app.igh import session_setup

from app.helper import valid_uuid
from app.core.maintenance import photos as pndphotos

from app.core.maintenance import assignment as assignmembers
from app.models import db
from app.models import Issue
from werkzeug.wrappers import response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from app.models import Maintenance
from app.models import ProductionStage
from app.models import Pest
from app.models import Media
igh_maintenance = Blueprint('igh_maintenance', __name__,
                            static_folder='../../static', template_folder='.../../templates')
igh_maintenance.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


@igh_maintenance.route('/', methods=['POST', 'GET'], defaults={"page": 1})
@igh_maintenance.route('/<int:page>', methods=['POST', 'GET'])
def alerts(page):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    page = page
    prev = page-1
    next = page+1

    no_of_posts = config.PAGE_LIMIT
    # _maintenance = maintenance.fetch_all().get_json()
    action_response = {}
    _pestndesease = {}
    closure_data = []
    _maintain1 = []
    
    # print("Close data",closure_data)

    # no_of_posts = 10

    # last = math.ceil(len(_maintenances)/no_of_posts)
    # page = request.args.get('page')
    # if (not str(page).isnumeric()):
    #     page = 1
    # page = int(page)
    # _maintenance = _maintenances[(page-1)*no_of_posts :(page-1)*no_of_posts+ no_of_posts]
    # if page==1:
    #     prev = "#"
    #     next = "/?page="+ str(page+1)
    # elif page==last:
    #     prev = "/?page="+ str(page-1)
    #     next = "#"
    # else:
    #     prev = "/?page="+ str(page-1)
    #     next = "/?page="+ str(page+1)

    _assign_users = igh_users.fetch_all().get_json()
    _this_org1 = session["profile"]["user"]["id"]
    _pestndesease = {}

    # _details = pnd.fetch_all().get_json()
    # print("Maintain",_agronomist_visit)

    # _maintain_real_id = maintenance.fetch_by_customer_id(_customer_id).get_json()

    # _igh_user = _maintain_real_id["id"]
    # print("Maintain real id",_maintain_real_id)
    

    total = {}
    total = round(len(_maintain1)/no_of_posts)

    if request.method == 'POST' and 'tag_main' in request.form:
        tag = request.form["tag_main"]
        if tag:
            # search = "%{}%".format(tag)
            search = tag
            if search:

                _maintainy = []
                if search.isnumeric() == True:
                    _maintainy = db.session.query(Maintenance).filter(or_((Maintenance.customer_id == search), (
                        Maintenance.serial == search), (Maintenance.creator_id == search))).all()
                # _customery = db.session.query(Maintenance).filter(or_(Maintenance.serial.like(
                #     '%'+search+'%'), Maintenance.type.like('%'+search+'%'))).limit(no_of_posts).offset((page-1)*no_of_posts)

                response = []
                for custmer in _maintainy:
                    response.append(maintenance.maintenance_obj(custmer))
                _maintenance = jsonify(response).get_json()

    # print(request.form.get("summary_details", "None provided"))

    posted = request.form
    if posted:
        _assign_id = posted.get('assign_id')
        _check_id = posted.get('check_id')
        notes = posted.get('note')
    #    if _check_id == None :

        print("Assign ID", _assign_id, _check_id)
        if _check_id:
            _maintain_iid = maintenance.fetch_one(_check_id, True).get_json()
            _maintain_real_id = _maintain_iid['real_id']
            assignments = _maintain_iid['assignment']

            print(_maintain_real_id, assignments)

            if _maintain_real_id != None:
                _pestndesease = pnd.fetch_by_maintenance(
                    _maintain_real_id).get_json()
                print("Details", _pestndesease)

            if _assign_id != None and _check_id != None and assignments == {}:

                _assign_member = assignmembers.add_new(
                    json.dumps({
                        "customer_rating": 3,
                        "scheduled": "",
                        "igh_user_id": _assign_id,
                        "maintenance_id": _check_id,
                        "creator_id": _this_org1,
                    }),
                    True

                )

            if assignments:
                assignment_id = assignments['id']
                assignment_real_id = assignmembers.fetch_one(
                    assignment_id, True).get_json()['real_id']
                print("Assignment Real ID", assignment_real_id)
                _assign_member = assignmembers.edit(
                    assignment_id,
                    json.dumps({
                        "customer_rating": 3,
                        "scheduled": "",
                        "igh_user_id": _assign_id,

                    }),
                    True
                )
                print('Its working', _assign_member)
        else:
            action_response = {
                "type": "danger",
                "message": "Please select an assignment"
            }
    #    if _check_id != None and notes != None:

    #        new_close = closure.add_new (
    #                         json.dumps({
    #                         "igh_user_id":_assign_id,
    #                         "maintenance_id" : _check_id,
    #                         "creator_id" : _this_org1,
    #                         "notes" : notes

    #                            }),
    #                             True
    #                              )
    #        if new_close:
    #            assign_close = maintenance.deactivate(_check_id)
    #            print(assign_close)

    _details = []
    _details = pnd.fetch_all().get_json()
    _agronomist_visit = []
    _agronomist_visit = agronomist_visit.fetch_all().get_json()
    _device_support = []
    _device_support = shield_issues.fetch_all().get_json()
    _screen_house = []
    _screen_house = screenhouse_issues.fetch_all().get_json()
    _shadnet_issue = []
    _shadnet_issue = shadenet_issues.fetch_all().get_json()
    _greenhouse_issue = []
    _greenhouse_issue = greenhouse_issues.fetch_all().get_json()
    _trace_report = []
    _trace_report = traceability_report_crops.fetch_all().get_json()
    _other_report = []
    _other_report = other_request.fetch_all().get_json()
    closure_data = closure.fetch_all().get_json()
    _maintain1 = maintenance.fetch_all().get_json()

    _maintenance = maintenance.fetch_all1(page).get_json()

    pndimages = pndphotos.fetch_all().get_json() 
    print("pndimages",pndimages)

    return render_template(
        'admin/maintenance/main.html',
        ADMIN_PORTAL=True,
        FORM_ACTION_RESPONSE=action_response,
        MAINTAIN=_maintenance,
        MAINTAIN1 = _maintain1,
        PESTDETAILS=_details,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        ASSIGN_USERS=_assign_users,
        PESTND=_pestndesease,
        CLOSURE=closure_data,
        VISIT=_agronomist_visit,
        DEVICESUPPORT=_device_support,
        SCREENHOUSE=_screen_house,
        GREENHOUSE=_greenhouse_issue,
        SHADNET=_shadnet_issue,
        prev=prev, next=next,
        total=total,
        TRACE=_trace_report,
        OTHER=_other_report,
        PESTIMAGES = pndimages,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_maintenance.route('/request', methods=['POST', 'GET'])
def request_type():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _assign_memb = real_customer.fetch_all().get_json()
    _maintain_request = []
    action_response = {}
    # print("all Assign members",_assign_memb)
    _this_org = session["profile"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    posted = request.form
    if posted:
        _serial = posted.get('serial')
        _type = None
    #  _type = posted.get('type')
        _customer = posted.get('customers_id')
    #  _photos = posted.get('photos')
    #  _detail = posted.get('detail')
    #  _assignment = posted.get('assignment')
    #  _closure = posted.get('closure')
    #  _created_by = posted.get('creator_id')
    #  _status = posted.get('status')
        _customer_user = real_customer.fetch_one(_customer, True).get_json()
        _customer_id = real_customer.fetch_one(
            _customer, True).get_json()['real_id']
        print("All received data", _serial, _this_org1, _customer_id)

        _support_admin_request = maintenance.add_new(
            json.dumps({
                "serial": _serial,
                "type": _type,
                "creator_id": _this_org1,
                "customer_id": _customer_id

            }),


        )
        print(str(_support_admin_request[0].get_json()))
        if _support_admin_request:
            action_response = {
                "type": "success",
                "message": "Support is requested"
            }

        if _customer_user:
            session["customer"] = {
                "user": _customer_user,
            }

    return render_template(
        'admin/maintenance/request.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        MAINTAINS=_maintain_request,
        ASSIGNMEMB=_assign_memb,
        FORM_ACTION_RESPONSE=action_response,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_maintenance.route('/request/type', methods=['POST', 'GET'])
def type():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    action_response = {}

    _type = request.form.get('type')

    _customer_id = session["customer"]["user"]["real_id"]

    posted = request.form
    if posted:

        _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()
        _type = posted.get('type')
        if _maintain != {}:
            _support_admin_request_types = maintenance.edit(
                _maintain['id'],
                json.dumps({

                    "type": _type,

                }),
                True
            )
            if _support_admin_request_types:

                action_response = {
                                "type": "success",
                                "message": "Type added."
                            }

    return render_template(
        'admin/maintenance/type.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        TYPE=_type,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/edit/<string:id>', methods=['POST', 'GET'])
def edit_type(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    action_response = {}

    _type = request.form.get('type')
    customer_id = {}
    _maintain = {}

    _maintain = maintenance.fetch_one(id,True).get_json()
    customer_id = _maintain['customer']
    print("Maintain details",_maintain)
    # _customer_id = session["customer"]["user"]["real_id"]

    posted = request.form
    if posted:

        # _maintain = maintenance.fetch_one(id,True).get_json()
        # customer_id = _maintain['customer_id']
        
        _type = posted.get('type')
        if _maintain != {}:
            _support_admin_request_types = maintenance.edit(
                _maintain['id'],
                json.dumps({

                    "type": _type,

                }),
                True
            )
            if _support_admin_request_types:

                action_response = {
                                "type": "success",
                                "message": "Type added."
                            }

    return render_template(
        'admin/maintenance/edit_type.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        TYPE=_type,
        CUSTOMER = customer_id,
        MAINTAIN = _maintain,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )



@igh_maintenance.route('/request/pests-diseases/<string:mid>/<string:cid>', methods=['POST', 'GET'])
def pests_diseases_type_edit(mid,cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _pestoo = pesto.fetch_all().get_json()
    _plant_all = plants.fetch_all().get_json()
    _stages = production_stages.fetch_all().get_json()
    _varities = varieties.fetch_all().get_json()
    # print("IDSSSSSSSSSSSSSSSSS",cid)
    _customer_id = customer.fetch_one(cid,True).get_json()['real_id']
    # print("customer _id",_customer_id)
    _this_org1 = session["profile"]["user"]["id"]
    _this_org = session["profile"]["user"]["real_id"]
    action_response = {}
    if _customer_id:
        _shields = shields.fetch_by_customer(_customer_id).get_json()
        _maintain = maintenance.fetch_one(mid,True).get_json()
    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _maintenance_id = posted.get('maintenance_id')
        _production_stage_id = posted.get('production_stage_id')
        _variety_id = posted.get('variety_id')
        _plant_id = posted.get('plant_id')
        _customer_greenhouse_id = posted.get('customer_greenhouse_id')
        _customer_shield_id = posted.get('customer_shield_id')
        _pest_id = posted.get('pest_id')
        # _pest_alter = posted.get('pest_alter')
        _satge_alter = posted.get('satge_alter')
        _disease_id = posted.get('disease_id')

        if _maintain != {}:
            pest_disease_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            pest_disease_check = pnd.fetch_by_maintenance(
                maintain_real).get_json()

        if _maintain != {} and _satge_alter and  _pest_id and pest_disease_check == {}:

            exists = production_stages.fetch_by_name(_satge_alter).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Staging name already exists please select from list."
                }

            else:
                _stagess = ProductionStage(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_satge_alter,
                    description=None,
                    creator_id=_this_org

                )
                db.session.add(_stagess)
                db.session.commit()

                stagyy = production_stages.fetch_by_id(
                    _stagess.id).get_json()['id']
                print("Stages", stagyy)
            exists = pesto.fetch_by_name(_pest_id).get_json()
            if exists:
                # action_response = {
                #     "type": "danger",
                #     "message": "Pest name already exists please select from list."
                # }

                if _maintain != {}:

                    _support_pest_details = pnd.add_new(
                        json.dumps({

                            "notes": _notes,
                            "maintenance_id": _maintain['id'],
                            "production_stage_id": stagyy,
                            "variety_id": _variety_id,
                            "plant_id": _plant_id,
                            "customer_greenhouse_id": _customer_greenhouse_id,
                            "customer_shield_id": _customer_shield_id,
                            "pest_id": _pest_id,
                            "disease_id": _disease_id,
                            "customer_id": _customer_id,
                            "creator_id": _this_org1
                        })

                    )
                    print(str(_support_pest_details[0].get_json()))

                    if _support_pest_details:
                        action_response = {
                            "type": "success",
                            "message": "Support maintenance updated successfully."
                        }


            else:
                _pest = Pest(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name= _pest_id,
                    description=None,
                    creator_id=_this_org

                )
                db.session.add(_pest)
                db.session.commit()

                pestyy = pesto.fetch_by_id(_pest.id).get_json()['name']
                print("pest_id", pestyy)

                if _maintain != {}:

                    _support_pest_details = pnd.add_new(
                        json.dumps({

                            "notes": _notes,
                            "maintenance_id": _maintain['id'],
                            "production_stage_id": stagyy,
                            "variety_id": _variety_id,
                            "plant_id": _plant_id,
                            "customer_greenhouse_id": _customer_greenhouse_id,
                            "customer_shield_id": _customer_shield_id,
                            "pest_id": pestyy,
                            "disease_id": _disease_id,
                            "customer_id": _customer_id,
                            "creator_id": _this_org1
                        })

                    )
                    print(str(_support_pest_details[0].get_json()))

                    if _support_pest_details:
                        action_response = {
                            "type": "success",
                            "message": "Support maintenance updated successfully."
                        }

        elif _maintain != {} and _satge_alter and pest_disease_check == {}:

            exists = production_stages.fetch_by_name(_satge_alter).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Staging name already exists please select from list."
                }

            else:
                _stagess = ProductionStage(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_satge_alter,
                    description=None,
                    creator_id=_this_org

                )
                db.session.add(_stagess)
                db.session.commit()

                stagyy = production_stages.fetch_by_id(
                    _stagess.id).get_json()['id']
                print("Stages", stagyy)

                _support_pest_details = pnd.add_new(
                    json.dumps({
                        "notes": _notes,
                        "maintenance_id": _maintain['id'],
                        "production_stage_id": stagyy,
                        "variety_id": _variety_id,
                        "plant_id": _plant_id,
                        "customer_greenhouse_id": _customer_greenhouse_id,
                        "customer_shield_id": _customer_shield_id,
                        "pest_id": _pest_id,
                        "disease_id": _disease_id,
                        "customer_id": _customer_id,
                        "creator_id": _this_org1

                    })

                )
                print(str(_support_pest_details[0].get_json()))

                if _support_pest_details:
                    action_response = {
                        "type": "success",
                        "message": "Support maintenance updated successfully."
                    }
        elif _maintain != {} and _pest_id and pest_disease_check == {}:

            exists = pesto.fetch_by_name(_pest_id).get_json()
            if exists:
                # action_response = {
                #     "type": "danger",
                #     "message": "Pest name already exists please select from list."
                # }
                _support_pest_details = pnd.add_new(
                    json.dumps({
                        "notes": _notes,
                        "maintenance_id": _maintain['id'],
                        "production_stage_id": _production_stage_id,
                        "variety_id": _variety_id,
                        "plant_id": _plant_id,
                        "customer_greenhouse_id": _customer_greenhouse_id,
                        "customer_shield_id": _customer_shield_id,
                        "pest_id": _pest_id,
                        "disease_id": _disease_id,
                        "customer_id": _customer_id,
                        "creator_id": _this_org1

                    })

                )
                print(str(_support_pest_details[0].get_json()))
                if _support_pest_details:
                    action_response = {
                        "type": "success",
                        "message": "Support maintenance updated successfully."
                    }

            else:
                _pest = Pest(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_pest_id,
                    description=None,
                    creator_id=_this_org

                )
                db.session.add(_pest)
                db.session.commit()

                pestyy = pesto.fetch_by_id(_pest.id).get_json()['name']
                print("pest_id", pestyy)

                _support_pest_details = pnd.add_new(
                    json.dumps({
                        "notes": _notes,
                        "maintenance_id": _maintain['id'],
                        "production_stage_id": _production_stage_id,
                        "variety_id": _variety_id,
                        "plant_id": _plant_id,
                        "customer_greenhouse_id": _customer_greenhouse_id,
                        "customer_shield_id": _customer_shield_id,
                        "pest_id": pestyy,
                        "disease_id": _disease_id,
                        "customer_id": _customer_id,
                        "creator_id": _this_org1

                    })

                )
                print(str(_support_pest_details[0].get_json()))
                if _support_pest_details:
                    action_response = {
                        "type": "success",
                        "message": "Support maintenance updated successfully."
                    }
        elif pest_disease_check == {} and _maintain != {}:

            _support_pest_details = pnd.add_new(
                json.dumps({
                    "notes": _notes,
                    "maintenance_id": _maintain['id'],
                    "production_stage_id": _production_stage_id,
                    "variety_id": _variety_id,
                    "plant_id": _plant_id,
                    "customer_greenhouse_id": _customer_greenhouse_id,
                    "customer_shield_id": _customer_shield_id,
                    "pest_id": _pest_id,
                    "disease_id": _disease_id,
                    "customer_id": _customer_id,
                    "creator_id": _this_org1

                })

            )
            print(str(_support_pest_details[0].get_json()))
            if _support_pest_details:
                action_response = {
                    "type": "success",
                    "message": "Support maintenance updated successfully."
                }

    return render_template(
        'admin/maintenance/pests_diseases.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PEST=_pestoo,
        SHIELD=_shields,
        PLANTS=_plant_all,
        VARITIES=_varities,
        STAGES=_stages,
        FORM_ACTION_RESPONSE=action_response,
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/request/pests-diseases', methods=['POST', 'GET'])
def pests_diseases():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _pestoo = pesto.fetch_all().get_json()
    _plant_all = plants.fetch_all().get_json()
    _stages = production_stages.fetch_all().get_json()
    _varities = varieties.fetch_all().get_json()
    _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    _this_org = session["profile"]["user"]["real_id"]
    action_response = {}
    if _customer_id:
        _shields = shields.fetch_by_customer(_customer_id).get_json()
        _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()
    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _maintenance_id = posted.get('maintenance_id')
        _production_stage_id = posted.get('production_stage_id')
        _variety_id = posted.get('variety_id')
        _plant_id = posted.get('plant_id')
        _customer_greenhouse_id = posted.get('customer_greenhouse_id')
        _customer_shield_id = posted.get('customer_shield_id')
        _pest_id = posted.get('pest_id')
        # _pest_alter = posted.get('pest_alter')
        _satge_alter = posted.get('satge_alter')
        _disease_id = posted.get('disease_id')

        if _maintain != {}:
            pest_disease_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            pest_disease_check = pnd.fetch_by_maintenance(
                maintain_real).get_json()

        if _maintain != {} and _satge_alter and _pest_id and pest_disease_check == {}:

            exists = production_stages.fetch_by_name(_satge_alter).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Staging name already exists please select from list."
                }

            else:
                _stagess = ProductionStage(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_satge_alter,
                    description=None,
                    creator_id=_this_org

                )
                db.session.add(_stagess)
                db.session.commit()

                stagyy = production_stages.fetch_by_id(
                    _stagess.id).get_json()['id']
                print("Stages", stagyy)
            exists = pesto.fetch_by_name(_pest_id).get_json()
            if exists:
                # action_response = {
                #     "type": "danger",
                #     "message": "Pest name already exists please select from list."
                # }

                 if _maintain != {}:

                    _support_pest_details = pnd.add_new(
                        json.dumps({

                            "notes": _notes,
                            "maintenance_id": _maintain['id'],
                            "production_stage_id": stagyy,
                            "variety_id": _variety_id,
                            "plant_id": _plant_id,
                            "customer_greenhouse_id": _customer_greenhouse_id,
                            "customer_shield_id": _customer_shield_id,
                            "pest_id": _pest_id,
                            "disease_id": _disease_id,
                            "customer_id": _customer_id,
                            "creator_id": _this_org1
                        })

                    )
                    print(str(_support_pest_details[0].get_json()))

                    if _support_pest_details:
                        action_response = {
                            "type": "success",
                            "message": "Support maintenance submitted successfully."
                        }

            else:
                _pest = Pest(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_pest_id,
                    description=None,
                    creator_id=_this_org

                )
                db.session.add(_pest)
                db.session.commit()

                pestyy = pesto.fetch_by_id(_pest.id).get_json()['name']
                print("pest_id", pestyy)

                if _maintain != {}:

                    _support_pest_details = pnd.add_new(
                        json.dumps({

                            "notes": _notes,
                            "maintenance_id": _maintain['id'],
                            "production_stage_id": stagyy,
                            "variety_id": _variety_id,
                            "plant_id": _plant_id,
                            "customer_greenhouse_id": _customer_greenhouse_id,
                            "customer_shield_id": _customer_shield_id,
                            "pest_id": pestyy,
                            "disease_id": _disease_id,
                            "customer_id": _customer_id,
                            "creator_id": _this_org1
                        })

                    )
                    print(str(_support_pest_details[0].get_json()))

                    if _support_pest_details:
                        action_response = {
                            "type": "success",
                            "message": "Support maintenance submitted successfully."
                        }

        elif _maintain != {} and _satge_alter and pest_disease_check == {}:

            exists = production_stages.fetch_by_name(_satge_alter).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Staging name already exists please select from list."
                }

            else:
                _stagess = ProductionStage(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_satge_alter,
                    description=None,
                    creator_id=_this_org

                )
                db.session.add(_stagess)
                db.session.commit()

                stagyy = production_stages.fetch_by_id(
                    _stagess.id).get_json()['id']
                print("Stages", stagyy)

                _support_pest_details = pnd.add_new(
                    json.dumps({
                        "notes": _notes,
                        "maintenance_id": _maintain['id'],
                        "production_stage_id": stagyy,
                        "variety_id": _variety_id,
                        "plant_id": _plant_id,
                        "customer_greenhouse_id": _customer_greenhouse_id,
                        "customer_shield_id": _customer_shield_id,
                        "pest_id": _pest_id,
                        "disease_id": _disease_id,
                        "customer_id": _customer_id,
                        "creator_id": _this_org1

                    })

                )
                print(str(_support_pest_details[0].get_json()))

                if _support_pest_details:
                    action_response = {
                        "type": "success",
                        "message": "Support maintenance submitted successfully."
                    }
        elif _maintain != {} and _pest_id and pest_disease_check == {}:

            exists = pesto.fetch_by_name(_pest_id).get_json()
            if exists:
                # action_response = {
                #     "type": "danger",
                #     "message": "Pest name already exists please select from list."
                # }

                _support_pest_details = pnd.add_new(
                    json.dumps({
                        "notes": _notes,
                        "maintenance_id": _maintain['id'],
                        "production_stage_id": _production_stage_id,
                        "variety_id": _variety_id,
                        "plant_id": _plant_id,
                        "customer_greenhouse_id": _customer_greenhouse_id,
                        "customer_shield_id": _customer_shield_id,
                        "pest_id": _pest_id,
                        "disease_id": _disease_id,
                        "customer_id": _customer_id,
                        "creator_id": _this_org1

                    })

                )
                print(str(_support_pest_details[0].get_json()))
                if _support_pest_details:
                    action_response = {
                        "type": "success",
                        "message": "Support maintenance submitted successfully."
                    }

            else:
                _pest = Pest(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_pest_id,
                    description=None,
                    creator_id=_this_org

                )
                db.session.add(_pest)
                db.session.commit()

                pestyy = pesto.fetch_by_id(_pest.id).get_json()['name']
                print("pest_id", pestyy)

                _support_pest_details = pnd.add_new(
                    json.dumps({
                        "notes": _notes,
                        "maintenance_id": _maintain['id'],
                        "production_stage_id": _production_stage_id,
                        "variety_id": _variety_id,
                        "plant_id": _plant_id,
                        "customer_greenhouse_id": _customer_greenhouse_id,
                        "customer_shield_id": _customer_shield_id,
                        "pest_id": pestyy,
                        "disease_id": _disease_id,
                        "customer_id": _customer_id,
                        "creator_id": _this_org1

                    })

                )
                print(str(_support_pest_details[0].get_json()))
                if _support_pest_details:
                    action_response = {
                        "type": "success",
                        "message": "Support maintenance submitted successfully."
                    }
        elif pest_disease_check == {} and _maintain != {}:

            _support_pest_details = pnd.add_new(
                json.dumps({
                    "notes": _notes,
                    "maintenance_id": _maintain['id'],
                    "production_stage_id": _production_stage_id,
                    "variety_id": _variety_id,
                    "plant_id": _plant_id,
                    "customer_greenhouse_id": _customer_greenhouse_id,
                    "customer_shield_id": _customer_shield_id,
                    "pest_id": _pest_id,
                    "disease_id": _disease_id,
                    "customer_id": _customer_id,
                    "creator_id": _this_org1

                })

            )
            print(str(_support_pest_details[0].get_json()))
            if _support_pest_details:
                action_response = {
                    "type": "success",
                    "message": "Support maintenance submitted successfully."
                }

    return render_template(
        'admin/maintenance/pests_diseases.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PEST=_pestoo,
        SHIELD=_shields,
        PLANTS=_plant_all,
        VARITIES=_varities,
        STAGES=_stages,
        FORM_ACTION_RESPONSE=action_response,
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/edit/<string:id>/<string:mid>/<string:cid>', methods=['POST', 'GET'])
def _edit_supports(id, mid, cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _pest = pesto.fetch_all().get_json()
    _plant_all = plants.fetch_all().get_json()
    _varities = varieties.fetch_all().get_json()
    # _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _pest = pesto.fetch_all().get_json()
    # print("Pest all ",_pest)
    _disease = disease.fetch_all().get_json()
    _plant_all = plants.fetch_all().get_json()
    _varities = varieties.fetch_all().get_json()
    _stages = production_stages.fetch_all().get_json()
    action_response = {}
    customer_pnd = {}
    customer_pnd = pnd.fetch_one(id).get_json()
    print("Customer Pest and Desease",customer_pnd)

    if cid:
        _shields = shields.fetch_by_customer(cid).get_json()
        _maintain = maintenance.fetch_by_customer_id(cid).get_json()

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _production_stage_id = posted.get('production_stage_id')
        _variety_id = posted.get('variety_id')
        _plant_id = posted.get('plant_id')
        _customer_greenhouse_id = posted.get('customer_greenhouse_id')
        _customer_shield_id = posted.get('customer_shield_id')
        _pest_id = posted.get('pest_id')
        _disease_id = posted.get('disease_id')
        _customer_id = cid

        _support_pest_edit = pnd.edit(
            id,
            json.dumps({
                "notes": _notes,
                "production_stage_id": _production_stage_id,
                "variety_id": _variety_id,
                "plant_id": _plant_id,
                "customer_greenhouse_id": _customer_greenhouse_id,
                "customer_shield_id": _customer_shield_id,
                "pest_id": _pest_id,
                "disease_id": _disease_id,
                "maintenance_id": mid,
                "customer_id": _customer_id,
                "creator_id": _this_org1




            })

        )
        print(str(_support_pest_edit[0].get_json()))
        if _support_pest_edit:
            action_response = {
                "type": "success",
                "message": "Support is requested"
            }

    return render_template(
        'admin/maintenance/pests_diseases_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        PEST=_pest,
        STAGES=_stages,
        DISEASE=_disease,
        PLANTS=_plant_all,
        VARITIES=_varities,
        FORM_ACTION_RESPONSE=action_response,
        SHIELD=_shields,
        CPND = customer_pnd,
        data={"date": str(datetime.now().year), "today": datetime.now()}
        # data={"date":str(datetime.now().year)}
    )


@igh_maintenance.route('/request/agronomy', methods=['POST', 'GET'])
def a_agronimist():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()

    _brand = supply.fetch_all().get_json()
    _components = components.fetch_all().get_json()

    posted = request.form
    if posted:
        _visit_date = posted.get('visit_date')
        _visit_time = posted.get('visit_time')
        _reorder_fertilizer = posted.get('reorder_fertilizer')
        _reorder_agrochemical = posted.get('reorder_agrochemical')
        _fertilizer_id = posted.get('fertilizer_id')
        _fertilizer_main_component_id = posted.get(
            'fertilizer_main_component_id')
        _fertilizer_amount = posted.get('fertilizer_amount')
        _agrochemical_id = posted.get('agrochemical_id')
        _agrochemical_main_component_id = posted.get(
            'agrochemical_main_component_id')
        _agrochemical_amount = posted.get('agrochemical_amount')
        _notes = posted.get('notes')
        _observations = posted.get('observations')
        _risk_observations = posted.get('risk_observations')
        _recommendations = posted.get('recommendations')
        _customer_id = _customer_id
        print("Agronomist Visit", _visit_date, _visit_time, _reorder_fertilizer, _reorder_agrochemical,
              _fertilizer_id, _fertilizer_main_component_id, _fertilizer_amount, _notes)
        _created_by = _this_org1
        _status = posted.get('status')

        if _maintain != {}:
            agronomy_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            agronomy_check = agronomist_visit.fetch_by_maintenance(
                maintain_real).get_json()

    #  print("Date and time ",_visit_date,_visit_time,_reorder_fertilizer,_reorder_agrochemical,_notes,_maintenance_id)
    #  if _visit_date != None and _visit_time != None and _reorder_fertilizer != None and _reorder_agrochemical != None\
    #     and _fertilizer_id != None and _fertilizer_main_component_id != None and _fertilizer_amount != None \
    #     and _agrochemical_id != None and _agrochemical_main_component_id != None and _agrochemical_amount != None \
    #     and _notes != None and _maintain != {} :
        if _maintain != {} and agronomy_check == {}:

            _support_agronomy_details = agronomist_visit.add_new(
                json.dumps({
                    "visit_date": _visit_date,
                    "visit_time": _visit_time,
                    "reorder_fertilizer": _reorder_fertilizer,
                    "reorder_agrochemical": _reorder_agrochemical,
                    "fertilizer_id": _fertilizer_id,
                    "fertilizer_main_component_id": _fertilizer_main_component_id,
                    "fertilizer_amount": _fertilizer_amount,
                    "agrochemical_id": _agrochemical_id,
                    "agrochemical_main_component_id": _agrochemical_main_component_id,
                    "agrochemical_amount": _agrochemical_amount,
                    "notes": _notes,
                    "observations": _observations,
                    "risk_observations": _risk_observations,
                    "recommendations": _recommendations,
                    "maintenance_id": _maintain['id'],
                    "customer_id": _customer_id,
                    "creator_id": _created_by

                })

            )
            print(str(_support_agronomy_details[0].get_json()))

    return render_template(
        'admin/maintenance/agronomy.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        BRAND=_brand,
        COMPONENT=_components,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_maintenance.route('/edit/agronomy/<string:id>/<string:vid>/<string:cid>', methods=['POST', 'GET'])
def _edit_agronomy(id, vid, cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _brand = []
    _components = []

    _brand = supply.fetch_all().get_json()
    _components = components.fetch_all().get_json()
    _maintain = maintenance.fetch_by_customer_id(cid).get_json()

    posted = request.form
    if posted:
        _visit_date = posted.get('visit_date')
        _visit_time = posted.get('visit_time')
        _reorder_fertilizer = posted.get('reorder_fertilizer')
        _reorder_agrochemical = posted.get('reorder_agrochemical')
        _fertilizer_id = posted.get('fertilizer_id')
        _fertilizer_main_component_id = posted.get(
            'fertilizer_main_component_id')
        _fertilizer_amount = posted.get('fertilizer_amount')
        _agrochemical_id = posted.get('agrochemical_id')
        _agrochemical_main_component_id = posted.get(
            'agrochemical_main_component_id')
        _agrochemical_amount = posted.get('agrochemical_amount')
        _notes = posted.get('notes')
        _observations = posted.get('observations')
        _risk_observations = posted.get('risk_observations')
        _recommendations = posted.get('recommendations')
        _maintenance_id = vid
        _customer_id = cid

        _created_by = _this_org1
        _status = posted.get('status')

        _support_agronomy_edit = agronomist_visit.edit(
            id,
            json.dumps({
                "visit_date": _visit_date,
                "visit_time": _visit_time,
                "reorder_fertilizer": _reorder_fertilizer,
                "reorder_agrochemical": _reorder_agrochemical,
                "fertilizer_id": _fertilizer_id,
                "fertilizer_main_component_id": _fertilizer_main_component_id,
                "fertilizer_amount": _fertilizer_amount,
                "agrochemical_id": _agrochemical_id,
                "agrochemical_main_component_id": _agrochemical_main_component_id,
                "agrochemical_amount": _agrochemical_amount,
                "notes": _notes,
                "observations": _observations,
                "risk_observations": _risk_observations,
                "recommendations": _recommendations,
                "maintenance_id": vid,
                "customer_id": _customer_id,
                "creator_id": _created_by



            })

        )
        print(str(_support_agronomy_edit[0].get_json()))

    return render_template(
        'admin/maintenance/agronomy_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        BRAND=_brand,
        COMPONENT=_components,
        data={"date": str(datetime.now().year), "today": datetime.now()}
        # data={"date":str(datetime.now().year)}
    )


@igh_maintenance.route('/request/device_issue', methods=['POST', 'GET'])
def device_issue():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()

    _issues = issues.fetch_by_applies_to(
        config.MAINTENANCE_SHIELD_ISSUE).get_json()

    _shield_c = shields.fetch_by_customer(_customer_id).get_json()
    # print("Shieldddd",_shield_c)
    action_response = {}
    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _issue_id = posted.get('issue_id')
        _shield_maintenance_id = posted.get('shield_id')
        _maintenance_id = posted.get('maintenance_id')
        _customer_id = _customer_id
        _created_by = _this_org1
        _status = posted.get('status')
        _dissue_alter = posted.get('dissue_alter')

        if _maintain != {}:
            device_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            device_check = shield_issues.fetch_by_maintenance(
                maintain_real).get_json()

    #  if _maintain != {} and _shield_maintenance_id != None and _issue_id != None and _notes != None:
        if _dissue_alter and device_check == {} and _maintain != {}:

            exists = issues.fetch_by_name(_dissue_alter).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Issue already exists please select from list."
                }

            else:

                d_issue = Issue(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_dissue_alter,
                    description=None,
                    applies_to=config.MAINTENANCE_SHIELD_ISSUE,
                    creator_id=_this_org
                )
                db.session.add(d_issue)
                db.session.commit()
                issueyy = issues.fetch_by_id(d_issue.id).get_json()['id']

                _support_device_details = shield_issues.add_new(
                    json.dumps({
                        "notes": _notes,
                        "issue_id": issueyy,
                        "shield_id": _shield_maintenance_id,
                        "maintenance_id": _maintain['id'],
                        "customer_id": _customer_id,
                        "creator_id": _created_by

                    })

                )
                print(str(_support_device_details[0].get_json()))
                if _support_device_details:
                    action_response = {
                        "type": "success",
                        "message": "Device maintenance submitted successfully."
                    }

        elif _maintain != {} and device_check == {}:
            _support_device_details = shield_issues.add_new(
                json.dumps({
                    "notes": _notes,
                    "issue_id": _issue_id,
                    "shield_id": _shield_maintenance_id,
                    "maintenance_id": _maintain['id'],
                    "customer_id": _customer_id,
                    "creator_id": _created_by

                })

            )
            print(str(_support_device_details[0].get_json()))
            if _support_device_details:
                action_response = {
                    "type": "success",
                    "message": "Device maintenance submitted successfully."
                }
        else:

             action_response = {
                    "type": "danger",
                    "message": "Device is not available."
                }

    return render_template(
        'admin/maintenance/device_issue.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        ISSUES=_issues,
        SHIELDC=_shield_c,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/edit/device_issue/<string:id>/<string:did>/<string:cid>', methods=['POST', 'GET'])
def device_issue_edit(id, did, cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]

    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(cid).get_json()

    _issues = issues.fetch_by_applies_to(
        config.MAINTENANCE_SHIELD_ISSUE).get_json()

    _shield_c = shields.fetch_by_customer(cid).get_json()
    # print("Shieldddd",_shield_c)

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _issue_id = posted.get('issue_id')
        _shield_maintenance_id = posted.get('shield_id')
        _maintenance_id = posted.get('maintenance_id')

        _created_by = _this_org1
        _status = posted.get('status')

        _support_device_details = shield_issues.edit(
            id,
            json.dumps({
                "notes": _notes,
                "issue_id": _issue_id,
                "shield_id": _shield_maintenance_id,
                "maintenance_id": did,
                "customer_id": cid,
                "creator_id": _created_by

            })

        )
        print(str(_support_device_details[0].get_json()))

    return render_template(
        'admin/maintenance/device_issue_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        ISSUES=_issues,
        SHIELDC=_shield_c,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/request/greenhouses_issue', methods=['POST', 'GET'])
def greenhouse_issuess():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    print("Customer ID", _customer_id)

    _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()
    _issues = issues.fetch_by_applies_to(
        config.MAINTENANCE_GREENHOUSE_ISSUE).get_json()
    _c_greens = g_house.fetch_by_customer_all(_customer_id).get_json()
    # print("Greenhouses",_c_greens)

    # _maintain_type = _maintain['type']
    # print("Maintain type ",_maintain_type)
    action_response = {}

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _issue_id = posted.get('issue_id')
        _greenhouse_maintenance_id = posted.get('greenh_id')
        _maintenance_id = posted.get('maintenance_id')
        _customer_id = _customer_id
        _created_by = _this_org1
        _status = posted.get('status')
        _gissue_alter = posted.get('gissue_alter')

        if _maintain != {}:
            green_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            green_check = greenhouse_issues.fetch_by_maintenance(
                maintain_real).get_json()
        if _greenhouse_maintenance_id != None and _maintain != {}:
            _green_c_main_id = g_house.fetch_one(
                _greenhouse_maintenance_id, True).get_json()['real_id']
            maintain_real_id = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']

        if _greenhouse_maintenance_id != None and _green_c_main_id != None and maintain_real_id != None:
            _new_greenhouse = GreenhouseMaintenance(
                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                notes=_notes,
                customer_greenhouse_id=_green_c_main_id,
                maintenance_id=maintain_real_id,
                customer_id=_customer_id,
                creator_id=_this_org


                # cover_photo_media_id = _cover_photo_media_id
                # phone_number = _phone_no,

            )
            db.session.add(_new_greenhouse)
            db.session.commit()

            if _new_greenhouse and green_check == {} and _maintain != {} and _gissue_alter:

                exists = issues.fetch_by_name(_gissue_alter).get_json()
                if exists:
                    action_response = {
                      "type": "danger",
                      "message": "Issue already exists please select from list."
                    }

                else:

                    d_issue = Issue(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _gissue_alter,
                        description = None,
                        applies_to = config.MAINTENANCE_GREENHOUSE_ISSUE,
                        creator_id = _this_org
                    )
                    db.session.add(d_issue)
                    db.session.commit()
                    gissuey = issues.fetch_by_id(d_issue.id).get_json()['id']

                    _support_greenhouse_details = greenhouse_issues.add_new(
                      json.dumps({
                        "notes": _notes,
                        "issue_id": gissuey,
                        "greenhouse_maintenance_id": _new_greenhouse.id,
                        "maintenance_id": _maintain['id'],
                        "customer_id": _customer_id,
                        "creator_id": _created_by

                       })

                    )
                    print(str(_support_greenhouse_details[0].get_json()))
                    if _support_greenhouse_details:
                      action_response = {
                    "type": "success",
                    "message": "Greenhouse maintenance submitted successfully."
                      }

            elif _new_greenhouse and green_check == {} and _maintain != {} :


                 _support_greenhouse_details = greenhouse_issues.add_new(
                      json.dumps({
                        "notes": _notes,
                        "issue_id": _issue_id,
                        "greenhouse_maintenance_id": _new_greenhouse.id,
                        "maintenance_id": _maintain['id'],
                        "customer_id": _customer_id,
                        "creator_id": _created_by

                       })

                    )
                 print(str(_support_greenhouse_details[0].get_json()))
                 if _support_greenhouse_details:
                      action_response = {
                    "type": "success",
                    "message": "Greenhouse maintenance submitted successfully."
                      }
        else:

                action_response = {
                    "type": "danger",
                    "message": "Greenhouse is not available."
                      }


    return render_template(
        'admin/maintenance/greenhouse_issue.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        ISSUES=_issues,
        GREENHOUSE=_c_greens,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/edit/greenhouseissue/<string:id>/<string:gid>/<string:cid>', methods=['POST', 'GET'])
def greenhouse_edit_s(id, gid, cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    # _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(cid).get_json()
    _issues = issues.fetch_by_applies_to(
        config.MAINTENANCE_GREENHOUSE_ISSUE).get_json()
    _c_greens = g_house.fetch_by_customer_all(cid).get_json()

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _issue_id = posted.get('issue_id')
        _greenhouse_maintenance_id = posted.get('greenh_id')
        _maintenance_id = posted.get('maintenance_id')
    #  _customer_id =  _customer_id
        _created_by = _this_org1
        _status = posted.get('status')
        _greenhouse_before_id = _greenhouse_issuebefore.fetch_by_customer(cid).get_json()[
            'id']
        _greenhouse_before_real_id = _greenhouse_issuebefore.fetch_one(
            _greenhouse_before_id, True).get_json()['real_id']

        _green_c_main_id = g_house.fetch_one(
            _greenhouse_maintenance_id, True).get_json()['real_id']
        maintain_real_id = maintenance.fetch_one(
            _maintain['id'], True).get_json()['real_id']

        if _greenhouse_maintenance_id:
            _edit_greenhouse = _greenhouse_issuebefore.edit(
                _greenhouse_before_id,
                json.dumps({

                    "notes": _notes,
                    "customer_greenhouse_id": _greenhouse_maintenance_id,
                    "maintenance_id": gid,
                    "customer_id": cid,
                    "creator_id": _created_by


                    # cover_photo_media_id = _cover_photo_media_id
                    # phone_number = _phone_no,

                })

            )
            print(str(_edit_greenhouse[0].get_json()))

        if _greenhouse_before_id:

            _support_greenhouse_edit = greenhouse_issues.edit(
                id,
                json.dumps({
                    "notes": _notes,
                    "issue_id": _issue_id,
                    "greenhouse_maintenance_id": _greenhouse_before_real_id,
                    "maintenance_id": gid,
                    "customer_id": cid,
                    "creator_id": _created_by

                })

            )
            print(str(_support_greenhouse_edit[0].get_json()))

    return render_template(
        'admin/maintenance/greenhouse_issue_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        ISSUES=_issues,
        GREENHOUSE=_c_greens,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/request/screenhouse_issue', methods=['POST', 'GET'])
def screenhouse_issues_type():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()
    _screenhouses = []
    _issues = []
    farms = []
    green_h = []

    _screenhouses = s_house.fetch_by_customer(_customer_id).get_json()
    _issues = issues.fetch_by_applies_to(
        config.MAINTENANCE_SCREENHOUSE_ISSUE).get_json()
    farms = _farms.fetch_by_customer(_customer_id).get_json()
    green_h = g_house.fetch_by_customer_all(_customer_id).get_json()
    action_response = {}
    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _issue_id = posted.get('issue_id')
        _customer_id = _customer_id
        _screenhouse_maintenance_id = posted.get('screenhouse_maintenance_id')
        _maintenance_id = posted.get('maintenance_id')
        _created_by = _this_org1
        _status = posted.get('status')
        _scissue_alter = posted.get('scissue_alter')

        if _maintain != {}:
            screen_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            screen_check = screenhouse_issues.fetch_by_maintenance(
                maintain_real).get_json()

    #  if _maintain != {} and _screenhouse_maintenance_id != None and _issue_id != None :
        if _maintain != {} and screen_check =={} and _scissue_alter :


                exists = issues.fetch_by_name(_scissue_alter).get_json()
                if exists:
                    action_response = {
                      "type": "danger",
                      "message": "Issue already exists please select from list."
                    }

                else:

                    sc_issue = Issue(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _scissue_alter,
                        description = None,
                        applies_to = config.MAINTENANCE_SCREENHOUSE_ISSUE,
                        creator_id = _this_org
                    )
                    db.session.add(sc_issue)
                    db.session.commit()
                    scissuey = issues.fetch_by_id(sc_issue.id).get_json()['id']




                    _support_screenhouse_details = screenhouse_issues.add_new(
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : scissuey,
                              "screenhouse_maintenance_id": _screenhouse_maintenance_id,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                        )
                    print(str(_support_screenhouse_details[0].get_json()))
                    if _support_screenhouse_details:
                      action_response = {
                    "type": "success",
                    "message": "Screenhouse maintenance submitted successfully."
                      }



        elif _maintain != {} and screen_check =={} :


                    _support_screenhouse_details = screenhouse_issues.add_new(
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : _issue_id,
                              "screenhouse_maintenance_id": _screenhouse_maintenance_id,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                        )
                    print(str(_support_screenhouse_details[0].get_json()))

                    if _support_screenhouse_details:
                      action_response = {
                    "type": "success",
                    "message": "Screenhouse maintenance submitted successfully."
                      }

            # _support_screenhouse_details = screenhouse_issues.add_new(
            #     json.dumps({
            #         "notes": _notes,
            #         "issue_id": _issue_id,
            #         "screenhouse_maintenance_id": _screenhouse_maintenance_id,
            #         "maintenance_id": _maintain['id'],
            #         "customer_id": _customer_id,
            #         "creator_id": _created_by

            #     })

            # )
            # print(str(_support_screenhouse_details[0].get_json()))

    return render_template(
        'admin/maintenance/screenhouse_issue.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        SCREENHOUSE=_screenhouses,
        ISSUES=_issues,
        FARMS=farms,
        GREENHOUSE=green_h,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/edit/screenhouseissue/<string:id>/<string:sid>/<string:cid>', methods=['POST', 'GET'])
def screenhouse_issue_edit(id, sid, cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(cid).get_json()
    # _maintain_type = _maintain['type']
    _screenhouse = []
    _screenhouse = s_house.fetch_by_customer(cid).get_json()
    _issues = []
    _issues = issues.fetch_by_applies_to(
        config.MAINTENANCE_SCREENHOUSE_ISSUE).get_json()
    farms = []
    green_h = []
    farms = _farms.fetch_by_customer(cid).get_json()
    green_h = g_house.fetch_by_customer_all(cid).get_json()

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _issue_id = posted.get('issue_id')
        _customer_id = cid
        _screenhouse_maintenance_id = posted.get('screenhouse_maintenance_id')
        _maintenance_id = posted.get('maintenance_id')
        _created_by = _this_org1
        _status = posted.get('status')

        _support_screenhouse_edit = screenhouse_issues.edit(
            id,
            json.dumps({
                "notes": _notes,
                "issue_id": _issue_id,
                "screenhouse_maintenance_id": _screenhouse_maintenance_id,
                "maintenance_id": sid,
                "customer_id": _customer_id,
                "creator_id": _created_by

            })

        )
        print(str(_support_screenhouse_edit[0].get_json()))

    return render_template(
        'admin/maintenance/screenhouse_issue_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        GREENHOUSE=green_h,
        SCREENHOUSE=_screenhouse,
        ISSUES=_issues,

        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/request/shadnet_issue', methods=['POST', 'GET'])
def shadnet_issues():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()

    # _maintain_type = _maintain['type']

    _shadnet = []
    _shadnet = sh_net.fetch_by_customer(_customer_id).get_json()
    _issues = []
    _issues = issues.fetch_by_applies_to(
        config.MAINTENANCE_SHADENET_ISSUE).get_json()
    farms = []
    farms = _farms.fetch_by_customer(_customer_id).get_json()
    green_h = []
    green_h = g_house.fetch_by_customer_all(_customer_id).get_json()
    action_response ={}

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _issue_id = posted.get('issue_id')
        _customer_id = _customer_id
        _shadenet_maintenance_id = posted.get('shadenet_maintenance_id')
        _maintenance_id = posted.get('maintenance_id')
        _created_by = _this_org1
        _status = posted.get('status')
        _sdissue_alter = posted.get('sdissue_alter')

        if _maintain != {}:
            shad_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            shad_check = shadenet_issues.fetch_by_maintenance(
                maintain_real).get_json()
    #  if _issue_id != None and _shadenet_maintenance_id != None and _maintain != {} :
        if _maintain != {} and shad_check == {} and _sdissue_alter:


                exists = issues.fetch_by_name(_sdissue_alter).get_json()
                if exists:
                    action_response = {
                      "type": "danger",
                      "message": "Issue already exists please select from list."
                    }

                else:

                    sd_issue = Issue(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _sdissue_alter,
                        description = None,
                        applies_to = config.MAINTENANCE_SHADENET_ISSUE,
                        creator_id = _this_org
                    )
                    db.session.add(sd_issue)
                    db.session.commit()
                    sdissuey = issues.fetch_by_id(sd_issue.id).get_json()['id']

                    _support_shadnet_details = shadenet_issues.add_new(
                        json.dumps({
                            "notes": _notes,
                            "issue_id": sdissuey,
                            "shadenet_maintenance_id": _shadenet_maintenance_id,
                            "maintenance_id": _maintain['id'],
                            "customer_id": _customer_id,
                            "creator_id": _created_by

                          })

                         )
                    print(str(_support_shadnet_details[0].get_json()))
                    if _support_shadnet_details:
                      action_response = {
                            "type": "success",
                            "message": "Shadenet maintenance submitted successfully."
                        }

        elif _maintain != {} and shad_check == {} :


             _support_shadnet_details = shadenet_issues.add_new(
                        json.dumps({
                            "notes": _notes,
                            "issue_id": _issue_id,
                            "shadenet_maintenance_id": _shadenet_maintenance_id,
                            "maintenance_id": _maintain['id'],
                            "customer_id": _customer_id,
                            "creator_id": _created_by

                          })

                         )
             print(str(_support_shadnet_details[0].get_json())) 
             if _support_shadnet_details:
                      action_response = {
                            "type": "success",
                            "message": "Shadenet maintenance submitted successfully."
                        }  

    return render_template(
        'admin/maintenance/shadnet_issue.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        SHADENET=_shadnet,
        ISSUES=_issues,
        FARMS=farms,
        GREENHOUSE=green_h,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/edit/shadenetissue/<string:id>/<string:shid>/<string:cid>', methods=['POST', 'GET'])
def shadnet_edit_issue(id, shid, cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    farms = []

    _shadnet = []
    _shadnet = sh_net.fetch_by_customer(cid).get_json()
    _issues = []
    _issues = issues.fetch_by_applies_to(
        config.MAINTENANCE_SHADENET_ISSUE).get_json()
    farms = _farms.fetch_by_customer(cid).get_json()
    green_h = []
    green_h = g_house.fetch_by_customer_all(cid).get_json()

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _issue_id = posted.get('issue_id')
        _customer_id = cid
        _shadenet_maintenance_id = posted.get('shadenet_maintenance_id')
        _maintenance_id = posted.get('maintenance_id')
        _created_by = _this_org1
        _status = posted.get('status')

        _support_shadnet_edit = shadenet_issues.edit(
            id,
            json.dumps({
                "notes": _notes,
                "issue_id": _issue_id,
                "shadenet_maintenance_id": _shadenet_maintenance_id,
                "maintenance_id": shid,
                "customer_id": _customer_id,
                "creator_id": _created_by

            })

        )
        print(str(_support_shadnet_edit[0].get_json()))

    return render_template(
        'admin/maintenance/shadnet_issue_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        ISSUES=_issues,
        SHADENET=_shadnet,
        GREENHOUSE=green_h,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/request/trace_report', methods=['POST', 'GET'])
def trace_reports():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    cust_user_id = cuser.fetch_by_customer(_customer_id).get_json()
    cuser_id = cust_user_id['user']['id']
    cuser_real_id = user.fetch_one(cuser_id,True).get_json()['real_id']

    all_crop_by_customer = crops.fetch_by_creator(cuser_real_id).get_json()
    _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()
    if _maintain != {}:
        maintain_real = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']
    action_response = {}
    # _maintain_type = _maintain['type']

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _maintenance_id = posted.get('maintenance_id')
        _customer_id = _customer_id
        _created_by = _this_org1
        _crops = posted.get('crop_planting_id')
        _status = posted.get('status')

        _support_tracereport_details = traceability_report_requests.add_new(
            json.dumps({
                "notes": _notes,
                "maintenance_id": _maintain['id'],
                "customer_id": _customer_id,
                "creator_id": _created_by

            })

        )
        print(str(_support_tracereport_details[0].get_json()))

        if _support_tracereport_details :

         s_t_id = traceability_report_requests.fetch_by_maintenance(maintain_real).get_json()['id']
         _customer_id_uid = customer.fetch_by_id(_customer_id).get_json()['id']
         _support_cropreport_details = traceability_report_crops.add_new(
                             json.dumps({
                            #   "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id_uid,
                              "crop_planting_id": _crops,
                              "creator_id" : _created_by,
                              "traceability_report_request_id" : s_t_id,
           

                            
                             })
                             
                       )
         print(str(_support_cropreport_details[0].get_json()))
         action_response = {
                            "type": "success",
                            "message": "Traceability maintenance submitted successfully."
                      }

    return render_template(
        'admin/maintenance/trace_report.html',
        ADMIN_PORTAL=True,
        CROP = all_crop_by_customer,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )



@igh_maintenance.route('/edit/traceability/<string:id>/<string:trmid>/<string:cid>', methods=['POST', 'GET'])
def trace_reports_edit(id,trmid,cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    # _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    # cust_user_id = cuser.fetch_by_customer(_customer_id).get_json()
    # cuser_id = cust_user_id['user']['id']
    # cuser_real_id = user.fetch_one(cuser_id,True).get_json()['real_id']

    # all_crop_by_customer = crops.fetch_by_creator(cuser_real_id).get_json()
    # _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()
    # if _maintain != {}:
    #     maintain_real = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']
    # action_response = {}
    # _maintain_type = _maintain['type']

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _maintenance_id = posted.get('maintenance_id')
        _customer_id = _customer_id
        _created_by = _this_org1
        _crops = posted.get('crop_planting_id')
        _status = posted.get('status')

        # _support_tracereport_details = traceability_report_requests.edit(
        #     json.dumps({
        #         "notes": _notes,
        #         "maintenance_id": trmid,
        #         "customer_id": _customer_id,
        #         "creator_id": _created_by

        #     })

        # )
        # print(str(_support_tracereport_details[0].get_json()))

        # if _support_tracereport_details :

        # s_t_id = traceability_report_requests.fetch_by_maintenance(maintain_real).get_json()['id']
        # _customer_id_uid = customer.fetch_by_id(_customer_id).get_json()['id']
        # _support_cropreport_details = traceability_report_crops.edit(
        #                      json.dumps({
        #                     #   "notes": _notes,
                              
        #                       "customer_id" : _customer_id_uid,
        #                       "crop_planting_id": _crops,
        #                       "creator_id" : _created_by,
                             
           

                            
        #                      })
                             
        #                )
        # print(str(_support_cropreport_details[0].get_json()))
        # action_response = {
        #                     "type": "success",
        #                     "message": "Traceability maintenance edited successfully."
        #               }

    return render_template(
        'admin/maintenance/trace_report_edit.html',
        ADMIN_PORTAL=True,
        CROP = all_crop_by_customer,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/request/others', methods=['POST', 'GET'])
def others():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()

    # _maintain_type = _maintain['type']

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _maintenance_id = posted.get('maintenance_id')
        _customer_id = _customer_id
        _created_by = _this_org1
        _status = posted.get('status')

        if _maintain != {} and _notes != None:

            _support_other_details = other_request.add_new(
                json.dumps({
                    "notes": _notes,
                    "maintenance_id": _maintain['id'],
                    "customer_id": _customer_id,
                    "creator_id": _created_by

                })

            )
            print(str(_support_other_details[0].get_json()))

    return render_template(
        'admin/maintenance/others.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/edit/others/<string:id>/<string:oid>/<string:cid>', methods=['POST', 'GET'])
def others_edit(id, oid, cid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    # _customer_id = session["customer"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(cid).get_json()

    # _maintain_type = _maintain['type']

    posted = request.form
    if posted:
        _notes = posted.get('notes')
        _maintenance_id = posted.get('maintenance_id')
    #  _customer_id = _customer_id
        _created_by = _this_org1
        _status = posted.get('status')

        _support_other_details = other_request.edit(
            id,
            json.dumps({
                "notes": _notes,
                "maintenance_id": oid,
                "customer_id": cid,
                "creator_id": _created_by

            })

        )
        print(str(_support_other_details[0].get_json()))

    return render_template(
        'admin/maintenance/others_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_maintenance.route('/close/<string:id>/<string:aid>', methods=['POST', 'GET'])
def closer_assignment(id, aid):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _maintenances = maintenance.fetch_all().get_json()
    _this_org1 = session["profile"]["user"]["id"]
    action_response = {}

    posted = request.form
    if posted:

        _notes = posted.get('note')
        print("Notes", _notes)
       


        if aid:
            _new_c = closure.add_new(
                json.dumps({

                    "igh_user_id": aid,
                    "maintenance_id": id,
                    "creator_id": _this_org1,
                    "notes": _notes


                }))

            print(str(_new_c[0].get_json()))

            if _new_c:
                assign_close = maintenance.deactivate(id)
                print(assign_close)
                
                action_response = {
                    "type": "success",
                    "message": "The Request is closed successfully."
                }

    # posted = request.form
    # if posted:
    #    _assign_id = posted.get('assign_id')
    #    _check_id = posted.get('check_id')
    #    print("Assign ID",_assign_id,_check_id)
    #    _maintain_iid = maintenance.fetch_one(_check_id,True).get_json()
    #    _maintain_real_id = _maintain_iid['real_id']
    #    assignments = _maintain_iid['assignment']
    #    if _maintain_real_id:
    #     _pestndesease = pnd.fetch_by_maintenance(_maintain_real_id).get_json()
    #     print("Details",_pestndesease)

    #    else :
    #         assignment_id = assignments['id']
    #         assignment_real_id = assignmembers.fetch_one(assignment_id,True).get_json()['real_id']
    #         print("Assignment Real ID",assignment_real_id)
    #         _assign_member = assignmembers.edit(
    #                     assignment_id,
    #                     json.dumps({
    #                         "customer_rating": 3,
    #                         "scheduled" : "",
    #                         "igh_user_id" : _assign_id,

    #                     }),
    #                     True
    #                     )
    #         print('Its working',_assign_member)

    return render_template(
        'admin/maintenance/close-assignment.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_maintenance.route('/request/photos', methods=['POST', 'GET'])
def photos():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org1 = session["profile"]["user"]["id"]
    _this_org = session["profile"]["user"]["real_id"]
    _customer_id = session["customer"]["user"]["real_id"]
    action_response ={}
    _maintain = maintenance.fetch_by_customer_id(_customer_id).get_json()
    # saved_filename = ""
    
    #  uploaded_file = request.files['file']

    # print("Maintain ID",_maintain['id'])
    

    # image_file = request.files.getlist('fileim[]')
	
    if request.method=='POST':
            image_file = request.files.getlist('fileim[]')
            print("filename",image_file)
          
            for ifile in image_file:
                  actual_filename = secure_filename(ifile.filename)
                  pref_filename = _this_org1 + "_" + str(uuid.uuid4())
                  saved_filename = files.uploadFile(
                      ifile,
                      "pestndesease",
                       pref_filename
                   )

                  if saved_filename:
                    image_media = Media(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        file_url = "pestndesease/" + saved_filename,
                        caption = actual_filename,
                        creator_id = _this_org
                    )
                    db.session.add(image_media)
                    db.session.commit()
                    
                    
                    
                    
                    image_m_id = media.fetch_by_id(image_media.id).get_json()['id']
                    
                    
                    
                    
                    if image_media and _maintain != {}:

                        maintain_real_id = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']

                        pndphoto = pndphotos.add_new(
                             json.dumps({

                                 "photo_media_id" : image_m_id,
                                  "maintenance_id" : maintain_real_id,
                                  "creator_id" : _this_org1

                                        }),
                     
                        )
        
        
                       # print(str(pndphoto[0].get_json()))
                        if image_media:
                               action_response = {
                                "type": "success",
                                "message": "Images files are added successfully."
                               
                            }
                        print("Action",action_response)
   
    
    
    

                        
                          

    return render_template(
        'admin/maintenance/photos.html',
        FORM_ACTION_RESPONSE=action_response,
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        
        data={"date": str(datetime.now().year)}
    )
