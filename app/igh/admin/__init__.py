from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import datetime as dt
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime
from dateutil.relativedelta import relativedelta
from sqlalchemy import extract
from app.core import customer as customeruser
from app.core import farm
from app.core import maintenance as maint
from app.core import greenhouse
from app.core import shield
from app.core import screenhouse
from app.core import shadenet
from app.core.planting import varieties
from app.core.report import harvest
from app.core.report import fertilizer_application
from app.core.report import agrochemical_application
from app.core import supplier
from app.core.user import igh_users
from app.core.maintenance import agronomist_visit
from app.core.customer import shields
from app.core.customer import greenhouses as cgren


from app.config import activateConfig

from app.igh import portal_check

admin = Blueprint('admin', __name__,  static_folder='../static', template_folder='../templates')
admin.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def contact_person(contact_person_id):
    _response = {}
    # _user = db\
    #     .session\
    #     .query(User, UserRole, Role, CustomerUser)\
    #     .join(UserRole, UserRole.user_id == User.id)\
    #     .join(Role, Role.id == UserRole.role_id)\
    #     .outerjoin(CustomerUser, CustomerUser.user_id == User.id)\
    #     .filter(User.id == contact_person_id)\
    #     .filter(User.status > config.STATUS_DELETED)\
    #     .first()
    # if _user:
    #     _response = {
    #         "id": _user.User.uid,
    #         "first_name": _user.User.first_name,
    #         "last_name": _user.User.last_name,
    #         "phone_number": _user.User.phone_number,
    #         "email_address": _user.User.email_address,
    #         "id_number": _user.CustomerUser.id_number if _user.CustomerUser and _user.CustomerUser.id_number is not None else "-",
    #         "role": _user.Role.name,
    #         "status": "Active" if _user.User.status == config.STATUS_ACTIVE else "Inactive",
    #         "created": _user.User.created_at,
    #         "updated": _user.User.modified_at,
    #     }
    #
    return _response

def clientale_growth(duration="Month"):
    chart_data = []

    start_date = dt.date(2020, 1, 1)
    today = dt.date.today()
    #
    # if duration == "Month":
    #     all_months_label = []
    #     all_months_data = []
    #     while start_date <= today:
    #         customers_in_month = Customer\
    #             .query\
    #             .filter(extract('year', Customer.created_at)==start_date.strftime('%Y'))\
    #             .filter(extract('month', Customer.created_at)==start_date.strftime('%m'))\
    #             .filter(Customer.status > config.STATUS_DELETED)
    #
    #         all_months_data.append(customers_in_month.count())
    #         all_months_label.append(start_date.strftime('%b %y'))
    #         start_date += relativedelta(months=1)
    #
    #     chart_data.append(','.join(all_months_label))
    #     chart_data.append(all_months_data)

    return chart_data

@admin.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
       return realm

    
    all_customer = []
    all_farms = []
    greenhouses = []
    farms = []
    devices = []
    screenh = []
    shadn = []
    data_batches = []
    users = []
    services = []
    customers = []
    maintenances =[]
    crops =[]
    prod_harvest = []
    fertilize = []
    suppler = []
    agro_chem = []
    igh_user = []
    agro_visit = []
    customer_device = []
    customer_greenhouse = []

    all_customer = customeruser.fetch_all().get_json()
    all_farms = farm.fetch_all().get_json()
    customer_device = shields.fetch_all().get_json()
    customer_greenhouse = cgren.fetch_all().get_json()
    maintenances = maint.fetch_all().get_json()
    greenhouses = greenhouse.fetch_all().get_json()
    devices = shield.fetch_all().get_json()
    screenh = screenhouse.fetch_all().get_json()
    shadn = shadenet.fetch_all().get_json()
    crops = varieties.fetch_all().get_json()
    prod_harvest = harvest.fetch_all().get_json()
    fertilize = fertilizer_application.fetch_all().get_json()
    suppler = supplier.fetch_all().get_json()
    agro_chem = agrochemical_application.fetch_all().get_json()
    igh_user = igh_users.fetch_all().get_json()
    agro_visit = agronomist_visit.fetch_all().get_json()

    

    # print("production harvest",prod_harvest)

    # try:
    #     greenhouses = CustomerGreenhouse\
    #         .query\
    #         .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
    #         .all()
    #
    #     farms = Farm\
    #         .query\
    #         .filter(Farm.status > config.STATUS_DELETED)\
    #         .all()
    #
    #     devices = CustomerDevice\
    #         .query\
    #         .filter(CustomerDevice.status > config.STATUS_DELETED)\
    #         .all()
    #
    #     data_batches = DeviceDataBatch\
    #         .query\
    #         .filter(DeviceDataBatch.status > config.STATUS_DELETED)\
    #         .all()
    #
    #     users = User\
    #         .query\
    #         .filter(User.status > config.STATUS_DELETED)\
    #         .all()
    #
    #     services = Service\
    #         .query\
    #         .filter(Service.status > config.STATUS_DELETED)\
    #         .all()
    #
    #     customers = Customer\
    #         .query\
    #         .filter(Customer.status > config.STATUS_DELETED)\
    #         .all()
    # except:
    #     db.session.rollback()

    return render_template(
        'admin/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_DASHBOARD_ACTIVE='active',
        GREENHOUSES=greenhouses,
        DEVICES = devices,
        SCREENHOUSE = screenh,
        SHADNET = shadn,
        ALLFARMS=all_farms,
        FARMS = farms,
        USER=users,
        GROWTH_CHART=clientale_growth(),
        PROFILE=session['profile'],
        CUSTOMER = all_customer,
        MAINTAIN = maintenances,
        VARIETIES = crops,
        FERTILIZE = fertilize,
        HARVEST = prod_harvest,
        SUPPLY = suppler,
        AGRO = agro_chem,
        IGH_USER = igh_user,
        AGRO_VISIT = agro_visit,
        CUSTOMERD = customer_device,
        CUSTOMERG = customer_greenhouse,
        # SERVICES=session['services'],
        
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )
