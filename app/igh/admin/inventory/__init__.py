from functools import wraps

import json
import os
import re
import urllib.parse
import requests
import importlib
import uuid
import math
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_, true
from werkzeug.exceptions import HTTPException
import pdfkit
from dotenv import load_dotenv
from flask import Flask,Response
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from app.config import activateConfig
from app.core.greenhouse import types
from app.core import greenhouse as greenh
from app.core import screenhouse as screenh
from app.core import shadenet as shadenets
from app.core import shield as shields
from app.core.customer import shields as cshield
from app.core.customer import screenhouses as cscreen
from app.core.customer import shadenets as snet
from app.models import db
from app.core.shield import types as shield_types
from app.igh import portal_check
from app.igh.admin import contact_person
from app.core import spear
from app.core.shield import spears
from app.models import Greenhouse
from app.models import Shield
from app.models import Screenhouse
from app.models import Shadenet
from app.igh import session_setup
from werkzeug.wrappers import response
from app.models import SuggestedPlants
from app.models import SuggestedVariety
from app.core.planting import suggested_plant


from app.helper import valid_uuid

igh_inventory = Blueprint('igh_inventory', __name__,
                          static_folder='../../static', template_folder='.../../templates')
igh_inventory.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


@igh_inventory.route('/', methods=['POST', 'GET'], defaults={"page": 1})
@igh_inventory.route('/<int:page>', methods=['GET', 'POST'])
def main(page):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    page = page
    prev = page-1
    next = page+1
    _greenhouse = []
    _types = []
    _device_types =[]
    _devices = []
    _screenhouse = []
    _shadenet =[]
    _customer_shield = []
    _customer_screenhouse = []
    _customer_shadnet = []
    _greens = []
    _device = []
    _screens = []
    _shads = []
    

   
    print("shield", _customer_shield)
    action_response = {}
    response = []
    no_of_posts = config.PAGE_LIMIT
    total = {}
    total = round(len(_greenhouse)/no_of_posts)
    total1 = round(len(_devices)/no_of_posts)
    total2 = round(len(_screenhouse)/no_of_posts)
    total3 = round(len(_shadenet)/no_of_posts)
    print("Devicesssssssss", _devices)

    # _typesz= types.fetch_all().get_json()
    # allPosts = Posts.query.filter_by().all()

    

    # print("Device",_device)

    last = math.ceil(len(_greenhouse)/int(no_of_posts))
    # print("lasttttttt",len(_greenhouse))
    # posts = _greenhouse[(page-1)*no_of_posts :(page-1)*no_of_posts+ no_of_posts]

    _actives = []
    _inactives = []

  

    posted = request.form
    if posted:
        print("green ID", posted.getlist("greenhouse_a_id[]"))
        green_d_id = posted.getlist("greenhouse_id[]")
        green_a_id = posted.getlist("greenhouse_a_id[]")
        device_d_id = posted.getlist("device_d_id[]")
        device_a_id = posted.getlist("device_a_id[]")
        screen_d_id = posted.getlist("screen_d_id[]")
        screen_a_id = posted.getlist("screen_a_id[]")
        shadenet_d_id = posted.getlist("shadenet_d_id[]")
        shadenet_a_id = posted.getlist("shadenet_a_id[]")

       
        if green_d_id:
            for greend_id in green_d_id:
             greenh.deactivate(greend_id)
             action_response = {
                "type": "success",
                "message": "Greenhouse Successfully Deactivated"
             }

            

        elif green_a_id:
            for green_aa in green_a_id:
            
             greenh.activate(green_aa)
             action_response = {
                "type": "success",
                "message": "Successfully Activated"
            }
           

        elif screen_d_id:
            for screen_dd in screen_d_id:
             screenh.deactivate(screen_dd)
             action_response = {
                "type": "success",
                "message": "Successfully Deactivated"
             }

        elif screen_a_id:
            for screen_aa in screen_a_id:
             screenh.activate(screen_aa)
             action_response = {
                "type": "success",
                "message": "Successfully Activated"
             }

        elif device_d_id:
            for device_dd in device_d_id:
             shields.deactivate(device_dd)
             action_response = {
                "type": "success",
                "message": "Successfully Deactivated"
            }
        elif device_a_id:
            for device_aa in device_a_id:
             shields.activate(device_aa)
             action_response = {
                "type": "success",
                "message": "Successfully Activated"
             }
        elif shadenet_d_id:
            for shadenet_dd in shadenet_d_id:
             shadenets.deactivate(shadenet_dd)
             action_response = {
                "type": "success",
                "message": "Successfully Deactivated"
            }
        elif shadenet_a_id:
            for shadenet_aa in shadenet_a_id:
             shadenets.activate(shadenet_aa)
             action_response = {
                "type": "success",
                "message": "Successfully Activated"
            }
        else:
          action_response = {
            "type": "danger",
            "message": "Please select an element for Activate or Deactivate "
            # "message": ""
           }
    _greens = greenh.fetch_all1(page).get_json()
    _device = shields.fetch_all1(page).get_json()
    _screens = screenh.fetch_all1(page).get_json()
    _shads = shadenets.fetch_all1(page).get_json()

        # if posted.get("action") == "deactivate_greenhouses" and "greenhouse_id" in posted:
        #         return greenh.deactivate(posted.get("greenhouse_id"))
        # if "action" in posted:
        #      print("green ID",posted.get("greenhouse_id"))
        #      if posted.get("action") == "activate_greenhouses" and "greenhouse_id" in posted:
        #         return greenh.activate(posted.get("greenhouse_id"))

        #      if posted.get("action") == "deactivate_greenhouses" and "greenhouse_id" in posted:
        #         return greenh.deactivate(posted.get("greenhouse_id"))

        #      if posted.get("action") == "activate_screenhouse" and "screenhouse_id" in posted:
        #         return screenh.activate(posted.get("screenhouse_id"))

        #      if posted.get("action") == "deactivate_screenhouse" and "screenhouse_id" in posted:
        #         return screenh.deactivate(posted.get("screenhouse_id"))

        #      if posted.get("action") == "activate_shadenet" and "shadenet_id" in posted:
        #         return shadenets.activate(posted.get("shadenet_id"))

        #      if posted.get("action") == "deactivate_shadenet" and "shadenet_id" in posted:
        #         return shadenets.deactivate(posted.get("shadenet_id"))

    if request.method == 'POST' and 'tag' and 'tagd' and 'tagsc' and 'tagsd' in request.form:
        tag = request.form["tag"]
        tagd = request.form["tagd"]
        tagsc = request.form["tagsc"]
        tagsd = request.form["tagsd"]

        # if tag != '' and tagd != '' and tagsc != '' and tagsd != '' :
        if tag:
            print("tagggggg", tag)
            search = "%{}%".format(tag)
            if search:
                _greeny = db.session.query(Greenhouse).filter(or_(Greenhouse.greenhouse_ref.like('%'+search+'%'), Greenhouse.size.like('%'+search+'%'))).limit(
                    no_of_posts).offset((page-1)*no_of_posts)  # OR: from sqlalchemy import or_  filter(or_(User.name == 'ednalan', User.name == 'caite'))
                for greenhouse in _greeny:
                    response = []
                    response.append(greenh.greenhouse_obj(greenhouse))
                    _greens = jsonify(response).get_json()
        if tagd:
            search1 = "%{}%".format(tagd)
            if search1:
                _devicy = db.session.query(Shield).filter(or_(Shield.serial.like(
                    '%'+search1+'%'))).limit(no_of_posts).offset((page-1)*no_of_posts)
                for device in _devicy:
                    response = []
                    response.append(shields.shield_obj(device))
                    _device = jsonify(response).get_json()
            #   print("Search result device",_device)
        if tagsc:
            search2 = "%{}%".format(tagsc)
            if search2:
                _screeny = db.session.query(Screenhouse).filter(or_(Screenhouse.screenhouse_ref.like(
                    '%'+search2+'%'), Screenhouse.size.like('%'+search2+'%'))).limit(no_of_posts).offset((page-1)*no_of_posts)
                for device in _screeny:
                    response = []
                    response.append(screenh.screenhouse_obj(device))
                    _screens = jsonify(response).get_json()
        if tagsd:
            search3 = "%{}%".format(tagsd)

            if search3:
                _shady = db.session.query(Shadenet).filter(or_(Shadenet.shadenet_ref.like(
                    '%'+search3+'%'), Shadenet.size.like('%'+search3+'%'))).limit(no_of_posts).offset((page-1)*no_of_posts)
                for device in _shady:
                    response = []
                    response.append(shadenets.shadenet_obj(device))
                    _shads = jsonify(response).get_json()
            

    _greenhouse = greenh.fetch_all().get_json()
    _types = types.fetch_all().get_json()
    _device_types = shield_types.fetch_all().get_json()
    _devices = shields.fetch_all().get_json()
    _screenhouse = screenh.fetch_all().get_json()
    _shadenet = shadenets.fetch_all().get_json()
    
    _customer_shield = cshield.fetch_all().get_json()
    
    _customer_screenhouse = cscreen.fetch_all().get_json()
    
    _customer_shadnet = snet.fetch_all().get_json()


    return render_template(
        'admin/inventory/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        GREENH=_greenhouse,
        GREENS=_greens,
        DEVICES=_device,
        DEVICE_ALL=_devices,
        TYPES=_types,
        DEVICETYPE=_device_types,
        SCREENH=_screens,
        SCREEN_ALL=_screenhouse,
        SHADEN=_shads,
        SHAD_ALL=_shadenet,
        prev=prev, next=next,
        total=total,
        t1=total1,
        t2=total2,
        t3=total3,
        CSHIELD=_customer_shield,
        CSCREEN=_customer_screenhouse,
        CSHAD=_customer_shadnet,
        ACTIVETBL=_actives,
        INACTIVETBL=_inactives,
        data={"date": str(datetime.now().year)},
        data1={"date": str(datetime.now().year),
               "utc_date": datetime.utcnow()},

    )


@igh_inventory.route('/greenhouse-types', methods=['POST', 'GET'])
def greenhouse_types():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    no_of_posts = 100
    # page = request.args.get("number")
    # if page is None:
    #     page = 1
    # else:
    #     page = int(page)
    _typesz = types.fetch_all().get_json()
    # allPosts = Posts.query.filter_by().all()

    last = math.ceil(len(_typesz)/int(no_of_posts))
    page = request.args.get('page')
    if (not str(page).isnumeric()):
        page = 1
    page = int(page)
    posts = _typesz[(page-1)*no_of_posts:(page-1)*no_of_posts + no_of_posts]
    if page == 1:
        prev = "#"
        next = "/?page=" + str(page+1)
    elif page == last:
        prev = "/?page=" + str(page-1)
        next = "#"
    else:
        prev = "/?page=" + str(page-1)
        next = "/?page=" + str(page+1)

    # _types = types.fetch_all().get_json()
    _multiple = []
    action_response ={}

    posted = request.form
    if posted:
        if posted.get("delete_type"):
                print("Delete Type",posted.get("delete_type"))

                types.delete(posted.get("delete_type"))
                posts = types.fetch_all().get_json()

        if "action" in posted:
            # if posted.get("action") == "edit_types" and "type-edit-name" in posted:
             type_id_edit = posted.get("type_id")
             print("EDIT TYPEEEE",posted.get("type_id"))
             print("Name",posted.get("name1"))
             print("Size",posted.get("size1"))
             print("Infolink",posted.get("info_link1"))
             print("Description",posted.get("description1"))
             edit_grn_type = types.edit(
                type_id_edit,
                json.dumps({
                     "name": posted.get("name1"),
                     "size": posted.get("size1"),
                    "description": posted.get("description1"),
                    "info_link": posted.get("info_link1"),
                     "photo_media_id": None,


                })

             )
             print(str(edit_grn_type[0].get_json()))
        # if posted.get("type-edit-name"):
        #     print("EDIT TYPEEEE",posted.get("edit_type"))
        #     print("Name",posted.get("type-edit-name"))
        #     print("Size",posted.get("type-edit-size"))
            # return types.edit(
            #         json.dumps({
            #             "name": posted.get("name"),
            #             "size": posted.get("size"),
            #             "description": posted.get("description"),
            #             "info_link": posted.get("info_link"),
            #             # "user_realm": config.USER_IGH,
            #             "creator_id": session['profile']['user']['id'],
            #             "photo_media_id": ""
            #         }),
            #         True
            #     )
        # if "action" in posted:
        #     if posted.get("action") == "edit_types" and "name1" and "size1" and "info_link1" and "description1" in posted:
        #      print("all",posted.get("name1"),posted.get("size1"))

        if "action" in posted:

            exists = types.fetch_by_name(posted.get("name")).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Greenhouse Type Name already in use, please use a different one."
                }
                print("Its working")
            else :
             if posted.get("action") == "add_types" and "name" and "size" and "info_link" and "description" in posted:
                print("value name",posted.get("name"))
                return types.add_new(
                    json.dumps({
                        "name": posted.get("name"),
                        "size": posted.get("size"),
                        "description": posted.get("description"),
                        "info_link": posted.get("info_link"),
                        # "user_realm": config.USER_IGH,
                        "creator_id": session['profile']['user']['id'],
                        "photo_media_id": ""
                    }),
                    True
                )
       
                # _types = json.loads(posted.get("types"))
                # for type in _types:
                #     type.edit(
                #         types["id"],
                #         json.dumps(
                #             {
                #                 "name": posted.get("name1"),
                #                 "size": posted.get("size1"),
                #                 "description": posted.get("description1"),
                #                 "info_link": posted.get("info_link1"),
                #                 "photo_media_id": ""

                #             }
                #         )
                #     )
                # return jsonify(_types)
        # action_response = {
        #             "type": "success",
        #             "message": "Greenhouse Type added successfully"
        #         }
            

    return render_template(
        'admin/inventory/greenhouse_types.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        TYPES=posts,
        prev=prev, next=next,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_inventory.route('/greenhouse-types/delete/<string:id>', methods=['POST', 'GET'])
def delete_greenh_types(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _types = types.fetch_all().get_json()

    if id:
        types.delete(id)
    _types = types.fetch_all().get_json()

    return render_template(
        'admin/inventory/greenhouse_types.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        TYPES=_types,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_inventory.route('/greenhouses/add', methods=['POST', 'GET'])
def greenhouse_add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _types = types.fetch_all().get_json()
    # print("types", _types)
    _multiple = []
    action_response = {}
    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:
        
            _greenhouse_type_id = posted.getlist("greenhouse_type_id[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _greenhouse_ref = posted.getlist("greenhouse_ref[]")
            _creator_id = posted.getlist("creator_id")
            _assign = posted.get("assigned")
            _status = posted.get("status")
           
            
            x = 0
            for _sz in _size:
                _multiple.append({
                    "greenhouse_ref": _greenhouse_ref[x],
                    "size": _size[x],
                    "info_link": _info_link[x],
                    "greenhouse_type_id":  _greenhouse_type_id[x]
                })
                x = x + 1

            #  exists = greenh.fetch_by_green_ref(_greenhouse_ref[x]).get_json()
            #  if exists:
            #     action_response = {
            #         "type": "danger",
            #         "message": "Greenhouse ID already in use, please use a different one."
            #     }
            #  else:

            #    print("Types",_greenhouse_type_id[x])
            #    print("Size",_size[x])
            #    print("info link",_info_link[x])
            #    print("greenhouse ref",_greenhouse_ref[x])
        else:

            _greenhouse_type_id = posted.getlist("greenhouse_type_id[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _greenhouse_ref = posted.getlist("greenhouse_ref[]")
            _creator_id = posted.getlist("creator_id")
            _assign = posted.get("assigned")
            _status = posted.get("status")

            x = 0
            # for grn_type in _greenhouse_type_id:
           
            print("Data",_greenhouse_type_id[x], _size[x],_info_link[x],_greenhouse_ref[x])
            if _greenhouse_type_id[x] and _size[x] and _info_link[x] :
                  _new_greenhouse = greenh.add_new(
                   json.dumps({
                    "greenhouse_ref": _greenhouse_ref[x],
                    "size": _size[x],
                    "info_link": _info_link[x],
                    "assigned": config.NOT_ASSIGNED,
                    "greenhouse_type_id": _greenhouse_type_id[x],
                    "creator_id": session['profile']['user']['id'],
                    
                   }),
                
                   )
                  print(str(_new_greenhouse[0].get_json()))

                  if _new_greenhouse[1] == 200:
                        pass

                  x = x + 1
                

                 
                  if _new_greenhouse:
                     action_response = {
                    "type": "success",
                    "message": "Greenhouse added successfully."
                     }

                
                  else :

                      action_response = {
                                "type": "danger",
                                "message": "Greenhouse is invalid."
                            }
              
               
            else :

                  action_response = {
                    "type": "danger",
                    "message": "Please add Greenhouse ID, Size and Product Link."
                }
                 
              
             
    return render_template(
        'admin/inventory/greenhouse_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        MULTIGREEN=_multiple,
        TYPESS=_types,
        FORM_ACTION_RESPONSE=action_response,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/edit/greenhouses/<string:id>', methods=['POST', 'GET'])
def greenhouse_edit(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _types = types.fetch_all().get_json()
    print("types", _types)
    greenhouse_details = {}
    action_response = {}
    greenhouse_details = greenh.fetch_one(id,True).get_json()
    _multiple = []
    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:
            _greenhouse_type_id = posted.get("greenhouse_type_id")
            _size = posted.getlist("size[x]")
            _info_link = posted.getlist("info_link[x]")
            # _greenhouse_ref = posted.getlist("greenhouse_ref[x]")
            _creator_id = posted.getlist("creator_id")
            _assign = posted.getlist("assigned")
            _status = posted.getlist("status")
            print("greenhouse type", _size)
            x = 0
            for _name in _greenhouse_type_id:
                _multiple.append({
                    # "greenhouse_ref": _greenhouse_ref,
                    "size": _size,
                    "info_link": _info_link,
                    "assigned": _assign,
                    "greenhouse_type_id": _greenhouse_type_id,
                    "creator_id": _creator_id,
                    "status": _status
                })
                x = x + 1
                print("greenhouse type 1", _greenhouse_type_id)

        else:
            _greenhouse_type_id = posted.get("greenhouse_type_id")
            # _greenhouse_ref = posted.getlist("greenhouse_ref[]")
            _size = posted.getlist("size[]")
            _assign = config.NOT_ASSIGNED
            _info_link = posted.getlist("info_link[]")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            print("greenhouse type 2", _greenhouse_type_id)
           
            print("size", _size[0])
            print("created_by", _creator_id)
            print("info link", _info_link[0])
            print("status", _status)
            x = 0
            if _size[0] != '' and _info_link[0] != '' and _greenhouse_type_id != '':
             _new_greenhouse = greenh.edit(
                id,
                json.dumps({
                    # "greenhouse_ref": _greenhouse_ref[0],
                    "size": _size[0],
                    "info_link": _info_link[0],
                    "assigned": _assign,
                    "greenhouse_type_id": _greenhouse_type_id,
                    "creator_id": _creator_id,
                    "status": _status
                }),
                 True
             )
             action_response = {
                    "type": "success",
                    "message": "Greenhouse Successfully Updated."
                }
             print(str(_new_greenhouse[0].get_json()))
             if _new_greenhouse[1] == 200:

                x = x + 1

            else:

                 action_response = {
                    "type": "warning",
                    "message": "Please fill all required values"
                }


    return render_template(
        'admin/inventory/greenhouse_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        MULTIGREEN=_multiple,
        TYPESS=_types,
        GREENHOUSE = greenhouse_details,
        FORM_ACTION_RESPONSE=action_response,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/greenhouses/bulk', methods=['POST', 'GET'])
def greenhouse_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/inventory/greenhouse_bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_inventory.route('/device-types', methods=['POST', 'GET'])
def device_types():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _device_types = shield_types.fetch_all().get_json()

    _multiple = []

    posted = request.form
    if posted:
        if "action" in posted:
            if posted.get("action") == "add_devices" and "name" and "info_link" and "description" in posted:
                print("deails", posted.get("name"), posted.get("description"),
                      posted.get("info_link"), session['profile']['user']['id'])
                return shield_types.add_new(
                    json.dumps({
                        "name": posted.get("name"),
                        "description": posted.get("description"),
                        "info_link": posted.get("info_link"),
                        "creator_id": session['profile']['user']['id'],
                        "photo_url": '',
                        "placement": 1
                    }),
                    True
                )
            if posted.get("action") == "update_types" and "types" in posted:
                _types = json.loads(posted.get("types"))
                for type in _types:
                    type.edit(
                        types["id"],
                        json.dumps(
                            {
                                "name": types["name"],
                                "description": types["description"],
                                "info_link": types["info_link"],
                                "creator_id": types["creator_id"],
                                "photo_media_id": ""


                            }
                        )
                    )
                return jsonify(_types)

    return render_template(
        'admin/inventory/device_types.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        DEVICE=_device_types,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_inventory.route('/device-types/delete/<string:id>', methods=['POST', 'GET'])
def delete_device_types(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    # _device_types = shield_types.fetch_all().get_json()

    if id:
        shield_types.delete(id)
    _device_types = shield_types.fetch_all().get_json()

    return render_template(
        'admin/inventory/device_types.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        DEVICE=_device_types,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_inventory.route('/devices/add', methods=['POST', 'GET'])
def device_add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _greenhouses = []
    _device_types = []
    _device_types = shield_types.fetch_all().get_json()
    _greenhouses = greenh.fetch_all().get_json()
    _screenhouses = screenh.fetch_all().get_json()
    _shadenets = shadenets.fetch_all().get_json()
    _multiple_device = []
    action_response = {}
    
    posted = request.form
    if posted:

        _serial = posted.getlist("serial[]")
        _mac_address = posted.getlist("mac_address[]")
        _type_id = posted.getlist("type_id[]")
        _spear_id = posted.getlist("spear_id[]")
        _greenhouse_id = posted.getlist("greenhouse_id[]")
        _creator_id = session['profile']['user']['id']
        _status = config.STATUS_ACTIVE
        _assigned = config.NOT_ASSIGNED

        
        x = 0

        for _s in _serial:

            exists = shields.fetch_by_serial(_serial[x]).get_json()
            if exists:

                 action_response = {
                                "type": "danger",
                                "message": "Device is already exists"
                            }
            else:



             print("_greenhouse_id",_greenhouse_id)

             if _serial[0] and _mac_address[0] and _type_id[0] :

             

              _new_device = shields.add_new(
                json.dumps({
                    "serial": _serial[x],
                    "assigned": _assigned,
                    "mac_address": _mac_address[x],
                    "type_id": _type_id[x],
                    "creator_id": _creator_id,
                    "greenhouse_id": _greenhouse_id[x],
                    "status": _status
                })
               
              )
              x = x + 1



        # if _mac_address[x] != None and _serial != None:
        #     print("Devices Add", _serial,
        #           _mac_address[x], _type_id[x], _creator_id)
        #     _new_device = shields.add_new(
        #         json.dumps({
        #             "serial": _serial[x],
        #             "assigned": _assigned[x],
        #             "mac_address": _mac_address[x],
        #             "type_id": _type_id[x],
        #             "creator_id": _creator_id,
        #             "greenhouse_id": _greenhouse_id[x],
        #             "status": _status
        #         })
               
        #     )

              if _new_device:
                     action_response = {
                        "type": "success",
                        "message": "Device is added successfully"
                        }
              else :

                      action_response = {
                                "type": "danger",
                                "message": "Device is invalid."
                            }
                     
            
             print(str(_new_device[0].get_json()))
            #    if _new_device[1] == 200:

            #     x = x + 1

        # if _mac_address[x] != None :

        #     _add_spear = spear.add_new(
        #         json.dumps({
        #             "serial": _spear_id[x],
        #             "mac_address": _mac_address[x],
        #             "creator_id": _creator_id

        #         }),
        #         True

        #     )
        # if _creator_id:

            #    _type_ids = shield_types.fetch_one(_type_id[0],True).get_json()['real_id']
            # _shield_ids = shields.fetch_by_serial(_serial).get_json()
            # if _shield_ids == True:
            #     _shield_idn = _shield_ids['id']
            #     _shields_id = shields.fetch_one(
            #         _shield_idn, True).get_json()['real_id']
            #     if _spear_id[x] == True:
            #         _spears_idn = spear.fetch_by_serial(
            #             _spear_id[x]).get_json()['id']
            #         _spears_id = spear.fetch_one(
            #             _spears_idn, True).get_json()['real_id']

            #         print("shield id and spear id", _shields_id, _spears_id)

            #         _add_spears = spears.add_new(
            #             json.dumps({
            #                 "shield_id": _shields_id,
            #                 "spear_id": _spears_id,
            #                 "creator_id": _creator_id

            #             }),
            #             True

            #         )
        
        
               

    return render_template(
        'admin/inventory/device_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        DTYPE=_device_types,
        FORM_ACTION_RESPONSE= action_response,
        DEVICES=_multiple_device,
        GREENHOUSES=_greenhouses,
        SCREENHOUSES = _screenhouses,
        SHADENETS = _shadenets,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/edit/device/<string:id>', methods=['POST', 'GET'])
def device_edit(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _device_types = shield_types.fetch_all().get_json()
    _multiple_device = []
    posted = request.form
    if posted:

        _serial = posted.get("serial")
        _mac_address = posted.getlist("mac_address[]")
        _type_id = posted.getlist("type_id[]")
        _spear_id = posted.getlist("spear_id[]")
        _creator_id = session['profile']['user']['id']
        _status = config.STATUS_ACTIVE
        _assigned = config.NOT_ASSIGNED
        x = 0

        if _mac_address[x]:
            print("Devices Add", _serial,
                  _mac_address[x], _type_id[x], _creator_id)
            _new_device = shields.add_new(
                json.dumps({
                    "serial": _serial,
                    "assigned": _assigned,
                    "mac_address": _mac_address[x],
                    "type_id": _type_id[x],
                    "creator_id": _creator_id,
                    "boron_id": "2324444",
                    "status": _status
                }),
                True
            )
            #    print(str(_new_device[0].get_json()))
            #    if _new_device[1] == 200:

            #     x = x + 1

        if _mac_address[x] != None and _spear_id[x] != None:

            _add_spear = spear.add_new(
                json.dumps({
                    "serial": _spear_id[x],
                    "mac_address": _mac_address[x],
                    "creator_id": _creator_id

                }),
                True

            )
        if _creator_id:

            #    _type_ids = shield_types.fetch_one(_type_id[0],True).get_json()['real_id']
            _shield_ids = shields.fetch_by_serial(_serial).get_json()
            if _shield_ids == True:
                _shield_idn = _shield_ids['id']
                _shields_id = shields.fetch_one(
                    _shield_idn, True).get_json()['real_id']
                if _spear_id[x] == True:
                    _spears_idn = spear.fetch_by_serial(
                        _spear_id[x]).get_json()['id']
                    _spears_id = spear.fetch_one(
                        _spears_idn, True).get_json()['real_id']

                    print("shield id and spear id", _shields_id, _spears_id)

                    _add_spears = spears.add_new(
                        json.dumps({
                            "shield_id": _shields_id,
                            "spear_id": _spears_id,
                            "creator_id": _creator_id

                        }),
                        True

                    )

    return render_template(
        'admin/inventory/device_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        DTYPE=_device_types,
        DEVICES=_multiple_device,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/devices/bulk', methods=['POST', 'GET'])
def device_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/inventory/device_bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_inventory.route('/download/device', methods=['POST', 'GET'])

def getPlotCSV():
     redirect, realm = portal_check(session, 'admin')
     if redirect:
        return realm
    # with open("outputs/Adjacency.csv") as fp:
    #     csv = fp.read()
     csv = '1,2,3\n4,5,6\n'
    
     return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=device.csv"})



    


@igh_inventory.route('/screenhouses/add', methods=['POST', 'GET'])
def screenhouse_add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _screenhouse = []
    action_response = {}
    posted = request.form
    if posted:
        # add_multiple = posted.get("add-multiple")
        # if add_multiple:
        #     _id = posted.getlist("id[]")
        #     _screenhouse_ref = posted.getlist("screenhouse_ref[]")
        #     _size = posted.getlist("size[]")
        #     _info_link = posted.getlist("info_link[]")
        #     _assigned = config.NOT_ASSIGNED
        #     _creator_id = posted.getlist("creator_id")
        #     _status = posted.getlist("status")
        #     # _created = posted.getlist("created")

        #     x = 0
        #     for _name in _screenhouse_ref:
        #         _screenhouse.append({
        #             "id": _id[x],
        #             "screenhouse_ref": _screenhouse_ref[x],
        #             "size": _size[x],
        #             "info_link": _info_link[x],
        #             "creator_id": _creator_id,
        #             "assigned": _assigned,
        #             "status": _status
        #         })
        #         x = x + 1

        # else:
            # _id = posted.getlist("id[]")
            _screenhouse_ref = posted.getlist("screenhouse_ref[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            _assigned = config.NOT_ASSIGNED

            x = 0
            for _name in _screenhouse_ref:

            #   print("info link", _info_link[x])
              exists = screenh.fetch_by_screen_ref(_screenhouse_ref[x]).get_json()
              print("Exists",exists)
              if exists:
                action_response = {
                    "type": "danger",
                    "message": "Screenhouse ID already in use, please use a different one."
                }
            #  elif _screenhouse_ref[x] == '' and _size[x] == '' and  _info_link[x] == '':
            #     action_response = {
            #         "type": "danger",
            #         "message": "Please add Screenhouse Id, Size and Product Link."
            #     }
              else :

            #   x = 0
                _new_screenhouse = screenh.add_new(
                   json.dumps({

                    "screenhouse_ref": _screenhouse_ref[x],
                    "size": _size[x],
                    "info_link": _info_link[x],
                    "creator_id": _creator_id,
                    "status": _status,
                    "assigned": _assigned,
                }),
               
                  )
                # if _new_screenhouse[1] == 200:  
                x = x + 1
                action_response = {
                    "type": "success",
                    "message": "Screenhouse added successfully."
                }
                print(str(_new_screenhouse[0].get_json()))








        
        
              

               

    return render_template(
        'admin/inventory/screenhouse_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        SCREENHS=_screenhouse,
        FORM_ACTION_RESPONSE=action_response,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/edit/screenhouses/<string:id>', methods=['POST', 'GET'])
def screenhouse_edit(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _screenhouse = []
    action_response = {}
    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:
            _id = posted.getlist("id[x]")
            _screenhouse_ref = posted.getlist("screenhouse_ref[x]")
            _size = posted.getlist("size[x]")
            _info_link = posted.getlist("info_link[x]")
            _assigned = config.NOT_ASSIGNED
            _creator_id = posted.getlist("creator_id")
            _status = posted.getlist("status")
            # _created = posted.getlist("created")

            x = 0
            for _name in _screenhouse_ref:
                _screenhouse.append({
                    "id": _id,
                    "screenhouse_ref": _screenhouse_ref,
                    "size": _size,
                    "info_link": _info_link,
                    "creator_id": _creator_id,
                    "assigned": _assigned,
                    "status": _status
                })
                x = x + 1

        else:
            _id = posted.getlist("id[]")
            _screenhouse_ref = posted.getlist("screenhouse_ref[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            _assigned = config.NOT_ASSIGNED

            print("info link", _info_link[0])

            

            x = 0
            _new_screenhouse = screenh.add_new(
                json.dumps({

                    "screenhouse_ref": _screenhouse_ref[0],
                    "size": _size[0],
                    "info_link": _info_link[0],
                    "creator_id": _creator_id,
                    "status": _status,
                    "assigned": _assigned,
                }),
                True
            )
            print(str(_new_screenhouse[0].get_json()))
            if _new_screenhouse[1] == 200:

                x = x + 1

    return render_template(
        'admin/inventory/screenhouse_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        SCREENHS=_screenhouse,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/screenhouses/bulk', methods=['POST', 'GET'])
def screenhouse_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/inventory/screenhouse_bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/shadenets/add', methods=['POST', 'GET'])
def shadenet_add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _shadenet = []
    action_response = {}
    posted = request.form
    if posted:
        # add_multiple = posted.get("add-multiple")
        # if add_multiple:

        #     _shadenet_ref = posted.getlist("shadenet_ref[x]")
        #     _size = posted.getlist("size[x]")
        #     _info_link = posted.getlist("info_link[x]")
        #     _assigned = config.NOT_ASSIGNED
        #     _creator_id = posted.getlist("creator_id")
        #     _status = posted.getlist("status")
        #     # _created = posted.getlist("created")

        #     x = 0
        #     for _name in _shadenet_ref:
        #         _shadenet.append({

        #             "shadenet_ref": _shadenet_ref,
        #             "size": _size,
        #             "info_link": _info_link,
        #             "creator_id": _creator_id,
        #             "assigned": _assigned,
        #             "status": _status
        #         })
        #         x = x + 1

        # else:

            _shadenet_ref = posted.getlist("shadenet_ref[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            _assigned = config.NOT_ASSIGNED
          
            x = 0
            for _name in _shadenet_ref:
             exists = shadenets.fetch_by_shadenet_ref(_shadenet_ref[x]).get_json()
             if exists:
                action_response = {
                    "type": "danger",
                    "message": "Shadenet ID already in use, please use a different one."
                }
            #  elif _shadenet_ref[x] == '' and _size[x] == '' and  _info_link[x] == '':
            #     action_response = {
            #         "type": "danger",
            #         "message": "Please add shadenet Id, Size and Product Link."
            #     }
           
             else :
            #  x = 0
              _new_shadenet = shadenets.add_new(
                json.dumps({

                    "shadenet_ref": _shadenet_ref[x],
                    "size": _size[x],
                    "info_link": _info_link[x],
                    "creator_id": _creator_id,
                    "status": _status,
                           "assigned": _assigned
                           }),
                
              )
              x = x + 1

              action_response = {
                    "type": "success",
                    "message": "Shadenet added successfully ."
                }

            #   if _new_shadenet[1] == 200:

            #     return render_template('admin/inventory/main.html')

    return render_template(
        'admin/inventory/shadenet_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        SHADENET=_shadenet,
        FORM_ACTION_RESPONSE=action_response,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/edit/shadenets/<string:id>', methods=['POST', 'GET'])
def shadenet_edit(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _shadenet = []
    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:

            _shadenet_ref = posted.getlist("shadenet_ref[x]")
            _size = posted.getlist("size[x]")
            _info_link = posted.getlist("info_link[x]")
            _assigned = config.NOT_ASSIGNED
            _creator_id = posted.getlist("creator_id")
            _status = posted.getlist("status")
            # _created = posted.getlist("created")

            x = 0
            for _name in _shadenet_ref:
                _shadenet.append({

                    "shadenet_ref": _shadenet_ref,
                    "size": _size,
                    "info_link": _info_link,
                    "creator_id": _creator_id,
                    "assigned": _assigned,
                    "status": _status
                })
                x = x + 1

        else:

            _shadenet_ref = posted.getlist("shadenet_ref[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            _assigned = config.NOT_ASSIGNED
            # _created = posted.getlist("created")

            x = 0
            _new_shadenet = shadenets.add_new(
                json.dumps({

                    "shadenet_ref": _shadenet_ref[0],
                    "size": _size[0],
                    "info_link": _info_link[0],
                    "creator_id": _creator_id,
                    "status": _status,
                           "assigned": _assigned
                           }),
                True
            )

            if _new_shadenet[1] == 200:

                return render_template('admin/inventory/main.html')

    return render_template(
        'admin/inventory/shadenet_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        SHADENET=_shadenet,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/shadenets/bulk', methods=['POST', 'GET'])
def shadenet_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/inventory/shadenet_bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_inventory.route('/addplant', methods=['POST', 'GET'])
def add_plant():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _multiple = []

    plant_suggest = suggested_plant.fetch_all().get_json()
    _this_org = session["profile"]["user"]["real_id"]
   
  
  

    action_response = {}
  

    posted = request.form
    if posted:

        _plant_name = posted.getlist("plantname[]")
        _plant_variety = posted.getlist("varietyofplant[]")
        _planting_period = posted.getlist("planting_period[]")
        _production_period = posted.getlist("production_period[]")
        _harvest_period = posted.getlist("harvest_period[]")

        exists = suggested_plant.fetch_by_name(_plant_name[0]).get_json()
       
        if _plant_name[0] == '' and _plant_variety[0] == '' :

            print("Plant is invalid")
        elif exists == {}:
        
        

            
                _plant = SuggestedPlants(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    plant_name= _plant_name[0],
                    creator_id=_this_org

                )
                db.session.add(_plant)
                db.session.commit()

                # print("Plantttt", _plant.id, _plant_name[x])

                

                _planty_verieties = SuggestedVariety(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    variety_name=_plant_variety[0],
                    plantings_id=_plant.id,
                    planting_period = _planting_period[0],
                    production_period = _production_period[0],
                    harvest_period = _harvest_period[0],

                    creator_id=_this_org
                )
                db.session.add(_planty_verieties)
                db.session.commit()

        else:

            plant_id = exists['id']
            real_p_id = suggested_plant.fetch_one(plant_id,true).get_json()['real_id']


            print("Plant id ",real_p_id)

            _planty_verieties = SuggestedVariety(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    variety_name=_plant_variety[0],
                    plantings_id= real_p_id,
                    planting_period = _planting_period[0],
                    production_period = _production_period[0],
                    harvest_period = _harvest_period[0],

                    creator_id=_this_org
                )
            db.session.add(_planty_verieties)
            db.session.commit()

                

   

    return render_template(
        'admin/inventory/add_plant.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        MULTIGREEN=_multiple,
        PSUGGEST = plant_suggest,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )
