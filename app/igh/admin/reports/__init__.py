from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid
import flask_excel as excel
import io
import xlwt
import pdfkit
import openpyxl
from werkzeug.exceptions import HTTPException
import psycopg2
from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from flask import Response
from flask import make_response
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from werkzeug.wrappers import response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from app.core.customer import farms
from app.core.customer import shields
from app.core.maintenance import traceability_report_requests
from app.core.report import waste_pollution_management
from app.core.report import fertilizer_application
from app.core.report import agrochemical_application
from app.core.report import preharvest_checklist
from app.core.report import harvest
from app.core.report import scouting
from app.core.report import other
from app.models import WastePollutionManagement
from app.core import customer, report
from app.core.report import scouting_agronomist_recommendations
from app.core.report import waste_n_p_agronomy_recommend
from app.core.report import fertilizer_app_agronomist_rec
from app.core.report import agrochem_app_agronomist_rec
from app.core.report import preharv_agronomist_recom
from app.core.report import harvest_agronomy_recomend
from app.core.report import other_agronomist_recom
from app.core.planting import crops, plants
from app.core.report import traceability
from app.models import Report
from app.models import TraceabilityReportRequest
from app.core.user import logs
from app.config import activateConfig
from app.core.user import customer_users
from app.core import user
from datetime import timedelta
from app.igh import portal_check
from app.igh.admin import contact_person

from app.igh import session_setup

from app.helper import valid_uuid
from app.core.supply import application_methods
from app.models import db
from werkzeug.wrappers import response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from app.models import WastePollutionManagement
from app.core.report import traceability_plant_queries
from app.core.report import traceability_fertilizer_queries
from app.core.report import traceability_agrochemical_queries
from app.core.report import traceability_irrigation_queries
from app.core.report import traceability_harvest_queries

igh_reports = Blueprint('igh_reports', __name__,
                        static_folder='../../static', template_folder='.../../templates')
igh_reports.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


@igh_reports.route('/daily', methods=['POST', 'GET'], defaults={"page": 1})
@igh_reports.route('/daily/<int:page>', methods=['POST', 'GET'])
def daily_reports(page):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    page = page
    prev = page-1
    next = page+1

    no_of_posts = config.PAGE_LIMIT

    _waste_pollution = []
    _fertilizer_app = []
    _agrochem_app = []
    _preharv_check = []
    _harvest_details = []
    _scouting = []
    _other_reports = []
    _application_methods = []
    action_response = {}
    _agronomist_recomendation = []
    _wnp_recomendation = []
    _fetilizer_recomend = []
    _agrochem_recomend = []
    _preharv_recomend = []
    _harvest_recomend = []
    _other_recomend = []

    total = {}
    total = round(len(_waste_pollution)/no_of_posts)

    _waste_pollution = waste_pollution_management.fetch_all1(page).get_json()
    # if request.method == 'POST' and 'tag_wnp' in request.form:
    #     tag = request.form["tag_wnp"]
    #     print("taggggggg", tag)
    #     if tag:
    #         # search = "%{}%".format(tag)
    #         search = tag
    #         if search:

    #             _reporty = []
    #             if search.isnumeric() == True:
    #                 _reporty = db.session.query(WastePollutionManagement).filter(or_((WastePollutionManagement.customer_id == search), (
    #                     WastePollutionManagement.customer_shield_id == search), (WastePollutionManagement.creator_id == search))).all()
    #                 # _customery = db.session.query(WastePollutionManagement).filter(or_(WastePollutionManagement.residue.like(
    #                 #     '%'+search+'%'), WastePollutionManagement.source.like('%'+search+'%'))).limit(no_of_posts).offset((page-1)*no_of_posts)
    #                 response = []
    #                 for custmer in _reporty:

    #                     response.append(
    #                         waste_pollution_management.wpm_obj(custmer))
    #                 _waste_pollution = jsonify(response).get_json()

    #                 print("Waste pollution", _waste_pollution)

    posted = request.form
    if posted:

        # _assign_id = posted.get('wndp_check_id')
        _assign_id = posted.get('report_sghs_hidden')
        _score_id = posted.get('score_good')
        # _score_id_f = posted.get('score_fair')
        # _score_id_b = posted.get('score_bad')

        print("All assignments", _assign_id, _score_id)

        # _assign_id_f = posted.get('fert_check_id')
        _assign_id_f = posted.get('report_f_hidden')

        _score_id_fert = posted.get('score_f_good')
        # _score_id_f_fert = posted.get('score_f_fair')
        # _score_id_b_fert = posted.get('score_f_bad')

        _recomend_id = None

        # _assign_id_ag = posted.get('agro_check_id')
        _assign_id_ag = posted.get('report_ag_hidden')
        _score_id_agro = posted.get('score_ag_good')
        # _score_id_f_agro = posted.get('score_ag_fair')
        # _score_id_b_agro = posted.get('score_ag_bad')

        # _assign_id_preharv = posted.get('preharv_check_id')
        _assign_id_preharv = posted.get('report_preharv_hidden')
        _score_id_preharv = posted.get('score_preharv_good')
        # _score_id_f_preharv = posted.get('score_preharv_fair')
        # _score_id_b_preharv = posted.get('score_preharv_bad')

        _harvest_fetch_id = posted.get('harvest_fetch_id')
        _score_harv_good = posted.get('score_harv_good')

        # _assign_id_scout = posted.get('scout_check_id')
        _assign_id_scout = posted.get('report_scout_hidden')
        _score_id_scout = posted.get('score_scout_good')

        # print("scouttttttttttttt",_assign_id_scout,_score_id_scout)
        # _score_id_f_scout = posted.get('score_scout_fair')
        # _score_id_b_scout = posted.get('score_scout_bad')

        # _assign_id_other = posted.get('other_check_id')
        _assign_id_other = posted.get('report_other_hidden')
        _score_id_other = posted.get('score_other_good')
        # _score_id_f_other = posted.get('score_other_fair')
        # _score_id_b_other = posted.get('score_other_bad')

        # _spray = posted.get('spray')
        # _fbrand = posted.get('fbrand')
        # _active_ingradient = posted.get('active_ingradient')
        # _pesti_dosage = posted.get('pesti_dosage')
        # _pesti_qantity = posted.get('pesti_qantity')
        # _water_amount = posted.get('water_amount')
        # _tech_mode = posted.get('tech_mode')
        # _date_of_app = posted.get('date_of_app')
        # print("All values",_spray,_fbrand,_active_ingradient,_pesti_dosage,_pesti_qantity,_water_amount)
        _app_recommend = posted.get('app_recommend')
        _app_wnp_recommend = posted.get('app_wnp_recommend')

        _app_fert_recommend = posted.get('app_fert_recommend')
        _app_fert_id = posted.get('fert-recomend-id')

        _app_agro_recommend = posted.get('app_agro_recommend')
        _app_agro_id = posted.get('agro-recomend-id')

        _app_preharv_recommend = posted.get('app_preharv_recommend')
        _app_preharv_id = posted.get('preharv-recomend-id')

        _app_harv_recommend = posted.get('app_harv_recommend')
        _app_harv_id = posted.get('harv-recomend-id')

        _app_others_recommend = posted.get('app_oth_recommend')
        _app_others_id = posted.get('other-recomend-id')

        #  Edit And Delete Section

        if "action" in posted:
            if posted.get("action") == "update_fert_reccomend" and "fertrecomedid" in posted:

                fert_t_id = posted.get('ferteditid')

                fert_reccomend_id = posted.get('fertrecomedid')

                fert_reccomend_details = posted.get('fertrecomenddetails')

                # print("RECCOMEND DETAILS",fert_t_id,fert_reccomend_id,fert_reccomend_details)

                fertilizer_recomend_details = fertilizer_app_agronomist_rec.fetch_one(
                    fert_reccomend_id, True).get_json()
                print("All details ", fertilizer_recomend_details)
                # if fertilizer_recomend_details != {}:
                #   _app_fert_rec_id = fertilizer_recomend_details['fertilize_id']['id']
                #   _report_id_recn = fertilizer_recomend_details['report_id']['id']
                #   _customer_id_recn = fertilizer_recomend_details['customer_id']['id']

                edit_fertilize_recomend = fertilizer_app_agronomist_rec.edit(
                    fert_reccomend_id,
                    json.dumps({
                        "recomend": fert_reccomend_details,

                        # "fertilize_id": _app_fert_rec_id,
                        # "report_id": _report_id_recn,
                        # "customer_id": _customer_id_recn,
                        # "creator_id": session['profile']['user']['id'],

                    }),

                )
                print(str(edit_fertilize_recomend[0].get_json()))

            if posted.get("action") == "update_agro_reccomend" and "agrorecomedid" in posted:

                agro_t_id = posted.get('agroeditid')

                agro_reccomend_id = posted.get('agrorecomedid')

                agro_reccomend_details = posted.get('agrorecomenddetails')

                # print("AGRO RECCOMEND DETAILS",agro_t_id,agro_reccomend_id,agro_reccomend_details)

                edit_agrochemical_recomend = agrochem_app_agronomist_rec.edit(
                    agro_reccomend_id,
                    json.dumps({
                        "recomend": agro_reccomend_details,

                    }),

                )
                print(str(edit_agrochemical_recomend[0].get_json()))

            if posted.get("action") == "update_scout_reccomend" and "scoutrecomedid" in posted:

                scout_t_id = posted.get('scouteditid')

                scout_reccomend_id = posted.get('scoutrecomedid')

                scout_reccomend_details = posted.get('scoutrecomenddetails')

            if posted.get("action") == "update_preharv_reccomend" and "preharvrecomedid" in posted:

                preharv_t_id = posted.get('preharveditid')

                preharv_reccomend_id = posted.get('preharvrecomedid')

                preharv_reccomend_details = posted.get(
                    'preharvrecomenddetails')

                # print("PREHARV RECCOMEND DETAILS",preharv_t_id,preharv_reccomend_id,preharv_reccomend_details)

                edit_preharvest_recomend = preharv_agronomist_recom.edit(
                    preharv_reccomend_id,
                    json.dumps({
                        "recomend": preharv_reccomend_details,

                    }),

                )
                print(str(edit_preharvest_recomend[0].get_json()))

            if posted.get("action") == "update_harv_reccomend" and "harvrecomedid" in posted:

                harv_t_id = posted.get('harveditid')

                harv_reccomend_id = posted.get('harvrecomedid')

                harv_reccomend_details = posted.get('harvrecomenddetails')

                # print("HARV RECCOMEND DETAILS",harv_t_id,harv_reccomend_id,harv_reccomend_details)

                edit_harvest_recomend = harvest_agronomy_recomend.edit(
                    harv_reccomend_id,
                    json.dumps({
                        "recomend": harv_reccomend_details,

                    }),

                )
                print(str(edit_harvest_recomend[0].get_json()))

            if posted.get("action") == "update_wnp_reccomend" and "wnprecomedid" in posted:

                wnp_t_id = posted.get('wnpeditid')

                wnp_reccomend_id = posted.get('wnprecomedid')

                wnp_reccomend_details = posted.get('wnprecomenddetails')

                # print("WNP RECCOMEND DETAILS",wnp_t_id,wnp_reccomend_id,wnp_reccomend_details)

                edit_wnp_recomend = waste_n_p_agronomy_recommend.edit(
                    wnp_reccomend_id,
                    json.dumps({
                        "wnprec": wnp_reccomend_details,

                    }),

                )
                print(str(edit_wnp_recomend[0].get_json()))

            if posted.get("action") == "update_other_reccomend" and "otherrecomedid" in posted:

                other_t_id = posted.get('othereditid')

                other_reccomend_id = posted.get('otherrecomedid')

                other_reccomend_details = posted.get('otherrecomenddetails')

                # print("OTHER RECCOMEND DETAILS",other_t_id,other_reccomend_id,other_reccomend_details)

                edit_other_recomend = other_agronomist_recom.edit(
                    other_reccomend_id,
                    json.dumps({
                        "recomend": other_reccomend_details,

                    }),

                )
                print(str(edit_other_recomend[0].get_json()))

     #     DELETE SECTION FOR RECCOMEND AGRONOMIST

            if posted.get("action") == "delete_fert_reccomend" and "deleterecomedid" in posted:

                delete_fert_reccom_id = posted.get('deleterecomedid')

                print("FERT DELETE RECOMEND ID", delete_fert_reccom_id)

                delete_fertilize_recomend = fertilizer_app_agronomist_rec.delete(
                    delete_fert_reccom_id)

            if posted.get("action") == "delete_agro_reccomend" and "deleteagrorecomedid" in posted:

                delete_agro_reccom_id = posted.get('deleteagrorecomedid')

                delete_agrochemical_recomend = agrochem_app_agronomist_rec.delete(delete_agro_reccom_id)

            if posted.get("action") == "delete_scout_reccomend" and "deletescoutrecomedid" in posted:

                delete_scout_reccom_id = posted.get('deletescoutrecomedid')

            if posted.get("action") == "delete_preharv_reccomend" and "deletepreharvrecomedid" in posted:

                delete_preharv_reccom_id = posted.get('deletepreharvrecomedid')

                delete_preharvest_recomend = preharv_agronomist_recom.delete(delete_preharv_reccom_id)

            if posted.get("action") == "delete_harv_reccomend" and "deleteharvrecomedid" in posted:

                delete_harv_reccom_id = posted.get('deleteharvrecomedid')

                delete_harvest_recomend = harvest_agronomy_recomend.delete(delete_harv_reccom_id)

            if posted.get("action") == "delete_wnp_reccomend" and "deletewnprecomedid" in posted:

                delete_wnp_reccom_id = posted.get('deletewnprecomedid')

                print("WNp delete",delete_wnp_reccom_id)

                delete_wnp_recomend = waste_n_p_agronomy_recommend.delete(delete_wnp_reccom_id)

            if posted.get("action") == "delete_other_reccomend" and "deleteotherrecomedid" in posted:

                delete_other_reccom_id = posted.get('deleteotherrecomedid')

                

                delete_other_recomend = other_agronomist_recom.delete(delete_other_reccom_id)

        if _assign_id_scout != None:

            report_ids = scouting.fetch_one(_assign_id_scout, True).get_json()
            _scouting_real = report_ids['real_id']
            # _report_id = report_id['report_id']['id']

            _spray = posted.get('spray')
            _fbrand = posted.get('fbrand')
            _active_ingradient = posted.get('active_ingradient')
            _pesti_dosage = posted.get('pesti_dosage')
            _pesti_qantity = posted.get('pesti_qantity')
            _water_amount = posted.get('water_amount')
            _tech_mode = posted.get('tech_mode')
            _date_of_app = posted.get('date_of_app')

            if report_ids != {}:
                _report_ids = report_ids['report_id']['id']
                _report_reals = report.fetch_one(
                    _report_ids, True).get_json()['real_id']
                _report_types = report_ids['report_id']["type"]
                if _report_types == 'Waste pollution management':
                    _report_new_types = config.REPORT_WASTE_POLLUTION_MANAGEMENT
                elif _report_types == 'Fertilizer application':
                    _report_new_types = config.REPORT_FERTILIZER_APPLICATION
                elif _report_types == 'Aggrochemical application':
                    _report_new_types = config.REPORT_AGROCHEMICAL_APPLICATION
                elif _report_types == 'Pre-harvest checklist':
                    _report_new_types = config.REPORT_PREHARVEST_CHECKLIST
                elif _report_types == 'Harvest detail':
                    _report_new_types = config.REPORT_HARVEST_DETAIL
                elif _report_types == 'Scouting report':
                    _report_new_types = config.REPORT_SCOUTING_REPORT
                elif _report_types == 'Others':
                    _report_new_types = config.REPORT_OTHERS

            _customer_id = report_ids['customer_id']['id']

            _report_recommends = report_ids['report_id']["recomend_id"]
            _report_scores = report_ids['report_id']["score"]

            # print("All Scouting details", _customer_id, _report_ids, _spray, _fbrand,
            #       _active_ingradient, _pesti_dosage, _pesti_qantity, _water_amount, _tech_mode, _date_of_app)
            if _report_recommends == None:
                agrochem_recomend = scouting_agronomist_recommendations.add_new(
                    json.dumps({
                        "spray": _spray,
                        "agrochem": _fbrand,
                        "active_ingredient": _active_ingradient,
                        "agrochem_quantity": _pesti_qantity,
                        "water_amount": _water_amount,
                        "application_rate": _pesti_dosage,
                        "application_method_id": _tech_mode,
                        "application_start_date": _date_of_app,
                        "scouting_id": _assign_id_scout,
                        "report_id": _report_ids,
                        "customer_id": _customer_id,
                        "creator_id": session['profile']['user']['id'],

                    }),

                )
                print(str(agrochem_recomend[0].get_json()))

                if agrochem_recomend:
                    # _report_score = report_id['report_id']['score']
                    _recomended_id = scouting_agronomist_recommendations.fetch_by_scouting(
                        _scouting_real).get_json()['id']
                    _recomended_real_id = scouting_agronomist_recommendations.fetch_one(
                        _recomended_id, True).get_json()['real_id']
                    print("Report ID", _report_ids)
                    report_agro_rec = report.edit(
                        _report_ids,
                        json.dumps({
                            "type": _report_new_types,
                            "score": _report_scores,
                            "creator_id": session['profile']['user']['id'],
                            "recomend_id": _recomended_real_id

                        }),

                    )
                    print(str(report_agro_rec[0].get_json()))

        if _app_wnp_recommend:

            # print("Assign ID 1",posted.get('wnp-recomend-id'))

            # print("Assign ID 2",posted.get('report_sghs_hiddens'))

            print("Assign ID", posted.get('report_sghs_hidden'))
            report_id1 = waste_pollution_management.fetch_one(
                posted.get('wnp-recomend-id'), True).get_json()

            if report_id1 != {}:
                _report_id1 = report_id1['report_id']['id']
                _report_real = report.fetch_one(
                    _report_id1, True).get_json()['real_id']
                _report_type = report_id1['report_id']["type"]
                if _report_type == 'Waste pollution management':
                    _report_new_type = config.REPORT_WASTE_POLLUTION_MANAGEMENT
                elif _report_type == 'Fertilizer application':
                    _report_new_type = config.REPORT_FERTILIZER_APPLICATION
                elif _report_type == 'Aggrochemical application':
                    _report_new_type = config.REPORT_AGROCHEMICAL_APPLICATION
                elif _report_type == 'Pre-harvest checklist':
                    _report_new_type = config.REPORT_PREHARVEST_CHECKLIST
                elif _report_type == 'Harvest detail':
                    _report_new_type = config.REPORT_HARVEST_DETAIL
                elif _report_type == 'Scouting report':
                    _report_new_type = config.REPORT_SCOUTING_REPORT
                elif _report_type == 'Others':
                    _report_new_type = config.REPORT_OTHERS

                _report_recommend = report_id1['report_id']["recomend_id"]
                _report_score = report_id1['report_id']["score"]
                # _report_creator = report_id1['report_id']["created_by"]['id']
                # print("Creator ID", _report_creator)
                _customer_id1 = report_id1['customer_id']['id']
                # print("recommendddddddddd", _report_recommend)

                if posted.get('wnp-recomend-id') != None and _app_wnp_recommend != None and _report_id1 != None and _report_recommend == None:

                    wnp_recomend = waste_n_p_agronomy_recommend.add_new(
                        json.dumps({
                            "wnprec": _app_wnp_recommend,

                            "wnp_id": posted.get('wnp-recomend-id'),
                            "report_id": _report_id1,
                            "customer_id": _customer_id1,
                            "creator_id": session['profile']['user']['id'],

                        }),

                    )
                    print(str(wnp_recomend[0].get_json()))

                    if wnp_recomend:
                        _recomend_id = waste_n_p_agronomy_recommend.fetch_by_report_single(
                            _report_real).get_json()['id']
                        _recomend_real = waste_n_p_agronomy_recommend.fetch_one(
                            _recomend_id, True).get_json()['real_id']
                        print("Recomend", _recomend_real)
                        report_update = report.edit(
                            _report_id1,
                            json.dumps({
                                "type": _report_new_type,
                                "score": _report_score,
                                "recomend_id": _recomend_real,
                                "creator_id": session['profile']['user']['id'],
                            }),

                        )
                        print(str(report_update[0].get_json()))

            #   if wnp_recomend:
            #      wnp_details = waste_pollution_management.fetch_one(posted.get('wnp-recomend-id'),True).get_json()
            #      _residue = wnp_details['residue']
            #      _source = wnp_details['source']
            #      _eliminated_amount =  wnp_details['eliminated_amount']
            #      _reduced_amount =  wnp_details['reduced_amount']
            #      _recycled_amount =  wnp_details['recycled_amount']
            #      _wnp_real_id = wnp_details['real_id']
            #      _recommendations = waste_n_p_agronomy_recommend.fetch_by_wnp(_wnp_real_id).get_json()['id']

            #      wnp_report = waste_pollution_management.edit(

            #           json.dumps({
            #                "residue": _residue,
            #                "source" : _source,
            #                "eliminated_amount" : _eliminated_amount,
            #                "reduced_amount": _reduced_amount,
            #                "recycled_amount" : _recycled_amount,
            #                "recommendations" : _recommendations,

            #                }),

            #      )
            #      print(str(wnp_report[0].get_json()))

        if _app_fert_id and _app_fert_recommend:

            print("App recomendeddddd", _app_fert_recommend)
            print("Fertilizer ID ", _app_fert_id)
            report_id2 = fertilizer_application.fetch_one(
                _app_fert_id, True).get_json()

            if report_id2 != {}:
                _report_id2 = report_id2['report_id']['id']

                _report_real2 = report.fetch_one(
                    _report_id2, True).get_json()['real_id']
                _report_type2 = report_id2['report_id']["type"]
                if _report_type2 == 'Waste pollution management':
                    _report_new_type2 = config.REPORT_WASTE_POLLUTION_MANAGEMENT
                elif _report_type2 == 'Fertilizer application':
                    _report_new_type2 = config.REPORT_FERTILIZER_APPLICATION
                elif _report_type2 == 'Aggrochemical application':
                    _report_new_type2 = config.REPORT_AGROCHEMICAL_APPLICATION
                elif _report_type2 == 'Pre-harvest checklist':
                    _report_new_type2 = config.REPORT_PREHARVEST_CHECKLIST
                elif _report_type2 == 'Harvest detail':
                    _report_new_type2 = config.REPORT_HARVEST_DETAIL
                elif _report_type2 == 'Scouting report':
                    _report_new_type2 = config.REPORT_SCOUTING_REPORT
                elif _report_type2 == 'Others':
                    _report_new_type2 = config.REPORT_OTHERS

                _report_recommend2 = report_id2['report_id']["recomend_id"]
                _report_score2 = report_id2['report_id']["score"]

                _customer_id2 = report_id2['customer_id']['id']

                if _app_fert_recommend != None and _report_id2 != None and _report_recommend2 == None:

                    fertilize_recomend = fertilizer_app_agronomist_rec.add_new(
                        json.dumps({
                            "recomend": _app_fert_recommend,

                            "fertilize_id": _app_fert_id,
                            "report_id": _report_id2,
                            "customer_id": _customer_id2,
                            "creator_id": session['profile']['user']['id'],

                        }),

                    )
                    print(str(fertilize_recomend[0].get_json()))

                    if fertilize_recomend:
                        _recomend_id2 = fertilizer_app_agronomist_rec.fetch_by_report_single(
                            _report_real2).get_json()['id']
                        _recomend_real2 = fertilizer_app_agronomist_rec.fetch_one(
                            _recomend_id2, True).get_json()['real_id']
                        print("Recomend", _recomend_real2)
                        report_update2 = report.edit(
                            _report_id2,
                            json.dumps({
                                "type": _report_new_type2,
                                "score": _report_score2,
                                "recomend_id": _recomend_real2,
                                "creator_id": session['profile']['user']['id'],
                            }),

                        )
                        print(str(report_update2[0].get_json()))

        if _app_agro_recommend:

            print("App agro", _app_agro_id)
            print("app reccomended", _app_agro_recommend)

            report_id3 = agrochemical_application.fetch_one(
                _app_agro_id, True).get_json()

            if report_id3 != {}:
                _report_id3 = report_id3['report_id']['id']
                _customer_id3 = report_id3['customer_id']['id']

                _report_real3 = report.fetch_one(
                    _report_id3, True).get_json()['real_id']
                _report_type3 = report_id3['report_id']["type"]
                if _report_type3 == 'Waste pollution management':
                    _report_new_type3 = config.REPORT_WASTE_POLLUTION_MANAGEMENT
                elif _report_type3 == 'Fertilizer application':
                    _report_new_type3 = config.REPORT_FERTILIZER_APPLICATION
                elif _report_type3 == 'Aggrochemical application':
                    _report_new_type3 = config.REPORT_AGROCHEMICAL_APPLICATION
                elif _report_type3 == 'Pre-harvest checklist':
                    _report_new_type3 = config.REPORT_PREHARVEST_CHECKLIST
                elif _report_type3 == 'Harvest detail':
                    _report_new_type3 = config.REPORT_HARVEST_DETAIL
                elif _report_type3 == 'Scouting report':
                    _report_new_type3 = config.REPORT_SCOUTING_REPORT
                elif _report_type3 == 'Others':
                    _report_new_type3 = config.REPORT_OTHERS

                _report_recommend3 = report_id3['report_id']["recomend_id"]
                _report_score3 = report_id3['report_id']["score"]

                if _app_agro_recommend != None and _report_id3 != None and _report_recommend3 == None:

                    agrochemical_recomend = agrochem_app_agronomist_rec.add_new(
                        json.dumps({
                            "recomend": _app_agro_recommend,

                            "agronomy_id": _app_agro_id,
                            "report_id": _report_id3,
                            "customer_id": _customer_id3,
                            "creator_id": session['profile']['user']['id'],

                        }),

                    )
                    print(str(agrochemical_recomend[0].get_json()))

                    if agrochemical_recomend:
                        _recomend_id3 = agrochem_app_agronomist_rec.fetch_by_report_single(
                            _report_real3).get_json()['id']
                        _recomend_real3 = agrochem_app_agronomist_rec.fetch_one(
                            _recomend_id3, True).get_json()['real_id']

                        report_update3 = report.edit(
                            _report_id3,
                            json.dumps({
                                "type": _report_new_type3,
                                "score": _report_score3,
                                "recomend_id": _recomend_real3,
                                "creator_id": session['profile']['user']['id'],
                            }),

                        )
                        print(str(report_update3[0].get_json()))

        if _app_preharv_id and _app_preharv_recommend:

            report_id4 = preharvest_checklist.fetch_one(
                _app_preharv_id, True).get_json()

            if report_id4 != {}:
                _report_id4 = report_id4['report_id']['id']
                _customer_id4 = report_id4['customer_id']['id']

                _report_real4 = report.fetch_one(
                    _report_id4, True).get_json()['real_id']
                _report_type4 = report_id4['report_id']["type"]
                if _report_type4 == 'Waste pollution management':
                    _report_new_type4 = config.REPORT_WASTE_POLLUTION_MANAGEMENT
                elif _report_type4 == 'Fertilizer application':
                    _report_new_type4 = config.REPORT_FERTILIZER_APPLICATION
                elif _report_type4 == 'Aggrochemical application':
                    _report_new_type4 = config.REPORT_AGROCHEMICAL_APPLICATION
                elif _report_type4 == 'Pre-harvest checklist':
                    _report_new_type4 = config.REPORT_PREHARVEST_CHECKLIST
                elif _report_type4 == 'Harvest detail':
                    _report_new_type4 = config.REPORT_HARVEST_DETAIL
                elif _report_type4 == 'Scouting report':
                    _report_new_type4 = config.REPORT_SCOUTING_REPORT
                elif _report_type4 == 'Others':
                    _report_new_type4 = config.REPORT_OTHERS

                _report_recommend4 = report_id4['report_id']["recomend_id"]
                _report_score4 = report_id4['report_id']["score"]

                if _app_preharv_recommend != None and _report_id4 != None and _report_recommend4 == None:

                    preharv_recomend = preharv_agronomist_recom.add_new(
                        json.dumps({
                            "recomend": _app_preharv_recommend,

                            "preharvest_id": _app_preharv_id,
                            "report_id": _report_id4,
                            "customer_id": _customer_id4,
                            "creator_id": session['profile']['user']['id'],

                        }),

                    )
                    print(str(preharv_recomend[0].get_json()))

                    if preharv_recomend:
                        _recomend_id4 = preharv_agronomist_recom.fetch_by_report_single(
                            _report_real4).get_json()['id']
                        _recomend_real4 = preharv_agronomist_recom.fetch_one(
                            _recomend_id4, True).get_json()['real_id']

                        report_update4 = report.edit(
                            _report_id4,
                            json.dumps({
                                "type": _report_new_type4,
                                "score": _report_score4,
                                "recomend_id": _recomend_real4,
                                "creator_id": session['profile']['user']['id'],
                            }),

                        )
                        print(str(report_update4[0].get_json()))

        if _app_harv_recommend:

            print("Harvest ID", _app_harv_id)

            report_id5 = harvest.fetch_one(
                _app_harv_id, True).get_json()

            if report_id5 != {}:
                _report_id5 = report_id5['report_id']['id']
                _customer_id5 = report_id5['customer_id']['id']

                _report_real5 = report.fetch_one(
                    _report_id5, True).get_json()['real_id']
                _report_type5 = report_id5['report_id']["type"]
                if _report_type5 == 'Waste pollution management':
                    _report_new_type5 = config.REPORT_WASTE_POLLUTION_MANAGEMENT
                elif _report_type5 == 'Fertilizer application':
                    _report_new_type5 = config.REPORT_FERTILIZER_APPLICATION
                elif _report_type5 == 'Aggrochemical application':
                    _report_new_type5 = config.REPORT_AGROCHEMICAL_APPLICATION
                elif _report_type5 == 'Pre-harvest checklist':
                    _report_new_type5 = config.REPORT_PREHARVEST_CHECKLIST
                elif _report_type5 == 'Harvest detail':
                    _report_new_type5 = config.REPORT_HARVEST_DETAIL
                elif _report_type5 == 'Scouting report':
                    _report_new_type5 = config.REPORT_SCOUTING_REPORT
                elif _report_type5 == 'Others':
                    _report_new_type5 = config.REPORT_OTHERS

                _report_recommend5 = report_id5['report_id']["recomend_id"]
                _report_score5 = report_id5['report_id']["score"]

                if _app_harv_recommend != None and _report_id5 != None and _report_recommend5 == None:

                    harvest_recomend = harvest_agronomy_recomend.add_new(
                        json.dumps({
                            "recomend": _app_harv_recommend,

                            "harvest_id": _app_harv_id,
                            "report_id": _report_id5,
                            "customer_id": _customer_id5,
                            "creator_id": session['profile']['user']['id'],

                        }),

                    )
                    print(str(harvest_recomend[0].get_json()))

                    if harvest_recomend:
                        _recomend_id5 = harvest_agronomy_recomend.fetch_by_report_single(
                            _report_real5).get_json()['id']
                        _recomend_real5 = harvest_agronomy_recomend.fetch_one(
                            _recomend_id5, True).get_json()['real_id']

                        report_update5 = report.edit(
                            _report_id5,
                            json.dumps({
                                "type": _report_new_type5,
                                "score": _report_score5,
                                "recomend_id": _recomend_real5,
                                "creator_id": session['profile']['user']['id'],
                            }),

                        )
                        print(str(report_update5[0].get_json()))

# Others Add reccomendation
        if _app_others_recommend:

            print("Other ID", _app_others_id)

            report_id6 = other.fetch_one(_app_others_id, True).get_json()

            if report_id6 != {}:
                _report_id6 = report_id6['report_id']['id']
                _customer_id6 = report_id6['customer_id']['id']

                _report_real6 = report.fetch_one(
                    _report_id6, True).get_json()['real_id']
                _report_type6 = report_id6['report_id']["type"]
                if _report_type6 == 'Waste pollution management':
                    _report_new_type6 = config.REPORT_WASTE_POLLUTION_MANAGEMENT
                elif _report_type6 == 'Fertilizer application':
                    _report_new_type6 = config.REPORT_FERTILIZER_APPLICATION
                elif _report_type6 == 'Aggrochemical application':
                    _report_new_type6 = config.REPORT_AGROCHEMICAL_APPLICATION
                elif _report_type6 == 'Pre-harvest checklist':
                    _report_new_type6 = config.REPORT_PREHARVEST_CHECKLIST
                elif _report_type6 == 'Harvest detail':
                    _report_new_type6 = config.REPORT_HARVEST_DETAIL
                elif _report_type6 == 'Scouting report':
                    _report_new_type6 = config.REPORT_SCOUTING_REPORT
                elif _report_type6 == 'Others':
                    _report_new_type6 = config.REPORT_OTHERS

                _report_recommend6 = report_id6['report_id']["recomend_id"]
                _report_score6 = report_id6['report_id']["score"]

                if _app_others_recommend != None and _report_id6 != None and _report_recommend6 == None:

                    others_recomend = other_agronomist_recom.add_new(
                        json.dumps({
                            "recomend": _app_others_recommend,

                            "other_id": _app_others_id,
                            "report_id": _report_id6,
                            "customer_id": _customer_id6,
                            "creator_id": session['profile']['user']['id'],

                        }),

                    )
                    print(str(others_recomend[0].get_json()))

                    if others_recomend:
                        _recomend_id6 = other_agronomist_recom.fetch_by_report_single(
                            _report_real6).get_json()['id']
                        _recomend_real6 = other_agronomist_recom.fetch_one(
                            _recomend_id6, True).get_json()['real_id']

                        report_update6 = report.edit(
                            _report_id6,
                            json.dumps({
                                "type": _report_new_type6,
                                "score": _report_score6,
                                "recomend_id": _recomend_real6,
                                "creator_id": session['profile']['user']['id'],
                            }),

                        )
                        print(str(report_update6[0].get_json()))

        # ..........................................................................................................................................................

        if _assign_id != None and _score_id != None:

            report_id = waste_pollution_management.fetch_one(
                _assign_id, True).get_json()
            _report_id = report_id['report_id']['id']
            _report_real_id = report_id['real_id']
            _customer_r_id = report_id["customer_id"]['id']
            if _score_id == "3":
                _score_type = "Good"
            elif _score_id == "2":
                _score_type = "Fair"
            elif _score_id == "4":
                _score_type = "Bad"

        #  print("_report",_report_id)

            report_good = report.edit(
                _report_id,
                json.dumps({
                           "type": config.REPORT_WASTE_POLLUTION_MANAGEMENT,
                           "score": _score_id,
                           "creator_id": session['profile']['user']['id'],
                           "recomend_id": _recomend_id

                           }),
                True
            )
            print(str(report_good[0].get_json()))

            _customer_wnpscore_log = logs.add_new(
                json.dumps({
                    "action": "Waste and pollution score is assigned",
                    "ref": _score_type,
                    "ref_id": _report_real_id,
                    "customer_id": _customer_r_id,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )
            print(str(_customer_wnpscore_log[0].get_json()))

        # elif _assign_id != None and _score_id != None:

        #     report_id = waste_pollution_management.fetch_one(
        #         _assign_id, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     report_fair = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_WASTE_POLLUTION_MANAGEMENT,
        #             "score": _score_id,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(report_fair[0].get_json()))

        # elif _assign_id != None and _score_id_b != None:

        #     report_id = waste_pollution_management.fetch_one(
        #         _assign_id, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     report_bad = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_WASTE_POLLUTION_MANAGEMENT,
        #             "score":  _score_id_b,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(report_bad[0].get_json()))

        elif _assign_id_f != None and _score_id_fert != None:

            report_id = fertilizer_application.fetch_one(
                _assign_id_f, True).get_json()
            _report_id = report_id['report_id']['id']

            print("_report", _report_id)
            _report_f_real_id = report_id['real_id']
            _customer_fert_id = report_id["customer_id"]['id']

            if _score_id_fert == "3":
                _score_f_type = "Good"
            elif _score_id_fert == "2":
                _score_f_type = "Fair"
            elif _score_id_fert == "4":
                _score_f_type = "Bad"

            f_report_good = report.edit(
                _report_id,
                json.dumps({
                    "type": config.REPORT_FERTILIZER_APPLICATION,
                    "score":  _score_id_fert,
                    "creator_id": session['profile']['user']['id'],
                    "recomend_id": _recomend_id
                }),
                True
            )
            print(str(f_report_good[0].get_json()))

            _customer_fertscore_log = logs.add_new(
                json.dumps({
                    "action": "Fertilizer application score is assigned",
                    "ref": _score_f_type,
                    "ref_id": _report_f_real_id,
                    "customer_id": _customer_fert_id,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )
            print(str(_customer_fertscore_log[0].get_json()))

        # elif _assign_id_f != None and _score_id_f_fert != None:

        #     report_id = fertilizer_application.fetch_one(
        #         _assign_id_f, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     f_report_fair = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_FERTILIZER_APPLICATION,
        #             "score":  _score_id_f_fert,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(f_report_fair[0].get_json()))
        # elif _assign_id_f != None and _score_id_b_fert != None:

        #     report_id = fertilizer_application.fetch_one(
        #         _assign_id_f, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     f_report_fair = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_FERTILIZER_APPLICATION,
        #             "score":  _score_id_b_fert,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(f_report_fair[0].get_json()))

        elif _assign_id_ag != None and _score_id_agro != None:

            report_id = agrochemical_application.fetch_one(
                _assign_id_ag, True).get_json()
            _report_id = report_id['report_id']['id']

            _report_ag_real_id = report_id['real_id']

            _customer_agro_id = report_id["customer_id"]['id']

            print("_report", _report_id)

            if _score_id_agro == "3":
                _score_ag_type = "Good"
            elif _score_id_agro == "2":
                _score_ag_type = "Fair"
            elif _score_id_agro == "4":
                _score_ag_type = "Bad"

            ag_report_good = report.edit(
                _report_id,
                json.dumps({
                    "type": config.REPORT_AGROCHEMICAL_APPLICATION,
                    "score":  _score_id_agro,
                    "creator_id": session['profile']['user']['id'],
                    "recomend_id": _recomend_id
                }),
                True
            )
            print(str(ag_report_good[0].get_json()))

            _customer_agroscore_log = logs.add_new(
                json.dumps({
                    "action": "Agrochemical application score is assigned",
                    "ref": _score_ag_type,
                    "ref_id": _report_ag_real_id,
                    "customer_id": _customer_agro_id,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )
            print(str(_customer_agroscore_log[0].get_json()))
        # elif _assign_id_ag != None and _score_id_f_agro != None:

        #     report_id = agrochemical_application.fetch_one(
        #         _assign_id_ag, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     ag_report_fair = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_AGROCHEMICAL_APPLICATION,
        #             "score":  _score_id_f_agro,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(ag_report_fair[0].get_json()))
        # elif _assign_id_ag != None and _score_id_b_agro != None:

        #     report_id = agrochemical_application.fetch_one(
        #         _assign_id_ag, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     ag_report_bad = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_AGROCHEMICAL_APPLICATION,
        #             "score":  _score_id_b_agro,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(ag_report_bad[0].get_json()))
        elif _harvest_fetch_id != None and _score_harv_good != None:

            report_id = harvest.fetch_one(
                _harvest_fetch_id, True).get_json()
            _report_id = report_id['report_id']['id']

            _report_harv_real_id = report_id['real_id']

            _customer_harv_id = report_id["customer_id"]['id']

            if _score_harv_good == "3":
                _score_harv_type = "Good"
            elif _score_harv_good == "2":
                _score_harv_type = "Fair"
            elif _score_harv_good == "4":
                _score_harv_type = "Bad"

            harv_report_good = report.edit(
                _report_id,
                json.dumps({
                    "type": config.REPORT_HARVEST_DETAIL,
                    "score":  _score_harv_good,
                    "creator_id": session['profile']['user']['id'],
                    "recomend_id": _recomend_id
                }),
                True
            )
            print(str(harv_report_good[0].get_json()))

            _customer_harvscore_log = logs.add_new(
                json.dumps({
                    "action": "Harvest score is assigned",
                    "ref": _score_harv_type,
                    "ref_id": _report_harv_real_id,
                    "customer_id": _customer_harv_id,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )
            print(str(_customer_harvscore_log[0].get_json()))

        elif _assign_id_preharv != None and _score_id_preharv != None:

            report_id = preharvest_checklist.fetch_one(
                _assign_id_preharv, True).get_json()
            _report_id = report_id['report_id']['id']

            print("_report", _report_id)

            _report_preharv_real_id = report_id['real_id']

            _customer_preharv_id = report_id["customer_id"]['id']

            if _score_id_preharv == "3":
                _score_preharv_type = "Good"
            elif _score_id_preharv == "2":
                _score_preharv_type = "Fair"
            elif _score_id_preharv == "4":
                _score_preharv_type = "Bad"

            preharv_report_good = report.edit(
                _report_id,
                json.dumps({
                    "type": config.REPORT_PREHARVEST_CHECKLIST,
                    "score":  _score_id_preharv,
                    "creator_id": session['profile']['user']['id'],
                    "recomend_id": _recomend_id
                }),
                True
            )
            print(str(preharv_report_good[0].get_json()))

            _customer_preharvscore_log = logs.add_new(
                json.dumps({
                    "action": "Pre-harvest checklist score is assigned",
                    "ref": _score_preharv_type,
                    "ref_id": _report_preharv_real_id,
                    "customer_id": _customer_preharv_id,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )
            print(str(_customer_preharvscore_log[0].get_json()))
        # elif _assign_id_preharv != None and _score_id_f_preharv != None:

        #     report_id = preharvest_checklist.fetch_one(
        #         _assign_id_preharv, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     preharv_report_fair = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_PREHARVEST_CHECKLIST,
        #             "score":  _score_id_f_preharv,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(preharv_report_fair[0].get_json()))

        # elif _assign_id_preharv != None and _score_id_b_preharv != None:

        #     report_id = preharvest_checklist.fetch_one(
        #         _assign_id_preharv, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     preharv_report_bad = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_PREHARVEST_CHECKLIST,
        #             "score":  _score_id_b_preharv,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(preharv_report_bad[0].get_json()))

        elif _assign_id_scout != None and _score_id_scout != None:

            report_id = scouting.fetch_one(_assign_id_scout, True).get_json()
            _report_id = report_id['report_id']['id']
            _recomends_id = None

            print("_report", _report_id)

            _report_scouting_real_id = report_id['real_id']

            _customer_scouting_id = report_id["customer_id"]['id']

            if _score_id_scout == "3":
                _score_scouting_type = "Good"
            elif _score_id_scout == "2":
                _score_scouting_type = "Fair"
            elif _score_id_scout == "4":
                _score_scouting_type = "Bad"

            scout_report_good = report.edit(
                _report_id,
                json.dumps({
                    "type": config.REPORT_SCOUTING_REPORT,
                    "score":  _score_id_scout,
                    "creator_id": session['profile']['user']['id'],
                    "recomend_id": _recomends_id

                }),
                True
            )
            print(str(scout_report_good[0].get_json()))

            _customer_scoutingscore_log = logs.add_new(
                json.dumps({
                    "action": "Scouting report score is assigned",
                    "ref": _score_scouting_type,
                    "ref_id": _report_scouting_real_id,
                    "customer_id": _customer_scouting_id,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )
            print(str(_customer_scoutingscore_log[0].get_json()))

        # elif _assign_id_scout != None and _score_id_f_scout != None:

        #     report_id = scouting.fetch_one(_assign_id_scout, True).get_json()
        #     _report_id = report_id['report_id']['id']
        #     _recomends_id = None

        #     print("_report", _report_id)

        #     scout_report_fair = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_SCOUTING_REPORT,
        #             "score":  _score_id_f_scout,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomends_id
        #         }),
        #         True
        #     )
        #     print(str(scout_report_fair[0].get_json()))

        # elif _assign_id_scout != None and _score_id_b_scout != None:

        #     report_id = scouting.fetch_one(_assign_id_scout, True).get_json()
        #     _report_id = report_id['report_id']['id']
        #     # _recomends_id = report_id['report_id']['recomend_id']['id']
        #     _recomends_id = None

        #     print("_report", _report_id)

        #     scout_report_bad = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_SCOUTING_REPORT,
        #             "score":  _score_id_b_scout,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomends_id
        #         }),
        #         True
        #     )
        #     print(str(scout_report_bad[0].get_json()))

        elif _assign_id_other != None and _score_id_other != None:

            report_id = other.fetch_one(_assign_id_other, True).get_json()
            _report_id = report_id['report_id']['id']

            print("_report", _report_id)

            _report_other_real_id = report_id['real_id']

            _customer_other_id = report_id["customer_id"]['id']

            if _score_id_other == "3":
                _score_other_type = "Good"
            elif _score_id_other == "2":
                _score_other_type = "Fair"
            elif _score_id_other == "4":
                _score_other_type = "Bad"

            other_report_good = report.edit(
                _report_id,
                json.dumps({
                    "type": config.REPORT_OTHERS,
                    "score":  _score_id_other,
                    "creator_id": session['profile']['user']['id'],
                    "recomend_id": _recomend_id
                }),
                True
            )
            print(str(other_report_good[0].get_json()))

            _customer_othercore_log = logs.add_new(
                json.dumps({
                    "action": "Other report score is assigned",
                    "ref": _score_other_type,
                    "ref_id": _report_other_real_id,
                    "customer_id": _customer_other_id,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )
            print(str(_customer_othercore_log[0].get_json()))

        # elif _assign_id_other != None and _score_id_f_other != None:

        #     report_id = other.fetch_one(_assign_id_other, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     other_report_fair = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_OTHERS,
        #             "score":  _score_id_f_other,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(other_report_fair[0].get_json()))

        # elif _assign_id_other != None and _score_id_b_other != None:

        #     report_id = other.fetch_one(_assign_id_other, True).get_json()
        #     _report_id = report_id['report_id']['id']

        #     print("_report", _report_id)

        #     other_report_bad = report.edit(
        #         _report_id,
        #         json.dumps({
        #             "type": config.REPORT_OTHERS,
        #             "score":  _score_id_b_other,
        #             "creator_id": session['profile']['user']['id'],
        #             "recomend_id": _recomend_id
        #         }),
        #         True
        #     )
        #     print(str(other_report_bad[0].get_json()))

        else:
            action_response = {
                "type": "danger",
                "message": "Please check the Report assignment ."

            }

    _waste_pollution = waste_pollution_management.fetch_all().get_json()
    _fertilizer_app = fertilizer_application.fetch_all().get_json()
    _agrochem_app = agrochemical_application.fetch_all().get_json()
    _preharv_check = preharvest_checklist.fetch_all().get_json()
    _harvest_details = harvest.fetch_all().get_json()
    _scouting = scouting.fetch_all().get_json()
    _other_reports = other.fetch_all().get_json()
    _agronomist_recomendation = scouting_agronomist_recommendations.fetch_all().get_json()
    _wnp_recomendation = waste_n_p_agronomy_recommend.fetch_all().get_json()
    _fetilizer_recomend = fertilizer_app_agronomist_rec.fetch_all().get_json()
    _agrochem_recomend = agrochem_app_agronomist_rec.fetch_all().get_json()
    _preharv_recomend = preharv_agronomist_recom.fetch_all().get_json()
    _harvest_recomend = harvest_agronomy_recomend.fetch_all().get_json()

    _other_recomend = other_agronomist_recom.fetch_all().get_json()

    _application_methods = application_methods.fetch_all().get_json()

    #    if "action" in posted:

    #         if posted.get("action") == "add_agrorecomend" and "spray" in posted:

    #             report_id = scouting.fetch_one(_assign_id_scout,True).get_json()
    #             _report_id = report_id['report_id']['id']

    # column_names = ['id', 'residue','source','eliminated_amount','reduced_amount','recycled_amount']

    # return excel.make_response_from_query_sets(query_sets, column_names, "xls")
    # return excel.make_response_from_array([[1,2], [3, 4]], "csv")

    return render_template(
        'admin/reports/daily.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        WNP=_waste_pollution,
        FERTAPP=_fertilizer_app,
        AGROAPP=_agrochem_app,
        PREHARV=_preharv_check,
        HARVEST=_harvest_details,
        SCOUT=_scouting,
        RECOMEND=_agronomist_recomendation,
        WNPRECOMEND=_wnp_recomendation,
        FERTRECOMEND=_fetilizer_recomend,
        AGRORECOMEND=_agrochem_recomend,
        PREHARVRECOMEND=_preharv_recomend,
        HARVESTRECOMEND=_harvest_recomend,
        OTHERRECOMEND=_other_recomend,
        prev=prev, next=next,
        total=total,
        APPMETHOD=_application_methods,
        OTHERRE=_other_reports,
        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/archives', methods=['POST', 'GET'])
def report_archives():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    posted = request.form
    if posted:
        wnp_arch = posted.get('wnp_archieve')
        fert_arch = posted.get('fert_archieve')
        agro_arch = posted.get('agro_archieve')
        oth_arch = posted.get('other_archieve')
        preharv_arch = posted.get('preharv_archieve')
        scout_arch = posted.get('scout_archieve')
        harv_arch = posted.get('harv_archieve')
    #    print("Wnp Archieve id ",wnp_arch)
        _score_id = 1
        _recomend_id = None
        if wnp_arch:
            report_recover = report.edit(
                wnp_arch,
                json.dumps({
                           "type": config.REPORT_WASTE_POLLUTION_MANAGEMENT,
                           "score": _score_id,
                           "creator_id": session['profile']['user']['id'],
                           "recomend_id": _recomend_id

                           }),
                True
            )
            print(str(report_recover[0].get_json()))
        if fert_arch:

            freport_recover = report.edit(
                fert_arch,
                json.dumps({
                           "type": config.REPORT_FERTILIZER_APPLICATION,
                           "score": _score_id,
                           "creator_id": session['profile']['user']['id'],
                           "recomend_id": _recomend_id

                           }),
                True
            )
            print(str(freport_recover[0].get_json()))
        if agro_arch:

            agreport_recover = report.edit(
                agro_arch,
                json.dumps({
                           "type": config.REPORT_AGROCHEMICAL_APPLICATION,
                           "score": _score_id,
                           "creator_id": session['profile']['user']['id'],
                           "recomend_id": _recomend_id

                           }),
                True
            )
            print(str(agreport_recover[0].get_json()))
        if preharv_arch:

            phrvreport_recover = report.edit(
                preharv_arch,
                json.dumps({
                           "type": config.REPORT_PREHARVEST_CHECKLIST,
                           "score": _score_id,
                           "creator_id": session['profile']['user']['id'],
                           "recomend_id": _recomend_id

                           }),
                True
            )
            print(str(phrvreport_recover[0].get_json()))
        if scout_arch:

            scoutreport_recover = report.edit(
                scout_arch,
                json.dumps({
                           "type": config.REPORT_SCOUTING_REPORT,
                           "score": _score_id,
                           "creator_id": session['profile']['user']['id'],
                           "recomend_id": _recomend_id

                           }),
                True
            )
            print(str(scoutreport_recover[0].get_json()))

        if oth_arch:

            othreport_recover = report.edit(
                oth_arch,
                json.dumps({
                           "type": config.REPORT_OTHERS,
                           "score": _score_id,
                           "creator_id": session['profile']['user']['id'],
                           "recomend_id": _recomend_id

                           }),
                True
            )
            print(str(othreport_recover[0].get_json()))

    _waste_pollution = []
    _fertilizer_app = []
    _agrochem_app = []
    _preharv_check = []
    _harvest_details = []
    _scouting = []
    _other_reports = []
    _application_methods = []
    action_response = {}
    _agronomist_recomendation = []
    _wnp_recomendation = []
    _fetilizer_recomend = []
    _agrochem_recomend = []
    _preharv_recomend = []
    _harvest_recomend = []

    _waste_pollution = waste_pollution_management.fetch_all().get_json()
    _fertilizer_app = fertilizer_application.fetch_all().get_json()
    _agrochem_app = agrochemical_application.fetch_all().get_json()
    _preharv_check = preharvest_checklist.fetch_all().get_json()
    _harvest_details = harvest.fetch_all().get_json()
    _scouting = scouting.fetch_all().get_json()
    _other_reports = other.fetch_all().get_json()
    _agronomist_recomendation = scouting_agronomist_recommendations.fetch_all().get_json()
    _wnp_recomendation = waste_n_p_agronomy_recommend.fetch_all().get_json()
    _fetilizer_recomend = fertilizer_app_agronomist_rec.fetch_all().get_json()
    _agrochem_recomend = agrochem_app_agronomist_rec.fetch_all().get_json()
    _preharv_recomend = preharv_agronomist_recom.fetch_all().get_json()
    _harvest_recomend = harvest_agronomy_recomend.fetch_all().get_json()

    _application_methods = application_methods.fetch_all().get_json()

    return render_template(
        'admin/reports/archives.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        WNP=_waste_pollution,
        FERTAPP=_fertilizer_app,
        AGROAPP=_agrochem_app,
        PREHARV=_preharv_check,
        HARVEST=_harvest_details,
        SCOUT=_scouting,
        RECOMEND=_agronomist_recomendation,
        WNPRECOMEND=_wnp_recomendation,
        FERTRECOMEND=_fetilizer_recomend,
        AGRORECOMEND=_agrochem_recomend,
        PREHARVRECOMEND=_preharv_recomend,
        HARVESTRECOMEND=_harvest_recomend,
        OTHERRE=_other_reports,
        APPMETHOD=_application_methods,
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/delete/<string:id>', methods=['POST', 'GET'])
def delete_report(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    report_id = id
    if report_id != None:
        waste_pollution_management.delete(report_id)
    # elif report_id != None:
    #    fertilizer_application.delete(report_id)
    # elif report_id != None:
    #    agrochemical_application.delete(report_id)

    # elif report_id != None:
    #    preharvest_checklist.delete(report_id)
    # elif report_id != None:
    #    harvest.delete(report_id)

    # elif report_id != None:
    #    scouting.delete(report_id)
    # elif report_id != None:
    #    other.delete(report_id)

    _waste_pollution = []
    _fertilizer_app = []
    _agrochem_app = []
    _preharv_check = []
    _harvest_details = []
    _scouting = []
    _other_reports = []

    _waste_pollution = waste_pollution_management.fetch_all().get_json()
    _fertilizer_app = fertilizer_application.fetch_all().get_json()
    _agrochem_app = agrochemical_application.fetch_all().get_json()
    _preharv_check = preharvest_checklist.fetch_all().get_json()
    _harvest_details = harvest.fetch_all().get_json()
    _scouting = scouting.fetch_all().get_json()
    _other_reports = other.fetch_all().get_json()

    return render_template(
        'admin/reports/wnpdelete.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],

        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/delete-fertilizer/<string:id>', methods=['POST', 'GET'])
def delete_f_report(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    report_id = id
    # if report_id != None:
    #    waste_pollution_management.delete(report_id)
    # elif report_id != None:
    fertilizer_application.delete(report_id)
    # elif report_id != None:
    #    agrochemical_application.delete(report_id)

    # elif report_id != None:
    #    preharvest_checklist.delete(report_id)
    # elif report_id != None:
    #    harvest.delete(report_id)

    # elif report_id != None:
    #    scouting.delete(report_id)
    # elif report_id != None:
    #    other.delete(report_id)

    # _waste_pollution = []
    # _fertilizer_app = []
    # _agrochem_app = []
    # _preharv_check = []
    # _harvest_details = []
    # _scouting = []
    # _other_reports = []

    # _waste_pollution = waste_pollution_management.fetch_all().get_json()
    # _fertilizer_app = fertilizer_application.fetch_all().get_json()
    # _agrochem_app = agrochemical_application.fetch_all().get_json()
    # _preharv_check = preharvest_checklist.fetch_all().get_json()
    # _harvest_details = harvest.fetch_all().get_json()
    # _scouting = scouting.fetch_all().get_json()
    # _other_reports = other.fetch_all().get_json()

    return render_template(
        'admin/reports/fertilizer-report.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/delete-agro/<string:id>', methods=['POST', 'GET'])
def delete_ag_report(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    report_id = id
    # if report_id != None:
    #    waste_pollution_management.delete(report_id)
    # elif report_id != None:
    # fertilizer_application.delete(report_id)
    # elif report_id != None:
    agrochemical_application.delete(report_id)

    # elif report_id != None:
    #    preharvest_checklist.delete(report_id)
    # elif report_id != None:
    #    harvest.delete(report_id)

    # elif report_id != None:
    #    scouting.delete(report_id)
    # elif report_id != None:
    #    other.delete(report_id)

    # _waste_pollution = []
    # _fertilizer_app = []
    # _agrochem_app = []
    # _preharv_check = []
    # _harvest_details = []
    # _scouting = []
    # _other_reports = []

    # _waste_pollution = waste_pollution_management.fetch_all().get_json()
    # _fertilizer_app = fertilizer_application.fetch_all().get_json()
    # _agrochem_app = agrochemical_application.fetch_all().get_json()
    # _preharv_check = preharvest_checklist.fetch_all().get_json()
    # _harvest_details = harvest.fetch_all().get_json()
    # _scouting = scouting.fetch_all().get_json()
    # _other_reports = other.fetch_all().get_json()

    return render_template(
        'admin/reports/agrochem-report.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        # WNP=_waste_pollution,
        # FERTAPP=_fertilizer_app,
        # AGROAPP=_agrochem_app,
        # PREHARV=_preharv_check,
        # HARVEST=_harvest_details,
        # SCOUT=_scouting,
        # OTHERRE=_other_reports,
        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/delete-preharvest/<string:id>', methods=['POST', 'GET'])
def delete_ph_report(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    report_id = id
    # if report_id != None:
    #    waste_pollution_management.delete(report_id)
    # elif report_id != None:
    # fertilizer_application.delete(report_id)
    # elif report_id != None:
    # agrochemical_application.delete(report_id)

    # elif report_id != None:
    preharvest_checklist.delete(report_id)
    # elif report_id != None:
    #    harvest.delete(report_id)

    # elif report_id != None:
    #    scouting.delete(report_id)
    # elif report_id != None:
    #    other.delete(report_id)

    # _waste_pollution = []
    # _fertilizer_app = []
    # _agrochem_app = []
    # _preharv_check = []
    # _harvest_details = []
    # _scouting = []
    # _other_reports = []

    # _waste_pollution = waste_pollution_management.fetch_all().get_json()
    # _fertilizer_app = fertilizer_application.fetch_all().get_json()
    # _agrochem_app = agrochemical_application.fetch_all().get_json()
    # _preharv_check = preharvest_checklist.fetch_all().get_json()
    # _harvest_details = harvest.fetch_all().get_json()
    # _scouting = scouting.fetch_all().get_json()
    # _other_reports = other.fetch_all().get_json()

    return render_template(
        'admin/reports/preharv-report.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        # WNP=_waste_pollution,
        # FERTAPP=_fertilizer_app,
        # AGROAPP=_agrochem_app,
        # PREHARV=_preharv_check,
        # HARVEST=_harvest_details,
        # SCOUT=_scouting,
        # OTHERRE=_other_reports,
        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/delete-harvest/<string:id>', methods=['POST', 'GET'])
def delete_h_report(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    report_id = id
    # if report_id != None:
    #    waste_pollution_management.delete(report_id)
    # elif report_id != None:
    # fertilizer_application.delete(report_id)
    # elif report_id != None:
    # agrochemical_application.delete(report_id)

    # elif report_id != None:
    # preharvest_checklist.delete(report_id)
    # elif report_id != None:
    harvest.delete(report_id)

    # elif report_id != None:
    #    scouting.delete(report_id)
    # elif report_id != None:
    #    other.delete(report_id)

    # _waste_pollution = []
    # _fertilizer_app = []
    # _agrochem_app = []
    # _preharv_check = []
    # _harvest_details = []
    # _scouting = []
    # _other_reports = []

    # _waste_pollution = waste_pollution_management.fetch_all().get_json()
    # _fertilizer_app = fertilizer_application.fetch_all().get_json()
    # _agrochem_app = agrochemical_application.fetch_all().get_json()
    # _preharv_check = preharvest_checklist.fetch_all().get_json()
    # _harvest_details = harvest.fetch_all().get_json()
    # _scouting = scouting.fetch_all().get_json()
    # _other_reports = other.fetch_all().get_json()

    return render_template(
        'admin/reports/harvest-report.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        # WNP=_waste_pollution,
        # FERTAPP=_fertilizer_app,
        # AGROAPP=_agrochem_app,
        # PREHARV=_preharv_check,
        # HARVEST=_harvest_details,
        # SCOUT=_scouting,
        # OTHERRE=_other_reports,
        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/delete-scouting/<string:id>', methods=['POST', 'GET'])
def delete_s_report(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    report_id = id
    # if report_id != None:
    #    waste_pollution_management.delete(report_id)
    # elif report_id != None:
    # fertilizer_application.delete(report_id)
    # elif report_id != None:
    # agrochemical_application.delete(report_id)

    # elif report_id != None:
    # preharvest_checklist.delete(report_id)
    # elif report_id != None:
    #    harvest.delete(report_id)

    # elif report_id != None:
    scouting.delete(report_id)
    # elif report_id != None:
    #    other.delete(report_id)

    # _waste_pollution = []
    # _fertilizer_app = []
    # _agrochem_app = []
    # _preharv_check = []
    # _harvest_details = []
    # _scouting = []
    # _other_reports = []

    # _waste_pollution = waste_pollution_management.fetch_all().get_json()
    # _fertilizer_app = fertilizer_application.fetch_all().get_json()
    # _agrochem_app = agrochemical_application.fetch_all().get_json()
    # _preharv_check = preharvest_checklist.fetch_all().get_json()
    # _harvest_details = harvest.fetch_all().get_json()
    # _scouting = scouting.fetch_all().get_json()
    # _other_reports = other.fetch_all().get_json()

    return render_template(
        'admin/reports/scouting-report.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        # WNP=_waste_pollution,
        # FERTAPP=_fertilizer_app,
        # AGROAPP=_agrochem_app,
        # PREHARV=_preharv_check,
        # HARVEST=_harvest_details,
        # SCOUT=_scouting,
        # OTHERRE=_other_reports,
        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/delete-other/<string:id>', methods=['POST', 'GET'])
def delete_o_report(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    report_id = id
    # if report_id != None:
    #    waste_pollution_management.delete(report_id)
    # elif report_id != None:
    # fertilizer_application.delete(report_id)
    # elif report_id != None:
    # agrochemical_application.delete(report_id)

    # elif report_id != None:
    # preharvest_checklist.delete(report_id)
    # elif report_id != None:
    #    harvest.delete(report_id)

    # elif report_id != None:
    # scouting.delete(report_id)
    # elif report_id != None:
    other.delete(report_id)

    # _waste_pollution = []
    # _fertilizer_app = []
    # _agrochem_app = []
    # _preharv_check = []
    # _harvest_details = []
    # _scouting = []
    # _other_reports = []

    # _waste_pollution = waste_pollution_management.fetch_all().get_json()
    # _fertilizer_app = fertilizer_application.fetch_all().get_json()
    # _agrochem_app = agrochemical_application.fetch_all().get_json()
    # _preharv_check = preharvest_checklist.fetch_all().get_json()
    # _harvest_details = harvest.fetch_all().get_json()
    # _scouting = scouting.fetch_all().get_json()
    # _other_reports = other.fetch_all().get_json()

    return render_template(
        'admin/reports/other-report.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        # WNP=_waste_pollution,
        # FERTAPP=_fertilizer_app,
        # AGROAPP=_agrochem_app,
        # PREHARV=_preharv_check,
        # HARVEST=_harvest_details,
        # SCOUT=_scouting,
        # OTHERRE=_other_reports,
        data={"date": str(datetime.now().year)}
    )

@igh_reports.route('/traceability', methods=['POST', 'GET'], defaults={"page": 1})
@igh_reports.route('/traceability/<int:page>', methods=['POST', 'GET'])
def traceability_reports(page):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    
    page = page
    prev = page-1
    next = page+1

    no_of_posts = config.PAGE_LIMIT

    _traceability = []
    _traceability = traceability.fetch_all().get_json()
    _traceability_fertilizer = traceability_fertilizer_queries.fetch_all().get_json()
    _traceability_agro = traceability_agrochemical_queries.fetch_all().get_json()
    _traceability_plants = traceability_plant_queries.fetch_all().get_json()
    _traceability_harvest = traceability_harvest_queries.fetch_all().get_json()

    return render_template(
        'admin/reports/traceability.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        TRACE=_traceability,
        TRACEFERT=_traceability_fertilizer,
        TRACEAGRO=_traceability_agro,
        TRACEHARV=_traceability_harvest,
        TRACEPLANT=_traceability_plants,
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_reports.route('/export', methods=['POST', 'GET'])
def export_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    html = render_template(
        "admin/reports/export.html",
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)})

    pdf = pdfkit.from_string(html, False)
    response = make_response(pdf)
    response.headers["Content-Type"] = "application/pdf"
    response.headers["Content-Disposition"] = "inline; filename=output.pdf"
    return response

    # with open('admin/reports/export.html') as f:
    #     pdfkit.from_file(f, 'out.pdf')

    # return render_template(
    #     'admin/reports/export.html',
    #     ADMIN_PORTAL=True,
    #     ADMIN_PORTAL_REPORTS_ACTIVE='active',
    #     PROFILE=session['profile'],
    #     data={"date": str(datetime.now().year)}
    # )


@igh_reports.route('/create', methods=['POST', 'GET'])
def create_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _fertilizer_app = {}
    _agrochem_app = {}
    _fertilizer_all_range = []
    _agrochem_appz = []
    _harvest_details = []
    _crop_details = []
    get_trace_data = {}
    customer_list = []
    farm_list = []
    shield_list = []
    plant_list = []
    customer_list = customer.fetch_all().get_json()
    farm_list = farms.fetch_all().get_json()
    shield_list = shields.fetch_all().get_json()
    plant_list = plants.fetch_all().get_json()

    posted = request.form
    if posted:
        custr = posted.get('customer_id')
        farmr = posted.get('farm_id')
        shieldr = posted.get('shield_id')
        plantr = posted.get('plaint_id')
        plantingdate = posted.get('planting_date')

        crop_variety = posted.get('veriety_crop')
        fert_name = posted.get('fertilizer_name')
        fert_comp = posted.get('fertilizer_comp')
        app_date = posted.get('application_date')
        ag_name = posted.get('agro_name')
        ag_comp = posted.get('agro_comp')
        ag_app_date = posted.get('agro_app_date')
        harvst_date = posted.get('harv_date')
        harvst_time = posted.get('harv_time')
        irrigte = posted.get('irrigation')

        dt_from = posted.get('date_from')
        dt_to = posted.get('date_to')
        # print("Planting Date",plantingdate,crop_variety)

        if custr != '' and farmr != '' and shieldr != '' and plantr != '':

            customer_real = customer.fetch_one(
                custr, True).get_json()['real_id']

            print("Customer data", customer_real)

            _new_h_report = Report(

                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                type=config.REPORT_HARVEST_DETAIL,
                score=1,
                customer_id=customer_real,
                creator_id=session['profile']['user']['real_id']

            )
            db.session.add(_new_h_report)
            db.session.commit()
            print("Report Id", _new_h_report.id)

            traceability_maintain_id = TraceabilityReportRequest(
                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                notes="Export a report",
                maintenance_id=None,
                customer_id=customer_real,
                creator_id=session['profile']['user']['real_id']
            )
            db.session.add(traceability_maintain_id)
            db.session.commit()
            print("Tracebitlity Id", traceability_maintain_id.id)

            # traceability_report_requests.add_new

            trace_report_id = traceability_report_requests.fetch_by_id(
                traceability_maintain_id.id).get_json()['id']

            # create_repo = report.add_new(
            #         json.dumps({
            #             "type": 8,
            #             "score":1,
            #             "recomend_id":1,
            #             "customer_id": custr,
            #             "creator_id": session['profile']['user']['id'],
            #         }),

            #     )
            # print(str(create_repo[0].get_json()))
            # customer_real = customer.fetch_one(custr,True).get_json()['real_id']
            _report_id = report.fetch_by_id(_new_h_report.id).get_json()['id']
            _report_real = report.fetch_one(
                _report_id, True).get_json()['real_id']
            _traceability = traceability.add_new(
                json.dumps({
                    "traceability_report_request_id": trace_report_id,
                    "report_id": _report_id,
                    "plant_id": plantr,
                    "customer_shield_id": shieldr,
                    "customer_farm_id": farmr,
                    "customer_id": custr,

                    "creator_id": session['profile']['user']['id'],
                }),


            )
            print(str(_traceability[0].get_json()))
            if _traceability:
                get_trace_data = traceability.fetch_by_report_id(
                    _report_real).get_json()
                get_trace_report_id = get_trace_data['id']
            if fert_name == '1' and fert_comp == '1' and custr != '' and app_date == '1' and dt_from != '' and dt_to != '':
                customer_real = customer.fetch_one(
                    custr, True).get_json()['real_id']

                # _fertilizer_app = fertilizer_application.fetch_by_customer_recent(customer_real).get_json()
                # print("Fertilizer Details", _fertilizer_app)
                date_to = datetime.strptime(dt_to, '%Y-%m-%d')
                date_end = date_to + timedelta(days=1)
                _fertilizer_all_range = fertilizer_application.fetch_by_customer_date_range(
                    customer_real, dt_from, date_end).get_json()
                if _fertilizer_all_range != []:

                    for _fert_data in _fertilizer_all_range:

                        if _fert_data != {}:
                            if _fert_data['fertilizer'] != {}:

                                _name = _fert_data['fertilizer']['name']
                                _component = _fert_data['fertilizer']['active_component']['component']

                            else:
                                _name = None
                                _component = None

                            _application_date = _fert_data['date_time_applied']

                            trace_data_fert = traceability_fertilizer_queries.add_new(
                                json.dumps({
                                           "name": _name,
                                           "component": _component,
                                           "application_date": _application_date,
                                           "traceability_report_id": get_trace_report_id,
                                           "creator_id": session['profile']['user']['id'],
                                           }),


                            )
                            print(str(trace_data_fert[0].get_json()))

            if ag_name == '1' and ag_comp == '1' and ag_app_date == '1' and custr != '':

                # print(" Agro details",ag_name,ag_comp,ag_app_date)

                # _agrochem_app = agrochemical_application.fetch_by_customer_recent(customer_real).get_json()
                date_to = datetime.strptime(dt_to, '%Y-%m-%d')
                date_end = date_to + timedelta(days=1)
                _agrochem_appz = agrochemical_application.fetch_by_customer_date_range(
                    customer_real, dt_from, date_end).get_json()

                if _agrochem_appz != []:

                    for _agro_data in _agrochem_appz:
                        if _agro_data != {}:
                            if _agro_data['agrochemical'] != {}:

                                _name = _agro_data['agrochemical']['name']
                                _component = _agro_data['agrochemical']['active_component']['component']

                            else:
                                _name = None
                                _component = None

                            _application_date = _agro_data['date_time_applied']

                            trace_data_agro = traceability_agrochemical_queries.add_new(
                                json.dumps({
                                    "name": _name,
                                    "component": _component,
                                    "application_date": _application_date,
                                    "traceability_report_id": get_trace_report_id,
                                    "creator_id": session['profile']['user']['id'],
                                }),


                            )
                            print(str(trace_data_agro[0].get_json()))
            if harvst_date == '1' and harvst_time != '' and custr != '':

                date_to = datetime.strptime(dt_to, '%Y-%m-%d')
                date_end = date_to + timedelta(days=1)

                _harvest_details = harvest.fetch_by_customer_date_range(
                    customer_real, dt_from, date_end).get_json()

                if _harvest_details != []:

                    for _harvest_data in _harvest_details:
                        if _harvest_data != {}:

                            _harvest_date = _harvest_data['harvest_date_time']
                            _harvest_time = _harvest_data['kilos_harvested']

                            trace_data_harvest = traceability_harvest_queries.add_new(
                                json.dumps({
                                    "harvest_date": _harvest_date,
                                    "harvest_time": _harvest_time,
                                    "traceability_report_id": get_trace_report_id,
                                    "creator_id": session['profile']['user']['id'],
                                }),


                            )
                            print(str(trace_data_harvest[0].get_json()))
            if plantingdate == '1' and custr != '':
                creator_uid = customer_users.fetch_by_customer(
                    customer_real).get_json()['user']['id']
                creator_real = user.fetch_one(
                    creator_uid, True).get_json()['real_id']
                date_to = datetime.strptime(dt_to, '%Y-%m-%d')
                date_end = date_to + timedelta(days=1)
                _crop_details = crops.fetch_by_customer_date_range(
                    creator_real, dt_from, date_end).get_json()

                if _crop_details != []:

                    for _crop_data in _crop_details:
                        if _crop_data != {}:

                            _planting_date = _crop_data['start_date']
                            _crop_variety = _crop_data['crop']['name']

                            trace_data_plant = traceability_plant_queries.add_new(
                                json.dumps({
                                    "planting_date": _planting_date,
                                    "crop_variety": _crop_variety,
                                    "traceability_report_id": get_trace_report_id,
                                    "creator_id": session['profile']['user']['id'],
                                }),


                            )
                            print(str(trace_data_plant[0].get_json()))

            html = render_template(
                'admin/reports/export.html',
                ADMIN_PORTAL=True,
                ADMIN_PORTAL_REPORTS_ACTIVE='active',
                PROFILE=session['profile'],
                FERT=_fertilizer_all_range,
                AGRO=_agrochem_appz,
                HARV=_harvest_details,
                TRACE=get_trace_data,
                CROP=_crop_details,
                data={"date": str(datetime.now().year),
                      "utc_date": datetime.utcnow()}
            )

            options = {
                "enable-local-file-access": True,

            }
            WKHTMLTOPDF_PATH = '/usr/bin/wkhtmltopdf'
            css = ['app/igh/static/css/skins/skin-igh.css',
                   'app/igh/static/css/AdminLTE.css', 'app/igh/static/bootstrap/css/bootstrap.css']
            return html
            # config2 = pdfkit.configuration(wkhtmltopdf=WKHTMLTOPDF_PATH)
            # config1 = pdfkit.configuration(wkhtmltopdf=bytes('/usr/bin/wkhtmltopdf', 'utf-8'))
            # pdfkit.from_string(html,'out3.pdf',options=options, verbose=True,css=css,configuration=config2)
            # pdfkit.from_string(html, 'out1.pdf', verbose=True)

            # pdf = pdfkit.from_string(html,options=options, verbose=True,css=css)
            # response = Response(pdf)
            # response.headers["Content-Type"] = "application/pdf"
            # response.headers["Content-Disposition"] = "inline; filename=output.pdf"
            # return response

            # return render_template(
            #     'admin/reports/export.html',
            #     ADMIN_PORTAL=True,
            #     ADMIN_PORTAL_REPORTS_ACTIVE='active',
            #     PROFILE=session['profile'],
            #     FERT=_fertilizer_all_range,
            #     AGRO=_agrochem_appz,
            #     HARV=_harvest_details,
            #     TRACE=get_trace_data,
            #     data={"date": str(datetime.now().year),
            #           "utc_date": datetime.utcnow()}
            # )

    return render_template(
        'admin/reports/create.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        CUSTOMER=customer_list,
        FARMS=farm_list,
        SHIELDS=shield_list,
        PLANTS=plant_list,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_reports.route('/export-wnp/', methods=['POST', 'GET'])
def wnpexport():

    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template('index.html')


@igh_reports.route('/download/report/excel')
def download_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _waste_pollution = waste_pollution_management.fetch_all().get_json()

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Waste and pollution Management')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'residue')
    sh.write(0, 2, 'source')
    sh.write(0, 3, 'eliminated_amount')
    sh.write(0, 4, 'reduced_amount')
    sh.write(0, 5, 'recycled_amount')

    idx = 0
    for row in _waste_pollution:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['residue'])
        sh.write(idx+1, 2, row['source'])
        sh.write(idx+1, 3, row['eliminated_amount'])
        sh.write(idx+1, 4, row['reduced_amount'])
        sh.write(idx+1, 5, row['recycled_amount'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=wnp_report.xls"})


@igh_reports.route('/download/fertilizerreport/excel')
def download_fertilizer_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _fertilizer_application = fertilizer_application.fetch_all().get_json()
    # print("Fert APP",_fertilizer_application)

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Fertilizer Application')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'date_time_applied')
    sh.write(0, 2, 'quantity_applied')
    sh.write(0, 3, 'sources')
    sh.write(0, 4, 'supply_application_method')
    sh.write(0, 5, 'fertilizer')
    sh.write(0, 6, 'fertilizer_main_component')
    sh.write(0, 7, 'customer_shield_id')
    sh.write(0, 8, 'report_id')
    sh.write(0, 9, 'customer_id')

    idx = 0
    for row in _fertilizer_application:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['date_time_applied'])
        sh.write(idx+1, 2, row['quantity_applied'])
        sh.write(idx+1, 3, row['sources'])
        if row['supply_application_method'] != {}:
            sh.write(idx+1, 4, row['supply_application_method']['name'])
        else:
            sh.write(idx+1, 4, None)
        if row['fertilizer'] != {}:
            sh.write(idx+1, 5, row['fertilizer']['name'])
        else:
            sh.write(idx+1, 5, None)
        if row['fertilizer_main_component'] != {}:
            sh.write(idx+1, 6, row['fertilizer_main_component']['component'])
        else:
            sh.write(idx+1, 6, None)
        if row['customer_shield_id'] != {}:
            sh.write(idx+1, 7, row['customer_shield_id']
                     ['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 7, None)
        sh.write(idx+1, 8, row['report_id']['main_id'])
        sh.write(idx+1, 9, row['customer_id']['main_id'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=fertilizer_report.xls"})


@igh_reports.route('/download/agrochemicalreport/excel')
def download_agrochemical_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _agrochem_app = agrochemical_application.fetch_all().get_json()

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Agrochemical Application')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'date_time_applied')
    sh.write(0, 2, 'rei')
    sh.write(0, 3, 'phi')
    sh.write(0, 4, 'sources1')
    sh.write(0, 5, 're_entry_date')
    sh.write(0, 6, 'preharvest_interval_expiration_date')
    sh.write(0, 7, 'pest')
    sh.write(0, 8, 'supply_application_method')
    sh.write(0, 9, 'agrochemical')
    sh.write(0, 10, 'report_id')
    sh.write(0, 11, 'customer_id')

    idx = 0
    for row in _agrochem_app:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['date_time_applied'])
        sh.write(idx+1, 2, row['rei'])
        sh.write(idx+1, 3, row['phi'])
        sh.write(idx+1, 4, row['sources1'])
        sh.write(idx+1, 5, row['re_entry_date'])
        sh.write(idx+1, 6, row['preharvest_interval_expiration_date'])
        if row['pest'] != {}:
            sh.write(idx+1, 7, row['pest']["name"])
        else:
            sh.write(idx+1, 7, None)
        if row['supply_application_method'] != {}:
            sh.write(idx+1, 8, row['supply_application_method']['name'])
        else:
            sh.write(idx+1, 8, None)
        if row['agrochemical'] != {}:
            sh.write(idx+1, 9, row['agrochemical']['name'])
        else:
            sh.write(idx+1, 9, None)
        sh.write(idx+1, 10, row['report_id']['main_id'])
        sh.write(idx+1, 11, row['customer_id']['main_id'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=agrochemical_report.xls"})


@igh_reports.route('/download/preharvestreport/excel')
def download_preharvest_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _preharv_check = preharvest_checklist.fetch_all().get_json()

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Preharvest Report')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'animal intrusion')
    sh.write(0, 2, 'crew washed hands')
    sh.write(0, 3, 'sources')
    sh.write(0, 4, 'crew clothing is clean')
    sh.write(0, 5, 'crew without jewellery')
    sh.write(0, 6, 'packing material clean')
    sh.write(0, 7, 'customer shield')
    sh.write(0, 8, 'report_id')
    sh.write(0, 9, 'customer_id')

    idx = 0
    for row in _preharv_check:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['animal_intrusion'])
        sh.write(idx+1, 2, row['crew_washed_hands'])
        sh.write(idx+1, 3, row['sources2'])
        sh.write(idx+1, 4, row['crew_clothing_is_clean'])
        sh.write(idx+1, 5, row['crew_without_jewellery'])
        sh.write(idx+1, 6, row['packing_material_clean'])
        if row['customer_shield_id'] != {}:
            sh.write(idx+1, 7, row['customer_shield_id']
                     ['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 7, None)
        sh.write(idx+1, 8, row['report_id']['main_id'])
        sh.write(idx+1, 9, row['customer_id']['main_id'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=preharvest_report.xls"})


@igh_reports.route('/download/harvest/excel')
def download_harvest_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _harvest_details = harvest.fetch_all().get_json()

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Harvest Report')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'harvest date')
    sh.write(0, 2, 'Harvested in kilos')
    sh.write(0, 3, 'Rejected in kilos')
    sh.write(0, 4, 'Kilos lost in transit')
    sh.write(0, 5, 'kilos sold')
    sh.write(0, 6, 'kilo price')
    sh.write(0, 7, 'Total income')
    sh.write(0, 8, 'variety')
    sh.write(0, 9, 'customer shield')
    sh.write(0, 10, 'report id')
    sh.write(0, 11, 'customer id')

    idx = 0
    for row in _harvest_details:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['harvest_date_time'])
        sh.write(idx+1, 2, row['kilos_harvested'])
        sh.write(idx+1, 3, row['kilos_rejected'])
        sh.write(idx+1, 4, row['kilos_lost_in_transit'])
        sh.write(idx+1, 5, row['kilos_sold'])
        sh.write(idx+1, 6, row['kilo_price'])
        sh.write(idx+1, 7, row['total_income'])
        if row['variety'] != {}:
            sh.write(idx+1, 8, row['variety']['name'])
        else:
            sh.write(idx+1, 8, None)
        if row['customer_shield_id'] != {}:
            sh.write(idx+1, 9, row['customer_shield_id']
                     ['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 9, None)
        sh.write(idx+1, 10, row['report_id']['main_id'])
        sh.write(idx+1, 11, row['customer_id']['main_id'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=harvest_report.xls"})


@igh_reports.route('/download/scoutreport/excel')
def download_scout_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _scouting = scouting.fetch_all().get_json()

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Preharvest Report')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'leaf miners')
    sh.write(0, 2, 'ball worms')
    sh.write(0, 3, 'thrips')
    sh.write(0, 4, 'caterpillars')
    sh.write(0, 5, 'mites')
    sh.write(0, 6, 'scales')
    sh.write(0, 7, 'bean_flies')
    sh.write(0, 8, 'white_flies')
    sh.write(0, 9, 'cut_worms')
    sh.write(0, 10, 'other_insects')
    sh.write(0, 11, 'aschochtya')
    sh.write(0, 12, 'botrytis')
    sh.write(0, 13, 'downy')
    sh.write(0, 14, 'powdery')
    sh.write(0, 15, 'leaf_spots')
    sh.write(0, 16, 'halo_blight')
    sh.write(0, 17, 'other_diseases')
    sh.write(0, 18, 'weeds')
    sh.write(0, 19, 'customer_shield_id')
    sh.write(0, 20, 'report_id')
    sh.write(0, 21, 'customer_id')

    idx = 0
    for row in _scouting:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row["leaf_miners"])
        sh.write(idx+1, 2, row["ball_worms"])
        sh.write(idx+1, 3, row["thrips"])
        sh.write(idx+1, 4, row["caterpillars"])
        sh.write(idx+1, 5, row["mites"])
        sh.write(idx+1, 6, row["scales"])
        sh.write(idx+1, 7, row["bean_flies"])
        sh.write(idx+1, 8, row["white_flies"])
        sh.write(idx+1, 9, row["cut_worms"])
        sh.write(idx+1, 10, row["other_insects"])
        sh.write(idx+1, 11, row["aschochtya"])
        sh.write(idx+1, 12, row["botrytis"])
        sh.write(idx+1, 13, row["downy"])
        sh.write(idx+1, 14, row["powdery"])
        sh.write(idx+1, 15, row["leaf_spots"])
        sh.write(idx+1, 16, row["halo_blight"])
        sh.write(idx+1, 17, row["other_diseases"])
        sh.write(idx+1, 18, row["weeds"])
        if row['customer_shield_id'] != {}:
            sh.write(idx+1, 19, row['customer_shield_id']
                     ['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 19, None)
        sh.write(idx+1, 20, row['report_id']['main_id'])
        sh.write(idx+1, 21, row['customer_id']['main_id'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=scout_report.xls"})


@igh_reports.route('/download/otherreport/excel')
def download_other_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _other_reports = other.fetch_all().get_json()

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Preharvest Report')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'notes')
    # sh.write(0, 2, 'farm action')
    sh.write(0, 3, 'customer shield')
    sh.write(0, 4, 'report_id')
    sh.write(0, 5, 'customer_id')

    idx = 0
    for row in _other_reports:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['notes'])
        # if row['farm_action'] != {}:
        #   sh.write(idx+1, 2, row['farm_action'])
        if row['customer_shield_id'] != {}:
            sh.write(idx+1, 3, row['customer_shield_id']
                     ['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 3, None)
        sh.write(idx+1, 4, row['report_id']['main_id'])
        sh.write(idx+1, 5, row['customer_id']['main_id'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=other_report.xls"})
