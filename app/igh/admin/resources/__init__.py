from functools import wraps

import json
import os
import re
import urllib.parse
import requests
import importlib
import uuid
import math
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from werkzeug.exceptions import HTTPException


from dotenv import load_dotenv
from flask import Flask,Response
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime
from app.config import activateConfig
from app.core.greenhouse import types
from app.core import greenhouse as greenh
from app.core import screenhouse as screenh
from app.core import shadenet as shadenets
from app.core import shield as shields
from app.core.customer import shields as cshield
from app.core.customer import screenhouses as cscreen
from app.core.customer import shadenets as snet
from app.models import db
from app.core.shield import types as shield_types
from app.igh import portal_check
from app.igh.admin import contact_person
from app.core import spear
from app.core.shield import spears
from app.models import Greenhouse
from app.models import Shield
from app.models import Screenhouse
from app.models import Shadenet
from app.igh import session_setup
from werkzeug.wrappers import response

from app.helper import valid_uuid

igh_resources = Blueprint('igh_resources', __name__,static_folder='../../static', template_folder='.../../templates')
igh_resources.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


@igh_resources.route('/', methods=['POST', 'GET'])
def main_resources():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
       return realm

    
   

    return render_template(
        'resources/main.html',
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_resources.route('/add', methods=['POST', 'GET'])
def add_resources():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
       return realm


    


    

    
   

    return render_template(
        'resources/add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
     )



