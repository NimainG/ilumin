from functools import wraps

import json
import os
import urllib.parse
from numpy import integer
import requests
import importlib
import uuid
import math
import io
import xlwt

from werkzeug.exceptions import HTTPException
from flask import Response
from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime
from app.core import customer as customer_user
from app.core.user import customers
from app.core.customer import subscription as customer_sub
from app.core import greenhouse as greenh
from app.core import screenhouse as screenh
from app.core import shadenet as shadenets
from app.core.customer import shields
from app.core.user import auth as user_auth
from app.config import activateConfig
from app.core.customer import greenhouses as green
from app.core import shield as device
from app.core.user import logs
from app.core.customer import farms as custfarm
from app.igh import portal_check
from app.igh.admin import contact_person
from app.models import BaseModel, db
from app.models import Farm
from app.igh import session_setup
from app.core.customer import farm_greenhouses
from app.core import invoice

from app.helper import valid_uuid

from app.helper import validateEmailAddress

from app.core.user import igh

from app.core import user

from flask_wtf import Form
from werkzeug.wrappers import response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from app.core import customer
from wtforms.fields import DateField
from app.core import farm
from app.models import CustomerGreenhouse
from app.core.customer import screenhouses
from app.core.customer import shadenets as sh
from app.core import subscription
from app.igh.customer.inventory import product_inventory
from app.core import alert
from app.core.customer import farms as ownfarms
from app.core.customer import greenhouses
from app.models import Customer
from app.core.customer import greenhouse_shields
from app.models import CustomerShield
from app.core.report import harvest
from app.core import supply
from app.core import maintenance
from app.models import User
igh_customers = Blueprint('igh_customers', __name__,
                          static_folder='../../static', template_folder='.../../templates')
igh_customers.config = {}


load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


# class MyForm(Form):
#     date = DateField(id='datepick')

_customer = []


@igh_customers.route('/', methods=['POST', 'GET'], defaults={"page": 1})
@igh_customers.route('/<int:page>', methods=['POST', 'GET'])
def main(page):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    # page = request.args.get('page', 1, type=int)

    page = page
    prev = page-1
    next = page+1

    no_of_posts = config.PAGE_LIMIT

    _customers = customer_user.fetch_all().get_json()
    _customer1 = customer_user.farms.fetch_all().get_json()
    # print("Customer details",_customer)

    total = {}
    total = round(len(_customers)/no_of_posts)
    _customer_green_h = []
    _customer_devices = []
    _customer_screenhouse = []
    _customer_shadnet = []
    _billing = {}
    _subscription = []
    _prod_harvest = []
    _customer_inventory_agro = []
    _maintain = []
    _maintain = maintenance.fetch_all().get_json()
    _customer_green_h = greenhouses.fetch_all().get_json()
    # _customer_user = customers.fetch_all().get_json()
    _subscription = subscription.fetch_all().get_json()
    _allfarm = custfarm.fetch_all().get_json()
    _customer_devices = shields.fetch_all().get_json()
    _customer_screenhouse = screenhouses.fetch_all().get_json()
    _customer_shadnet = sh.fetch_all().get_json()
    _prod_harvest = harvest.fetch_all().get_json()
    _customer_inventory_agro = supply.fetch_all().get_json()
    # print("Supplyyyyyyy", _customer_inventory_agro)
    _customer_farm = []

    # print("Customer farm",_customer_farm)

    # _customer_id = session["customer"]["user"]["id"]
    # _customer_filter = Customer.query.paginate(per_page =10,page = page_num,error_out=True)
    # last = math.ceil(len(_customer_filter)/int(params['no_of_posts']))
    # print("_allfarm",_customer)
    # page = request.args.get('page')

    # if (not str(page).isnumeric()):
    #  page = 0

    # posts = _customer_filter[(page-1)*int(params['no_of_posts']):(page-1)*int(params['no_of_posts'])+ int(params['no_of_posts']))]
    # if page==1:
    #   prev = "#"
    #   next = "/?page="+ str(page+1)
    # elif page==last:
    #   prev = "/?page="+ str(page-1)
    #   next = "#"
    # else:
    #   prev = "/?page="+ str(page-1)
    #   next = "/?page="+ str(page+1)
    _customer = customer_user.fetch_all1(page).get_json()

    if request.method == 'POST' and 'tag' in request.form:
        tag = request.form["tag"]
        if tag:
            search = tag
            # search1 = "%{}%".format(tag)
            if search:
                # _customery = db.session.query(Customer).filter(or_(Customer.name.like(
                #     '%'+search+'%'), Customer.billing_address.like('%'+search+'%'))).all()
                # _customery = db.session.query(Customer.name.like('%'+search+'%')).filter(Customer.billing_address.like('%'+search+'%')).slice((page - 1) * no_of_posts, page * no_of_posts).all()
                # if search:
                #  _customery = db.session.query(Customer).filter((search == Customer.id)).all()
                _customery = []
                if search.isnumeric() == True:
                    print("Passsssssss")
                    _customery = db.session.query(Customer).filter(
                        or_(Customer.id == search)).all()
                else:
                    print("Passsssssss")
                    _customery = db.session.query(Customer).filter(
                        or_(Customer.name == search)).all()
                response = []
                for custmers in _customery:
                    response.append(customer_user.customer_obj(custmers))
                _customer = jsonify(response).get_json()
                print("All customer", _customer)

    print(request.form.get("summary_details", "None provided"))

    posted = request.form
    if posted:
        _customer_summary = request.form.get("summary_details")
        # _customer_checked_id = posted.get("customer_checked")
        print("customer checked", _customer_summary)
        if _customer_summary:
            customer_real_id = customer_user.fetch_one(
                _customer_summary, True).get_json()['real_id']
            print("customer checked", customer_real_id)
            _customer_farm = custfarm.fetch_by_customer(
                customer_real_id).get_json()
            _billing = invoice.fetch_by_customer_id(
                customer_real_id).get_json()

    return render_template(
        'admin/customers/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        prev=prev, next=next,
        total=total,
        CUSTOMER=_customer,
        CUSTOMERS=_customers,
        FARMS=_allfarm,
        CFARM=_customer_farm,
        CGREEN=_customer_green_h,
        CSHIELD=_customer_devices,
        CSCREEN=_customer_screenhouse,
        CSHAD=_customer_shadnet,
        BILL=_billing,
        SUBSC=_subscription,
        HARVEST=_prod_harvest,
        MAINTAIN=_maintain,
        AGRO_APP=_customer_inventory_agro,

        data={"date": str(datetime.now().year)}
    )


@igh_customers.route('/remove/<string:id>', methods=['POST', 'GET'])
def delete_gr(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    greenhouses_details = greenhouses.fetch_one(id, True).get_json()
    if greenhouses_details["greenhouse"] != {}:
     greenhouse_ref_type = greenhouses_details["greenhouse"]["greenhouse_ref"]
    green_real_id = greenhouses_details['real_id']
    green_customer_id = greenhouses_details['customer']['id']
    greenhouses.delete(id)

    _customer_grremove_log = logs.add_new(
        json.dumps({
            "action": "Greenhouse is removed from customer",
            "ref": greenhouse_ref_type,
            "ref_id": green_real_id,
            "customer_id": green_customer_id,
            "creator_id": session["profile"]["user"]["id"]
        }),

    )
    print(str(_customer_grremove_log[0].get_json()))

    return render_template(
        'admin/customers/greenhouse_delete.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_customers.route('/remove/devices/<string:id>', methods=['POST', 'GET'])
def delete_sh(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    shields.delete(id)

    return render_template(
        'admin/customers/shield_delete.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_customers.route('/remove/screenhouses/<string:id>', methods=['POST', 'GET'])
def delete_scrn(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    screenhouses.delete(id)

    return render_template(
        'admin/customers/remove_screenhouse.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_customers.route('/remove/shadnets/<string:id>', methods=['POST', 'GET'])
def delete_shadnet(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    sh.delete(id)

    return render_template(
        'admin/customers/remove_shadenets.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_customers.route('/add', methods=['POST', 'GET'])
def add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _assign_memb = igh.fetch_all().get_json()
    _customer_subcription = []
    _user_customer = []

    # form = MyForm()
    action_response = {}
    action_response1 = {}
    action_response2 = {}
    _posted = request.form

    if _posted:
        if 'first_name' in _posted and _posted['first_name'].strip() != "" and \
            'billing_address' in _posted and _posted['billing_address'].strip() != "" and \
            'shipping_address' in _posted and _posted['shipping_address'].strip() != "" and \
            'email_address' in _posted and validateEmailAddress(_posted['email_address'].strip()) != "" and\
            _posted['farm_lat'].strip() != "" and _posted['farm_lng'].strip() != "" and _posted['farm_area'].strip() != "" and\
            _posted['amount'].strip() != "" and _posted['farm_name'].strip() != "" and _posted['farm_address'].strip() != "" and\
                _posted['farm_state'].strip() != "" and _posted['farm_city'].strip() != "" and _posted['farm_pincode'].strip() != "":

            exists = user_auth.fetch_by_username(
                _posted['email_address']).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Email address already in use, please use a different one."
                }
            # print("Exists",exists)
            # _name = _posted.getlist("first_name")
            # _email = _posted.getlist("email_address")
            # _last = _posted.getlist("last_name")
            # _billing = _posted.getlist("billing_address")
            # _shipping = _posted.getlist("shipping_address")
            # _igh_contact = _posted.getlist("igh_contact_id")

            # for name in _name:
            #     _user_customer.append({
            #         "first_name": _name,
            #         "email_address": _email,
            #         "last_name": _last,
            #         "billing_address": _billing,
            #         "shipping_address": _shipping,
            #         "igh_contact_id": _igh_contact

            #     })
            else:
                _role_id = None
                _default_role = user.roles.fetch_by_name(
                    config.CUSTOMER_USER).get_json()
                _creator_id = session['profile']['user']['id']
                _creator_real_id = user.fetch_one(
                    _creator_id, True).get_json()['real_id']
                _last = ""
                if _default_role:
                    _role_id = _default_role['id']
                _name = _posted.getlist("first_name")
                # print("Details",_default_role,_creator_id,_role_id,_posted['email_address'].strip())
                new_user = user.add_new(
                    json.dumps({
                        "email_address": _posted['email_address'].strip(),
                        "first_name": _posted['first_name'].strip(),
                        "last_name": _posted['last_name'].strip(),
                        "phone_number": _posted['phone_number'].strip(),
                        "user_realm": config.USER_CUSTOMER,
                        "role_id": _role_id,
                        "creator_id": _creator_id
                    })
                )
                if new_user:
                    action_response = {
                        "type": "success",
                        "message": "Customer user added successfully."
                    }

                print(str(new_user[0].get_json()))
                if new_user:
                    _customer_user_id1 = user.fetch_by_email(
                        _posted['email_address']).get_json()
                #  print('Customer details',_customer_user_id1)
                if _customer_user_id1 != None:
                    _customer_user_id = _customer_user_id1['id']

                if new_user:

                    new_customer = customer_user.add_new(
                        json.dumps({
                                   "name": _posted['first_name'].strip(),
                                   "billing_address": _posted['billing_address'].strip(),
                                   "shipping_address": _posted['shipping_address'].strip(),
                                   "igh_contact_id": _posted['igh_contact_id'].strip(),
                                   "creator_id": _creator_id,
                                   "contact_person_id": _customer_user_id

                                   })
                    )

                    print(str(new_customer[0].get_json()))

                if new_user:
                    cuser_real_id = user.fetch_one(
                        _customer_user_id, True).get_json()['real_id']
                    customer_selected = customer_user.fetch_by_user(
                        cuser_real_id).get_json()

                if customer_selected:
                    session["customer"] = {
                        "user": customer_selected,
                    }

                _customer_idz = customer_selected['id']

                _customer_real_id = customer_user.fetch_one(
                    _customer_idz, True).get_json()['real_id']

                if new_user:
                    new_customer_id = customers.add_new(
                        json.dumps({
                                   "employee_id": "",
                                   "customer_id": _customer_real_id,
                                   "user_id": _customer_user_id,
                                   "creator_id": _creator_id
                                   })
                    )
                    print(str(new_customer_id[0].get_json()))

                    if _customer_real_id != None:

                        new_subscription = subscription.add_new(
                            json.dumps({
                                "name": _posted['name'].strip(),
                                "cylce": _posted['cylce'].strip(),
                                "amount": _posted['amount'].strip(),
                                "customer_id": _customer_real_id,
                                "creator_id": _creator_id,
                            })
                        )
                        print(str(new_subscription[0].get_json()))

                        if new_subscription:
                            print("all details ", _posted['amount'].strip(
                            ), _customer_real_id, _creator_id)
                            new_invoice = invoice.add_new(
                                json.dumps({
                                    "gross": 1,
                                    "tax": 1,
                                    "deductions": 1,
                                    "net": _posted['amount'].strip(),
                                    "fully_paid": 1,
                                    "customer_id": _customer_real_id,
                                    "creator_id": _creator_id,
                                })
                            )

                            if new_invoice:
                                action_response2 = {
                                    "type": "success",
                                    "message2": "Bill added successfully."
                                }
                            print(str(new_invoice[0].get_json()))

                # _billing,_shipping,_posted['phone_number'].strip()
                if new_user != None and _posted.get("farm_lat") != '' and _posted.get("farm_lng") != '' and _posted.get("farm_area") != '':

                    _farm = Farm(
                        uid=uuid.uuid4(),

                        name=_posted.get("farm_name"),
                        farm_ref=_posted.get("farm_name"),
                        address=_posted.get("farm_address"),
                        state=_posted.get("farm_state"),
                        city=_posted.get("farm_city"),
                        pincode=_posted.get("farm_pincode"),
                        latitude=_posted.get("farm_lat"),
                        longitude=_posted.get("farm_lng"),
                        area=_posted.get("farm_area"),
                        status=config.STATUS_ACTIVE,
                        creator_id=_creator_real_id,
                        # cover_photo_media_id = _cover_photo_media_id
                        # phone_number = _phone_no,

                    )
                    db.session.add(_farm)
                    db.session.commit()

                    if _farm:
                        action_response = {
                            "type": "success",
                            "message": "Customer added successfully."
                        }
                    if _farm:
                        _farm_owner = ownfarms.add_new(
                            json.dumps({
                                "farm_id": _farm.id,
                                "customer_id": _customer_real_id,
                                "creator_id":  _creator_real_id

                            })
                        )
                        if _farm_owner:
                            action_response1 = {
                                "type": "success",
                                "message1": "Farm added successfully."
                            }

                    print(str(_farm_owner[0].get_json()))
                    if _farm_owner:
                        new_alert_admin = alert.add_new(
                            json.dumps({
                                "type": config.ALERT_USER,
                                "customer_id": _customer_real_id,
                                "severity": config.ALERT_INFORMATIONAL,
                                "creator_id": _creator_id,
                                "name": _name,
                                "detail": _name,
                                "trigger": "Customer Invite",
                                "action_text": "",
                                "action_link": "",

                            })
                        )
                    print(str(new_alert_admin[0].get_json()))

        else:
            action_response = {
                "type": "warning",
                "message": "Customer deatils, Farm details and Bill details are required to register."
            }

    return render_template(
        'admin/customers/add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        FORM_ACTION_RESPONSE=action_response,
        FARM_ACTION_RESPONSE=action_response1,
        BILLING_ACTION_RESPONSE=action_response2,
        PROFILE=session['profile'],
        POSTEDCUSTOMER=_user_customer,
        CUSTOMER_REAL=_customer,
        ASSIGN_MEMB=_assign_memb,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        CUSTOMER=_customer_subcription,
        # form=form,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_customers.route('/edit/<string:id>/', methods=['POST', 'GET'])
def edit(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _assign_memb = igh.fetch_all().get_json()
    _customer_subcription = []
    _user_customer = []
    fetched_customer = {}
    subscription_value = {}
    # form = MyForm()
    action_response = {}
    action_response1 = {}
    action_response2 = {}
    _posted = request.form

    fetched_customer = customer_user.fetch_one(id, True).get_json()
    fetch_customer_real = fetched_customer['real_id']
    customer_user_id = fetched_customer['contact_person_id']['id']
    subscription_value = subscription.fetch_by_customer_id(
        fetch_customer_real).get_json()
    # print("Fetched customer",subscription_value)

    farm_details_customer = ownfarms.fetch_by_customer_recent(
        fetch_customer_real).get_json()
    # print("Farm details customer",farm_details_customer)

    if _posted:
        if 'first_name' in _posted and _posted['first_name'].strip() != "" and \
            'billing_address' in _posted and _posted['billing_address'].strip() != "" and \
            'shipping_address' in _posted and _posted['shipping_address'].strip() != "" and \
            _posted['farm_lat'].strip() != "" and _posted['farm_lng'].strip() != "" and _posted['farm_area'].strip() != "" and\
            _posted['amount'].strip() != "" and _posted['farm_name'].strip() != "" and _posted['farm_address'].strip() != "" and\
                _posted['farm_state'].strip() != "" and _posted['farm_city'].strip() != "" and _posted['farm_pincode'].strip() != "":

            # exists = user_auth.fetch_by_username(
            #     _posted['email_address']).get_json()
            # if exists:
            #     action_response = {
            #         "type": "danger",
            #         "message": "Email address already in use, please use a different one."
            #     }

            # else:
            _role_id = None
            _default_role = user.roles.fetch_by_name(
                config.CUSTOMER_USER).get_json()
            _creator_id = session['profile']['user']['id']
            # _creator_real_id = user.fetch_one(
            #     _creator_id, True).get_json()['real_id']
            # _last = ""
            # if _default_role:
            #     _role_id = _default_role['id']
            _name = _posted.getlist("first_name")
            # print("Details",_default_role,_creator_id,_role_id,_posted['email_address'].strip())
            edit_user = user.edit(
                customer_user_id,
                json.dumps({
                    # "email_address": _posted['email_address'].strip(),
                    "first_name": _posted['first_name'].strip(),
                    "last_name": _posted['last_name'].strip(),
                    "phone_number": _posted['phone_number'].strip(),


                })
            )
            if edit_user:
                action_response = {
                    "type": "success",
                    "message": "Customer user updated successfully."
                }

            print(str(edit_user[0].get_json()))
            # if edit_user:
            #     _customer_user_id1 = user.fetch_by_email(
            #         _posted['email_address']).get_json()
            # #  print('Customer details',_customer_user_id1)
            # if _customer_user_id1 != None:
            #     _customer_user_id = _customer_user_id1['id']

            print("IGH CONTACT ID", _posted['igh_contact_id'].strip())
            edit_customer = customer_user.edit(
                id,
                json.dumps({
                    "name": _posted['first_name'].strip(),
                    "billing_address": _posted['billing_address'].strip(),
                    "shipping_address": _posted['shipping_address'].strip(),
                    "igh_contact_id": _posted['igh_contact_id'].strip(),


                })
            )

            print(str(edit_customer[0].get_json()))

            if edit_user:
                cuser_real_id = user.fetch_one(
                    customer_user_id, True).get_json()['real_id']
                customer_selected = customer_user.fetch_by_user(
                    cuser_real_id).get_json()

            if customer_selected:
                session["customer"] = {
                    "user": customer_selected,
                }

            _customer_idz = customer_selected['id']

            _customer_real_id = customer_user.fetch_one(
                _customer_idz, True).get_json()['real_id']

            # if new_user:
            #     new_customer_id = customers.add_new(
            #         json.dumps({
            #                    "employee_id": "",
            #                    "customer_id": _customer_real_id,
            #                    "user_id": _customer_user_id,
            #                    "creator_id": _creator_id
            #                    })
            #     )
            #     print(str(new_customer_id[0].get_json()))
            # if subscription_value != {} and fetch_customer_real != None:

            if subscription_value != {}:
                subscription_id = subscription_value['id']

                edit_subscription = subscription.edit(
                    subscription_id,
                    json.dumps({
                        "name": _posted['name'].strip(),
                        "cylce": _posted['cylce'].strip(),
                        "amount": _posted['amount'].strip(),
                        "creator_id": _creator_id,
                    })
                )
                print(str(edit_subscription[0].get_json()))

            else:

                new_subscription = subscription.add_new(
                    json.dumps({
                               "name": _posted['name'].strip(),
                               "cylce": _posted['cylce'].strip(),
                               "amount": _posted['amount'].strip(),
                               "customer_id": _customer_real_id,
                               "creator_id": _creator_id,
                               })
                )
                print(str(new_subscription[0].get_json()))

            #      if new_subscription:

                #   new_invoice = invoice.edit(

                #     json.dumps({
                #         "gross": 1,
                #         "tax": 1,
                #         "deductions": 1,
                #         "net": _posted['amount'].strip(),
                #         "fully_paid": 1,
                #         "customer_id": _customer_real_id,
                #         "creator_id": _creator_id,
                #     })
                #   )

                #   if new_invoice:
                #       action_response2 = {
                #         "type": "success",
                #         "message2": "Bill updated successfully."
                #     }
                #   print(str(new_invoice[0].get_json()))

            # _billing,_shipping,_posted['phone_number'].strip()
            if farm_details_customer != {}:
                customer_farm_id = farm_details_customer['farm']['id']
                print("Farm Area", _posted.get("farm_area").strip())
                if edit_user != None and _posted.get("farm_lat") != '' and _posted.get("farm_lng") != '' and _posted.get("farm_area") != '':
                    _farm_edit = farm.edit(
                        customer_farm_id,
                        json.dumps({
                            "name": _posted.get("farm_name").strip(),
                            "farm_ref": _posted.get("farm_name").strip(),
                            "address": _posted.get("farm_address").strip(),
                            "state": _posted.get("farm_state").strip(),
                            "city": _posted.get("farm_city").strip(),
                            "pincode": _posted.get("farm_pincode").strip(),
                            "latitude": _posted.get("farm_lat").strip(),
                            "longitude": _posted.get("farm_lng").strip(),
                            "area": _posted.get("farm_area").strip(),


                        })
                    )

                    print(str(_farm_edit[0].get_json()))

                # _farm = farm(
                #     uid=uuid.uuid4(),

                #     name=_posted.get("farm_name"),
                #     farm_ref=_posted.get("farm_name"),
                #     address=_posted.get("farm_address"),
                #     state=_posted.get("farm_state"),
                #     city=_posted.get("farm_city"),
                #     pincode=_posted.get("farm_pincode"),
                #     latitude= _posted.get("farm_lat"),
                #     longitude=_posted.get("farm_lng"),
                #     area=_posted.get("farm_area"),
                #     status=config.STATUS_ACTIVE,
                #     creator_id=_creator_real_id,
                #     # cover_photo_media_id = _cover_photo_media_id
                #     # phone_number = _phone_no,

                # )
                # db.session.add(_farm)
                # db.session.commit()

                # if _farm:
                #     action_response = {
                #         "type": "success",
                #         "message": "Customer updated successfully."
                #     }
                # if _farm:
                #  _farm_owner = ownfarms.add_new(
                #     json.dumps({
                #                "farm_id": _farm.id,
                #                "customer_id": _customer_real_id,
                #                "creator_id":  _creator_real_id

                #                })
                # )
                #  if _farm_owner:
                #     action_response1 = {
                #         "type": "success",
                #         "message1": "Farm updated successfully."
                #     }

                # print(str(_farm_owner[0].get_json()))
                # if _farm_owner:
                    new_alert_admin = alert.add_new(
                        json.dumps({
                            "type": config.ALERT_USER,
                            "customer_id": _customer_real_id,
                            "severity": config.ALERT_INFORMATIONAL,
                            "creator_id": _creator_id,
                            "name": _name,
                            "detail": _name,
                            "trigger": "Customer is Updated",
                            "action_text": "",
                            "action_link": "",

                        })
                    )
                    print(str(new_alert_admin[0].get_json()))

            else:
                creator_real = user.fetch_one(
                    _creator_id, True).get_json()['real_id']
                if _posted.get("farm_lat") != '' and _posted.get("farm_lng") != '' and _posted.get("farm_area") != '':
                    _farm = Farm(
                        uid=uuid.uuid4(),

                        name=_posted.get("farm_name"),
                        farm_ref=_posted.get("farm_name"),
                        address=_posted.get("farm_address"),
                        state=_posted.get("farm_state"),
                        city=_posted.get("farm_city"),
                        pincode=_posted.get("farm_pincode"),
                        latitude=_posted.get("farm_lat"),
                        longitude=_posted.get("farm_lng"),
                        area=_posted.get("farm_area"),
                        status=config.STATUS_ACTIVE,
                        creator_id=creator_real,
                        # cover_photo_media_id = _cover_photo_media_id
                        # phone_number = _phone_no,

                    )
                    db.session.add(_farm)
                    db.session.commit()

                    if _farm:
                        action_response = {
                            "type": "success",
                            "message": "Customer added successfully."
                        }
                    if _farm:
                        _farm_owner = ownfarms.add_new(
                            json.dumps({
                                "farm_id": _farm.id,
                                "customer_id": _customer_real_id,
                                "creator_id":  creator_real

                            })
                        )
                    if _farm_owner:
                        action_response1 = {
                            "type": "success",
                            "message1": "Farm added successfully."
                        }
                    new_alert_admin = alert.add_new(
                        json.dumps({
                            "type": config.ALERT_USER,
                            "customer_id": _customer_real_id,
                            "severity": config.ALERT_INFORMATIONAL,
                            "creator_id": _creator_id,
                            "name": _posted.get("farm_name"),
                            "detail": _posted.get("farm_name"),
                            "trigger": "New Farm is Added",
                            "action_text": "",
                            "action_link": "",

                        })
                    )
                    print(str(new_alert_admin[0].get_json()))

        else:
            action_response = {
                "type": "warning",
                "message": "Customer deatils, Farm details and Bill details are required to register."
            }

    return render_template(
        'admin/customers/edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        FORM_ACTION_RESPONSE=action_response,
        FARM_ACTION_RESPONSE=action_response1,
        BILLING_ACTION_RESPONSE=action_response2,
        PROFILE=session['profile'],
        POSTEDCUSTOMER=_user_customer,
        CUSTOMER_REAL=_customer,
        ASSIGN_MEMB=_assign_memb,
        FETCHCUST=fetched_customer,
        SUBSCRIBE=subscription_value,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        CUSTOMER=_customer_subcription,
        FARMDETAILS=farm_details_customer,
        # form=form,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_customers.route('/deactivate/<string:id>', methods=['POST', 'GET'])
def customer_deactivate(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _assign_memb = igh.fetch_all().get_json()

    _user_customer = []

    customer_user.deactivate(id)

    # form = MyForm()

    return render_template(
        'admin/customers/deactivate.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_customers.route('/activate/<string:id>', methods=['POST', 'GET'])
def customer_activate(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    customer_user.activate(id)

    # form = MyForm()

    return render_template(
        'admin/customers/activate.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )


@igh_customers.route('/add/greenhouses', methods=['POST', 'GET'])
def add_greenhouses():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    
    # _c_green_house = greenhouses.fetch_all().get_json()
    # print("Greenhouse", _greenhouse)
    _customer_greenhouses = []
    if "customer" in session:
        _customer_id = session["customer"]["user"]["id"]
        _customer_real_id = customer_user.fetch_one(
            _customer_id, True).get_json()['real_id']

        _farm_idz = ownfarms.fetch_by_customer_recent(
            _customer_real_id).get_json()['id']
        _farm_idn = ownfarms.fetch_one(_farm_idz, True).get_json()
        _farm_ido = _farm_idn['farm']['id']
        _farm_id = farm.fetch_one(_farm_ido, True).get_json()['real_id']

    #  print("Farm ID",_farm_id)
    else:
        _customer_id = None
        _customer_real_id = None

    greenhouse_selected = []

    # print("greenhouse customer",_customer_first)

    _posted = request.form

    if _posted:

        _greenhouse_idz = _posted.get("greenhouse_select")
        
        greenhouse_selected = greenh.fetch_one(
            _greenhouse_idz, True).get_json()
        if greenhouse_selected:
            _green_uid = greenhouse_selected['id']
        _greenhouse_id = greenh.fetch_one(
            _greenhouse_idz, True).get_json()["real_id"]
        greenhouse_name = greenhouse_selected['greenhouse_ref']
        _green_type = greenhouse_selected["greenhouse_type_id"]['id']
        _green_size = greenhouse_selected['size']
        _green_link = greenhouse_selected['info_link']
        _creator_idz = session['profile']['user']['id']
        _creator_id = user.fetch_one(_creator_idz, True).get_json()['real_id']
        _created = _posted.get("created")
        _status = _posted.get("status")

        # if _farm_id :
        #  _updated_customer = customer_user.edit(
        #     _customer_id,
        #     json.dumps({

        #         "farm_id" : _farm_id

        #     })
        #  )

        # print(str(_updated_customer[0].get_json()))

        if _greenhouse_id and _creator_id:
            _new_greenhouse_c = CustomerGreenhouse(

                uid=uuid.uuid4(),
                status=config.STATUS_INACTIVE,
                greenhouse_id=_greenhouse_id,
                customer_id=_customer_real_id,
                creator_id=_creator_id,

            )
            db.session.add(_new_greenhouse_c)
            db.session.commit()

            if _new_greenhouse_c:
                action_response = {
                    "type": "success",
                    "message": "Greenhouse added successfully."
                }
            if _new_greenhouse_c:
                _update_inventory = greenh.edit(
                    _green_uid,
                    json.dumps({

                        "size": _green_size,
                        "info_link": _green_link,

                        "assigned": config.ASSIGNED,
                        "greenhouse_type_id":_green_type

                    }))
                print(str(_update_inventory[0].get_json()))

            # if _new_greenhouse_c:
            #     _new_farm_greenhouse_c = farm_greenhouses.add_new(
            #         json.dumps({

            #             "name": greenhouse_name,
            #             "customer_farm_id": _farm_idz,
            #             "customer_greenhouse_id": _new_greenhouse_c.id,
            #             "creator_id": _creator_idz

            #         }))

            #     print(str(_new_farm_greenhouse_c[0].get_json()))

        _user_log = logs.add_new(
            json.dumps({
                "action": "Assigned a new Greenhouse",
                "ref": greenhouse_name,
                "ref_id": greenhouse_selected['real_id'],
                "customer_id": _customer_id,
                "creator_id": session["profile"]["user"]["id"]
            }),

        )

        print(str(_user_log[0].get_json()))
        new_greemh_alert_admin = alert.add_new(
            json.dumps({
                "type": config.ALERT_USER,
                "customer_id": _customer_real_id,
                "severity": config.ALERT_INFORMATIONAL,
                "creator_id": _creator_idz,
                "name": greenhouse_name,
                "detail": greenhouse_name,
                "trigger": "Greenhouse Added",
                "action_text": "",
                "action_link": "",

            })
        )
        print(str(new_greemh_alert_admin[0].get_json()))

    else:
        action_response = {
            "type": "warning",
            "message": "Greenhouse not selected."
        }
    _greenhouse = greenh.fetch_all().get_json()

    return render_template(
        'admin/customers/greenhouses.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        CUSTOMER=_customer,
        GREENH=_greenhouse,
        CGREENHOUSES=_customer_greenhouses,
        SELECTED=greenhouse_selected,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}

    )


@igh_customers.route('/add/greenhouses/<string:id>', methods=['POST', 'GET'])
def add_g_greenhouses(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _greenhouse = greenh.fetch_all().get_json()
    # _c_green_house = greenhouses.fetch_all().get_json()
    # print("Greenhouse", _greenhouse)
    _customer_greenhouses = []
    greenhouse_selected = []
    action_response = {}

    _customer_id = id
    _customer_real_id = customer_user.fetch_one(
        _customer_id, True).get_json()['real_id']

    _farm_idc = ownfarms.fetch_by_customer_recent(_customer_real_id).get_json()
    if _farm_idc != {}:
        _farm_idz = _farm_idc['id']
        _farm_idn = ownfarms.fetch_one(_farm_idz, True).get_json()
        _farm_ido = _farm_idn['farm']['id']
        _farm_id = farm.fetch_one(_farm_ido, True).get_json()['real_id']

    _posted = request.form

    if _posted:

        _greenhouse_idz = _posted.get("greenhouse_select")
        print("Greenhouse select ID", _greenhouse_idz)
        greenhouse_selected = greenh.fetch_one(
            _greenhouse_idz, True).get_json()
        _greenhouse_id = greenh.fetch_one(
            _greenhouse_idz, True).get_json()["real_id"]
        if greenhouse_selected:
            _green_uid = greenhouse_selected['id']

        greenhouse_name = greenhouse_selected['greenhouse_ref']
        _green_size = greenhouse_selected['size']
        _green_link = greenhouse_selected['info_link']
        _green_type = greenhouse_selected["greenhouse_type_id"]['id']
        _creator_idz = session['profile']['user']['id']
        _creator_id = user.fetch_one(_creator_idz, True).get_json()['real_id']
        _created = _posted.get("created")
        _status = _posted.get("status")

        if _greenhouse_id and _creator_id:
            _new_greenhouse_c = CustomerGreenhouse(
                uid=uuid.uuid4(),
                status=config.STATUS_INACTIVE,
                greenhouse_id=_greenhouse_id,
                customer_id=_customer_real_id,
                creator_id=_creator_id,

            )
            db.session.add(_new_greenhouse_c)
            db.session.commit()

            print("Greenhouse customer added", _new_greenhouse_c)

            if _new_greenhouse_c:
                action_response = {
                    "type": "success",
                    "message": "Greenhouse added successfully."
                }

            if _new_greenhouse_c:
                _update_inventory = greenh.edit(
                    _green_uid,
                    json.dumps({
                        "size": _green_size,
                        "info_link": _green_link,
                        "assigned": config.ASSIGNED,
                        "greenhouse_type_id": _green_type

                    }))
                print(str(_update_inventory[0].get_json()))
            #   if _new_greenhouse_c:
            #       _new_farm_greenhouse_c = farm_greenhouses.add_new(
            #         json.dumps({

            #             "name": greenhouse_name,
            #             "customer_farm_id": _farm_idz,
            #             "customer_greenhouse_id": _new_greenhouse_c.id,
            #             "creator_id": _creator_idz

            #         }))

            #       print(str(_new_farm_greenhouse_c[0].get_json()))

                if _new_greenhouse_c:

                    _user_log = logs.add_new(
                        json.dumps({
                            "action": "Assigned a new Greenhouse",
                            "ref": greenhouse_name,
                            "ref_id": greenhouse_selected['real_id'],
                            "customer_id": _customer_id,
                            "creator_id": session["profile"]["user"]["id"]
                        }),

                    )

                    print(str(_user_log[0].get_json()))
                    new_greemh_alert_admin = alert.add_new(
                        json.dumps({
                            "type": config.ALERT_USER,
                            "customer_id": _customer_real_id,
                            "severity": config.ALERT_INFORMATIONAL,
                            "creator_id": _creator_idz,
                            "name": greenhouse_name,
                            "detail": greenhouse_name,
                            "trigger": "Greenhouse Added",
                            "action_text": "",
                            "action_link": "",

                        })
                    )
                    print(str(new_greemh_alert_admin[0].get_json()))

    else:
        action_response = {
            "type": "warning",
            "message": ""
        }
    # else:
    #        action_response = {
    #         "type": "warning",
    #         "message": "Before add any product Must add a Farm."
    #     }

    _greenhouse = greenh.fetch_all().get_json()

    return render_template(
        'admin/customers/greenhouses_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        CUSTOMER=_customer,
        GREENH=_greenhouse,
        CGREENHOUSES=_customer_greenhouses,
        SELECTED=greenhouse_selected,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}

    )


@igh_customers.route('/add/devices', methods=['POST', 'GET'])
def add_devices():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    if "customer" in session:
        _customer_idz = session["customer"]["user"]["id"]
        _customer_real_id = customer_user.fetch_one(
            _customer_idz, True).get_json()['real_id']
        _farm_idz = ownfarms.fetch_by_customer_recent(
            _customer_real_id).get_json()['id']
        _farm_idn = ownfarms.fetch_one(_farm_idz, True).get_json()['real_id']
        # _green_r_real_id = farm_greenhouses.fetch_by_customer_farm(_farm_idn).get_json()['id']
        # print("Greenhouse Id", _green_r_real_id)
    else:
        _customer_idz = None
    _customer_real_id = customer_user.fetch_one(
        _customer_idz, True).get_json()['real_id']
    # print("Customer user",_customer_real_id)
    _shield_all = device.fetch_all().get_json()
    _device_selected = []

    posted = request.form

    if posted:
        _device_id = posted.get('device_id')

        _device_selected = device.fetch_one(_device_id, True).get_json()

        if _device_selected:

            _shield_name_id = _device_selected['serial']['boron_id']

            _shield_mac = _device_selected['mac_address']

        _issue_id = posted.get('issue_id')
        _shield_maintenance_id = posted.get('shield_maintenance_id')
        _maintenance_id = posted.get('maintenance_id')
        _customer_id = _customer_idz
        _created_by = session['profile']['user']['id']
        _status = posted.get('status')
        print("Before Added", _device_id, _customer_id, _created_by)
        _shield_f_id = device.fetch_one(_device_id, True).get_json()['real_id']
        _customer_f_id = customer.fetch_one(
            _customer_id, True).get_json()['real_id']
        _creator_f_id = user.fetch_one(_created_by, True).get_json()['real_id']
        if _shield_f_id:
            _customer_device_d = CustomerShield(
                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                shield_id=_shield_f_id,
                customer_id=_customer_f_id,
                creator_id=_creator_f_id
            )
            db.session.add(_customer_device_d)
            db.session.commit()

            if _customer_device_d:
                action_response = {
                    "type": "success",
                    "message": "Device added successfully."
                }

    #   _customer_device_details = shields.add_new(
    #                          json.dumps({
    #                           "shield_id": _device_id,
    #                           "customer_id" : _customer_id,
    #                           "creator_id" : _created_by

    #                          })

    #                    )

    #   if _customer_device_details:
    #                 action_response = {
    #                     "type": "success",
    #                     "message": "Device added successfully."
    #                 }
    #   print(str( _customer_device_details[0].get_json()))

        if _customer_device_d:
            inventory_device = device.edit(
                _device_id,
                json.dumps({
                    "serial": _shield_name_id,
                    "mac_address": _shield_mac,
                    "assigned": config.ASSIGNED


                })

            )
            print(str(inventory_device[0].get_json()))
        # if _customer_device_d:

        #     _greenhouse_device = greenhouse_shields.add_new(

        #         json.dumps({
        #             "latitude": None,
        #             "longitude": None,
        #             "growth_area": None,
        #             "soil_type_id": 1,
        #             "customer_farm_greenhouse_id": _green_r_real_id,
        #             "customer_shield_id": _customer_device_d.id,
        #             "creator_id": _created_by

        #         })

        #     )
        #     print(str(_greenhouse_device[0].get_json()))

            _user_log = logs.add_new(
                json.dumps({
                    "action": "Assigned a new device",
                    "ref": _shield_name_id,
                    "ref_id": _shield_f_id,
                    "customer_id": _customer_idz,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )

            print(str(_user_log[0].get_json()))

            new_shield_alert_admin = alert.add_new(
                json.dumps({
                    "type": config.ALERT_USER,
                    "customer_id": _customer_real_id,
                    "severity": config.ALERT_INFORMATIONAL,
                    "creator_id": _created_by,
                    "name": _shield_name_id,
                    "detail":  _shield_name_id,
                    "trigger": "Device Added",
                    "action_link": "./view/_device_id",


                })
            )
            print(str(new_shield_alert_admin[0].get_json()))

    else:
        action_response = {
            "type": "warning",
            "message": ""
        }

    return render_template(
        'admin/customers/devices.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        DEVICES=_shield_all,
        DSELECTED=_device_selected,
        FORM_ACTION_RESPONSE=action_response,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_customers.route('/add/devices/<string:id>', methods=['POST', 'GET'])
def add_c_devices(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _customer_idz = id
    _customer_real_id = customer_user.fetch_one(
        _customer_idz, True).get_json()['real_id']
    print("Customer user", _customer_real_id)
    _shield_all = device.fetch_all().get_json()
    _device_selected = {}
    action_response = {}
    # customer_greenhouse = session["customer"]["green"]["id"]
    # print("greenhouse_id",customer_greenhouse)

    posted = request.form

    if posted:
        _device_id = posted.get('device_id')
        print("Device ID", _device_id)

        _device_selected = device.fetch_one(_device_id, True).get_json()

        if _device_selected:

            _shield_name_id = _device_selected['serial']['boron_id']

            _shield_mac = _device_selected['mac_address']

        _issue_id = posted.get('issue_id')
        _shield_maintenance_id = posted.get('shield_maintenance_id')
        _maintenance_id = posted.get('maintenance_id')
        _customer_id = _customer_idz
        _created_by = session['profile']['user']['id']
        _status = posted.get('status')
        print("Before Added", _device_id, _customer_id, _created_by)

        _customer_device_details = shields.add_new(
            json.dumps({
                "shield_id": _device_id,
                "customer_id": _customer_id,
                "creator_id": _created_by

            })

        )

        if _customer_device_details:
            action_response = {
                "type": "success",
                "message": "Device added successfully."
            }
        print(str(_customer_device_details[0].get_json()))

        if _customer_device_details:
            inventory_device = device.edit(
                _device_id,
                json.dumps({
                    "serial": _shield_name_id,
                    "mac_address": _shield_mac,
                    "assigned": config.ASSIGNED


                })

            )
            print(str(inventory_device[0].get_json()))

        _user_log = logs.add_new(
            json.dumps({
                "action": "Assigned a new device",
                "ref": _shield_name_id,
                "ref_id": _device_selected['real_id'],
                "customer_id": _customer_idz,
                "creator_id": session["profile"]["user"]["id"]
            }),

        )

        print(str(_user_log[0].get_json()))

        new_shield_alert_admin = alert.add_new(
            json.dumps({
                "type": config.ALERT_SHIELD,
                "customer_id": _customer_real_id,
                "severity": config.ALERT_INFORMATIONAL,
                "creator_id": _created_by,
                "name": _shield_name_id,
                "detail":  _shield_name_id,
                "trigger": "Device Added",
                "action_link": "./view/_device_id",



            })
        )
        print(str(new_shield_alert_admin[0].get_json()))

    else:
        action_response = {
            "type": "warning",
            "message": ""
        }

    return render_template(
        'admin/customers/devices_edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        DEVICES=_shield_all,
        DSELECTED=_device_selected,
        FORM_ACTION_RESPONSE=action_response,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_customers.route('/add/screenhouses', methods=['POST', 'GET'])
def add_screenhhouses():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _screenhouse = screenh.fetch_all().get_json()
    _customer_screenhouses = []
    if "customer" in session:
        _customer_idz = session["customer"]["user"]["id"]
        _customer_real_id = customer_user.fetch_one(
            _customer_idz, True).get_json()['real_id']
    else:
        _customer_idz = None

    posted = request.form

    if posted:
        _screenhouse_id = posted.get('screenhouse_select')
        print("Screen house ID", _screenhouse_id)
        _customer_screenhouses = screenh.fetch_one(
            _screenhouse_id, True).get_json()
        print("Current Screenhouse", _customer_screenhouses)
        if _customer_screenhouses:
            _screenhouse_name = _customer_screenhouses['screenhouse_ref']
            _screen_size = _customer_screenhouses['size']
            _screen_link = _customer_screenhouses['info_link']
        _customer_id = _customer_idz
        _creator_id = session['profile']['user']['id']

        _new_screen = screenhouses.add_new(
            json.dumps({
                "screenhouse_id": _screenhouse_id,
                "customer_id": _customer_id,
                "creator_id": _creator_id,


            })
        )
        print(str(_new_screen[0].get_json()))
        if _new_screen:
            action_response = {
                "type": "success",
                "message": "Screenhouse added successfully."
            }
        if _new_screen:

            _inventory_screen = screenh.edit(
                _screenhouse_id,
                json.dumps({
                    "size": _screen_size,
                    "info_link": _screen_link,
                    "assigned": config.ASSIGNED,


                })
            )
            print(str(_inventory_screen[0].get_json()))

        if _new_screen:

            _user_log = logs.add_new(
                json.dumps({
                    "action": "Assigned a new Screenhouse",
                    "ref": _screenhouse_name,
                    "ref_id": _customer_screenhouses['real_id'],
                    "customer_id": _customer_idz,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )

            print(str(_user_log[0].get_json()))
            new_screen_alert_admin = alert.add_new(
                json.dumps({
                    "type": config.ALERT_SHIELD,
                    "customer_id": _customer_real_id,
                    "severity": config.ALERT_INFORMATIONAL,
                    "creator_id": _creator_id,
                    "name": _screenhouse_name,
                    "current_read": "",
                    "ideal_read": ""

                })
            )
            print(str(new_screen_alert_admin[0].get_json()))
    else:
        action_response = {
            "type": "warning",
            "message": ""
        }

    return render_template(
        'admin/customers/screenhouses.html',
        ADMIN_PORTAL=True,
        SCREENH=_screenhouse,
        CUSTOMER_SCREEN=_customer_screenhouses,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_customers.route('/add/screenhouses/<string:id>', methods=['POST', 'GET'])
def add_c_screenhhouses(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _screenhouse = screenh.fetch_all().get_json()
    _customer_screenhouses = []
    # if "customer" in session:
    #  _customer_idz = session["customer"]["user"]["id"]
    # else:
    _customer_idz = id
    _customer_real_id = customer_user.fetch_one(
        _customer_idz, True).get_json()['real_id']
    posted = request.form

    if posted:
        _screenhouse_id = posted.get('screenhouse_select')
        print("Screen house ID", _screenhouse_id)
        _customer_screenhouses = screenh.fetch_one(
            _screenhouse_id, True).get_json()
        print("Current Screenhouse", _customer_screenhouses)
        if _customer_screenhouses:
            _screenhouse_name = _customer_screenhouses['screenhouse_ref']
            _screen_size = _customer_screenhouses['size']
            _screen_link = _customer_screenhouses['info_link']
        _customer_id = _customer_idz
        _creator_id = session['profile']['user']['id']

        _new_screen = screenhouses.add_new(
            json.dumps({
                "screenhouse_id": _screenhouse_id,
                "customer_id": _customer_id,
                "creator_id": _creator_id,


            })
        )
        print(str(_new_screen[0].get_json()))
        if _new_screen:
            action_response = {
                "type": "success",
                "message": "Screenhouse added successfully."
            }
        if _new_screen:

            _inventory_screen = screenh.edit(
                _screenhouse_id,
                json.dumps({
                    "size": _screen_size,
                    "info_link": _screen_link,
                    "assigned": config.ASSIGNED,


                })
            )
            print(str(_inventory_screen[0].get_json()))

        if _new_screen:

            _user_log = logs.add_new(
                json.dumps({
                    "action": "Assigned a new Screenhouse",
                    "ref": _screenhouse_name,
                    "ref_id": _customer_screenhouses['real_id'],
                    "customer_id": _customer_idz,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )

            print(str(_user_log[0].get_json()))
            new_screen_alert_admin = alert.add_new(
                json.dumps({
                    "type": config.ALERT_SHIELD,
                    "customer_id": _customer_real_id,
                    "severity": config.ALERT_INFORMATIONAL,
                    "creator_id": _creator_id,
                    "name": _screenhouse_name,
                    "current_read": "",
                    "ideal_read": ""

                })
            )
            print(str(new_screen_alert_admin[0].get_json()))
    else:
        action_response = {
            "type": "warning",
            "message": ""
        }

    return render_template(
        'admin/customers/screenhouses_add.html',
        ADMIN_PORTAL=True,
        SCREENH=_screenhouse,
        CUSTOMER_SCREEN=_customer_screenhouses,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_customers.route('/add/shadnets', methods=['POST', 'GET'])
def add_shadnets():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _shadenet = shadenets.fetch_all().get_json()
    _customer_shadenet = []
    if "customer" in session:
        _customer_idz = session["customer"]["user"]["id"]
        _customer_real_id = customer_user.fetch_one(
            _customer_idz, True).get_json()['real_id']
    else:
        _customer_idz = None

    _posted = request.form

    if _posted:
        _customer_checked_id = _posted.get("customer_checked")
        print("customer checked", _customer_checked_id)

        _shadenet_id = _posted.get("shadnet_select")
        print("Shadnet ID", _shadenet_id)
        _customer_shadenet = shadenets.fetch_one(_shadenet_id, True).get_json()
        if _customer_shadenet:
            _shad_size = _customer_shadenet['size']
            _shad_link = _customer_shadenet['info_link']
            _shad_name = _customer_shadenet['shadenet_ref']
        _customer_id = _customer_idz
        _creator_id = session['profile']['user']['id']

        _new_shadnet = sh.add_new(
            json.dumps({
                "shadenet_id": _shadenet_id,
                "customer_id": _customer_id,
                "creator_id": _creator_id,


            }),

        )
        print(str(_new_shadnet[0].get_json()))

        if _new_shadnet:
            action_response = {
                "type": "success",
                "message": "Shadenet added successfully."
            }
        if _new_shadnet:
            _inventory_shad = shadenets.edit(
                _shadenet_id,
                json.dumps({
                    "size": _shad_size,
                    "info_link": _shad_link,
                    "assigned": config.ASSIGNED,


                })
            )
            print(str(_inventory_shad[0].get_json()))

        if _new_shadnet:
            print("Shadnet alert", config.ALERT_SHIELD,
                  config.ALERT_INFORMATIONAL, _shad_name)

            _user_log = logs.add_new(
                json.dumps({
                    "action": "Assigned a new Shadenets",
                    "ref": _shad_name,
                    "ref_id": _customer_shadenet['real_id'],
                    "customer_id": _customer_idz,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )

            print(str(_user_log[0].get_json()))

            new_screen_alert_admin = alert.add_new(
                json.dumps({
                    "type": config.ALERT_SHIELD,
                    "customer_id": _customer_real_id,
                    "severity": config.ALERT_INFORMATIONAL,
                    "creator_id": _creator_id,
                    "name": _shad_name,
                    "current_read": "",
                    "ideal_read": ""

                })
            )
            print(str(new_screen_alert_admin[0].get_json()))
    else:
        action_response = {
            "type": "warning",
            "message": ""
        }

    return render_template(
        'admin/customers/shadnets.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        SHADENET=_customer_shadenet,
        SHADENETSALL=_shadenet,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_customers.route('/add/shadnets/<string:id>', methods=['POST', 'GET'])
def add_cshadnets(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _shadenet = shadenets.fetch_all().get_json()
    _customer_shadenet = []
    # if "customer" in session:
    #  _customer_idz = session["customer"]["user"]["id"]
    # else:
    _customer_idz = id
    _customer_real_id = customer_user.fetch_one(
        _customer_idz, True).get_json()['real_id']
    _posted = request.form

    if _posted:
        _customer_checked_id = _posted.get("customer_checked")
        print("customer checked", _customer_checked_id)

        _shadenet_id = _posted.get("shadnet_select")
        print("Shadnet ID", _shadenet_id)
        _customer_shadenet = shadenets.fetch_one(_shadenet_id, True).get_json()
        if _customer_shadenet:
            _shad_size = _customer_shadenet['size']
            _shad_link = _customer_shadenet['info_link']
            _shad_name = _customer_shadenet['shadenet_ref']
        _customer_id = _customer_idz
        _creator_id = session['profile']['user']['id']

        _new_shadnet = sh.add_new(
            json.dumps({
                "shadenet_id": _shadenet_id,
                "customer_id": _customer_id,
                "creator_id": _creator_id,


            }),

        )
        print(str(_new_shadnet[0].get_json()))

        if _new_shadnet:
            action_response = {
                "type": "success",
                "message": "Shadenet added successfully."
            }
        if _new_shadnet:
            _inventory_shad = shadenets.edit(
                _shadenet_id,
                json.dumps({
                    "size": _shad_size,
                    "info_link": _shad_link,
                    "assigned": config.ASSIGNED,


                })
            )
            print(str(_inventory_shad[0].get_json()))

        if _new_shadnet:
            print("Shadnet alert", config.ALERT_SHIELD,
                  config.ALERT_INFORMATIONAL, _shad_name)
            new_screen_alert_admin = alert.add_new(
                json.dumps({
                    "type": config.ALERT_SHIELD,
                    "customer_id": _customer_real_id,
                    "severity": config.ALERT_INFORMATIONAL,
                    "creator_id": _creator_id,
                    "name": _shad_name,
                    "current_read": "",
                    "ideal_read": ""

                })
            )
            print(str(new_screen_alert_admin[0].get_json()))

            _user_log = logs.add_new(
                json.dumps({
                    "action": "Assigned a new Shadenets",
                    "ref": _shad_name,
                    "ref_id": _customer_shadenet['real_id'],
                    "customer_id": _customer_idz,
                    "creator_id": session["profile"]["user"]["id"]
                }),

            )

            print(str(_user_log[0].get_json()))
    else:
        action_response = {
            "type": "warning",
            "message": ""
        }

    return render_template(
        'admin/customers/shadnets_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        SHADENET=_customer_shadenet,
        SHADENETSALL=_shadenet,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_customers.route('/add/bulk', methods=['POST', 'GET'])
def add_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/customers/bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_customers.route('/download/customer/excel')
def download_creport():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _customer = customer_user.fetch_all().get_json()

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Customers')

    # add headers
    sh.write(0, 0, 'Customer id')
    sh.write(0, 1, 'Name')
    sh.write(0, 2, 'Billing address')
    sh.write(0, 3, 'Shipping address')
    sh.write(0, 4, 'Status')
    sh.write(0, 5, 'Created')

    idx = 0
    for row in _customer:
        sh.write(idx+1, 0, str(row['main_id']))
        sh.write(idx+1, 1, row['name'])
        sh.write(idx+1, 2, row['billing_address'])
        sh.write(idx+1, 3, row['shipping_address'])
        sh.write(idx+1, 4, row['status'])
        sh.write(idx+1, 5, row['created'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=customers.xls"})


@igh_customers.route('/download/invoice/<string:id>', methods=['POST', 'GET'])
def invoices(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    custmer_final_id = id
    customer_details = {}
    customer_details = customer_user.fetch_one(
        custmer_final_id, True).get_json()
    customer_real = customer_details['real_id']
    invoice_bill = {}
    invoice_bill = invoice.fetch_by_customer_id(customer_real).get_json()

    return render_template(
        'admin/customers/invoice.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        INVOICE=invoice_bill,
        CUSTOMER=customer_details,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


#  new_farm = farm.add_new(
    #     json.dumps({

    #     "name"     :    _posted['farm_name'].strip(),
    #     "farm_ref" :_posted['farm_name'].strip(),
    #     "address"  :_posted['farm_address'].strip(),
    #     "state"    : _posted['farm_state'].strip(),
    #     "city"     : _posted['farm_city'].strip(),
    #     "pincode"  : _posted['farm_pincode'].strip(),
    #     "latitude" : _posted['farm_lat'].strip(),
    #     "longitude": _posted['farm_lng'].strip(),
    #     "area"     : _posted['farm_area'].strip(),

    #     "creator_id" : _creator_id,

    #       })
    #      )
    #  print(str(new_farm[0].get_json()))

    #  if new_farm:
    #     action_response = {
    #         "type": "success",
    #         "message": "Farm added successfully."
    #     }

    # print("All cycle details",_posted['name'].strip(),_posted['cylce'].strip(),_posted['amount'].strip())
