from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid
import csv
import datetime
from werkzeug.exceptions import HTTPException
from werkzeug.utils import secure_filename

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from app.core.user import logs

from app.config import activateConfig

from app.igh import portal_check
from app.igh.admin import contact_person

from app.core import alert

from app.core import user
from app.core.user import auth
from app.core.user import roles
from app.core.user import user_roles
from app.core.user import igh
from app.core.user import invites
from app.core.user import configs

from app.core import media
from app.core.media import files
from app.core import customer as cu
from app.core import module
from app.core.module import features
from app.core.module import permissions
from app.core.user import igh as igh_u
from app.models import db
from app.models import Customer
from sqlalchemy import extract

from app.igh import session_setup

from app.helper import valid_uuid

igh_settings = Blueprint('igh_settings', __name__,
                         static_folder='../../static', template_folder='.../../templates')
igh_settings.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def fetch_customer_report(user_id,year):
    customer_report = {}
    date = datetime.now().strftime('%Y-%m-%d')
    _data_date = date
    current_year = year
    # print("Date Time",_data_date)
    customer_all = []
    # customer_all = cu.fetch_all().get_json()
   
    # customer_data = cu.fetch_by_month(_data_date).get_json()
    # current_yr = str(datetime.now())
    
    # customer_data_current = cu.fetch_by_month(_data_date).get_json()
   

    if user_id:
        customer_all = cu.fetch_by_dates('12/12/2022','12/12/2021').get_json()
        # jan22 = cu.fetch_by_dates('31/1/2022','1/1/2022').get_json()
        # # print("Jan data",jan22)
       
        
        # feb22 = cu.fetch_by_dates('28/2/2022','1/2/2022').get_json()
        # mar22 = cu.fetch_by_dates('31/3/2022','1/3/2022').get_json()
        # apr22 = cu.fetch_by_dates('30/4/2022','1/4/2022').get_json()
        # may22 = cu.fetch_by_dates('31/5/2022','1/5/2022').get_json()
        # june22 = cu.fetch_by_dates('30/6/2022','1/6/2022').get_json()
        # july22 = cu.fetch_by_dates('31/7/2022','1/7/2022').get_json()
        # aug22 = cu.fetch_by_dates('31/8/2022','1/8/2022').get_json()
        # sept22 = cu.fetch_by_dates('30/9/2022','1/9/2022').get_json()
        # oct22 = cu.fetch_by_dates('31/10/2022','1/10/2022').get_json()
        # nov22 = cu.fetch_by_dates('30/11/2022','1/11/2022').get_json()
        # dec22 = cu.fetch_by_dates('31/12/2022','1/12/2022').get_json()
        c_length = len(customer_all)
        # jlen = len(jan22)
        # flen = len(feb22)
        # mlen = len(mar22)
        # aplen = len(apr22)
        # maylen = len(may22)
        # junlen = len(june22)
        # julylen = len(july22)
        # auglen = len(aug22)
        # seplen = len(sept22)
        # octlen = len(oct22)
        # novlen = len(nov22)
        # declen = len(dec22)


        # print("Customer All",jlen)
        customer_report = {
            "current_yr": year,
            "customers_y": c_length,
            "customers_5y": c_length,
            "labels": [
                "Jan'22",
                "Feb'22",
                "Mar'22",
                "Apr'22",
                "May'22",
                "Jun'22",
                "Jul'22",
                "Aug'22",
                "Sep'22",
                "Oct'22",
                "Nov'22",
                "Dec'22"
            ],
            "series": [
                [10,
                 2,
                 3,
                 5,
                 7,
                 9,
                 5,
                 0,
                 0,
                 4,
                 0,
                 0]
                # [ jlen,
                #  flen,
                #  mlen,
                #  aplen,
                #  maylen,
                #  junlen,
                #  julylen,
                #  auglen,
                #  seplen,
                #  octlen,
                #  novlen,
                #  declen]
            ],
            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }
        if year == "2022":
         customer_report = {
            "current_yr": year,
            "customers_y": c_length,
            "customers_5y": c_length,
            "labels": [
                "Jan'22",
                "Feb'22",
                "Mar'22",
                "Apr'22",
                "May'22",
                "Jun'22",
                "Jul'22",
                "Aug'22",
                "Sep'22",
                "Oct'22",
                "Nov'22",
                "Dec'22"
            ],
            "series": [
                [10,
                 2,
                 3,
                 5,
                 7,
                 9,
                 5,
                 0,
                 0,
                 4,
                 0,
                 0]
                # [ jlen,
                #  flen,
                #  mlen,
                #  aplen,
                #  maylen,
                #  junlen,
                #  julylen,
                #  auglen,
                #  seplen,
                #  octlen,
                #  novlen,
                #  declen]
            ],
            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }
        elif year == "2021":
          customer_report = {
            "current_yr": year, 
            "customers_y": c_length,
            "customers_5y": c_length,
            "labels": [
                "Jan'21",
                "Feb'21",
                "Mar'21",
                "Apr'21",
                "May'21",
                "Jun'21",
                "Jul'21",
                "Aug'21",
                "Sep'21",
                "Oct'21",
                "Nov'21",
                "Dec'21"
            ],
            "series": [
                [10,
                 2,
                 3,
                 5,
                 7,
                 9,
                 5,
                 0,
                 0,
                 4,
                 0,
                 0]
            ],
            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }
        elif year == "2020":
            customer_report = {
             "current_yr": year,
            "customers_y": c_length,
            "customers_5y": c_length,
            "labels": [
                "Jan'20",
                "Feb'20",
                "Mar'20",
                "Apr'20",
                "May'20",
                "Jun'20",
                "Jul'20",
                "Aug'20",
                "Sep'20",
                "Oct'20",
                "Nov'20",
                "Dec'20"
            ],
            "series": [
                [0,
                 2,
                 3,
                 0,
                 0,
                 9,
                 0,
                 0,
                 0,
                 4,
                 0,
                 0]
            ],
            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }
        elif year == "2019":
            customer_report = {
             "current_yr": year,
            "customers_y": c_length,
            "customers_5y": c_length,
            "labels": [
                "Jan'20",
                "Feb'20",
                "Mar'20",
                "Apr'20",
                "May'20",
                "Jun'20",
                "Jul'20",
                "Aug'20",
                "Sep'20",
                "Oct'20",
                "Nov'20",
                "Dec'20"
            ],
            "series": [
                [0,
                 2,
                 3,
                 0,
                 0,
                 9,
                 0,
                 0,
                 0,
                 4,
                 0,
                 0]
            ],
            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }
        elif year == "2018":
            customer_report = {
             "current_yr": year,
            "customers_y": c_length,
            "customers_5y": c_length,
            "labels": [
                "Jan'20",
                "Feb'20",
                "Mar'20",
                "Apr'20",
                "May'20",
                "Jun'20",
                "Jul'20",
                "Aug'20",
                "Sep'20",
                "Oct'20",
                "Nov'20",
                "Dec'20"
            ],
            "series": [
                [0,
                 2,
                 3,
                 0,
                 0,
                 9,
                 0,
                 0,
                 0,
                 4,
                 0,
                 0]
            ],
            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }
        else :
           customer_report = {
            "current_yr": year,
            "customers_y": c_length,
            "customers_5y": c_length,
            "labels": [
                "Jan'22",
                "Feb'22",
                "Mar'22",
                "Apr'22",
                "May'22",
                "Jun'22",
                "Jul'22",
                "Aug'22",
                "Sep'22",
                "Oct'22",
                "Nov'22",
                "Dec'22"
            ],
            "series": [
                [10,
                 2,
                 3,
                 5,
                 7,
                 9,
                 5,
                 0,
                 0,
                 4,
                 0,
                 0]
            ],
            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }

    return customer_report


def fetch_business_report(user_id):
    business_report = {}

    if user_id:
        business_report = {
            "customers_5y": 100000,
            "revenue_5y": 100000,
            "devices_5y": 10,
            "greenhouses_5y": 10
        }

    return business_report


def fetch_revenue_report(user_id):
    sales_report = {}

    if user_id:
        sales_report = {
            "total_y": 100000,
            "subscriptions": 10,
            "one_time": 10,
            "devices": 10,
            "greenhouses": 10,
            "labels": [
                "Jan'20",
                "Feb'20",
                "Mar'20",
                "Apr'20",
                "May'20",
                "Jun'20",
                "Jul'20",
                "Aug'20",
                "Sep'20",
                "Oct'20",
                "Nov'20",
                "Dec'20"
            ],
            "series": [
                [0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0,
                 0]
            ],
            "axis": {
                "y_axis": "Revenue (KES)",
                "x_axis": "",
            },
            "metric": "K"
        }

    return sales_report


@igh_settings.route('/profile', methods=['POST', 'GET'])
def profile():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    
   
    igh_users = igh_u.fetch_all().get_json()
    date = datetime.now().strftime('%Y-%m-%d')
    _data_date = date
    customer_report = {}
    customer_all = []
    customer_all_five_yrs = cu.fetch_by_fiveyears(_data_date).get_json()
    c_five_length = len(customer_all_five_yrs)
    # print("ALL",customer_all_five_yrs)
    customer_all = cu.fetch_all().get_json()
    c_length = len(customer_all)

    posted = request.form
    if posted:
        if "avatar" in posted:
            saved_filename = ""

            image_file = request.files.get("image_file")
            if image_file:
                actual_filename = secure_filename(image_file.filename)
                pref_filename = session["profile"]["user"]["id"] + \
                    "_" + str(uuid.uuid4())
                saved_filename = files.uploadFile(
                    request.files.get("image_file"),
                    "avatar",
                    pref_filename
                )

                if saved_filename:
                    avatar_media = media.add_new(
                        json.dumps({
                            "file_url": "avatar/" + saved_filename,
                            "caption": actual_filename,
                            "creator_id": session["profile"]["user"]["id"],
                        }),
                        True
                    )
                    if avatar_media[1] == 200:
                        _user = user.edit(
                            session["profile"]["user"]["id"],
                            json.dumps({
                                "avatar_media": avatar_media[0].get_json()['id']
                            }),
                            True
                        )
                        if _user[1] == 200:
                            session_setup(session["profile"]["user"]["id"])

                            return jsonify({
                                "status": True,
                                "avatar": str(saved_filename)
                            })

            return jsonify({
                "status": False,
            })

        if "password" in posted:
            password1 = posted.get("new_password").strip()
            password2 = posted.get("confirm_password").strip()
            if password1 and password2 and password1 == password2:
                _this_auth = auth.fetch_by_user(
                    session["profile"]["user"]["real_id"]).get_json()
                if _this_auth:
                    _password_reset = auth.edit(
                        _this_auth['id'],
                        json.dumps({
                            "password": password1
                        })
                    )
                    if _password_reset[1] == 200:
                        return jsonify({
                            "status": True,
                        })

            return jsonify({
                "status": False,
            })
        

        if "metric" in posted:
            _config = configs.fetch_by_user(
                session["profile"]["user"]["real_id"]).get_json()
            if _config:
                if "temp" in posted:
                    if posted.get("temp").strip() == config.METRIC_TEMP_C or \
                            posted.get("temp").strip() == config.METRIC_TEMP_F:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "temp_metric": posted.get("temp").strip()
                            })
                        )
                        session_setup(session["profile"]["user"]["id"])

                if "liquid" in posted:
                    if posted.get("liquid").strip() == config.METRIC_LIQUID_L or \
                            posted.get("liquid").strip() == config.METRIC_LIQUID_GAL:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "liquid_metric": posted.get("liquid").strip()
                            })
                        )
                        session_setup(session["profile"]["user"]["id"])

                if "length" in posted:
                    if posted.get("length").strip() == config.METRIC_LENGTH_F or \
                            posted.get("length").strip() == config.METRIC_LENGTH_M:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "length_metric": posted.get("length").strip()
                            })
                        )
                        session_setup(session["profile"]["user"]["id"])
        if "year" in posted:
            year1 = posted.get("year").strip()
            print("Year",year1)
            customer_report = fetch_customer_report(
            session["profile"]["user"]["id"],year1)


    return render_template(
        'admin/settings/profile.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USERS_ACTIVE='active',
        PROFILE=session['profile'],
        IGHUSERS = igh_users,
        CLENGTH = c_length,
        CFIVEL = c_five_length,
        CUSTOMER_REPORT=customer_report,
        data={"date": str(datetime.now().year)}
    )

@igh_settings.route('profile/deactivate/<string:id>', methods=['POST', 'GET'])
def deactivate_user(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
   

    user_admin = user.fetch_one(id,True).get_json()['real_id']
    user_admin_name = user.fetch_one(id,True).get_json()['first_name']

    deactivate = user.deactivate(id)

    if deactivate:
        user_rol = user_roles.fetch_by_user(user_admin).get_json()

        if user_rol != {}:
            user_rol_id = user_rol['id']
            deavtivate_user_rol = user_roles.deactivate(user_rol_id)

            _customer_d_log = logs.add_new(
                             json.dumps({
                                    "action": "Admin account is Deactivated ",
                                    "ref": user_admin_name,
                                    "ref_id": user_admin,
                                    "creator_id" : session["profile"]["user"]["id"] 
                                    }),
                                    
                                    )
            print(str(_customer_d_log[0].get_json()))
            session.clear()
            
            




    return render_template(
        'admin/settings/deactivate.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USERS_ACTIVE='active',
       
        # PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )

@igh_settings.route('profile/activate/<string:id>', methods=['POST', 'GET'])
def activate_user(id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
   

    user_admin = user.fetch_one(id,True).get_json()['real_id']
    user_admin_name = user.fetch_one(id,True).get_json()['first_name']
    deactivate = user.activate(id)

    if deactivate:
        user_rol = user_roles.fetch_by_user(user_admin).get_json()

        if user_rol != {}:
            user_rol_id = user_rol['id']
            deavtivate_user_rol = user_roles.activate(user_rol_id)


            _customer_a_log = logs.add_new(
                             json.dumps({
                                    "action": "Admin account is Activated ",
                                    "ref": user_admin_name,
                                    "ref_id": user_admin,
                                    "creator_id" : session["profile"]["user"]["id"] 
                                    }),
                                    
                                    )
            print(str(_customer_a_log[0].get_json()))




    return render_template(
        'admin/settings/activate.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USERS_ACTIVE='active',
       
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )



@igh_settings.route('/profile/<user_id>', methods=['POST', 'GET'])
def user_profile(user_id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _user = []
    business_report = []
    revenue_report = []
    customer_report = []

    if valid_uuid(user_id):
        _staff_info = {}
        _role = {}
        _alert_config = {}

        business_report = fetch_business_report(user_id)
        revenue_report = fetch_revenue_report(user_id)
        customer_report = fetch_customer_report(user_id,2022)

        user_detail = user.fetch_one(user_id, True).get_json()
        if user_detail:
            _staff_info = igh.fetch_by_user(user_detail["real_id"]).get_json()
            _role = user_roles.fetch_by_user(user_detail["real_id"]).get_json()
            _alert_config = alert.fetch_alert_config(
                user_detail["real_id"])[0].get_json()

        _user = {
            "staff_info": _staff_info,
            "detail": user_detail,
            "role": _role,
            "alert_config": _alert_config,
        }
        print("Users",user_detail)

    posted = request.form
    if posted:
        if "avatar" in posted:
            saved_filename = ""

            image_file = request.files.get("image_file")
            if image_file:
                actual_filename = secure_filename(image_file.filename)
                pref_filename = user_id + "_" + str(uuid.uuid4())
                saved_filename = files.uploadFile(
                    request.files.get("image_file"),
                    "avatar",
                    pref_filename
                )

                if saved_filename:
                    avatar_media = media.add_new(
                        json.dumps({
                            "file_url": "avatar/" + saved_filename,
                            "caption": actual_filename,
                            "creator_id": user_id,
                        }),
                        True
                    )
                    if avatar_media[1] == 200:
                        _user = user.edit(
                            user_id,
                            json.dumps({
                                "avatar_media": avatar_media[0].get_json()['id']
                            }),
                            True
                        )
                        if _user[1] == 200:

                            return jsonify({
                                "status": True,
                                "avatar": str(saved_filename)
                            })

            return jsonify({
                "status": False,
            })

        if "password" in posted:
            password1 = posted.get("new_password").strip()
            password2 = posted.get("confirm_password").strip()
            if password1 and password2 and password1 == password2:
                user_real_id = user.fetch_one(user_id,True).get_json()['real_id']
                _this_auth = auth.fetch_by_user(user_real_id).get_json()
                if _this_auth:
                    _password_reset = auth.edit(
                        _this_auth['id'],
                        json.dumps({
                            "password": password1
                        })
                    )
                    if _password_reset[1] == 200:
                        return jsonify({
                            "status": True,
                        })

            return jsonify({
                "status": False,
            })

        if "metric" in posted:
            _config = configs.fetch_by_user(
                session["profile"]["user"]["real_id"]).get_json()
            if _config:
                if "temp" in posted:
                    if posted.get("temp").strip() == config.METRIC_TEMP_C or \
                            posted.get("temp").strip() == config.METRIC_TEMP_F:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "temp_metric": posted.get("temp").strip()
                            })
                        )

                if "liquid" in posted:
                    if posted.get("liquid").strip() == config.METRIC_LIQUID_L or \
                            posted.get("liquid").strip() == config.METRIC_LIQUID_GAL:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "liquid_metric": posted.get("liquid").strip()
                            })
                        )

                if "length" in posted:
                    if posted.get("length").strip() == config.METRIC_LENGTH_F or \
                            posted.get("length").strip() == config.METRIC_LENGTH_M:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "length_metric": posted.get("length").strip()
                            })
                        )
        

    return render_template(
        'admin/settings/user-profile.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USERS_ACTIVE='active',
        PROFILE=session['profile'],
        USER=_user,
        BUSINESS_REPORT=business_report,
        REVENUE_REPORT=revenue_report,
        CUSTOMER_REPORT=customer_report,
        data={"date": str(datetime.now().year)}
    )


@igh_settings.route('/profile/edit', methods=['POST', 'GET'])
def edit_profile():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    action_response = {}

    _roles = roles.fetch_by_user_realm(config.USER_IGH).get_json()

    customer_report = fetch_customer_report(session["profile"]["user"]["id"],2022)

    posted = request.form
    if posted:
        if "avatar" in posted:
            saved_filename = ""

            image_file = request.files.get("image_file")
            print("Filename",image_file)
            if image_file:
                actual_filename = secure_filename(image_file.filename)
                pref_filename = session["profile"]["user"]["id"] + \
                    "_" + str(uuid.uuid4())
                saved_filename = files.uploadFile(
                    request.files.get("image_file"),
                    "avatar",
                    pref_filename
                )

                if saved_filename:
                    avatar_media = media.add_new(
                        json.dumps({
                            "file_url": "avatar/" + saved_filename,
                            "caption": actual_filename,
                            "creator_id": session["profile"]["user"]["id"],
                        }),
                        True
                    )
                    if avatar_media[1] == 200:
                        _user = user.edit(
                            session["profile"]["user"]["id"],
                            json.dumps({
                                "avatar_media": avatar_media[0].get_json()['id']
                            }),
                            True
                        )
                        if _user[1] == 200:
                            session_setup(session["profile"]["user"]["id"])

                            return jsonify({
                                "status": True,
                                "avatar": str(saved_filename)
                            })

            return jsonify({
                "status": False,
            })

        if "password" in posted:
            password1 = posted.get("new_password").strip()
            password2 = posted.get("confirm_password").strip()
            if password1 and password2 and password1 == password2:
                _this_auth = auth.fetch_by_user(
                    session["profile"]["user"]["real_id"]).get_json()
                if _this_auth:
                    _password_reset = auth.edit(
                        _this_auth['id'],
                        json.dumps({
                            "password": password1
                        })
                    )
                    if _password_reset[1] == 200:
                        return jsonify({
                            "status": True,
                        })

            return jsonify({
                "status": False,
            })

        if "edit_profile" in posted:
            fullname = posted.get("fullname").strip()
            # email_address = posted.get("email_address").strip()
            phone_number = posted.get("phone_number").strip()
            role = posted.get("role").strip()
            alerts_subscription = posted.get("alerts_subscription").strip()

            # User
            if fullname and phone_number:
                _names = fullname.split(" ")

                first_name = _names[0]
                last_name = ""
                if len(_names) > 1:
                    _names.pop(0)
                    last_name = " " . join(_names)

                _user = user.edit(
                    session["profile"]["user"]["id"],
                    json.dumps({
                        "first_name": first_name,
                        "last_name": last_name,
                        "phone_number": phone_number,
                        # "email_address": email_address,
                    }),
                )
                if _user[1] != 200:
                    action_response = {
                        "type": "danger",
                        "message": str(_user[0].get_json()['error'])
                    }

            # Role
            # print("Rolessssssss",session["profile"]["role"]["id"])
            role_id = user_roles.fetch_one(session["profile"]["role"]["id"],True).get_json()
            print("Role Details",role_id)
            if role_id['role']['name'] != 'Super Admin' and role:
                _user_role_update = user_roles.edit(
                    session["profile"]["role"]["id"],
                    json.dumps({
                        "role_id": role
                    })
                )

                if _user_role_update[1] != 200:
                    action_response = {
                        "type": "danger",
                        "message": str(_user_role_update[0].get_json()['error'])
                    }

            else:
                 action_response = {
                    "type": "danger",
                    "message": "Role can't update for SuperAdmin."
                    }

            # Alerts Config
            if alerts_subscription:
                _alerts_subscription_update = alert.set_alert_config(
                    session["profile"]["user"]["id"],
                    alerts_subscription,
                    session["profile"]["user"]["id"]
                )

            if not action_response:
                action_response = {
                    "type": "success",
                    "message": "Profile updated successfully."
                }

            session_setup(session["profile"]["user"]["id"])

    return render_template(
        'admin/settings/profile-edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USERS_ACTIVE='active',
        PROFILE=session['profile'],
        ROLES=_roles,
        ACTION_RESPONSE=action_response,
        CUSTOMER_REPORT=customer_report,
        data={"date": str(datetime.now().year)}
    )


@igh_settings.route('/users', methods=['POST', 'GET'])
def user_invites():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _invites = []
    _total_invites = invites.fetch_by_realm(
        config.USER_IGH, config.STATUS_ACTIVE).get_json()
    # _total_invites = invites.fetch_by_onlyrealm(config.USER_IGH).get_json()
    for _ti in _total_invites:
        _role = {}
        _alert_config = {}
        _user = user.fetch_one(_ti["user"]["id"], True).get_json()
        if _user:
            _role = user_roles.fetch_by_user(_user["real_id"]).get_json()
            _alert_config = alert.fetch_alert_config(_user["real_id"])[
                0].get_json()
        _invites.append({
            "user": _ti,
            "role": _role,
            "alert_config": _alert_config,
        })

    _actives = []
    _active = igh.fetch_all().get_json()
    print("Active users",_active)
    for _a in _active:
        _role = {}
        _alert_config = {}
        if _a['user'] != {}:
         _user = user.fetch_one(_a["user"]["id"], True).get_json()
         if _user:
            _role = user_roles.fetch_by_user(_user["real_id"]).get_json()
            _alert_config = alert.fetch_alert_config(_user["real_id"])[
                0].get_json()
         _actives.append({
            "user": _a,
            "role": _role,
            "alert_config": _alert_config,
         })

    _inactives = []
    _inactive = igh.fetch_all().get_json()
    print("Inactive users",_inactive)

    if _inactive != [] :
     for _a in _inactive:
        _role = {}
        _alert_config = {}
        # print("USER ID ",_a["user"]["id"])
        if _a['user'] != {}:
         _user = user.fetch_one(_a["user"]["id"], True).get_json()
         if _user:
            _role = user_roles.fetch_by_user(_user["real_id"]).get_json()
            _alert_config = alert.fetch_alert_config(_user["real_id"])[
                0].get_json()
         _inactives.append({
            "user": _a,
            "role": _role,
            "alert_config": _alert_config,
         })

    customer_report = customer_report = fetch_customer_report(
        session["profile"]["user"]["id"],2022)

    posted = request.form
    if posted:
        # if "avatar" in posted:
        #     saved_filename = ""

        #     image_file = request.files.get("image_file")
        #     if image_file:
        #         actual_filename = secure_filename(image_file.filename)
        #         pref_filename = session["profile"]["user"]["id"] + \
        #             "_" + str(uuid.uuid4())
        #         saved_filename = files.uploadFile(
        #             request.files.get("image_file"),
        #             "avatar",
        #             pref_filename
        #         )

        #         if saved_filename:
        #             avatar_media = media.add_new(
        #                 json.dumps({
        #                     "file_url": "avatar/" + saved_filename,
        #                     "caption": actual_filename,
        #                     "creator_id": session["profile"]["user"]["id"],
        #                 }),
        #                 True
        #             )
        #             if avatar_media[1] == 200:
        #                 _user = user.edit(
        #                     session["profile"]["user"]["id"],
        #                     json.dumps({
        #                         "avatar_media": avatar_media[0].get_json()['id']
        #                     }),
        #                     True
        #                 )
        #                 if _user[1] == 200:
        #                     session_setup(session["profile"]["user"]["id"])

        #                     return jsonify({
        #                         "status": True,
        #                         "avatar": str(saved_filename)
        #                     })

        #     return jsonify({
        #         "status": False,
        #     })

        if "password" in posted:
            password1 = posted.get("new_password").strip()
            password2 = posted.get("confirm_password").strip()
            if password1 and password2 and password1 == password2:
                _this_auth = auth.fetch_by_user(
                    session["profile"]["user"]["real_id"]).get_json()
                if _this_auth:
                    _password_reset = auth.edit(
                        _this_auth['id'],
                        json.dumps({
                            "password": password1
                        })
                    )
                    if _password_reset[1] == 200:
                        return jsonify({
                            "status": True,
                        })

            return jsonify({
                "status": False,
            })

        if "metric" in posted:
            _config = configs.fetch_by_user(
                session["profile"]["user"]["real_id"]).get_json()
            if _config:
                if "temp" in posted:
                    if posted.get("temp").strip() == config.METRIC_TEMP_C or \
                            posted.get("temp").strip() == config.METRIC_TEMP_F:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "temp_metric": posted.get("temp").strip()
                            })
                        )
                        session_setup(session["profile"]["user"]["id"])

                if "liquid" in posted:
                    if posted.get("liquid").strip() == config.METRIC_LIQUID_L or \
                            posted.get("liquid").strip() == config.METRIC_LIQUID_GAL:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "liquid_metric": posted.get("liquid").strip()
                            })
                        )
                        session_setup(session["profile"]["user"]["id"])

                if "length" in posted:
                    if posted.get("length").strip() == config.METRIC_LENGTH_F or \
                            posted.get("length").strip() == config.METRIC_LENGTH_M:
                        _config_update = configs.edit(
                            _config["id"],
                            json.dumps({
                                "length_metric": posted.get("length").strip()
                            })
                        )
                        session_setup(session["profile"]["user"]["id"])

    return render_template(
        'admin/settings/users.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USERS_ACTIVE='active',
        PROFILE=session['profile'],
        INVITES=_invites,
        ACTIVE_USERS=_actives,
        INACTIVE_USERS=_inactives,
        CUSTOMER_REPORT=customer_report,
        data={"date": str(datetime.now().year)}
    )


@igh_settings.route('/users/add', methods=['POST', 'GET'])
def add_user():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _multiple = []
    _roles = roles.fetch_by_user_realm(config.USER_IGH).get_json()

    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:
            _full_name = posted.getlist("full_name[]")
            _employee_id = posted.getlist("employee_id[]")
            _email_address = posted.getlist("email_address[]")
            _password = posted.getlist("password[]")

            x = 0
            for _name in _full_name:
                _multiple.append({
                    "full_name": _full_name[x],
                    "employee_id": _employee_id[x],
                    "email_address": _email_address[x],
                    "password": _password[x],
                })
                x = x + 1

        else:
            _full_name = posted.getlist("full_name[]")
            _employee_id = posted.getlist("employee_id[]")
            _role = posted.getlist("role[]")
            _email_id = posted.getlist("email_id[]")
            _password = posted.getlist("password[]")

            x = 0
            for _f_n in _full_name:
                if _f_n and _email_id[x]:
                    y = 1
                    _first_name = ""
                    _last_name = ""
                    _names = _f_n.split(" ")
                    for _name in _names:
                        if y == 1:
                            _first_name = _name
                        else:
                            _last_name += _name + " "
                        y = y + 1

                    _new_user = user.add_new(
                        json.dumps({
                            "first_name": _first_name,
                            "last_name": _last_name,
                            "email_address": _email_id[x],
                            "employee_id": _employee_id[x],
                            "role_id": _role[x],
                            "user_realm": config.USER_IGH
                        }),
                        True
                    )
                    print(str(_new_user[0].get_json()))

                    if _new_user:

                      user_main_id = user.fetch_by_email(_email_id[x]).get_json()['id']
                      user_real_id = user.fetch_one(user_main_id,True).get_json()['real_id']

                    _user_log = logs.add_new(
                         json.dumps({
                            "action": "Added a new Admin User",
                            "ref": _first_name,
                            "ref_id": user_real_id,
                            "customer_id": None,
                            "creator_id" : session["profile"]["user"]["id"]
                         }),
                         
                         )

                    print(str(_user_log[0].get_json()))

                    if _new_user[1] == 200:
                        pass
                        # add password
                    x = x + 1

    return render_template(
        'admin/settings/add-user.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USER_INVITES_ACTIVE='active',
        PROFILE=session['profile'],
        ROLES=_roles,
        MULTIPLE=_multiple,
        # data={"date":str(datetime.now().year), "today": datetime.now()}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@igh_settings.route('/users/add-multiple', methods=['POST', 'GET'])
def bulk_add_user():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    uploaded_file = request.files.get("file-to-upload")
    if uploaded_file:
        if uploaded_file.filename.rsplit('.', 1)[1].lower() == "csv":
            users = []
            str_file_value = uploaded_file.read().decode('utf-8')
            file_t = str_file_value.splitlines()
            csv_reader = csv.reader(
                file_t, delimiter=',', skipinitialspace=True)
            _users = list(csv_reader)
            print("CSV reader",_users)
            _users.pop(0)
            for _user in _users:
                users.append({
                    "full_name": _user[0] + " " + _user[1],
                    "employee_id": _user[2],
                    "email_address": _user[3],
                    "password": _user[4],
                })
            return jsonify({"uploaded": users})
        else:
            return jsonify({"error": "Invalid file format selected."})

    return render_template(
        'admin/settings/bulk-add-user.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USER_INVITES_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year), "today": datetime.now()}
    )


@igh_settings.route('/users/edit/<user_id>', methods=['POST', 'GET'])
def edit_user_profile(user_id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    action_response = {}

    _roles = roles.fetch_by_user_realm(config.USER_IGH).get_json()

    _user = []
    customer_report = []
    if valid_uuid(user_id):

        posted = request.form
        if posted:
            if "avatar" in posted:
                saved_filename = ""

                image_file = request.files.get("image_file")
                
                if image_file:
                    actual_filename = secure_filename(image_file.filename)
                    pref_filename = user_id + "_" + str(uuid.uuid4())
                    print("Imagefile",image_file)
                    print("pref_filename",pref_filename)
                    print("actual file name",actual_filename)
                    saved_filename = files.uploadFile(
                        request.files.get("image_file"),
                        "avatar",
                        pref_filename
                    )

                    if saved_filename:
                        avatar_media = media.add_new(
                            json.dumps({
                                "file_url": "avatar/" + saved_filename,
                                "caption": actual_filename,
                                "creator_id": user_id,
                            }),
                            True
                        )
                        if avatar_media[1] == 200:
                            _user = user.edit(
                                user_id,
                                json.dumps({
                                    "avatar_media": avatar_media[0].get_json()['id']
                                }),
                                True
                            )
                            if _user[1] == 200:

                                return jsonify({
                                    "status": True,
                                    "avatar": str(saved_filename)
                                })

                return jsonify({
                    "status": False,
                })

            if "password" in posted:
                password1 = posted.get("new_password").strip()
                password2 = posted.get("confirm_password").strip()
                if password1 and password2 and password1 == password2:
                    _this_auth = auth.fetch_by_user(
                        session["profile"]["user"]["real_id"]).get_json()
                    if _this_auth:
                        _password_reset = auth.edit(
                            _this_auth['id'],
                            json.dumps({
                                "password": password1
                            })
                        )
                        if _password_reset[1] == 200:
                            return jsonify({
                                "status": True,
                            })

                return jsonify({
                    "status": False,
                })

            if "edit_profile" in posted:
                fullname = posted.get("fullname").strip()
                # email_address = posted.get("email_address").strip()
                phone_number = posted.get("phone_number").strip()
                role = posted.get("role").strip()
                alerts_subscription = posted.get("alerts_subscription").strip()

                # User
                if fullname and phone_number:
                    x = 1
                    first_name = ""
                    last_name = ""
                    _names = fullname.split(" ")
                    for _name in _names:
                        if x == 1:
                            first_name = _name
                        else:
                            last_name += _name + " "

                        x = x + 1

                    _user_update = user.edit(
                        user_id,
                        json.dumps({
                            "first_name": first_name.strip(),
                            "last_name": last_name.strip(),
                            "phone_number": phone_number.strip(),
                            # "email_address": email_address.strip(),
                        }),
                    )

                   

                    if _user_update[1] != 200:
                        action_response = {
                            "type": "danger",
                            "message": str(_user_update[0].get_json()['error'])
                        }

                # Role
                user_real = user.fetch_one(user_id,True).get_json()['real_id']
                user_role_id = user_roles.fetch_by_user(user_real).get_json()['id']
                if role:
                    _user_role_update = user_roles.edit(
                        user_role_id,
                        json.dumps({
                            "role_id": role
                        })
                    )

                    if _user_role_update:
                        role_name = roles.fetch_one(role,True).get_json()['name']

                    _user_log = logs.add_new(
                        json.dumps({
                            "action": "Edited for a new admin user role",
                            "ref": role_name,
                            "ref_id": user_real,
                            "creator_id" : session["profile"]["user"]["id"]
                        }),
                        
                         )

                    print(str(_user_log[0].get_json()))

                    if _user_role_update[1] != 200:
                        action_response = {
                            "type": "danger",
                            "message": str(_user_role_update[0].get_json()['error'])
                        }

                # Alerts Config
                if alerts_subscription:
                    _alerts_subscription_update = alert.set_alert_config(
                        user_id,
                        alerts_subscription,
                        session["profile"]["user"]["id"]
                    )

                if not action_response:
                    action_response = {
                        "type": "success",
                        "message": "Profile updated successfully."
                    }

        _staff_info = {}
        _role = {}
        _alert_config = {}

        customer_report = fetch_customer_report(user_id,2022)

        user_detail = user.fetch_one(user_id, True).get_json()
        if user_detail:
            _staff_info = igh.fetch_by_user(user_detail["real_id"]).get_json()
            _role = user_roles.fetch_by_user(user_detail["real_id"]).get_json()
            _alert_config = alert.fetch_alert_config(
                user_detail["real_id"])[0].get_json()

        _user = {
            "staff_info": _staff_info,
            "detail": user_detail,
            "role": _role,
            "alert_config": _alert_config,
        }

    return render_template(
        'admin/settings/edit-user-profile.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USERS_ACTIVE='active',
        PROFILE=session['profile'],
        ROLES=_roles,
        ACTION_RESPONSE=action_response,
        USER=_user,
        CUSTOMER_REPORT=customer_report,
        data={"date": str(datetime.now().year)}
    )


@igh_settings.route('/roles', methods=['POST', 'GET'])
def all_roles():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _roles = roles.fetch_all().get_json()
    _modules = []
    _actives = []
    _inactives = []
    action_response = {}

    posted = request.form
    if posted:
        if "action" in posted:
            if posted.get("action") == "role_users" and "role_id" in posted:
                _role_users = []

                _role = roles\
                    .fetch_one(posted.get("role_id"), True)\
                    .get_json()
                if _role:
                    _role_users = user_roles\
                        .fetch_by_role(_role["real_id"])\
                        .get_json()

                return jsonify(_role_users)

            if posted.get("action") == "add_role" and "role_name" in posted:
                
                exists = roles.fetch_by_name_realm(posted.get("role_name"),1).get_json()
                if exists:
                     
                 action_response = {
                            "type": "danger",
                            "message": "Role is already exists please add new role"
                              }

                else:

                 
                 return roles.add_new(
                    json.dumps({
                        "name": posted.get("role_name"),
                        "description": posted.get("role_name"),
                        "user_realm": config.USER_IGH,
                        "creator_id": session['profile']['user']['id'],
                    }),
                    True
                  )
                  

            if posted.get("action") == "activate_role" and "role_id" in posted:

                _role = roles\
                    .fetch_one(posted.get("role_id"), True)\
                    .get_json()
                _role_namezs = _role['name']
                _role_reals = _role['real_id']

                _user_log = logs.add_new(
                        json.dumps({
                            "action": "Role is Activated",
                            "ref": _role_namezs,
                            "ref_id": _role_reals,
                            "creator_id" : session["profile"]["user"]["id"]
                        }),
                        
                         )
                return roles.activate(posted.get("role_id"))

            if posted.get("action") == "deactivate_role" and "role_id" in posted:

                _role = roles\
                    .fetch_one(posted.get("role_id"), True)\
                    .get_json()
                _role_namez = _role['name']
                _role_real = _role['real_id']

                _user_log = logs.add_new(
                        json.dumps({
                            "action": "Role is deactivated",
                            "ref": _role_namez,
                            "ref_id": _role_real,
                            "creator_id" : session["profile"]["user"]["id"]
                        }),
                        
                         )

                print(str(_user_log[0].get_json()))
                return roles.deactivate(posted.get("role_id"))
                

            if posted.get("action") == "module_permissions" and \
                    "module_id" in posted and "role_id" in posted:

                _resp = []

                _module_id = posted.get("module_id")
                _role_id = posted.get("role_id")

                if valid_uuid(_module_id) and valid_uuid(_role_id):
                    _role = roles.fetch_one(_role_id, True).get_json()
                    _module = module.fetch_one(_module_id, True).get_json()
                    if _role and _module:
                        _feats = features.fetch_by_module(
                            _module['real_id'], True).get_json()
                        for _feat in _feats:
                            _mod_perm = {}
                            _perms = permissions.fetch_by_role_feature(
                                _role["real_id"], _feat['real_id']).get_json()
                            if _perms and _perms['role']['id'] == _role["id"]:
                                _mod_perm[str(_feat['id'])] = 1
                            else:
                                _mod_perm[str(_feat['id'])] = 0

                            _resp.append(_mod_perm)

                return jsonify(_resp)

            if posted.get("action") == "update_roles" and "roles" in posted:
                _roles = json.loads(posted.get("roles"))
                for role in _roles:
                    roles.edit(
                        role["role_id"],
                        json.dumps(
                            {
                                "name": role["role_name"]
                            }
                        )
                    )
                return jsonify(_roles)

            if posted.get("action") == "update_permission" \
                    and "add_remove" in posted \
                    and "module_id" in posted \
                    and "role_id" in posted\
                    and "feat_id" in posted:

                _module = module.fetch_one(
                posted.get("module_id"), True).get_json()
                _role = roles.fetch_one(posted.get("role_id"), True).get_json()
                _feat = features.fetch_one(
                    posted.get("feat_id"), True).get_json()

                if _role and _feat:
                    if posted.get("add_remove") == "1":
                        _new_perm = permissions.add_new(
                            json.dumps({
                                "role_id": _role['id'],
                                "module_feature_id": _feat['id'],
                                "creator_id": session["profile"]["user"]["id"]
                            })
                        )
                        return jsonify({"message": _new_perm[0].get_json()})
                    else:
                        _permission = permissions.fetch_by_role_feature(
                            _role["real_id"], _feat["real_id"], True).get_json()
                        if _permission:
                            _deactivate = permissions.deactivate(
                                _permission["id"])

                        return jsonify({"message": "removed"})

    _mods = module.fetch_by_user_realm(config.USER_IGH, True).get_json()
    for _mod in _mods:
        _mod["feats"] = features.fetch_by_module(
            _mod['real_id'], True).get_json()

        _modules.append(_mod)

    return render_template(
        'admin/settings/roles.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        PROFILE=session['profile'],
        ROLES=_roles,
        MODULES=_modules,
        FORM_ACTION_RESPONSE=action_response,
        ACTIVE_ROLES=_actives,
        INACTIVE_ROLES=_inactives,
        data={"date": str(datetime.now().year)}
    )


@igh_settings.route('/roles/edit', methods=['POST', 'GET'])
def edit_role():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _roles = roles.fetch_all().get_json()
    _modules = []
    _mods = module.fetch_by_user_realm(config.USER_IGH, True).get_json()
    for _mod in _mods:
        _mod["feats"] = features.fetch_by_module(
            _mod['real_id'], True).get_json()

        _modules.append(_mod)

    return render_template(
        'admin/settings/roles-edit.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        PROFILE=session['profile'],
        ROLES=_roles,
        MODULES=_modules,
        data={"date": str(datetime.now().year)}
    )


@igh_settings.route('/roles/<role_id>/view', methods=['POST', 'GET'])
def view_role(role_id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    role_real_id = roles.fetch_one(role_id,True).get_json()['real_id']
    role_real_idk = roles.fetch_one(role_id,True).get_json()
    
    users_by_role = []
    users_by_role = user_roles.fetch_by_role(role_real_id).get_json()

    return render_template(
        'admin/settings/user-role-view.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        USERROLES = users_by_role,
        ROLENAME = role_real_idk,
        ADMIN_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )


@igh_settings.route('/activity-log', methods=['POST', 'GET'])
def activity_log():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    logz = []
    logz = logs.fetch_all().get_json()

    return render_template(
        'admin/settings/activity-log.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        LOGS = logz,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date": str(datetime.now().year)}
    )
