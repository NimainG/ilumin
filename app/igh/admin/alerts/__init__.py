from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig

from app.igh import portal_check
from app.igh.admin import contact_person

from app.igh import session_setup

from app.core import alert

from app.helper import valid_uuid

from app.models import db
from werkzeug.wrappers import response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from app.models import Alert

igh_alerts = Blueprint('igh_alerts', __name__,  static_folder='../../static', template_folder='.../../templates')
igh_alerts.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@igh_alerts.route('/', methods=['POST', 'GET'], defaults={"page": 1})
@igh_alerts.route('/<int:page>', methods=['POST', 'GET'])
def alerts(page):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    
    page = page
    prev = page-1
    next = page+1
    no_of_posts = config.PAGE_LIMIT
    _alerts_page = alert.fetch_all().get_json()
    _alerts = alert.fetch_all1(page).get_json()
    total = {}
    total = round(len(_alerts_page)/no_of_posts)


    if request.method == 'POST' and 'tag_alert' in request.form:
        tag = request.form["tag_alert"]
        if tag:
            # search = "%{}%".format(tag)
            search = tag
            if search:

             _taggy = []
             if search.isnumeric() == True:
               _taggy = db.session.query(Alert).filter(or_((Alert.customer_id == search),(Alert.type == search),(Alert.creator_id == search))).limit(no_of_posts).offset((page-1)*no_of_posts).all()
               response = []
               for alrt in _taggy:

                   response.append(
                            alert.alert_obj(alrt))
               _alerts = jsonify(response).get_json()



   
    return render_template(
        'admin/alerts/main.html',
        ADMIN_PORTAL=True,
        TALERT = _alerts_page,
        ALERT = _alerts,
        ADMIN_PORTAL_ALERTS_ACTIVE='active',
        PROFILE=session['profile'],
        prev=prev, next=next,
        total=total,
        data={"date":str(datetime.now().year)}
    )
