from asyncio import shield
from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import datetime as dt
import random
import uuid
import calendar

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template,make_response
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime
from dateutil.relativedelta import relativedelta
from sqlalchemy import extract
from app.core.user import logs
from app.config import activateConfig


from app.core.report import waste_pollution_management 
from app.core.report import fertilizer_application
from app.core.report import agrochemical_application
from app.core.report import preharvest_checklist
from app.core.report import harvest
from app.core.report import scouting
from app.core.report import other
from app.core import report

from app.core.user import customers as _customer
from app.core import customer as customerfy
from app.core.customer import farms as _farms, shields
from app.core import farm as ownfarms
from app.core.customer import greenhouses as _greenhouses
from app.core.customer import shields as _shields
from app.core import shield as _core_shield
from app.core import greenhouse as _core_green
from app.core.customer import screenhouses as _screenhouses
from app.core.customer import shadenets as _shadenets
from app.core.customer import farm_greenhouses as _farm_greehouses
from app.core.customer import greenhouse_shields
from app.core import supply
from app.core.farm import actions
from app.models import Report, db
from app.models import User
from app.models import UserRole
from app.models import Role
from app.models import CustomerUser
from app.core.customer import shadenets
from app.core.customer import screenhouses
from app.igh import portal_check
from app.core import maintenance
from app.core.maintenance import pests_diseases
from app.core.maintenance import agronomist_visit
from app.core.maintenance import greenhouse_issues
from app.core.maintenance import screenhouse_issues
from app.core.maintenance import shadenet_issues
from app.core.maintenance import shield_issues
from app.core.maintenance import others
from app.core.data import shielddata
from app.models import ShieldCompleteData
from app.core.planting import crops

from app.helper import valid_uuid, valid_date

customer = Blueprint('customer', __name__, template_folder='../templates', static_folder='../static')
customer.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def contact_person(contact_person_id):
    _response = {}
    _user = db\
        .session\
        .query(User, UserRole, Role, CustomerUser)\
        .join(UserRole, UserRole.user_id == User.id)\
        .join(Role, Role.id == UserRole.role_id)\
        .outerjoin(CustomerUser, CustomerUser.user_id == User.id)\
        .filter(User.id == contact_person_id)\
        .filter(User.status > config.STATUS_DELETED)\
        .first()
    if _user:
        _response = {
            "id": _user.User.uid,
            "first_name": _user.User.first_name,
            "last_name": _user.User.last_name,
            "phone_number": _user.User.phone_number,
            "email_address": _user.User.email_address,
            "id_number": _user.CustomerUser.id_number if _user.CustomerUser and _user.CustomerUser.id_number is not None else "-",
            "role": _user.Role.name,
            "status": "Active" if _user.User.status == config.STATUS_ACTIVE else "Inactive",
            "created": _user.User.created_at,
            "updated": _user.User.modified_at,
        }
    
    return _response


def fetch_customer_water(device):
    customer_irrigation = {}
    date = datetime.now().strftime('%Y-%m-%d')
    _data_date = date
    water_per_year = {}
    customer_all = []
    device_data_2022 = []
    device_data_2021 = []
    device_data_2020 = []
    device_data_2019 = []
    device_data_2018 = []
    _water = 0
    _water1 = 0
    _water2 = 0
    _water3 = 0
    _water4 = 0
   
   

    if device:
        device_data_2022 = shielddata.fetch_by_date_boron_id(device,'2022-01-01 00:00:00','2022-12-31 00:00:00').get_json()
        device_data_2021 = shielddata.fetch_by_date_boron_id(device,'2021-01-01 00:00:00','2021-12-31 00:00:00').get_json()
        # print("2021 data",device_data_2021)
        device_data_2020 = shielddata.fetch_by_date_boron_id(device,'2020-01-01 00:00:00','2020-12-31 00:00:00').get_json()
        print("2020 data",device_data_2020)
        device_data_2019 = shielddata.fetch_by_date_boron_id(device,'2019-01-01 00:00:00','2019-12-31 00:00:00').get_json()
        print("2019 data",device_data_2019)
        device_data_2018 = shielddata.fetch_by_date_boron_id(device,'2018-01-01 00:00:00','2018-12-31 00:00:00').get_json()
        print("2018 data",device_data_2018)

        if device_data_2022 != [] :

            _water = 0
            for irr in device_data_2022:
                   try: 
                    _water += (irr[('water_dispensed')])
                    water_f = str(_water/1000)
                    print("water",_water)
                   except:
                     print('Can not convert', str ,"to int") 
        if device_data_2021 != [] :

            _water1 = 0
            for irr in device_data_2021:
                   try: 
                    _water1 += (irr[('water_dispensed')])
                    water_f = str(_water1/1000)
                    print("water1",_water1)
                   except:
                     print('Can not convert', str ,"to int") 
        if device_data_2020 != [] :

            _water2 = 0
            for irr in device_data_2020:
                   try: 
                    _water2 += (irr[('water_dispensed')])
                    water_f = str(_water2/1000)
                    print("water2",_water2)
                   except:
                     print('Can not convert', str ,"to int") 

        if device_data_2019 != [] :

            _water3 = 0
            for irr in device_data_2019:
                   try: 
                    _water3 += (irr[('water_dispensed')])
                    water_f = str(_water3/1000)
                    print("water3",_water3)
                   except:
                     print('Can not convert', str ,"to int") 

        if device_data_2018 != [] :

            _water4 = 0
            for irr in device_data_2018:
                   try: 
                    _water4 += (irr[('water_dispensed')])
                    water_f = str(_water4/1000)
                    print("water4",_water4)
                   except:
                     print('Can not convert', str ,"to int") 




        
       
        # jan22 = cu.fetch_by_dates('31/1/2022','1/1/2022').get_json()
        # # print("Jan data",jan22)
       
        
        # feb22 = cu.fetch_by_dates('28/2/2022','1/2/2022').get_json()
        # mar22 = cu.fetch_by_dates('31/3/2022','1/3/2022').get_json()
        # apr22 = cu.fetch_by_dates('30/4/2022','1/4/2022').get_json()
        # may22 = cu.fetch_by_dates('31/5/2022','1/5/2022').get_json()
        # june22 = cu.fetch_by_dates('30/6/2022','1/6/2022').get_json()
        # july22 = cu.fetch_by_dates('31/7/2022','1/7/2022').get_json()
        # aug22 = cu.fetch_by_dates('31/8/2022','1/8/2022').get_json()
        # sept22 = cu.fetch_by_dates('30/9/2022','1/9/2022').get_json()
        # oct22 = cu.fetch_by_dates('31/10/2022','1/10/2022').get_json()
        # nov22 = cu.fetch_by_dates('30/11/2022','1/11/2022').get_json()
        # dec22 = cu.fetch_by_dates('31/12/2022','1/12/2022').get_json()
        # c_length = len(customer_all)
        # jlen = len(jan22)
        # flen = len(feb22)
        # mlen = len(mar22)
        # aplen = len(apr22)
        # maylen = len(may22)
        # junlen = len(june22)
        # julylen = len(july22)
        # auglen = len(aug22)
        # seplen = len(sept22)
        # octlen = len(oct22)
        # novlen = len(nov22)
        # declen = len(dec22)

        water_per_year ={

            "levels":["2022",
            "2021","2020","2019","2018"],
            "series":[_water,_water1,_water2,_water3,_water4],

            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }


        # # print("Customer All",jlen)
        # customer_report = {
        #     "current_yr": year,
        #     "customers_y": c_length,
        #     "customers_5y": c_length,
        #     "labels": [
        #         "Jan'22",
        #         "Feb'22",
        #         "Mar'22",
        #         "Apr'22",
        #         "May'22",
        #         "Jun'22",
        #         "Jul'22",
        #         "Aug'22",
        #         "Sep'22",
        #         "Oct'22",
        #         "Nov'22",
        #         "Dec'22"
        #     ],
        #     "series": [
        #         [ jlen,
        #          flen,
        #          mlen,
        #          aplen,
        #          maylen,
        #          junlen,
        #          julylen,
        #          auglen,
        #          seplen,
        #          octlen,
        #          novlen,
        #          declen]
        #     ],
        #     "axis": {
        #         "y_axis": "Customers",
        #         "x_axis": "",
        #     },
        #     "metric": "K"
        # }
        # if year == "2022":
        #  customer_report = {
        #     "current_yr": year,
        #     "customers_y": c_length,
        #     "customers_5y": c_length,
        #     "labels": [
        #         "Jan'22",
        #         "Feb'22",
        #         "Mar'22",
        #         "Apr'22",
        #         "May'22",
        #         "Jun'22",
        #         "Jul'22",
        #         "Aug'22",
        #         "Sep'22",
        #         "Oct'22",
        #         "Nov'22",
        #         "Dec'22"
        #     ],
        #     "series": [
        #         [ jlen,
        #          flen,
        #          mlen,
        #          aplen,
        #          maylen,
        #          junlen,
        #          julylen,
        #          auglen,
        #          seplen,
        #          octlen,
        #          novlen,
        #          declen]
        #     ],
        #     "axis": {
        #         "y_axis": "Customers",
        #         "x_axis": "",
        #     },
        #     "metric": "K"
        # }
       
       

    return water_per_year

@customer.app_template_filter()
def numberFormat(value):
    return format(int(value), ',d')

@customer.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    print(" User ID",_this_org)
    if _this_org:
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    # print("Customers_uid",_cutomer_real_id)
    if _cutomer_real_id != None :
      cutomers_uid = _cutomer_real_id['customer']['id']
    farm_greenhousez =[]
    devices_farm = []
    water_per_year = {}
    greenhouse_device = []
    greenhouse_sh = []
    
    # Error in server at customers_uid
    
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     
     if farm_id_by_action != None :
      swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
      
      customer_farm_idz = _farms.fetch_by_farm_id(farm_id_by_action).get_json()
      if customer_farm_idz != {}:
          customer_farm_idn = customer_farm_idz['id']
          print("Farm id Action",customer_farm_idn)
          real_customer_id_switch = _farms.fetch_one(customer_farm_idn,True).get_json()['real_id']
          farm_greenhousez = _farm_greehouses.fetch_customer_farm(real_customer_id_switch).get_json()
          greenhouse_sh = greenhouse_shields.fetch_by_customer_farmz(real_customer_id_switch).get_json()

          print("Greenhouse Device Data",greenhouse_sh)
         
          
          if farm_greenhousez != []:
           for farm_grn in farm_greenhousez:
             if farm_grn != {} and farm_grn['customer_greenhouse'] != {} and farm_grn['customer_greenhouse']['greenhouse'] != {}:
              customer_greenhouse = farm_grn['customer_greenhouse']['greenhouse']["greenhouse_ref"]
              farm_id_green = farm_grn['id']
              farm_id_green_real = _farm_greehouses.fetch_one(farm_id_green,True).get_json()['real_id']
              print("Greenhouse real ID",farm_id_green_real)
             
            #  greenhouse_real = _core_green.fetch_one(customer_greenhouse,True).get_json()['real_id']
            #  print("Customer green IDS",greenhouse_real)
              greenhouse_device = _core_shield.fetch_by_greenhouses(customer_greenhouse).get_json()
              print("Greenhouse Device",greenhouse_device)
        #    devices_farm = _shields.fetch_by_customer(_customery_final_id).get_json()
     
     
    #  swapped_farm_id = swapped_firm_details['id']
     
    #  swapped_farm_real = ownfarms.fetch_one(swapped_farm_id,True).get_json()['real_id']
   
    else: 
     swapped_firm_details = None

   
    # _this_org = _customer.fetch_one(session["profile"]["user"]["id"], True).get_json()
    
    # print("swapped_farm_id",real_customer_id_switch)

    maintain = []

    # maintain = maintenance.fetch_all().get_json()
    maintain = maintenance.fetch_all_by_customer(_customery_final_id).get_json()
  
   
    # device_info = {}
    # device_info = shielddata.fetch_by_boron("e00fce68a8ec0eaf117e213d").get_json()
    # print("Devices Farmmmmmmmm",devices_farm)
    
    _details = []
    _details = pests_diseases.fetch_by_customer(_customery_final_id).get_json()
    _greenh_issue = []
    _greenh_issue = greenhouse_issues.fetch_by_customer_all(_customery_final_id).get_json()
    _screen_issue = []
    _shandet_issue = []
    _others = []
    _screen_issue = screenhouse_issues.fetch_by_customer(_customery_final_id).get_json()
    _shandet_issue = shadenet_issues.fetch_by_customer_all(_customery_final_id).get_json()
    _others = others.fetch_by_customer_all(_customery_final_id).get_json()
    

    _agronomist_visit = []
    _agronomist_visit = agronomist_visit.fetch_by_customer(_customery_final_id).get_json()
    all_customer_report = []
    all_customer_report = report.fetch_by_customer(_customery_final_id).get_json()
    # print("Mainttain",_agronomist_visit)

    this_farm = None

    data_date = request.args.get('dt')



    posted = request.form

    
    
    # if posted and 'farm' in posted and valid_uuid(posted.get('farm')):
    #     session['farm'] = posted.get('farm')

     
    
    
    if posted and 'action' in posted and posted.get('action') == "swap-farm":
        
           if posted and 'farm_id' in posted and valid_uuid(posted.get('farm_id')):
              session['farm'] = posted.get('farm_id')
              switched_firm_id = posted.get('farm_id')
             
            #   print("Farm swapped id 1",switched_firm_id)
             
              if switched_firm_id != None and farm_action_id != None and _customery_final_id :
                      
                       _farm_action = actions.edit(
                         farm_action_id,
                        json.dumps({
                            "current_firm_uid": switched_firm_id,
                            "customer_id": _customery_final_id
                        }),
                        True
                        )

                       if _farm_action[1] == 200:

                                return jsonify({
                                    "status": True,

                                })
                               
            
              return jsonify({"swapped": 1})
           
   
    

    # Anchor date should change on demand
    anchor_date = valid_date(data_date) if valid_date(data_date) else datetime(2020, 3, 30, 0, 0, 0)

    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    
   
    greenhouses = _greenhouses.fetch_by_customer(_customery_final_id).get_json()
    #shields = _shields.fetch_by_customer(_this_org["real_id"]).get_json()
    # shields = shields.fetch_by_customer(_customery_final_id).get_json()
    # farm_greenhouses = _farm_greehouses.fetch_all().get_json()
    # farm_greenhouses = _farm_greehouses.fetch_customer_farm(real_customer_id_switch).get_json() 
    # print("Farm Greenhouses",farm_greenhouses)
    _customer_screenhouse = screenhouses.fetch_by_customer(_customery_final_id).get_json()
    _customer_shadnets = shadenets.fetch_by_customer(_customery_final_id).get_json()

    devices = []
    users = []
    irrigation_total = 0

    greenhouse_devices = []
    greenhouse = []
    the_farm = []
    _shields_data= []
    plant_journey = []
    

    
   
    # session['greenhouse'] = posted.get('greenhouses_switch')
    green_id = posted.get('greenhouses_switch')
    device_id_chart = posted.get('shields_id')
    print("Greenid",green_id,device_id_chart)
    farm_greenswitch = _farm_greehouses.fetch_one(green_id,True).get_json() 
    # print("Farm greenswitch",farm_greenswitch)
    # print("Farm Greenswitch",farm_greenswitch)
    # session['greenhouse'] = farm_greenswitch
    # print("Swapped",session['greenhouse'])
    water_f = []
    if posted:
      

        month1 = posted.get("imonth")
        year1 = posted.get("iyear")
        daily1 = posted.get("idaily")
        weekly1 = posted.get("iweekly")
        monthly1 = posted.get("imonthly")
        yearly1 = posted.get("iyearly")


        if device_id_chart:
            irrigation_report_data = shielddata.fetch_by_boron_all(device_id_chart).get_json()
            # fetch_customer_water(device_id_chart)
            # device_data = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-01-01 00:00:00','2022-12-31 00:00:00').get_json()
            # print("Irrigation report",device_data)

        print("all dataaaaa",month1,year1,daily1,weekly1,monthly1,yearly1)

        for devices in devices_farm:
            _water = 0
           
            device = devices['shield']['serial']['boron_id']
            print("Devices",device)
            irrigation = shielddata.fetch_by_boron_all(device).get_json()
            # print("irrigation report",irrigation)
            # for dev in device:
            #     for irr in irrigation:
            #        try: 
            #         _water += (irr[('water_dispensed')])
            #         water_f = str(_water/1000)
            #         # print("water",_water)
            #        except:
            #          print('Can not convert', str ,"to int") 
    
    litre = posted.get("options_lit")
    cubic = posted.get("year")
    centigrade = posted.get("options_c")
    farenheight = posted.get("options_f")
    gallon = posted.get("options_gallon")
    # meter = posted.get("options_meter")
    val = posted.get("options")

    print("All valuessss",litre,cubic,centigrade,farenheight,gallon,val)

    logy = logs.fetch_by_customer(_customery_final_id).get_json()
    # logy = logs.fetch_by_customer(_customery_final_id).get_json()
    # print("All logs",logy)

    _supply = supply.fetch_by_customer(_customery_final_id).get_json()
    _shields_data = shields.fetch_by_customer(_customery_final_id).get_json()

    _waste_pollution = waste_pollution_management.fetch_by_customer(_customery_final_id).get_json()
    _fertilizer_app = fertilizer_application.fetch_by_customer(_customery_final_id).get_json()
    _agrochem_app = agrochemical_application.fetch_by_customer(_customery_final_id).get_json()
    _preharv_check = preharvest_checklist.fetch_by_customer(_customery_final_id).get_json()
    _harvest_details = harvest.fetch_by_customer(_customery_final_id).get_json()
    _scouting = scouting.fetch_by_customer(_customery_final_id).get_json()
    _other_reports = other.fetch_by_customer(_customery_final_id).get_json()
    plant_journey = crops.fetch_by_creator(_this_org).get_json()
               
                   


    response = make_response ( render_template(
        'customers/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_DASHBOARD_ACTIVE='active',
        ORG=_this_org,
        FARMS=farms,
        LOGS = logy,
        PLANT = plant_journey,
        GREENHOUSES=len(greenhouses),
        GREENHOUSE=greenhouse,
        WATER = water_f,
        GREENHOUSE_DEVICES=greenhouse_devices,
        DEVICES=len(devices),
        CUSTOMER_DEVICES=devices,
        FARM_GREENHOUSES=farm_greenhousez,
        FARM_GREENSWITCH = farm_greenswitch,
        FDEVICES = greenhouse_device,
        FREALDEVICES = greenhouse_sh,
        FARM=swapped_firm_details,
        IRRIGATION_TOTAL=irrigation_total,
        USER=users,
        TEMP_CHART=[],
        SUPPLY = _supply,
        MAINTAIN = maintain,
        IGREEN = _greenh_issue,
        ISCREEN = _screen_issue,
        ISHAD = _shandet_issue,
        IOTH = _others,
        VISIT = _agronomist_visit,
        CSCREENHOUSE = _customer_screenhouse,
        CSHADENETS = _customer_shadnets,
        PROFILE=session['profile'],
        CID = _customery_final_id,
        WATERYEAR = water_per_year,
        WNP = _waste_pollution,
        FERTAPP = _fertilizer_app,
        AGROCHEMAPP = _agrochem_app,
        PREHARV = _preharv_check,
        HARVEST = _harvest_details,
        SCOUTING = _scouting,
        OTHERREPORT = _other_reports,
        ALLREPORT = all_customer_report,
        # DEVICE = device_info,
        date = datetime.now().strftime('%Y-%m-%d'),
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    ))
    # response.headers['Content-Security-Policy'] = "default-src 'self'"
    return response

@customer.route('/query_data', methods=['POST', 'GET'])
def query_data():
    posted = request.form
    

    _this_org = session["profile"]["user"]["real_id"]
    print(" User ID",_this_org)
    if _this_org:
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    # print("Customers_uid",_cutomer_real_id)
    if _cutomer_real_id != None :
      cutomers_uid = _cutomer_real_id['customer']['id']
      _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    # value = fetch_customer_water(device_id_chart)
    # print("Deviceeeeeeeeeee",device_id_chart)

    devices_farm = []
    devices_farm = _shields.fetch_by_customer(_customery_final_id).get_json()
    # for devices in devices_farm:
    # device = devices['shield']['serial']['boron_id']
    # print("device",device)
    device_id_chart = 'e00fce683f0ea854ec94ca8d'
    customer_irrigation = {}
    date = datetime.now().strftime('%Y-%m-%d')
    _data_date = date
    water_per_year = {}
    customer_all = []
    device_data_2022 = []
    device_data_2021 = []
    device_data_2020 = []
    device_data_2019 = []
    device_data_2018 = []
    #   device_data_jan_2022 =[]
    #   device_data_feb_2022 = []
    #   device_data_mar_2022  =[]
    #   device_data_apr_2022  =[]
    #   device_data_may_2022  =[]
    #   device_data_jun_2022 = []
    #   device_data_july_2022 = []
    #   device_data_aug_2022 =[]
    #   device_data_sept_2022 =[]
    #   device_data_oct_2022 = []
    #   device_data_nov_2022 = []
    #   device_data_dec_2022 = []

    _water = 0
    _water1 = 0
    _water2 = 0
    _water3 = 0
    _water4 = 0
    #   _water_m1 = 0
    #   _water_m2 = 0
    #   _water_m3= 0
    #   _water_m4= 0
    #   _water_m5= 0
    #   _water_m6= 0
    #   _water_m7= 0
    #   _water_m8= 0
    #   _water_m9= 0
    #   _water_m10= 0
    #   _water_m11= 0
    #   _water_m12= 0
    device_data_20225 = shielddata.fetch_by_date_boron_id(device_id_chart,'2018-01-01 00:00:00','2022-12-31 00:00:00').get_json()
    # print("Filtered data",device_data_20225)

    if len(device_data_20225) > 0:

            _waters = 0.0
            _air_temp = []
            _soil_temp = []
            _air_humidity = []
            _soil_humidity = []
            _npk = {
                "n": [],
                "p": [],
                "k": []
            }

            # for _d in device_data_20225:
            #     for _b in _d:

                    # Water
                   
                    # Temperatures
                    # if _b['air_temperature']:
                    #     _air_temp.append(int(_b['air_temperature']))

                    # if _b['soil_temperature']:
                    #     _soil_temp.append(int(_b['soil_temperature']))

                    # Humidity
                    # if _b['air_humidity']:
                    #     _air_humidity.append(int(_b['air_humidity']))

                    # if _b['soil_humidity']:
                    #     _soil_humidity.append(int(_b['smsoil_humidity']))
                    # print("soil humidity",_soil_humidity)

                    # # NPK
                    # if 'npk' in _b and _b['npk']:
                    #     _npk['n'].append(_b['npk'][0])
                    #     _npk['p'].append(_b['npk'][1])
                    #     _npk['k'].append(_b['npk'][2])
    
    if posted:
    # if posted.get("action") == "irrigation" and "farm" in posted:
   
    #   if posted.get("irrigation") == "farm" and "duration" and "month" and "year" in posted:

        print("All Detailsssssssss",posted.get("farm"),posted.get("duration"),posted.get("month"),posted.get("year"))

        if device_id_chart:
          device_data_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-01-01 00:00:00','2022-12-31 00:00:00').get_json()
          device_data_2021 = shielddata.fetch_by_date_boron_id(device_id_chart,'2021-01-01 00:00:00','2021-12-31 00:00:00').get_json()
         
          device_data_2020 = shielddata.fetch_by_date_boron_id(device_id_chart,'2020-01-01 00:00:00','2020-12-31 00:00:00').get_json()
         
          device_data_2019 = shielddata.fetch_by_date_boron_id(device_id_chart,'2019-01-01 00:00:00','2019-12-31 00:00:00').get_json()
        
          device_data_2018 = shielddata.fetch_by_date_boron_id(device_id_chart,'2018-01-01 00:00:00','2018-12-31 00:00:00').get_json()
       
        #   device_data_jan_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-01-01 00:00:00','2022-01-31 00:00:00').get_json()
        #   device_data_feb_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-02-01 00:00:00','2022-02-31 00:00:00').get_json()
        #   device_data_mar_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-03-01 00:00:00','2022-03-31 00:00:00').get_json()
        #   device_data_apr_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-04-01 00:00:00','2022-04-31 00:00:00').get_json()
        #   device_data_may_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-05-01 00:00:00','2022-05-31 00:00:00').get_json()
        #   device_data_jun_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-06-01 00:00:00','2022-06-31 00:00:00').get_json()
        #   device_data_july_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-07-01 00:00:00','2022-07-31 00:00:00').get_json()
        #   device_data_aug_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-08-01 00:00:00','2022-08-31 00:00:00').get_json()
        #   device_data_sept_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-09-01 00:00:00','2022-09-31 00:00:00').get_json()
        #   device_data_oct_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-10-01 00:00:00','2022-10-31 00:00:00').get_json()
        #   device_data_nov_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-11-01 00:00:00','2022-11-31 00:00:00').get_json()
        #   device_data_dec_2022 = shielddata.fetch_by_date_boron_id(device_id_chart,'2022-12-01 00:00:00','2022-12-31 00:00:00').get_json()

         

          if device_data_2022 != [] :

            _water = 0
            for irr in device_data_2022:
                   try: 
                    _water += (irr[('water_dispensed')])
                    _air_temp = _air_temp.append((irr['air_temperature']))
                   
                    print("Air temp",_air_temp)
                   except:
                     print('Can not convert', str ,"to int") 
          if device_data_2021 != [] :

            _water1 = 0
            for irr in device_data_2021:
                   try: 
                    _water1 += (irr[('water_dispensed')])
                    _air_temp = _air_temp.append((irr['air_temperature']))
                   
                    print("Air temp 1",_air_temp)
                   except:
                     print('Can not convert', str ,"to int") 
          if device_data_2020 != [] :

            _water2 = 0
            for irr in device_data_2020:
                   try: 
                    _water2 += (irr[('water_dispensed')])
                    water_f = str(_water2/1000)
                    # print("water2",_water2)
                    _air_temp = _air_temp.append((irr['air_temperature']))
                   
                    print("Air temp 2",_air_temp)
                   except:
                     print('Can not convert', str ,"to int") 

          if device_data_2019 != [] :

            _water3 = 0
            for irr in device_data_2019:
                   try: 
                    _water3 += (irr[('water_dispensed')])
                    water_f = str(_water3/1000)
                    # print("water3",_water3)
                   except:
                     print('Can not convert', str ,"to int") 

          if device_data_2018 != [] :

            _water4 = 0
            for irr in device_data_2018:
                   try: 
                    _water4 += (irr[('water_dispensed')])
                    water_f = str(_water4/1000)
                    # print("water4",_water4)
                   except:
                     print('Can not convert', str ,"to int") 

                     
        #   if device_data_jan_2022 != [] :

        #     _water_m1 = 0
        #     for irr in device_data_jan_2022:
        #            try: 
        #             _water_m1 += (irr[('water_dispensed')])
        #             water_f = str(_water_m1/1000)
        #             # print("water4",_water4)
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_feb_2022 != [] :

        #     _water_m2 = 0
        #     for irr in device_data_feb_2022:
        #            try: 
        #             _water_m2 += (irr[('water_dispensed')])
                   
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_mar_2022 != [] :

        #     _water_m3 = 0
        #     for irr in device_data_mar_2022:
        #            try: 
        #             _water_m3 += (irr[('water_dispensed')])
                  
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_apr_2022 != [] :

        #     _water_m4 = 0
        #     for irr in device_data_apr_2022:
        #            try: 
        #             _water_m4 += (irr[('water_dispensed')])
        #             water_f = str(_water_m1/1000)
        #             # print("water4",_water4)
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_may_2022 != [] :

        #     _water_m5 = 0
        #     for irr in device_data_may_2022:
        #            try: 
        #             _water_m5 += (irr[('water_dispensed')])
                    
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_jun_2022 != [] :

        #     _water_m6 = 0
        #     for irr in device_data_jun_2022:
        #            try: 
        #             _water_m6 += (irr[('water_dispensed')])
                   
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_july_2022 != [] :

        #     _water_m7 = 0
        #     for irr in device_data_july_2022:
        #            try: 
        #             _water_m7 += (irr[('water_dispensed')])
                   
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_aug_2022 != [] :

        #     _water_m8 = 0
        #     for irr in device_data_aug_2022:
        #            try: 
        #             _water_m8 += (irr[('water_dispensed')])
                    
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_sept_2022 != [] :

        #     _water_m9 = 0
        #     for irr in device_data_sept_2022:
        #            try: 
        #             _water_m9 += (irr[('water_dispensed')])
                    
        #            except:
        #              print('Can not convert', str ,"to int") 

        #   if device_data_oct_2022 != [] :

        #     _water_m10 = 0
        #     for irr in device_data_oct_2022:
        #            try: 
        #             _water_m10 += (irr[('water_dispensed')])
                   
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_nov_2022 != [] :

        #     _water_m11 = 0
        #     for irr in device_data_nov_2022:
        #            try: 
        #             _water_m11 += (irr[('water_dispensed')])
                   
        #            except:
        #              print('Can not convert', str ,"to int") 
        #   if device_data_dec_2022 != [] :

        #     _water_m12 = 0
        #     for irr in device_data_dec_2022:
        #            try: 
        #             _water_m12 += (irr[('water_dispensed')])
                   
        #            except:
        #              print('Can not convert', str ,"to int") 

         
          water_per_year ={

            "labels":["2022",
                      "2021",
                      "2020",
                      "2019",
                      "2018"
                      ],
            "series":[
                [
                     _water,
                     _water1,
                     _water2,
                     _water3,
                     _water4]
                ],

            "axis": {
                "y_axis": "",
                "x_axis": "",
            },
            "metric": "litre"
        }

        #   if posted.get("duration") =="daily":

        #       water_per_year = {
            
        #        "labels": [
        #         "Jan'22",
        #         "Feb'22",
        #         "Mar'22",
        #         "Apr'22",
        #         "May'22",
        #         "Jun'22",
        #         "Jul'22",
        #         "Aug'22",
        #         "Sep'22",
        #         "Oct'22",
        #         "Nov'22",
        #         "Dec'22"
        #     ],
        #     "series": [
        #         [ _water_m1,
        #          _water_m2,
        #         _water_m3,
        #         _water_m4,
        #         _water_m5,
        #         _water_m6,
        #         _water_m7,
        #         _water_m8,
        #         _water_m9,
        #         _water_m10,
        #         _water_m11,
        #         _water_m12]
        #     ],
        #     "axis": {
        #        "y_axis": "Irrigation",
        #         "x_axis": "",
        #     },
        #     "metric": "Litre"
        # }


        
       
        # jan22 = cu.fetch_by_dates('31/1/2022','1/1/2022').get_json()
        # # print("Jan data",jan22)
       
        
        # feb22 = cu.fetch_by_dates('28/2/2022','1/2/2022').get_json()
        # mar22 = cu.fetch_by_dates('31/3/2022','1/3/2022').get_json()
        # apr22 = cu.fetch_by_dates('30/4/2022','1/4/2022').get_json()
        # may22 = cu.fetch_by_dates('31/5/2022','1/5/2022').get_json()
        # june22 = cu.fetch_by_dates('30/6/2022','1/6/2022').get_json()
        # july22 = cu.fetch_by_dates('31/7/2022','1/7/2022').get_json()
        # aug22 = cu.fetch_by_dates('31/8/2022','1/8/2022').get_json()
        # sept22 = cu.fetch_by_dates('30/9/2022','1/9/2022').get_json()
        # oct22 = cu.fetch_by_dates('31/10/2022','1/10/2022').get_json()
        # nov22 = cu.fetch_by_dates('30/11/2022','1/11/2022').get_json()
        # dec22 = cu.fetch_by_dates('31/12/2022','1/12/2022').get_json()
        # c_length = len(customer_all)
        # jlen = len(jan22)
        # flen = len(feb22)
        # mlen = len(mar22)
        # aplen = len(apr22)
        # maylen = len(may22)
        # junlen = len(june22)
        # julylen = len(july22)
        # auglen = len(aug22)
        # seplen = len(sept22)
        # octlen = len(oct22)
        # novlen = len(nov22)
        # declen = len(dec22)
          


       
        
        # if year == "2022":
        #  customer_report = {
        #     "current_yr": year,
        #     "customers_y": c_length,
        #     "customers_5y": c_length,
        #     "labels": [
        #         "Jan'22",
        #         "Feb'22",
        #         "Mar'22",
        #         "Apr'22",
        #         "May'22",
        #         "Jun'22",
        #         "Jul'22",
        #         "Aug'22",
        #         "Sep'22",
        #         "Oct'22",
        #         "Nov'22",
        #         "Dec'22"
        #     ],
        #     "series": [
        #         [ jlen,
        #          flen,
        #          mlen,
        #          aplen,
        #          maylen,
        #          junlen,
        #          julylen,
        #          auglen,
        #          seplen,
        #          octlen,
        #          novlen,
        #          declen]
        #     ],
        #     "axis": {
        #         "y_axis": "Customers",
        #         "x_axis": "",
        #     },
        #     "metric": "K"
        # }
       
       

    

    data = {
            
            "labels": [
                "Jan'20",
                "Feb'20",
                "Mar'20",
                "Apr'20",
                "May'20",
                "Jun'20",
                "Jul'20",
                "Aug'20",
                "Sep'20",
                "Oct'20",
                "Nov'20",
                "Dec'20"
            ],
            "series": [
                [3,
                0,
                5,
                0,
                0,
                6,
                0,
                0,
                6,
                0,
                0,
                0]
            ],
            "axis": {
                "y_axis": "Customers",
                "x_axis": "",
            },
            "metric": "K"
        }
    # data = {
    #     "labels": [],
    #     "series": [],
    #     "axis": {
    #         "y_axis": "Readings",
    #         "x_axis": "",
    #     },
    #     "metric": ""
    # }
    print(water_per_year)
    return jsonify(water_per_year)


def fetch_summaries(boron_id=None, data_date=None, limit=None):
    summary = {}

    if boron_id :
        _data_date = datetime(2020, 3, 31, 0, 0, 0)
        if data_date:
            _data_date = data_date

        _data = db\
            .session\
            .query(ShieldCompleteData)\
            .filter(ShieldCompleteData.boron_id == boron_id)\
            .filter(extract('year', ShieldCompleteData.created_at)==_data_date.strftime('%Y'))\
            .filter(extract('month', ShieldCompleteData.created_at)==_data_date.strftime('%m'))\
            .filter(extract('day', ShieldCompleteData.created_at)==_data_date.strftime('%d'))\
            .filter(ShieldCompleteData.status == config.STATUS_ACTIVE)\
            .all()

        if len(_data) > 0:

            _water = 0.0
            _air_temp = []
            _soil_temp = []
            _air_humidity = []
            _soil_humidity = []
            _npk = {
                "n": [],
                "p": [],
                "k": []
            }

            # for _d in _data:
            #     for _b in _d.DeviceDataBatch.batch:

            #         # Water
            #         if 'wd' in _b and _b['wd']:
            #             _water += _b['wd']

            #         # Temperatures
            #         if 'at' in _b and _b['at']:
            #             _air_temp.append(int(_b['at']))

            #         if 'st' in _b and _b['st']:
            #             _soil_temp.append(int(_b['st']))

            #         # Humidity
            #         if 'ah' in _b and _b['ah']:
            #             _air_humidity.append(int(_b['ah']))

            #         if 'sm' in _b and _b['sm']:
            #             _soil_humidity.append(int(_b['sm']))

            #         # NPK
            #         if 'npk' in _b and _b['npk']:
            #             _npk['n'].append(_b['npk'][0])
            #             _npk['p'].append(_b['npk'][1])
            #             _npk['k'].append(_b['npk'][2])

            air_temperature = round(sum(_air_temp)/len(_data), 2)
            soil_temperature = round(sum(_soil_temp)/len(_data), 2)
            air_humidity = round(sum(_air_humidity)/len(_data), 2)
            soil_humidity = round(sum(_soil_humidity)/len(_data), 2)
            npk = {
                "n": round(sum(_npk['n'])/len(_data), 2),
                "p": round(sum(_npk['p'])/len(_data), 2),
                "k": round(sum(_npk['k'])/len(_data), 2)
            }

            if limit:

                if limit == 'wd':
                    return _water

                if limit == 'at':
                    return air_temperature

                if limit == 'st':
                    return soil_temperature

                if limit == 'ah':
                    return air_humidity

                if limit == 'sh':
                    return soil_humidity

                if limit == 'npk':
                    return npk

            summary = {
                "data": {
                    "water": round(_water, 2),
                    "temperature": {
                        "air": air_temperature,
                        "soil": soil_temperature,
                    },
                    "humidity": {
                        "air": air_humidity,
                        "soil": soil_humidity
                    },
                    "npk": npk
                }
            }

    return summary


def fetch_weekly(device_id, this_date):
    this_date = this_date + relativedelta(days=1)
    begin_date = (this_date - relativedelta(weeks=1))
    temp_soil = {}
    temp_air = {}
    humidity_soil = {}
    humidity_air = {}
    water = {}
    npk = {}
    while begin_date <= this_date:
        _day_summary = fetch_summaries(device_id, begin_date)
        if 'data' in _day_summary:
            temp_soil[begin_date.strftime('%a')] = _day_summary['data']['temperature']['soil']
            temp_air[begin_date.strftime('%a')] = _day_summary['data']['temperature']['air']

            humidity_soil[begin_date.strftime('%a')] = _day_summary['data']['humidity']['soil']
            humidity_air[begin_date.strftime('%a')] = _day_summary['data']['humidity']['air']

            water[begin_date.strftime('%a')] = _day_summary['data']['water']

            npk[begin_date.strftime('%a')] = _day_summary['data']['npk']

        begin_date += relativedelta(days=1)

    return {"temp_soil": temp_soil, "temp_air": temp_air, "humidity_soil": humidity_soil, "humidity_air": humidity_air, "water": water, "npk": npk}

def fetch_monthly(device_id, this_date):
    begin_date = ((this_date + relativedelta(days=1)) - relativedelta(months=1))
    temp_soil = {}
    temp_air = {}
    humidity_soil = {}
    humidity_air = {}
    water = {}
    npk = {}
    while begin_date <= this_date:
        _day_summary = fetch_summaries(device_id, begin_date)
        if 'data' in _day_summary:
            temp_soil[begin_date.strftime('%d')] = _day_summary['data']['temperature']['soil']
            temp_air[begin_date.strftime('%d')] = _day_summary['data']['temperature']['air']

            humidity_soil[begin_date.strftime('%d')] = _day_summary['data']['humidity']['soil']
            humidity_air[begin_date.strftime('%d')] = _day_summary['data']['humidity']['air']

            water[begin_date.strftime('%d')] = _day_summary['data']['water']

            npk[begin_date.strftime('%d')] = _day_summary['data']['npk']

        begin_date += relativedelta(days=1)

    return {"temp_soil": temp_soil, "temp_air": temp_air, "humidity_soil": humidity_soil, "humidity_air": humidity_air, "water": water, "npk": npk}

def fetch_yearly(device_id, this_date):
    base_date = this_date
    begin_date = (base_date - relativedelta(years=1))
    temp_soil = {}
    temp_air = {}
    humidity_soil = {}
    humidity_air = {}
    water = {}
    npk = {}
    while begin_date <= base_date:
        _days_diff = (begin_date + relativedelta(months=1)) - begin_date

        _month_summary = fetch_monthly(device_id, begin_date)
        if 'temp_soil' in _month_summary:
            _temp_summary = [];
            for key, val in _month_summary['temp_soil'].items():
                _temp_summary.append(val)

            temp_soil[begin_date.strftime('%b') + "-" + begin_date.strftime('%y')] = round((sum(_temp_summary)/_days_diff.days), 2)

        if 'temp_air' in _month_summary:
            _temp_summary = [];
            for key, val in _month_summary['temp_air'].items():
                _temp_summary.append(val)

            temp_air[begin_date.strftime('%b') + "-" + begin_date.strftime('%y')] = round((sum(_temp_summary)/_days_diff.days), 2)

        if 'humidity_soil' in _month_summary:
            _humidity_summary = [];
            for key, val in _month_summary['humidity_soil'].items():
                _humidity_summary.append(val)

            humidity_soil[begin_date.strftime('%b') + "-" + begin_date.strftime('%y')] = round((sum(_humidity_summary)/_days_diff.days), 2)

        if 'humidity_air' in _month_summary:
            _humidity_summary = [];
            for key, val in _month_summary['humidity_air'].items():
                _humidity_summary.append(val)

            humidity_air[begin_date.strftime('%b') + "-" + begin_date.strftime('%y')] = round((sum(_humidity_summary)/_days_diff.days), 2)

        if 'water' in _month_summary:
            _water_summary = [];
            for key, val in _month_summary['water'].items():
                _water_summary.append(val)

            water[begin_date.strftime('%b') + "-" + begin_date.strftime('%y')] = round((sum(_water_summary)/_days_diff.days), 2)

        if 'npk' in _month_summary:
            _npk_summary_n = [];
            _npk_summary_p = [];
            _npk_summary_k = [];
            for key, val in _month_summary['npk'].items():
                _npk_summary_n.append(val['n'])
                _npk_summary_p.append(val['p'])
                _npk_summary_k.append(val['k'])

            _npk_summary = {
                "n": round((sum(_npk_summary_n)/_days_diff.days), 2),
                "p": round((sum(_npk_summary_p)/_days_diff.days), 2),
                "k": round((sum(_npk_summary_k)/_days_diff.days), 2),
            }
            npk[begin_date.strftime('%b') + "-" + begin_date.strftime('%y')] = _npk_summary

        begin_date += relativedelta(months=1)

    return {"temp_soil": temp_soil, "temp_air": temp_air, "humidity_soil": humidity_soil, "humidity_air": humidity_air, "water": water, "npk": npk}


def temperatures(duration="Week"):
    chart_data = []

    start_date = dt.date(2020, 1, 1)
    today = dt.date.today()

    if duration == "Week":
        all_days_label = []
        all_days_data = []
        while start_date <= today:
            all_days_label.append(start_date.strftime('%d %b'))
            all_days_data.append(random.randint(17, 55))
            start_date += relativedelta(weeks=1)

        chart_data.append(','.join(all_days_label))
        chart_data.append(all_days_data)

    return chart_data
