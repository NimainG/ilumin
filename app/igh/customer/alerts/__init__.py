from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth

from datetime import datetime

from app.core.user import customers as _customer
from app.core import customer as customerfy
from app.core.customer import farms as _farms
from app.core import farm as ownfarms
from app.core.farm import actions
from app.core import alert
from app.models import db
from werkzeug.wrappers import response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from app.models import Alert

from app.config import activateConfig



from app.igh import portal_check

customer_alerts = Blueprint('customer_alerts', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_alerts.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')



@customer_alerts.route('/', methods=['POST', 'GET'], defaults={"page": 1})
@customer_alerts.route('/<int:page>', methods=['POST', 'GET'])
def alerts(page):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    page = page
    prev = page-1
    next = page+1
    no_of_posts = config.PAGE_LIMIT
    

    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
     
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    alert_customer = alert.fetch_by_customer(_customery_final_id,page).get_json()

    total = {}
    total = round(len(alert_customer)/no_of_posts)



    if request.method == 'POST' and 'tag_alert' in request.form:
        tag = request.form["tag_alert"]
        if tag:
            # search = "%{}%".format(tag)
            search = tag
            if search:

             _taggy = []
             if search.isnumeric() == True:
               _taggy = db.session.query(Alert).filter(or_((Alert.customer_id == search),(Alert.type == search),(Alert.creator_id == search))).limit(no_of_posts).offset((page-1)*no_of_posts).all()
               response = []
               for alrt in _taggy:

                   response.append(
                            alert.alert_obj(alrt))
               alert_customer = jsonify(response).get_json()
   

    return render_template(
        'customers/alerts/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_ALERTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        prev=prev, next=next,
        total=total,
        ALERT = alert_customer,
        data={"date":str(datetime.now().year)}
    )
