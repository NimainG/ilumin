from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime

from app.models import db, Customer, User, Role, UserRole, Greenhouse, \
    CustomerGreenhouse, Farm, CustomerDevice, Device

from app.config import activateConfig
from app.igh import portal_check
from app.igh.admin import contact_person

customer_devices = Blueprint('customer_devices', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_devices.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def form_constructor():

    _type_options = [
        {
            "label": 'Shield',
            "value": config.DEVICE_SHIELD,
        },
        {
            "label": 'Spear',
            "value": config.DEVICE_SPEAR,
        },
        {
            "label": 'Sensor',
            "value": config.DEVICE_SENSOR,
        },
    ]

    _form_fields = [
        {
            "name": "name",
            "title": "Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"Preferred reference",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "device_type",
            "title": "Type",
            "element": [
                "select",
                {
                    "options": _type_options
                }
            ],
            "required": 1,
            "default": config.DEVICE_SHIELD,
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "serial",
            "title": "Serial",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "photo_url",
            "title": "Photo Link",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "info_url",
            "title": "More Info Link",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "description",
            "title": "Description",
            "element": [
                "textarea"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "status",
            "title": "Status",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Active",
                            "value": config.STATUS_ACTIVE,
                        },
                        {
                            "label": "Inactive",
                            "value": config.STATUS_INACTIVE,
                        }
                    ]
                }
            ],
            "required": 0,
            "default":"Active",
            "views": [
                "edit"
            ]
        },
    ]

    return _form_fields

def device_type_name(device_type):
    if device_type == config.DEVICE_SHIELD:
        return "Shield"

    if device_type == config.DEVICE_SPEAR:
        return "Spear"

    if device_type == config.DEVICE_SENSOR:
        return "Sensor"

    return ""

@customer_devices.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    devices = []
    _devices = db\
        .session\
        .query(Device, CustomerDevice, Customer)\
        .join(CustomerDevice, CustomerDevice.device_id == Device.id)\
        .join(Customer, Customer.id == CustomerDevice.customer_id)\
        .filter(Customer.uid == session["profile"]["org_id"])\
        .filter(Device.status > config.STATUS_DELETED)\
        .filter(CustomerDevice.status > config.STATUS_DELETED)\
        .filter(Customer.status > config.STATUS_DELETED)\
        .all()
    for _device in _devices:
        devices.append({
            "id": _device.Device.uid,
            "name": _device.Device.name,
            "type": device_type_name(_device.Device.type),
            "serial": _device.Device.serial,
            "photo_url": _device.Device.photo_url,
            "info_url": _device.Device.info_link,
            "description": _device.Device.description,
            "status": "Active" if _device.Device.status == config.STATUS_ACTIVE else "Inactive",
            "created": _device.Device.created_at,
            "updated": _device.Device.modified_at,
            "creator": contact_person(_device.Device.creator_id),
        })

    return render_template(
        'customers/devices/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_DEVICES_ACTIVE='active',
        CUSTOMER_PORTAL_DEVICES_LIST_ACTIVE='active',
        DEVICES=devices,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )
