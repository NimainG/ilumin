from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid
from sqlalchemy.sql.expression import join

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime

from app.config import activateConfig

from app.core import user as _user
from app.core import customer as _customer
from app.core.user import customers as _customery
from app.core.customer import farms as ownfarms
from app.igh.customer import contact_person
from app.core import customer as customerfy
from app.models import db
from app.models import CustomerGreenhouse,Greenhouse,User,CustomerUser,Farm
from app.models import CustomerFarm
from app.core.user import customers
from app.core.farm import actions
from app.core import alert
from app.core.user import logs

from app.igh import portal_check
from app.helper import valid_uuid

from app.core import farm as farmy

customer_farms = Blueprint('customer_farms', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_farms.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouses(farm_id):
    _response = []
    _ghs = db\
        .session\
        .query(CustomerGreenhouse, Greenhouse)\
        .join(Greenhouse, Greenhouse.id == CustomerGreenhouse.greenhouse_id)\
        .filter(CustomerGreenhouse.farm_id == farm_id)\
        .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
        .filter(Greenhouse.status > config.STATUS_DELETED)\
        .all()
    for _gh in _ghs:
        _response.append({
            "id": _gh.Greenhouse.uid,
            "name": _gh.Greenhouse.name,
            "description": _gh.Greenhouse.description,
            "more": _gh.Greenhouse.info_link,
            "status": "Active" if _gh.Greenhouse.status == config.STATUS_ACTIVE else "Inactive",
            "created": _gh.Greenhouse.created_at,
            "updated": _gh.Greenhouse.modified_at,
            "creator": contact_person(_gh.Greenhouse.creator_id),
        })

    return _response

def devices(farm_id):
    _response = []
    _devices = db\
        .session\
        .query(CustomerDevice, Device)\
        .join(Device, Device.id == CustomerDevice.device_id)\
        .filter(CustomerDevice.farm_id == farm_id)\
        .filter(CustomerDevice.status > config.STATUS_DELETED)\
        .filter(Device.status > config.STATUS_DELETED)\
        .all()
    for _device in _devices:
        _response.append({
            "id": _device.Device.uid,
            "name": _device.Device.name,
            "serial": _device.Device.serial,
            "photo_url": _device.Device.photo_url,
            "description": _device.Device.description,
            "more": _device.Device.info_link,
            "status": "Active" if _device.Device.status == config.STATUS_ACTIVE else "Inactive",
            "created": _device.Device.created_at,
            "updated": _device.Device.modified_at,
            "creator": contact_person(_device.Device.creator_id),
        })

    return _response

def farm_form_constructor():

    _user_options = []
    _users = db\
        .session\
        .query(User)\
        .filter(User.status > config.STATUS_DELETED)\
        .all()
    for _user in _users:
        _user_options.append({
            "label": _user.first_name + ' ' + _user.last_name,
            "value": _user.uid
        })

    _form_fields = [
        {
            "name": "name",
            "title": "Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "address",
            "title": "Address",
            "element": [
                "textarea"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "latitude",
            "title": "Latitude",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "longitude",
            "title": "Longitude",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "status",
            "title": "Status",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Active",
                            "value": config.STATUS_ACTIVE,
                        },
                        {
                            "label": "Inactive",
                            "value": config.STATUS_INACTIVE,
                        }
                    ]
                }
            ],
            "required": 0,
            "default":"Active",
            "views": [
                "edit"
            ]
        },
    ]

    return _form_fields

@customer_farms.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = _customer.fetch_one(session["profile"]["user"]["id"], True).get_json()
    _this_user = _user.fetch_one(session["profile"]["user"]["id"]).get_json()

    form_action = 'Add'
    action_response = {}

    farms = []
    _farms = db\
        .session\
        .query(Farm)\
        .filter(Farm.customer_id == _this_org.id)\
        .filter(Farm.status > config.STATUS_DELETED)\
        .all()
    for _farm in _farms:
        _contact_person = False
        _farm_contact_person = CustomerUser\
            .query\
            .filter(CustomerUser.id == _farm.contact_person_id)\
            .filter(User.status > config.STATUS_DELETED)\
            .first()
        if _farm_contact_person:
            _contact_person = contact_person(_farm_contact_person.user_id)

        farms.append({
            "id": _farm.uid,
            "name": _farm.name,
            "customer": _this_org.name,
            "contact_person": "",
            "address": _farm.address,
            "latitude": _farm.latitude,
            "longitude": _farm.longitude,
            "greenhouses": greenhouses(_farm.id),
            "devices": devices(_farm.id),
            "status": "Active" if _farm.status == config.STATUS_ACTIVE else "Inactive",
            "created": _farm.created_at,
            "updated": _farm.modified_at,
            "creator": contact_person(_farm.creator_id),
        })

    return render_template(
        'customers/farms/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        FORM=farm_form_constructor(),
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        FARMS=farms,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )

@customer_farms.route('/add', methods=['POST','GET'])
def add():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm


    farms = []
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_idy = _customery.fetch_by_user(_this_org).get_json()
    if _cutomer_real_idy != None :
     cutomers_uid = _cutomer_real_idy['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_idy = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_idy).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_idy).get_json()['current_firm_uid']
     swapped_firm_details = farmy.fetch_by_id(farm_id_by_action).get_json()
    else: 
     swapped_firm_details = None
    farms = ownfarms.fetch_by_customer(_customery_final_idy).get_json()

    # _this_org = customers.fetch_by_user(session["profile"]["user"]["id"]["real_id"]).get_json()
    # _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
   
    # _customer_fetch = customers.fetch_all().get_json()

    # print("hello world",_customer_fetch)

    
    

    _this_user = _user.fetch_one(session["profile"]["user"]["id"]).get_json()

    form_action = 'Edit'
    action_form = []
    action_response = {}


    _farms_fetch = farmy.fetch_all().get_json()
    _farm_action_type = actions.fetch_by_customer(_customery_final_id).get_json()
   


    _multiple = []

    
  
            
    _farm = []
    posted = request.form
    if posted:
        _name = None
        _name = posted.get("farm_ref")
        _name1 = None
        _name1 = posted.get("farm_ref")
        _address = None
        _address = posted.get("address")
        _state = None
        _state = posted.get("state")
        _city = None
        _city = posted.get("city")
        
        _pincode = posted.get("pincode")
        if _pincode == '':
          _pincode = None

        
        _latitude = posted.get("latitude")
        if _latitude == '':
            _latitude = None
        
        _longitude = posted.get("longitude")
        if _longitude == '':
            _longitude = None
        _total_farm_area = None
        _total_farm_area = posted.get("area")
        _status = None
        _status = posted.get("status")
        _this_customer = None
        _this_customer = _user.fetch_one(session['profile']['user']['id'], True).get_json()['real_id']
       

        # _phone_no = _user.fetch_one(session["profile"]["user"]["id"]).get_json()
        _cover_photo_media_id = '74daa98c-f6c7-4965-8206-c8b9fd006ede'  
       # _this_customer = _user.fetch_one(_this_user, True).get_json()
        

        

        if  _name:
            action_response = {
                "type": "warning",
                "message": "Farm not added, invalid input."
            }

          
            if  _this_user != '' and _this_customer != '' and _name != '' and _name1 != '' \
                  and _address != '' and _state != '' and _city != '' \
                  and _total_farm_area != '' \
                  and _status != '' and _this_customer != '':
                 _farm = Farm(
                    uid=uuid.uuid4(),
                   
                    name = _name,
                    farm_ref =_name1,
                    address=_address,
                    state=_state,
                    city=_city,
                    pincode=_pincode,
                    latitude=_latitude,
                    longitude=_longitude,
                    area=_total_farm_area,
                    status=_status,
                    creator_id=_this_customer,
                    # cover_photo_media_id = _cover_photo_media_id 
                    # phone_number = _phone_no,
                    
                 )
                 db.session.add(_farm)
                 db.session.commit()
            
                 if _farm:
                    action_response = {
                        "type": "success",
                        "message": "Farm added successfully."
                    }
                
                
                 cutomers_uid = _cutomer_real_id['customer']['id']
                 _customery_id = _customer.fetch_one(cutomers_uid, True).get_json()['real_id']
                 _farm_owner = ownfarms.add_new(
                            json.dumps({
                             "farm_id": _farm.id,
                             "customer_id" : _customery_id,
                             "creator_id" :  _this_customer
                             
                            })
                 )
                 print(str(_farm_owner[0].get_json()))
                #  and _farm_action_type == {}
                 if(_farm.id != False ):
                   
                    _farm_action = actions.add_new(
                        json.dumps({
                            "customer_id":_customery_final_id,
                            "current_firm_uid": _farm.id,
                            "creator_id" :  session["profile"]["user"]["id"] 

                        }),
                        True
                    )
                    print(str(_farm_action[0].get_json()))
            
                    if(_customery_final_id != False):
                      
                      _user_log = logs.add_new(
                         json.dumps({
                                    "action": "Farm is Created",
                                    "ref": _name,
                                    "ref_id": _farm.id,
                                    "customer_id" : cutomers_uid,
                                    "creator_id" : session["profile"]["user"]["id"] 
                                    }),
                                    
                                    )
                      print(str(_user_log[0].get_json()))
                      new_alert_customer = alert.add_new(
                            json.dumps({
                             "type": config.ALERT_USER,
                             "customer_id" :_customery_final_id,
                             "severity" : config.ALERT_INFORMATIONAL,
                             "creator_id" : session["profile"]["user"]["id"] ,
                             "name" : _name,
                             "detail" : _name,
                             "trigger" : "Farm Creation",
                             "action_text" : "",
                             "action_link" : "",      

                                }),
                                True
                         )

                      print(str(new_alert_customer[0].get_json()))
            elif _name == '' and _name1 == '' \
                  and _address != '' and _state != '' and _city != '' \
                  and _total_farm_area != '':

                action_response = {
                "type": "warning",
                "message": "Farm not added, missing the Farm name"
            }

            elif _name != '' and _name1 != '' \
                  and _address == '' and _state != '' and _city != '' \
                  and _total_farm_area != '':

                action_response = {
                "type": "warning",
                "message": "Farm not added, missing the Farm address"
            }


            elif _name != '' and _name1 != '' \
                  and _address != '' and _state == '' and _city != '' \
                  and _total_farm_area != '':

                action_response = {
                "type": "warning",
                "message": "Farm not added, missing the Country name"
            }

            elif _name != '' and _name1 != '' \
                  and _address != '' and _state != '' and _city == '' \
                  and _total_farm_area != '':

                action_response = {
                "type": "warning",
                "message": "Farm not added, missing the Town name"
            }


            elif _name != '' and _name1 != '' \
                  and _address != '' and _state != '' and _city != '' \
                  and _total_farm_area == '':

                action_response = {
                "type": "warning",
                "message": "Farm not added, missing the Farm area"
            }
            
            
               
            else:
                      action_response = {
                "type": "warning",
                "message": "Farm not added, missing the required input box"
            }
        

    
    return render_template(
        'customers/farms/add.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        CUSTOMER_PORTAL_FARMS_LIST_ACTIVE='active',
        FORM=action_form,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        # FARMS=_farms_fetch,
        FARMS=farms,
        FARM=swapped_firm_details,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )


@customer_farms.route('/addfirstfarm', methods=['POST','GET'])
def add_first():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm


    farms = []
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_idy = _customery.fetch_by_user(_this_org).get_json()
    if _cutomer_real_idy != None :
     cutomers_uid = _cutomer_real_idy['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_idy = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_idy).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_idy).get_json()['current_firm_uid']
     swapped_firm_details = farmy.fetch_by_id(farm_id_by_action).get_json()
    else: 
     swapped_firm_details = None
    farms = ownfarms.fetch_by_customer(_customery_final_idy).get_json()

    # _this_org = customers.fetch_by_user(session["profile"]["user"]["id"]["real_id"]).get_json()
    # _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
   
    # _customer_fetch = customers.fetch_all().get_json()

    # print("hello world",_customer_fetch)

    
    

    _this_user = _user.fetch_one(session["profile"]["user"]["id"]).get_json()

    form_action = 'Edit'
    action_form = []
    action_response = {}


    _farms_fetch = farmy.fetch_all().get_json()
    _farm_action_type = actions.fetch_by_customer(_customery_final_id).get_json()
   


    _multiple = []

    
  
            
    _farm = []
    posted = request.form
    if posted:
        _name = None
        _name = posted.get("farm_ref")
        _name1 = None
        _name1 = posted.get("farm_ref")
        _address = None
        _address = posted.get("address")
        _state = None
        _state = posted.get("state")
        _city = None
        _city = posted.get("city")
        
        _pincode = posted.get("pincode")
        _latitude = None
        _latitude = posted.get("latitude")
        _longitude = None
        _longitude = posted.get("longitude")
        _total_farm_area = None
        _total_farm_area = posted.get("area")
        _status = None
        _status = posted.get("status")
        _this_customer = None
        _this_customer = _user.fetch_one(session['profile']['user']['id'], True).get_json()['real_id']
       

        # _phone_no = _user.fetch_one(session["profile"]["user"]["id"]).get_json()
        _cover_photo_media_id = '74daa98c-f6c7-4965-8206-c8b9fd006ede'  
       # _this_customer = _user.fetch_one(_this_user, True).get_json()
        
        exist = farmy.fetch_by_id
        

        if  _name:
            action_response = {
                "type": "warning",
                "message": "Farm not added, invalid input."
            }

          
            if  _this_user != '' and _this_customer != '' and _name != '' and _name1 != '' \
                  and _address != '' and _state != '' and _city != '' and _pincode != ''\
                  and _latitude != '' and _longitude != '' and _total_farm_area != '' \
                  and _status != '' and _this_customer != '':
                 _farm = Farm(
                    uid=uuid.uuid4(),
                   
                    name = _name,
                    farm_ref =_name1,
                    address=_address,
                    state=_state,
                    city=_city,
                    pincode=_pincode,
                    latitude=_latitude,
                    longitude=_longitude,
                    area=_total_farm_area,
                    status=_status,
                    creator_id=_this_customer,
                    # cover_photo_media_id = _cover_photo_media_id 
                    # phone_number = _phone_no,
                    
                 )
                 db.session.add(_farm)
                 db.session.commit()
            
                 if _farm:
                    action_response = {
                        "type": "success",
                        "message": "Farm added successfully."
                    }
                
                
                 cutomers_uid = _cutomer_real_id['customer']['id']
                 _customery_id = _customer.fetch_one(cutomers_uid, True).get_json()['real_id']
                 _farm_owner = ownfarms.add_new(
                            json.dumps({
                             "farm_id": _farm.id,
                             "customer_id" : _customery_id,
                             "creator_id" :  _this_customer
                             
                            })
                 )
                 print(str(_farm_owner[0].get_json()))

                 if(_farm.id != False and _farm_action_type == {}):
                   
                    _farm_action = actions.add_new(
                        json.dumps({
                            "customer_id":_customery_final_id,
                            "current_firm_uid": _farm.id,
                            "creator_id" :  session["profile"]["user"]["id"] 

                        }),
                        True
                    )
                    # print(str(_farm_action[0].get_json()))
            #     else:
            #       action_response = {
            #     "type": "No worries",
            #     "message": "Farm Action not added, Already firm available."
            # }
                    if(_customery_final_id != None):

                      _user_log = logs.add_new(
                         json.dumps({
                                    "action": "First farm is Created",
                                    "ref": _name,
                                    "ref_id": _farm.id,
                                    "customer_id" : cutomers_uid,
                                    "creator_id" : session["profile"]["user"]["id"] 
                                    }),
                                    
                                    )
                      print(str(_user_log[0].get_json()))
                      new_alert_customer = alert.add_new(
                            json.dumps({
                             "type": config.ALERT_USER,
                             "customer_id" :_customery_final_id,
                             "severity" : config.ALERT_INFORMATIONAL,
                             "creator_id" : session["profile"]["user"]["id"] ,
                             "name" : _name,
                             "detail" : _name,
                             "trigger" : "Farm Creation",
                             "action_text" : "",
                             "action_link" : "",      

                                }),
                                
                      )

                      print(str(new_alert_customer[0].get_json()))



            
            
               
            else:
                      action_response = {
                "type": "warning",
                "message": "Farm not added, invalid input."
            }
        

    
    return render_template(
        'customers/farms/first_farm_add.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        CUSTOMER_PORTAL_FARMS_LIST_ACTIVE='active',
        FORM=action_form,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        # FARMS=_farms_fetch,
        FARMS=farms,
        FARM=swapped_firm_details,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )


@customer_farms.route('/<farm_id>/edit', methods=['POST', 'GET'])
def edit(farm_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    # _this_org = _customer.fetch_one(session["profile"]["user"]["id"], True).get_json()
    farms = []
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customery.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    

     

    farm = {}

    farm = farmy.fetch_one(farm_id,True).get_json()
    farm_real_id = farm['real_id']
    # print("Farm details",farm)

    form_action = 'Edit'
    action_form = []
    action_response = {}

    if farm_id != "":
        farmz = Farm\
            .query\
            .filter(Farm.uid == farm_id)\
            .filter(Farm.status > config.STATUS_DELETED)\
            .first()
        if farmz:
            posted = request.form
            if posted:
                _name = posted.get("farm_ref").strip()
                _address = posted.get("farm_address").strip()
                _state = posted.get("state").strip()
                _city = posted.get("city").strip()
                _pincode = posted.get("pincode").strip()
                _latitude = posted.get("latitude").strip()
                _longitude = posted.get("longitude").strip()
                _total_farm_area = posted.get("total_farm_area").strip()
                _status = posted.get("status").strip()
                
    
                if _name != '' and _address != '' and _state != '' and _city != '' \
                    and _pincode != '' and _latitude != '' and _longitude != '' \
                        and _total_farm_area != '' :
                    farmz.name = _name
                    farmz.farm_ref = _name
                    farmz.address = _address
                    farmz.state = _state
                    farmz.city = _city
                    farmz.pincode = _pincode
                    farmz.latitude = _latitude
                    farmz.longitude = _longitude
                    farmz.area = _total_farm_area
                    farmz.status = _status
    
                    db.session.commit()
    
                    action_response = {
                        "type": "success",
                        "message": "Farm edited successfully."
                    }
                    _farm_log = logs.add_new(
                         json.dumps({
                                    "action": "Farm is Updated",
                                    "ref": _name,
                                    "ref_id": farm_real_id,
                                    "customer_id" : cutomers_uid,
                                    "creator_id" : session["profile"]["user"]["id"] 
                                    }),
                                    
                                    )
                    print(str(_farm_log[0].get_json()))
    
                else:
                    action_response = {
                        "type": "warning",
                        "message": "Farm not edited, invalid input."
                    }
    
        else:
            action_response = {
                "type": "danger",
                "message": "Invalid input."
            }

    

    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = farmy.fetch_by_id(farm_id_by_action).get_json()
    else: 
     swapped_firm_details = None
    farms = ownfarms.fetch_by_customer(_customery_final_id).get_json()

    # farms = []
    # _farms = db\
    #     .session\
    #     .query(Farm)\
    #     .filter(Farm.customer_id == cutomers_uid)\
    #     .filter(Farm.status > config.STATUS_DELETED)\
    #     .all()
    
    # for _farm in _farms:
    #     _contact_person = False
    #     _farm_contact_person = CustomerUser\
    #         .query\
    #         .filter(CustomerUser.id == _farm.contact_person_id)\
    #         .filter(User.status > config.STATUS_DELETED)\
    #         .first()
    #     if _farm_contact_person:
    #         _contact_person = contact_person(_farm_contact_person.user_id)
    #     farms.append({
    #         "id": _farm.uid,
    #         "uid": _farm.uid,
    #         "name": _farm.name,
    #         "customer": _this_org.name,
    #         "contact_person": "",
    #         "address": _farm.address,
    #         "latitude": _farm.latitude,
    #         "longitude": _farm.longitude,
    #         "farms": greenhouses(_farm.id),
    #         "devices": devices(_farm.id),
    #         "status": "Active" if _farm.status == config.STATUS_ACTIVE else "Inactive",
    #         "created": _farm.created_at,
    #         "updated": _farm.modified_at,
    #         "creator": contact_person(_farm.creator_id),
    #     })

    return render_template(
        'customers/farms/edit.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        CUSTOMER_PORTAL_FARMS_LIST_ACTIVE='active',
        FORM=action_form,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        FARMS=farms,
        FARM=swapped_firm_details,
        FARMO = farm,
        PROFILE=session['profile'],
        # SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )

@customer_farms.route('/<farm_id>/delete', methods=['POST', 'GET'])
def delete(farm_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = _customer.fetch_one(session["profile"]["user"]["id"], True).get_json()

    action_response = {}

    if farm_id != "":
        farm = Farm\
            .query\
            .filter(Farm.uid == farm_id)\
            .filter(Farm.status > config.STATUS_DELETED)\
            .first()
        if farm:
            farm.status=config.STATUS_DELETED
            db.session.commit()
    
            action_response = {
                "type": "success",
                "message": farm.name + " deleted successfully."
            }
        else:
            action_response = {
                "type": "danger",
                "message": "Invalid input."
            }

    farms = []
    # _farms = db\
    #     .session\
    #     .query(Farm)\
    #     .filter(Farm.customer_id == _this_org.id)\
    #     .filter(Farm.status > config.STATUS_DELETED)\
    #     .all()
    # for _farm in _farms:
    #     _contact_person = False
    #     _farm_contact_person = CustomerUser\
    #         .query\
    #         .filter(CustomerUser.id == _farm.contact_person_id)\
    #         .filter(User.status > config.STATUS_DELETED)\
    #         .first()
    #     if _farm_contact_person:
    #         _contact_person = contact_person(_farm_contact_person.user_id)
    #     farms.append({
    #         "id": _farm.uid,
    #         "name": _farm.name,
    #         "customer": this_org(),
    #         "address": _farm.address,
    #         "latitude": _farm.latitude,
    #         "longitude": _farm.longitude,
    #         "status": "Active" if _farm.status == config.STATUS_ACTIVE else "Inactive",
    #         "created": _farm.created_at,
    #         "updated": _farm.modified_at,
    #         "creator": contact_person(_farm.creator_id),
    #     })

    return render_template(
        'customers/farms/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        FORM_ACTION_RESPONSE=action_response,
        FARMS=farms,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )
