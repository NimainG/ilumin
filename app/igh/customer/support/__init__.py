from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from flask import Flask,flash, redirect, url_for
from werkzeug.utils import secure_filename

from werkzeug.exceptions import HTTPException
from app.core import media
from app.core.media import files
from dotenv import load_dotenv
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime
from app.core.user import customers as _customers
from app.core import customer as customerfy, greenhouse
from app.core.customer import farms as _farms, greenhouses
from app.core import farm as ownfarms
from app.core.customer import greenhouses as grn_h
from app.core.farm import actions
from app.core import maintenance
from app.core.user import customers 
from app.config import activateConfig
from app.core import pest as pesto
from app.core import disease
from app.core.maintenance import greenhouses as greenmain
from app.core.maintenance import pests_diseases
from app.core.maintenance import agronomist_visit
from app.core.maintenance import shield_issues

from app.core.maintenance import greenhouse_issues
from app.core.maintenance import screenhouse_issues
from app.core.maintenance import shadenet_issues
from app.core.maintenance import traceability_report_requests
from app.core.maintenance import traceability_report_crops
from app.core.maintenance import others as other_request
from app.core.maintenance import closure
from app.core.planting import plants
from app.core.planting import varieties
from app.core.customer import shields
from app.igh import portal_check
from app.core.maintenance import issues
from app.core.customer import screenhouses
from app.core.customer import shadenets
from app.core import supply
from app.core.supply import components
from app.models import db
from app.models import Media
from app.models import Pest
from app.models import Issue
from app.models import Maintenance
from app.models import ProductionStage
from werkzeug.wrappers import response
from flask_sqlalchemy import SQLAlchemy
from app.core.maintenance import greenhouses as _greenhouse_issuebefore
from sqlalchemy import or_
from app.core.planting import crops
from app.helper import valid_uuid, valid_date
from app.models import GreenhouseMaintenance
from app.core.planting import production_stages
from app.core.maintenance import photos as pndphotos
customer_support = Blueprint('customer_support', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_support.config = {}

import boto3


load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')




@customer_support.route('/', methods=['POST', 'GET'], defaults={"page": 1})
@customer_support.route('/<int:page>', methods=['POST', 'GET'])
def main(page):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    page = page
    prev = page-1
    next = page+1

    no_of_posts = config.PAGE_LIMIT
    _device_support = []
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None

    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
     
    
    _details = []
    _details = pests_diseases.fetch_by_customer(_customery_final_id).get_json()
    _green_house = []
    _green_house = greenhouse_issues.fetch_by_customer_all(_customery_final_id).get_json()
    _device_support = shield_issues.fetch_by_customers(_customery_final_id).get_json()
    _screen_house = []
    _screen_house = screenhouse_issues.fetch_by_customer(_customery_final_id).get_json()
    _agronomist_visit = []
    _agronomist_visit = agronomist_visit.fetch_by_customer(_customery_final_id).get_json()
    _shadnet_issue = []
    _shadnet_issue = shadenet_issues.fetch_by_customer_all(_customery_final_id).get_json()
    _trace_report =[]
    _trace_report = traceability_report_crops.fetch_by_customer_all(_customery_final_id).get_json()
    _other_report = []
    _other_report = other_request.fetch_by_customer_all(_customery_final_id).get_json()
    closure_data= []
    closure_data = closure.fetch_all().get_json()
    
    print("Close data",closure_data)
    # _trace_report = traceability_report_requests.fetch_by_customerall(_customery_final_id).get_json()
    maintain1 = maintenance.fetch_all_by_customer(_customery_final_id).get_json()
    total = {}
    total = round(len(maintain1)/no_of_posts)

    maintain = maintenance.fetch_all_by_customer1(page,_customery_final_id).get_json()
    # _customer = customer_user.fetch_all1(page).get_json()
    if request.method == 'POST' and 'tag_main' in request.form:
        tag = request.form["tag_main"]
        print("Tagggggggg",tag)
        if tag:
            # search = "%{}%".format(tag)
            search = tag
            if search:
               _maintainy = []
               if search.isnumeric() == True:
                 _maintainy = db.session.query(Maintenance).filter(or_((Maintenance.customer_id == search),(Maintenance.serial == search),(Maintenance.creator_id == search))).all()
               response = []
               for mntn in _maintainy:
                   
                response.append(maintenance.maintenance_obj(mntn))
            maintain = jsonify(response).get_json()
   
    
    # print("Maintain234",maintain)
    


    return render_template(
        'customers/support/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        MAINTAIN = maintain,
        PESTDETAILS = _details,
        DEVICESUPPORT = _device_support,
        SCREENHOUSE = _screen_house,
        GREENHOUSE = _green_house,
        SHADNET = _shadnet_issue,
        FARM=swapped_firm_details,
        VISIT = _agronomist_visit,
        CLOSURE = closure_data,
        prev=prev, next=next,
        total=total,
        TRACE = _trace_report,
        OTHER = _other_report,
        data={"date":str(datetime.now().year)}
    )


   


@customer_support.route('/edit/<string:id>/<string:mid>', methods=['POST', 'GET'])
def _edit_support(id,mid):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    
    
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
    swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']


    _pest = pesto.fetch_all().get_json()
    # print("Pest all ",_pest)
    _disease = disease.fetch_all().get_json()
    _plant_all = plants.fetch_all().get_json()
    _varities = varieties.fetch_all().get_json()
    _stages = production_stages.fetch_all().get_json()
    _shields = shields.fetch_by_customer(_customery_final_id).get_json()
    action_response = {}



    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _production_stage_id = posted.get('production_stage_id')
     _variety_id = posted.get('variety_id')
     _plant_id = posted.get('plant_id')
     _customer_greenhouse_id = posted.get('customer_greenhouse_id')
     _customer_shield_id = posted.get('customer_shield_id')
     _pest_id = posted.get('pest_id')
     _disease_id = posted.get('disease_id')
     _customer_id = _customery_final_id

     print("All dataaaa",_notes,_production_stage_id,_variety_id,_plant_id,_customer_greenhouse_id, _customer_shield_id,_disease_id,_pest_id)


     _support_pest_edit = pests_diseases.edit(
                              id,
                             json.dumps({
                              "notes": _notes,
                              "production_stage_id": _production_stage_id,
                              "variety_id" : _variety_id,
                              "plant_id" : _plant_id,
                              "customer_greenhouse_id" : _customer_greenhouse_id,
                              "customer_shield_id" : _customer_shield_id,
                              "pest_id": _pest_id,
                              "disease_id" :_disease_id,
                              "maintenance_id":mid,
                              "customer_id" : _customer_id,
                              "creator_id" : _this_org1

                       
                       
                            
                             })
                             
                       )
     print(str(_support_pest_edit[0].get_json()))
     if _support_pest_edit :
         action_response = {
                              "type": "success",
                            "message": "Support is requested"
                                        }
     
    
    

   

    return render_template(
        'customers/support/detail_edit.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        PEST = _pest,
        STAGES = _stages,
        DISEASE = _disease,
        PLANTS = _plant_all,
        VARITIES = _varities,
        FORM_ACTION_RESPONSE=action_response,
        SHIELDS = _shields,
        data={"date":str(datetime.now().year), "today": datetime.now()}
        # data={"date":str(datetime.now().year)}
    )

@customer_support.route('/request', methods=['POST', 'GET'])
def _requests():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    
    farms = []
    farm_id_action ={}
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    action_response = {}
    _support_request = {}
    posted = request.form
    if posted:
     _serial = posted.get('serial')
     _type = None
    #  _type = posted.get('type3')
     _customer = _customery_final_id
     _photos = posted.get('photos')
     _detail = posted.get('detail')
     _assignment = posted.get('assignment')
     _closure = posted.get('closure')
     _created_by = posted.get('creator_id')
     _status = posted.get('status')
    

     _support_request = maintenance.add_new(
                             json.dumps({
                              "serial": _serial,
                              "type" : _type,
                              "creator_id" : _this_org1,
                              "customer_id" : _customer
                             
                             })
                             
                       )
     print(str(_support_request[0].get_json()))

     if _support_request :
         action_response = {
                              "type": "success",
                            "message": "Support is requested"
                                        }
    

   

    return render_template(
        'customers/support/request.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FORM_ACTION_RESPONSE=action_response,
        data={"date":str(datetime.now().year), "today": datetime.now()}
        # data={"date":str(datetime.now().year)}
    )
    

@customer_support.route('/request/type', methods=['POST', 'GET'])
def type():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    farms = []
    farm_id_action = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
   
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    # _maintain_type = maintenance.maintenance_type_name(1)
    # print("_maintain_type",_maintain_type)
    _type = request.form.get('type')
    action_response = {}
   
    posted = request.form
    if posted:

        # if "type" in posted:
              _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
             
    
            #   _serial = posted.get('serial')
              _type = posted.get('type')
             
              
              _customer = _customery_final_id
          
            #   print("seriel",_this_org1,_customer)
  
            #   print("IDSSSSSSSSSSkuhkkkkkkkiiiii",_maintain['id'])
              if _maintain != {}:
           
               _support_request_types = maintenance.edit(
                        _maintain['id'],
                        json.dumps({

                            "type": _type,
                            
                        }),
                        True
                        )
               if _support_request_types:

                action_response = {
                                "type": "success",
                                "message": "Type added."
                            }

            #   if _support_request_types[1] == 200:

            #                  return jsonify({
            #                 "status": True,

            #                     })

    return render_template(
        'customers/support/type.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        TYPE = _type,
        FORM_ACTION_RESPONSE=action_response,
        # DATA = _data,
        data={"date":str(datetime.now().year)}
    )


@customer_support.route('/edit/<string:id>', methods=['POST', 'GET'])
def type_edit(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    farms = []
    farm_id_action = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
   
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    # _maintain_type = maintenance.maintenance_type_name(1)
    # print("_maintain_type",_maintain_type)
    _type = request.form.get('type')
    action_response = {}
   
    posted = request.form
    if posted:

        # if "type" in posted:
              _maintain = maintenance.fetch_one(id,True).get_json()
              _type = posted.get('type')
             
              
              _customer = _customery_final_id
          
            #   print("seriel",_this_org1,_customer)
  
            #   print("IDSSSSSSSSSSkuhkkkkkkkiiiii",_maintain['id'])
              if _maintain != {}:
           
               _support_request_types = maintenance.edit(
                        _maintain['id'],
                        json.dumps({

                            "type": _type,
                            
                        }),
                        True
                        )
               if _support_request_types:

                action_response = {
                                "type": "success",
                                "message": "Type Updated."
                            }

            #   if _support_request_types[1] == 200:

            #                  return jsonify({
            #                 "status": True,

            #                     })

    return render_template(
        'customers/support/type.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        TYPE = _type,
        FORM_ACTION_RESPONSE=action_response,
        # DATA = _data,
        data={"date":str(datetime.now().year)}
    )

@customer_support.route('/request/detail', methods=['POST', 'GET'])
def detail():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action ={}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
   
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id :
     cutomers_uid = _cutomer_real_id['customer']['id']
    
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type']

    _pestoo = pesto.fetch_all().get_json()
    # print("Pest all ",_pest)
    _stages = production_stages.fetch_all().get_json()
    _disease = disease.fetch_all().get_json()
    _plant_all = plants.fetch_all().get_json()
    _varities = varieties.fetch_all().get_json()
    _shields = shields.fetch_by_customer(_customery_final_id).get_json()

    # print("Shieldsssss",_shields)
   
    
    _greenhouses1 = greenhouses.fetch_by_customer_all(_customery_final_id).get_json()
    # print("green houses",_greenhouses1)
    action_response ={}

    # _satge_alter = request.form.get('satge_alter')

    

    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _maintenance_id = posted.get('maintenance_id')
     _production_stage_id = posted.get('production_stage_id')
     _variety_id = posted.get('variety_id')
     _plant_id = posted.get('plant_id')
     _customer_greenhouse_id = posted.get('customer_greenhouse_id')
     _customer_shield_id = posted.get('customer_shield_id')
     _pest_id = posted.get('pest_id')
     _pest_alter = posted.get('pest_alter')
     _disease_id = posted.get('disease_id')
     _satge_alter = posted.get('satge_alter')
     _customer_id = _customery_final_id
    #  _created_by = posted.get('creator_id')
     _status = posted.get('status')
     print("Notes", _variety_id)
     stagyy = None
     if _maintain != {}:
      pest_disease_check = {}
      maintain_real = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']
      pest_disease_check = pests_diseases.fetch_by_maintenance(maintain_real).get_json()
     
     

     

     if _maintain != {} and _satge_alter and _pest_alter and pest_disease_check == {}:


         exists = production_stages.fetch_by_name(_satge_alter).get_json()
         if exists:
                action_response = {
                    "type": "danger",
                    "message": "Staging name already exists please select from list."
                }

         else:
            _stagess = ProductionStage(
                        uid=uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _satge_alter,
                        description = None,
                        creator_id = _this_org

                    )
            db.session.add(_stagess)
            db.session.commit()
            
            stagyy = production_stages.fetch_by_id(_stagess.id).get_json()['id']
            print("Stages",stagyy)
         exists = pesto.fetch_by_name(_pest_alter).get_json()
         if exists:
                action_response = {
                    "type": "danger",
                    "message": "Pest name already exists please select from list."
                }

         else:
            _pest = Pest(
                        uid=uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _pest_alter,
                        description = None,
                        creator_id = _this_org

                    )
            db.session.add(_pest)
            db.session.commit()

            
            
            pestyy = pesto.fetch_by_id(_pest.id).get_json()['id']
            print("pest_id",pestyy)



            if _maintain != {}: 
     
                _support_pest_details = pests_diseases.add_new(
                             json.dumps({
                              "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "production_stage_id": stagyy,
                              "variety_id" : _variety_id,
                              "plant_id" : _plant_id,
                              "customer_greenhouse_id" : _customer_greenhouse_id,
                              "customer_shield_id" : _customer_shield_id,
                              "pest_id": pestyy,
                              "disease_id" :_disease_id,
                              "customer_id" : _customer_id,
                              "creator_id" : _this_org1
                            
                             })
                             
                       )
                print(str(_support_pest_details[0].get_json()))
                if _support_pest_details:
                  action_response = {
                    "type": "success",
                    "message": "Support maintenance submitted successfully."
                }

     elif _maintain != {} and _satge_alter and pest_disease_check == {}:

         exists = production_stages.fetch_by_name(_satge_alter).get_json()
         if exists:
                action_response = {
                    "type": "danger",
                    "message": "Staging name already exists please select from list."
                }

         else:
            _stagess = ProductionStage(
                        uid=uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _satge_alter,
                        description = None,
                        creator_id = _this_org

                    )
            db.session.add(_stagess)
            db.session.commit()
            
            stagyy = production_stages.fetch_by_id(_stagess.id).get_json()['id']
            print("Stages",stagyy)

            
     
            _support_pest_details = pests_diseases.add_new(
                             json.dumps({
                              "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "production_stage_id": stagyy,
                              "variety_id" : _variety_id,
                              "plant_id" : _plant_id,
                              "customer_greenhouse_id" : _customer_greenhouse_id,
                              "customer_shield_id" : _customer_shield_id,
                              "pest_id": _pest_id,
                              "disease_id" :_disease_id,
                              "customer_id" : _customer_id,
                              "creator_id" : _this_org1
                            
                             })
                             
                       )
            print(str(_support_pest_details[0].get_json()))
            if _support_pest_details:
                  action_response = {
                    "type": "success",
                    "message": "Support maintenance submitted successfully."
                }
     elif _maintain != {} and _pest_alter and pest_disease_check == {}:

         exists = pesto.fetch_by_name(_pest_alter).get_json()
         if exists:
                action_response = {
                    "type": "danger",
                    "message": "Pest name already exists please select from list."
                }

         else:
            _pest = Pest(
                        uid=uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _pest_alter,
                        description = None,
                        creator_id = _this_org

                    )
            db.session.add(_pest)
            db.session.commit()

            
            
            pestyy = pesto.fetch_by_id(_pest.id).get_json()['id']
            print("pest_id",pestyy)

           
     
            _support_pest_details = pests_diseases.add_new(
                             json.dumps({
                              "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "production_stage_id": _production_stage_id,
                              "variety_id" : _variety_id,
                              "plant_id" : _plant_id,
                              "customer_greenhouse_id" : _customer_greenhouse_id,
                              "customer_shield_id" : _customer_shield_id,
                              "pest_id": pestyy,
                              "disease_id" :_disease_id,
                              "customer_id" : _customer_id,
                              "creator_id" : _this_org1
                            
                             })
                             
                       )
            print(str(_support_pest_details[0].get_json()))
            if _support_pest_details:
                  action_response = {
                    "type": "success",
                    "message": "Support maintenance submitted successfully."
                }
     elif pest_disease_check == {} and _maintain != {}:

          
     
                _support_pest_details = pests_diseases.add_new(
                             json.dumps({
                              "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "production_stage_id": _production_stage_id,
                              "variety_id" : _variety_id,
                              "plant_id" : _plant_id,
                              "customer_greenhouse_id" : _customer_greenhouse_id,
                              "customer_shield_id" : _customer_shield_id,
                              "pest_id": _pest_id,
                              "disease_id" :_disease_id,
                              "customer_id" : _customer_id,
                              "creator_id" : _this_org1
                            
                             })
                             
                       )
                print(str(_support_pest_details[0].get_json()))
                if _support_pest_details:
                  action_response = {
                    "type": "success",
                    "message": "Support maintenance submitted successfully."
                }
    
    

    return render_template(
        'customers/support/detail.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        PEST = _pestoo,
        DISEASE = _disease,
        PLANTS = _plant_all,
        VARITIES = _varities,
        SHIELDS = _shields,
        STAGES = _stages,
        FORM_ACTION_RESPONSE=action_response,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )




@customer_support.route('/request/agronomy', methods=['POST', 'GET'])
def agronimist():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farms = []
    farm_id_action = {}
    
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()

    _brand = supply.fetch_all().get_json()
    _components = components.fetch_all().get_json()


    posted = request.form
    if posted:
     _visit_date = posted.get('visit_date')
     _visit_time = posted.get('visit_time')
     _reorder_fertilizer = posted.get('reorder_fertilizer')
     _reorder_agrochemical = posted.get('reorder_agrochemical')
     _fertilizer_id = posted.get('fertilizer_id')
     _fertilizer_main_component_id = posted.get('fertilizer_main_component_id')
     _fertilizer_amount = posted.get('fertilizer_amount')
     _agrochemical_id = posted.get('agrochemical_id')
     _agrochemical_main_component_id = posted.get('agrochemical_main_component_id')
     _agrochemical_amount = posted.get('agrochemical_amount')
     _notes = posted.get('notes')
     _observations = posted.get('observations')
     _risk_observations = posted.get('risk_observations')
     _recommendations = posted.get('recommendations')
     _maintenance_id = _maintain['id'],
     _customer_id = _customery_final_id

     _created_by = _this_org1
     _status = posted.get('status')

     if _maintain != {}:
         agronomy_check = {}
         maintain_real = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']
         agronomy_check = agronomist_visit.fetch_by_maintenance(maintain_real).get_json()
     
    #  print("Date and time ",_visit_date,_visit_time,_reorder_fertilizer,_reorder_agrochemical,_notes,_maintenance_id)

     if _maintain != {} and agronomy_check == {}: 
      _support_agronomy_details = agronomist_visit.add_new(
                             json.dumps({
                              "visit_date": _visit_date,
                              "visit_time" : _visit_time,
                              "reorder_fertilizer": _reorder_fertilizer,
                              "reorder_agrochemical" : _reorder_agrochemical,
                              "fertilizer_id" : _fertilizer_id,
                              "fertilizer_main_component_id" : _fertilizer_main_component_id,
                              "fertilizer_amount" : _fertilizer_amount,
                              "agrochemical_id": _agrochemical_id,
                              "agrochemical_main_component_id" :_agrochemical_main_component_id,
                              "agrochemical_amount" : _agrochemical_amount,
                              "notes" : _notes,
                              "observations": _observations,
                              "risk_observations":_risk_observations,
                              "recommendations" : _recommendations,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
      print(str( _support_agronomy_details[0].get_json()))
    

    return render_template(
        'customers/support/agronomy.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        BRAND = _brand,
        COMPONENT = _components,
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_support.route('/edit/agronomy/<string:id>/<string:vid>', methods=['POST', 'GET'])
def _edit_agronomy(id,vid):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    farm_id_action = {}
    farms = []
    
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _brand =[]
    _components =[]

    _brand = supply.fetch_all().get_json()
    _components = components.fetch_all().get_json()


    posted = request.form
    if posted:
     _visit_date = posted.get('visit_date')
     _visit_time = posted.get('visit_time')
     _reorder_fertilizer = posted.get('reorder_fertilizer')
     _reorder_agrochemical = posted.get('reorder_agrochemical')
     _fertilizer_id = posted.get('fertilizer_id')
     _fertilizer_main_component_id = posted.get('fertilizer_main_component_id')
     _fertilizer_amount = posted.get('fertilizer_amount')
     _agrochemical_id = posted.get('agrochemical_id')
     _agrochemical_main_component_id = posted.get('agrochemical_main_component_id')
     _agrochemical_amount = posted.get('agrochemical_amount')
     _notes = posted.get('notes')
     _observations = posted.get('observations')
     _risk_observations = posted.get('risk_observations')
     _recommendations = posted.get('recommendations')
     _maintenance_id = vid,
     _customer_id = _customery_final_id

     _created_by = _this_org1
     _status = posted.get('status')


     _support_agronomy_edit = agronomist_visit.edit(
                              id,
                             json.dumps({
                              "visit_date": _visit_date,
                              "visit_time" : _visit_time,
                              "reorder_fertilizer": _reorder_fertilizer,
                              "reorder_agrochemical" : _reorder_agrochemical,
                              "fertilizer_id" : _fertilizer_id,
                              "fertilizer_main_component_id" : _fertilizer_main_component_id,
                              "fertilizer_amount" : _fertilizer_amount,
                              "agrochemical_id": _agrochemical_id,
                              "agrochemical_main_component_id" :_agrochemical_main_component_id,
                              "agrochemical_amount" : _agrochemical_amount,
                              "notes" : _notes,
                              "observations": _observations,
                              "risk_observations":_risk_observations,
                              "recommendations" : _recommendations,
                              "maintenance_id" : vid,
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by

                       
                            
                             })
                             
                       )
     print(str(_support_agronomy_edit[0].get_json()))
    
    

   

    return render_template(
        'customers/support/agronomy.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        BRAND = _brand,
        COMPONENT = _components,
        data={"date":str(datetime.now().year), "today": datetime.now()}
        # data={"date":str(datetime.now().year)}
    )



@customer_support.route('/request/device_issue', methods=['POST', 'GET'])
def device_issue():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    _issues = issues.fetch_by_applies_to(3).get_json()
    
    _shields = shields.fetch_by_customer(_customery_final_id).get_json()
   
    # print("issues",_shields)

    action_response ={}

    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _issue_id = posted.get('issue_id')
     _shield_maintenance_id = posted.get('shield_maintenance_id')
     _maintenance_id = posted.get('maintenance_id')
     _customer_id = _customery_final_id
     _created_by = _this_org1
     _status = posted.get('status')
     _dissue_alter = posted.get('dissue_alter')

     if _maintain != {}:
         device_check = {}
         maintain_real = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']
         device_check = shield_issues.fetch_by_maintenance(maintain_real).get_json()


     if _maintain != {} and device_check == {} and _dissue_alter : 

            exists = issues.fetch_by_name(_dissue_alter).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Issue already exists please select from list."
                }

            else:

             d_issue = Issue(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _dissue_alter,
                        description = None,
                        applies_to = config.MAINTENANCE_SHIELD_ISSUE,
                        creator_id = _this_org
             )
             db.session.add(d_issue)
             db.session.commit()
             issueyy = issues.fetch_by_id(d_issue.id).get_json()['id']

             _support_device_details = shield_issues.add_new(
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : issueyy,
                              "shield_id": _shield_maintenance_id,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
             print(str( _support_device_details[0].get_json()))


             if _support_device_details:
                  action_response = {
                    "type": "success",
                    "message": "Device maintenance submitted successfully."
                }
             

            

     elif _maintain != {} and device_check == {} :
            _support_device_details = shield_issues.add_new(
                json.dumps({
                    "notes": _notes,
                    "issue_id": _issue_id,
                    "shield_id": _shield_maintenance_id,
                    "maintenance_id": _maintain['id'],
                    "customer_id": _customer_id,
                    "creator_id": _created_by

                })

            )
            print(str(_support_device_details[0].get_json()))
            if _support_device_details:
                  action_response = {
                    "type": "success",
                    "message": "Device maintenance submitted successfully."
                }
     
    

    return render_template(
        'customers/support/device_issue.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        ISSUES = _issues,
        SHIELDS = _shields,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date":str(datetime.now().year)}
    )


@customer_support.route('/edit/device_issue/<string:id>/<string:did>', methods=['POST', 'GET'])
def device_issue_edit(id,did):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    _issues = issues.fetch_by_applies_to(3).get_json()
    
    _shields = shields.fetch_by_customer(_customery_final_id).get_json()
   
    # print("issues",_shields)


    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _issue_id = posted.get('issue_id')
     _shield_maintenance_id = posted.get('shield_maintenance_id')
     _maintenance_id = posted.get('maintenance_id')
     _customer_id = _customery_final_id
     _created_by = _this_org1
     _status = posted.get('status')


     _support_device_details = shield_issues.edit(
                                id,
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : _issue_id,
                              "shield_id": _shield_maintenance_id,
                              "maintenance_id" : did,
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
     print(str( _support_device_details[0].get_json()))
     
    

    return render_template(
        'customers/support/device_issue_edit.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        ISSUES = _issues,
        SHIELDS = _shields,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )



@customer_support.route('/request/greenhouse_issue', methods=['POST', 'GET'])
def greenhouse_issue():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type'] 
    _issues = []
    _c_greens = []
    _issues = issues.fetch_by_applies_to(config.MAINTENANCE_GREENHOUSE_ISSUE).get_json()
    _c_greens = grn_h.fetch_by_customer_all(_customery_final_id).get_json()
   
    action_response = {}

    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _issue_id = posted.get('issue_id')
     _greenhouse_maintenance_id = posted.get('greenh_id')
    #  _greenhouse_maintenance_id = posted.get('greenhouse_maintenance_id')
     _maintenance_id = posted.get('maintenance_id')
     _gissue_alter = posted.get('gissue_alter')
     _customer_id = _customery_final_id
     _created_by = _this_org1

     if _maintain != {}:
            green_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            green_check = greenhouse_issues.fetch_by_maintenance(
                maintain_real).get_json()

     if _greenhouse_maintenance_id:
      _green_c_main_id = grn_h.fetch_one(_greenhouse_maintenance_id,True).get_json()['real_id']
     maintain_real_id = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']

     if _greenhouse_maintenance_id:
                _new_greenhouse = GreenhouseMaintenance(
                        uid=uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        notes = _notes,
                        customer_greenhouse_id = _green_c_main_id,
                        maintenance_id = maintain_real_id,
                        customer_id = _customer_id,
                        creator_id = _this_org

                       
                        # cover_photo_media_id = _cover_photo_media_id
                        # phone_number = _phone_no,

                    )
                db.session.add(_new_greenhouse)
                db.session.commit()


     if _new_greenhouse and _maintain != {} and green_check =={} and _gissue_alter:
                
                exists = issues.fetch_by_name(_gissue_alter).get_json()
                if exists:
                    action_response = {
                      "type": "danger",
                      "message": "Issue already exists please select from list."
                    }

                else:

                    d_issue = Issue(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _gissue_alter,
                        description = None,
                        applies_to = config.MAINTENANCE_GREENHOUSE_ISSUE,
                        creator_id = _this_org
                    )
                    db.session.add(d_issue)
                    db.session.commit()
                    gissuey = issues.fetch_by_id(d_issue.id).get_json()['id']

                    _support_greenhouse_details = greenhouse_issues.add_new(
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : gissuey,
                              "greenhouse_maintenance_id": _new_greenhouse.id,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
                    print(str( _support_greenhouse_details[0].get_json()))

                    print(str(_support_greenhouse_details[0].get_json()))
                    if _support_greenhouse_details:
                      action_response = {
                    "type": "success",
                    "message": "Greenhouse maintenance submitted successfully."
                      }
     elif _new_greenhouse and green_check == {} and _maintain != {} :


                 _support_greenhouse_details = greenhouse_issues.add_new(
                      json.dumps({
                        "notes": _notes,
                        "issue_id": _issue_id,
                        "greenhouse_maintenance_id": _new_greenhouse.id,
                        "maintenance_id": _maintain['id'],
                        "customer_id": _customer_id,
                        "creator_id": _created_by

                       })

                    )
                 print(str(_support_greenhouse_details[0].get_json()))
                 if _support_greenhouse_details:
                      action_response = {
                    "type": "success",
                    "message": "Greenhouse maintenance submitted successfully."
                      }

    

  
    

    return render_template(
        'customers/support/greenhouse_issue.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        ISSUES = _issues,
        GREENHOUSE = _c_greens,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date":str(datetime.now().year)}
    )


     



@customer_support.route('/edit/greenhouseissue/<string:id>/<string:gid>/<string:cid>', methods=['POST', 'GET'])
def greenhouse_issue_edit(id,gid,cid):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type'] 
    _issues = []
    _c_greens = []
    _issues = issues.fetch_by_applies_to(config.MAINTENANCE_GREENHOUSE_ISSUE).get_json()
    _c_greens = grn_h.fetch_by_customer_all(_customery_final_id).get_json()


    
   
    _this_org1 = session["profile"]["user"]["id"]

    _maintain = maintenance.fetch_by_customer_id(cid).get_json()
    _issues = issues.fetch_by_applies_to(config.MAINTENANCE_GREENHOUSE_ISSUE).get_json()
    _c_greens = grn_h.fetch_by_customer_all(cid).get_json()
   


    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _issue_id = posted.get('issue_id')
     _greenhouse_maintenance_id = posted.get('greenh_id')
     _maintenance_id = posted.get('maintenance_id')
     _customer_id =  cid
     _created_by = _this_org1
     _status = posted.get('status')
     _greenhouse_before_id = _greenhouse_issuebefore.fetch_by_customer(cid).get_json()['id']
     _greenhouse_before_real_id = _greenhouse_issuebefore.fetch_one(_greenhouse_before_id,True).get_json()['real_id']

     _green_c_main_id = grn_h.fetch_one(_greenhouse_maintenance_id,True).get_json()['real_id']
     maintain_real_id = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']

     if _greenhouse_maintenance_id:
                _edit_greenhouse = _greenhouse_issuebefore.edit(
                        _greenhouse_before_id,
                        json.dumps({
                      
                        "notes" : _notes,
                        "customer_greenhouse_id" : _greenhouse_maintenance_id,
                        "maintenance_id" : gid,
                        "customer_id" : _customer_id,
                        "creator_id" : _created_by

                       
                        # cover_photo_media_id = _cover_photo_media_id
                        # phone_number = _phone_no,

                     })
                             
                       )
                print(str(_edit_greenhouse[0].get_json()))
               

            
    
     if _greenhouse_before_id:

      _support_greenhouse_edit = greenhouse_issues.edit(
                             id,
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : _issue_id,
                              "greenhouse_maintenance_id": _greenhouse_before_real_id,
                              "maintenance_id" : gid,
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
      print(str( _support_greenhouse_edit[0].get_json()))
    


    # posted = request.form
    # if posted:
    #  _notes = posted.get('notes')
    #  _issue_id = posted.get('issue_id')
    #  _greenhouse_maintenance_id = posted.get('greenh_id')
    # #  _greenhouse_maintenance_id = posted.get('greenhouse_maintenance_id')
    #  _maintenance_id = posted.get('maintenance_id')
    #  _customer_id = _customery_final_id
    #  _created_by = _this_org1
    #  if _greenhouse_maintenance_id:
    #   _green_c_main_id = grn_h.fetch_one(_greenhouse_maintenance_id,True).get_json()['real_id']
    #  maintain_real_id = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']

    #  if _greenhouse_maintenance_id:
    #             _new_greenhouse = GreenhouseMaintenance(
    #                     uid=uuid.uuid4(),
    #                     status = config.STATUS_ACTIVE,
    #                     notes = _notes,
    #                     customer_greenhouse_id = _green_c_main_id,
    #                     maintenance_id = maintain_real_id,
    #                     customer_id = _customer_id,
    #                     creator_id = _this_org

                       
    #                     # cover_photo_media_id = _cover_photo_media_id
    #                     # phone_number = _phone_no,

    #                 )
    #             db.session.add(_new_greenhouse)
    #             db.session.commit()


    #  if _new_greenhouse:
    #                _support_greenhouse_details = greenhouse_issues.add_new(
    #                          json.dumps({
    #                           "notes": _notes,
    #                           "issue_id" : _issue_id,
    #                           "greenhouse_maintenance_id": _new_greenhouse.id,
    #                           "maintenance_id" : _maintain['id'],
    #                           "customer_id" : _customer_id,
    #                           "creator_id" : _created_by
                            
    #                          })
                             
    #                    )
    #                print(str( _support_greenhouse_details[0].get_json()))

    

  
    

    return render_template(
        'customers/support/greenhouse_issue_edit.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        ISSUES = _issues,
        GREENHOUSE = _c_greens,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )



@customer_support.route('/request/screenhouse_issue', methods=['POST', 'GET'])
def screenhouse_issue():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type'] 
    _screenhouse =[]
    _screenhouse = screenhouses.fetch_by_customer(_customery_final_id).get_json()
    _greenhouse = []
    _greenhouse = greenhouses.fetch_by_customer_all(_customery_final_id).get_json()
    _issues =[]
    _issues = issues.fetch_by_applies_to(config.MAINTENANCE_SCREENHOUSE_ISSUE).get_json()

    action_response = {}

    # print("Screenhouse",farms)

    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _issue_id = posted.get('issue_id')
     _customer_id = _customery_final_id
     _screenhouse_maintenance_id = posted.get('screenhouse_maintenance_id')
     _maintenance_id = posted.get('maintenance_id')
     _created_by = _this_org1
     _status = posted.get('status')
     _scissue_alter = posted.get('scissue_alter')


     if _maintain != {}:
            screen_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            screen_check = screenhouse_issues.fetch_by_maintenance(
                maintain_real).get_json()

     if _maintain != {} and screen_check =={} and _scissue_alter : 

                  exists = issues.fetch_by_name(_scissue_alter).get_json()
                  if exists:
                    action_response = {
                      "type": "danger",
                      "message": "Issue already exists please select from list."
                    }

                  else:

                    sc_issue = Issue(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _scissue_alter,
                        description = None,
                        applies_to = config.MAINTENANCE_SCREENHOUSE_ISSUE,
                        creator_id = _this_org
                    )
                    db.session.add(sc_issue)
                    db.session.commit()
                    scissuey = issues.fetch_by_id(sc_issue.id).get_json()['id']




                    _support_screenhouse_details = screenhouse_issues.add_new(
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : scissuey,
                              "screenhouse_maintenance_id": _screenhouse_maintenance_id,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                        )
                    print(str(_support_screenhouse_details[0].get_json()))
                    if _support_screenhouse_details:
                      action_response = {
                    "type": "success",
                    "message": "Screenhouse maintenance submitted successfully."
                      }

     elif _maintain != {} and screen_check =={} :


                    _support_screenhouse_details = screenhouse_issues.add_new(
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : _issue_id,
                              "screenhouse_maintenance_id": _screenhouse_maintenance_id,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                        )
                    print(str(_support_screenhouse_details[0].get_json()))

                    if _support_screenhouse_details:
                      action_response = {
                    "type": "success",
                    "message": "Screenhouse maintenance submitted successfully."
                      }
   
    

    return render_template(
        'customers/support/screenhouse_issue.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        SCREENHOUSE = _screenhouse,
        ISSUES = _issues,
        GRNHOUSE =  _greenhouse,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date":str(datetime.now().year)}
    )


    

@customer_support.route('/edit/screenhouseissue/<string:id>/<string:sid>', methods=['POST', 'GET'])
def screenhouse_issue_edit(id,sid):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
   
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type'] 
    _screenhouse =[]
    _screenhouse = screenhouses.fetch_by_customer(_customery_final_id).get_json()
    _greenhouse = []
    _greenhouse = greenhouses.fetch_by_customer_all(_customery_final_id).get_json()
    _issues =[]
    _issues = issues.fetch_by_applies_to(5).get_json()

  

    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _issue_id = posted.get('issue_id')
     _customer_id = _customery_final_id
     _screenhouse_maintenance_id = posted.get('screenhouse_maintenance_id')
     _maintenance_id = posted.get('maintenance_id')
     _created_by = _this_org1
     _status = posted.get('status')

     _support_screenhouse_edit = screenhouse_issues.edit(
                              id,
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : _issue_id,
                              "screenhouse_maintenance_id": _screenhouse_maintenance_id,
                              "maintenance_id" : sid,
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
     print(str(_support_screenhouse_edit[0].get_json()))
   
    

    return render_template(
        'customers/support/screenhouse_issue_edit.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        SCREENHOUSE = _screenhouse,
        ISSUES = _issues,
        GRNHOUSE =  _greenhouse,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )



@customer_support.route('/request/shadnet_issue', methods=['POST', 'GET'])
def shadnet_issue():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type'] 
    # print("Maintain type ",_maintain_type)
    _shadnet =[]
    _shadnet = shadenets.fetch_by_customer(_customery_final_id).get_json()
    _greenhouse = []
    _greenhouse = greenhouses.fetch_by_customer_all(_customery_final_id).get_json()
    _issues =[]
    _issues = issues.fetch_by_applies_to(6).get_json()
    # print("Shadnet",_shadnet)
    action_response ={}



    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _issue_id = posted.get('issue_id')
     _customer_id = _customery_final_id
     _shadenet_maintenance_id = posted.get('shadenet_maintenance_id')
     _maintenance_id = posted.get('maintenance_id')
     _created_by = _this_org1
     _status = posted.get('status')
     _sdissue_alter = posted.get('sdissue_alter')

     if _maintain != {}:
            shad_check = {}
            maintain_real = maintenance.fetch_one(
                _maintain['id'], True).get_json()['real_id']
            shad_check = shadenet_issues.fetch_by_maintenance(
                maintain_real).get_json()

     if _maintain != {} and shad_check == {} and _sdissue_alter:



                exists = issues.fetch_by_name(_sdissue_alter).get_json()
                if exists:
                    action_response = {
                      "type": "danger",
                      "message": "Issue already exists please select from list."
                    }

                else:

                    sd_issue = Issue(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _sdissue_alter,
                        description = None,
                        applies_to = config.MAINTENANCE_SHADENET_ISSUE,
                        creator_id = _this_org
                    )
                    db.session.add(sd_issue)
                    db.session.commit()
                    sdissuey = issues.fetch_by_id(sd_issue.id).get_json()['id']

                    _support_shadnet_details = shadenet_issues.add_new(
                        json.dumps({
                            "notes": _notes,
                            "issue_id": sdissuey,
                            "shadenet_maintenance_id": _shadenet_maintenance_id,
                            "maintenance_id": _maintain['id'],
                            "customer_id": _customer_id,
                            "creator_id": _created_by

                          })

                         )
                    print(str(_support_shadnet_details[0].get_json()))
                    if _support_shadnet_details:
                      action_response = {
                            "type": "success",
                            "message": "Shadenet maintenance submitted successfully."
                        }


     elif _maintain != {} and shad_check == {} :


             _support_shadnet_details = shadenet_issues.add_new(
                        json.dumps({
                            "notes": _notes,
                            "issue_id": _issue_id,
                            "shadenet_maintenance_id": _shadenet_maintenance_id,
                            "maintenance_id": _maintain['id'],
                            "customer_id": _customer_id,
                            "creator_id": _created_by

                          })

                         )
             print(str(_support_shadnet_details[0].get_json())) 
             if _support_shadnet_details:
                      action_response = {
                            "type": "success",
                            "message": "Shadenet maintenance submitted successfully."
                      }

    #   _support_shadnet_details = shadenet_issues.add_new(
    #                          json.dumps({
    #                           "notes": _notes,
    #                           "issue_id" : _issue_id,
    #                           "shadenet_maintenance_id": _shadenet_maintenance_id,
    #                           "maintenance_id" : _maintain['id'],
    #                           "customer_id" : _customer_id,
    #                           "creator_id" : _created_by
                            
    #                          })
                             
    #                    )
    #   print(str(_support_shadnet_details[0].get_json()))
   
    

    return render_template(
        'customers/support/shadnet_issue.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        ISSUES = _issues,
        SHADENET = _shadnet,
        GRNHOUSE =  _greenhouse,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        FORM_ACTION_RESPONSE=action_response,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )





@customer_support.route('/edit/shadenetissue/<string:id>/<string:shid>', methods=['POST', 'GET'])
def shadnet_edit_issue(id,shid):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type'] 
    # print("Maintain type ",_maintain_type)
    _shadnet =[]
    _shadnet = shadenets.fetch_by_customer(_customery_final_id).get_json()
    _issues =[]
    _greenhouse = []
    _greenhouse = greenhouses.fetch_by_customer_all(_customery_final_id).get_json()
    _issues = issues.fetch_by_applies_to(6).get_json()
  



    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _issue_id = posted.get('issue_id')
     _customer_id = _customery_final_id
     _shadenet_maintenance_id = posted.get('shadenet_maintenance_id')
     _maintenance_id = posted.get('maintenance_id')
     _created_by = _this_org1
     _status = posted.get('status')
    

     _support_shadnet_edit = shadenet_issues.edit(
                               id,
                             json.dumps({
                              "notes": _notes,
                              "issue_id" : _issue_id,
                              "shadenet_maintenance_id": _shadenet_maintenance_id,
                              "maintenance_id" : shid,
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
     print(str(_support_shadnet_edit[0].get_json()))
   
    

    return render_template(
        'customers/support/shadnet_issue_edit.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        ISSUES = _issues,
        SHADENET = _shadnet,
        GRNHOUSE =  _greenhouse,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )


@customer_support.route('/request/trace_report', methods=['POST', 'GET'])
def trace_report():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    if _maintain != {}:
        maintain_real = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']
    all_crop_by_customer = []
    all_crop_by_customer = crops.fetch_by_creator(_this_org).get_json()
    # _maintain_type = _maintain['type'] 
    
    action_response = {}


    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _crops = posted.get('crop_planting_id')
     
     print("Cropzzzzzzzzzzzz",_crops)
     _customer_id = _customery_final_id
     _created_by = _this_org1
     _status = posted.get('status')
    
    
     _support_tracereport_details = traceability_report_requests.add_new(
                             json.dumps({
                              "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
     print(str(_support_tracereport_details[0].get_json()))
     
        
         

     if _support_tracereport_details :

      s_t_id = traceability_report_requests.fetch_by_maintenance(maintain_real).get_json()['id']
      _support_cropreport_details = traceability_report_crops.add_new(
                             json.dumps({
                            #   "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : cutomers_uid,
                              "crop_planting_id": _crops,
                              "creator_id" : _created_by,
                              "traceability_report_request_id" : s_t_id,
           

                            
                             })
                             
                       )
      print(str(_support_cropreport_details[0].get_json()))
      action_response = {
                            "type": "success",
                            "message": "Traceability maintenance submitted successfully."
                      }


    
    

    return render_template(
        'customers/support/trace_report.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        CROP = all_crop_by_customer,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date":str(datetime.now().year)}
    )



@customer_support.route('/edit/traceability/<string:id>/<string:mid>', methods=['POST', 'GET'])
def edit_trace_report(id,mid):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    if _maintain != {}:
        maintain_real = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']
    all_crop_by_customer = crops.fetch_by_creator(_this_org).get_json()
    # _maintain_type = _maintain['type'] 
    
    action_response = {}


    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _crops = posted.get('crop_planting_id')
     _customer_id = _customery_final_id
     _created_by = _this_org1
     _status = posted.get('status')
    
    
     _support_tracereport_details = traceability_report_requests.add_new(
                             json.dumps({
                              "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
     print(str(_support_tracereport_details[0].get_json()))
     
        
         

     if _support_tracereport_details :

      s_t_id = traceability_report_requests.fetch_by_maintenance(maintain_real).get_json()['id']
      _support_cropreport_details = traceability_report_crops.add_new(
                             json.dumps({
                            #   "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : cutomers_uid,
                              "crop_planting_id": _crops,
                              "creator_id" : _created_by,
                              "traceability_report_request_id" : s_t_id,
           

                            
                             })
                             
                       )
      print(str(_support_cropreport_details[0].get_json()))
      action_response = {
                            "type": "success",
                            "message": "Traceability maintenance Edited successfully."
                      }


    
    

    return render_template(
        'customers/support/trace_report_edit.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        CROP = all_crop_by_customer,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date":str(datetime.now().year)}
    )

@customer_support.route('/edit/otherissue/<string:id>', methods=['POST', 'GET'])
def edit_others(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type'] 
    # print("Maintain type ",_maintain_type)



    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _maintenance_id = posted.get('maintenance_id')
     _customer_id = _customery_final_id
     _created_by = _this_org1
     _status = posted.get('status')

     _support_other_edit = other_request.edit(
                               id,
                             json.dumps({
                              "notes": _notes,
                            #   "maintenance_id" : _maintain['id'],
                            #   "customer_id" : _customer_id,
                            #   "creator_id" : _created_by
                            
                             })
                             
                       )
     print(str(_support_other_edit[0].get_json()))
     
    

    return render_template(
        'customers/support/others_edit.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )


@customer_support.route('/request/others/', methods=['POST', 'GET'])
def others():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _this_org1 = session["profile"]["user"]["id"]
    print("Creator ID",_this_org1)
    _cutomer_real_id = customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()
    # _maintain_type = _maintain['type'] 
    # print("Maintain type ",_maintain_type)



    posted = request.form
    if posted:
     _notes = posted.get('notes')
     _maintenance_id = posted.get('maintenance_id')
     _customer_id = _customery_final_id
     _created_by = _this_org1
     _status = posted.get('status')

     if _maintain != {}: 

      _support_other_details = other_request.add_new(
                             json.dumps({
                              "notes": _notes,
                              "maintenance_id" : _maintain['id'],
                              "customer_id" : _customer_id,
                              "creator_id" : _created_by
                            
                             })
                             
                       )
      print(str(_support_other_details[0].get_json()))
     
    

    return render_template(
        'customers/support/others.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_support.route('/request/photos', methods=['POST','GET'])
def photos():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _this_org1 = session["profile"]["user"]["id"]
    farm_id_action = {}
    farms = []
    _cutomer_real_id = _customers.fetch_by_user(_this_org).get_json()
    cutomers_uid = _cutomer_real_id['customer']['id']
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    farm_id_action = actions.fetch_by_customer(_customery_final_id).get_json()
    if farm_id_action != {}:

        farm_id_by_action = farm_id_action['current_firm_uid']

        swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _maintain = maintenance.fetch_by_customer_id(_customery_final_id).get_json()

    action_response ={}


    if request.method=='POST':
            image_file = request.files.getlist('fileim[]')
            print("filename",image_file)
          
            for ifile in image_file:
                  actual_filename = secure_filename(ifile.filename)
                  pref_filename = _this_org1 + "_" + str(uuid.uuid4())
                  saved_filename = files.uploadFile(
                      ifile,
                      "pestndesease",
                       pref_filename
                   )

                  if saved_filename:
                    image_media = Media(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        file_url = "pestndesease/" + saved_filename,
                        caption = actual_filename,
                        creator_id = _this_org
                    )
                    db.session.add(image_media)
                    db.session.commit()

                    if image_media:
                        image_m_id = media.fetch_by_id(image_media.id).get_json()['id']
                    
                    
                    
                    if image_media and _maintain != {}:

                        maintain_real_id = maintenance.fetch_one(_maintain['id'],True).get_json()['real_id']

                        pndphoto = pndphotos.add_new(
                             json.dumps({

                                 "photo_media_id" : image_m_id,
                                  "maintenance_id" : maintain_real_id,
                                  "creator_id" : _this_org1

                                        }),
                     
                        )
                        print(str(pndphoto[0].get_json()))

                        if pndphoto and image_media:

                           action_response = {
                                "type": "success",
                                "message": "Images files are added successfully."
                            }


    
    # image_file = request.files.get("fileim")
    # print("Image File",image_file)
    # if image_file:
    #             actual_filename = secure_filename(image_file.filename)
    #             pref_filename = cutomers_uid + "_" + str(uuid.uuid4())
    #             saved_filename = files.uploadFile(
    #                 request.files.get("image_file"),
    #                 "avatar",
    #                 pref_filename
    #             )

    #             if saved_filename:
    #                 image_media = media.add_new(
    #                     json.dumps({
    #                         "file_url": "pestndisease/" + saved_filename,
    #                         "caption": actual_filename,
    #                         "creator_id": _this_org1,
    #                     }),
    #                     True
    #                 )

   
    # img = request.files.get('file')
    # print("image",img)
    # if img:
    #             filename = secure_filename(img.filename)
    #             img.save(filename)
    #             s3.upload_file(
    #                 Bucket = BUCKET_NAME,
    #                 Filename=filename,
    #                 Key = filename
    #             )
    #             msg = "Upload Done ! "

    return render_template(
        'customers/support/photos.html',
        CUSTOMER_PORTAL=True,
        FARMS=farms,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        data={"date":str(datetime.now().year)}
    )
