from functools import wraps

import json
import os
from sre_constants import SUCCESS
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException
from app.core.user import customers, igh
from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime
from app.core import user
from app.models import IghUser, db
from app.core import invoice
from app.models import Role
from app.models import User,UserRole,Customer,CustomerUser,Auth
from app.core.user import invites
from app.core import customer as customerfy
from app.core import farm as ownfarms
from app.core.farm import actions
from app.config import activateConfig
from app.core.user import customers as _customero
from app.core import customer as _customer
from app.core.customer import farms as _farms
from app.core.user import roles
from app.core.user import user_roles
from app.igh import portal_check
from app.helper import valid_uuid
from app.core import module
from app.core.module import features
from app.core.module import permissions
from app.core import user as baseuser
from app.core import subscription
from app.core import alert
from app.core import media
from app.core.user import logs
from app.core.user import auth
from app.core.media import files
from werkzeug.exceptions import HTTPException
from app.core import customer as customer_user
from werkzeug.utils import secure_filename
from app.igh import session_setup
customer_settings = Blueprint('customer_settings', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_settings.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def users_form_constructor():

    _user_role_options = []
    _user_roles = db\
        .session\
        .query(Role)\
        .filter(Role.name.like('Customer%'))\
        .filter(Role.status > config.STATUS_DELETED)\
        .all()
    for _user_role in _user_roles:
        _user_role_options.append({
            "label": _user_role.name,
            "value": _user_role.uid
        })

    _form_fields = [
        {
            "name": "first_name",
            "title": "First Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "last_name",
            "title": "Last Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "id_number",
            "title": "ID Number",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"ID Number",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "phone_number",
            "title": "Phone Number",
            "element": [
                "tel"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "email_address",
            "title": "Email Address",
            "element": [
                "email"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "user_role",
            "title": "User Role",
            "element": [
                "select",
                {
                    "options": _user_role_options
                }
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "status",
            "title": "Status",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Active",
                            "value": config.STATUS_ACTIVE,
                        },
                        {
                            "label": "Inactive",
                            "value": config.STATUS_INACTIVE,
                        }
                    ]
                }
            ],
            "required": 0,
            "default":"Active",
            "views": [
                "edit"
            ]
        },
    ]

    return _form_fields

def notifications_form_constructor():

    _user_options = []
    _users = db\
        .session\
        .query(User)\
        .filter(User.status > config.STATUS_DELETED)\
        .all()
    for _user in _users:
        _user_options.append({
            "label": _user.first_name + ' ' + _user.last_name,
            "value": _user.uid
        })

    _form_fields = [
        {
            "name": "post_mode",
            "title": "Select Mode",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Post on profile",
                            "value": config.NOTIFICATION_MODE_PROFILE,
                        },
                        {
                            "label": "Send on mail",
                            "value": config.NOTIFICATION_MODE_EMAIL,
                        },
                        {
                            "label": "Post to profile and email",
                            "value": config.NOTIFICATION_MODE_ALL,
                        }
                    ]
                }
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "mode"
            ]
        }
    ]

    return _form_fields

def get_co_details(org_id):
    org = {}
    if valid_uuid(org_id):
        _this_co = _customer\
            .query\
            .filter(_customer.uid == org_id)\
            .filter(_customer.status > config.STATUS_DELETED)\
            .first()
        if _this_co:
            org["real_id"] = _this_co.id
            org["id"] = _this_co.uid
            org["name"] = _this_co.name
            org["billing_address"] = _this_co.billing_address
            org["shipping_address"] = _this_co.shipping_address
            org["billing_cycle"] = _this_co.billing_cycle
            org["billing_cycle"] = _this_co.billing_cycle
            org["igh_contact"] = contact_person(_this_co.igh_contact_id)
            org["added_by"] = contact_person(_this_co.creator_id)

    return org

def get_users():
    users = []
    _users = db\
        .session\
        .query(User, UserRole, Role, CustomerUser, Customer)\
        .join(UserRole, UserRole.user_id == User.id)\
        .join(Role, Role.id == UserRole.role_id)\
        .join(CustomerUser, CustomerUser.user_id == User.id)\
        .join(Customer, Customer.id == CustomerUser.customer_id)\
        .filter(Role.name.like('Customer%'))\
        .filter(Customer.uid == session["profile"]["org_id"])\
        .filter(User.status > config.STATUS_DELETED)\
        .filter(UserRole.status > config.STATUS_DELETED)\
        .filter(Role.status > config.STATUS_DELETED)\
        .all()
    for _user in _users:
        users.append({
            "id": _user.User.uid,
            "name": _user.User.first_name + " " + _user.User.last_name,
            "phone_number": _user.User.phone_number,
            "email_address": _user.User.email_address,
            "user_role": _user.Role.name,
            "status": "Active" if _user.User.status == config.STATUS_ACTIVE else "Inactive",
            "created": _user.User.created_at,
            "updated": _user.User.modified_at,
            "creator": contact_person(_user.User.creator_id),
        })

    return users

def type_name(type=0):
    if type > 0:
        if type == config.NOTIFICATION_ALERT:
            return "Alert"

        if type == config.NOTIFICATION_USER:
            return "User"

        if type == config.NOTIFICATION_SYSTEM:
            return "System"

    return "General"

def mode_name(mode=0):
    if mode > 0:
        if mode == config.NOTIFICATION_MODE_PROFILE:
            return "Post on profile"

        if mode == config.NOTIFICATION_MODE_EMAIL:
            return "Send on email"

        if mode == config.NOTIFICATION_MODE_ALL:
            return "Post on profile and email"

    return "General"

def status_name(status=0):
    if status == 0:
        return "Not Seen"

    if status == 1:
        return "Seen"

    if status == 2:
        return "Interacted"

    return "Unknown"

@customer_settings.route('/profile', methods=['POST', 'GET'])
def profile():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    action_response = {}
    _cutomer_real_id = {}

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    #  print("_cutomer_real_id",_cutomer_real_id)
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _customery_id = customerfy.fetch_one(cutomers_uid, True).get_json()['id']
    _customery_details = {}
    _customery_details = customerfy.fetch_one(cutomers_uid, True).get_json()
    _subscription = {}
    _subscription = subscription.fetch_by_customer_id(_customery_final_id).get_json()
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    this_user = user.fetch_one(_creator_id,True).get_json()

    # _org_farms = _farms.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

   # this_co = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

    posted = request.form


    if posted:
        if "avatar" in posted:
            saved_filename = ""

            image_file = request.files.get("image_file")
            if image_file:
                actual_filename = secure_filename(image_file.filename)
                pref_filename = session["profile"]["user"]["id"] + \
                    "_" + str(uuid.uuid4())
                saved_filename = files.uploadFile(
                    request.files.get("image_file"),
                    "avatar",
                    pref_filename
                )

                if saved_filename:
                    avatar_media = media.add_new(
                        json.dumps({
                            "file_url": "avatar/" + saved_filename,
                            "caption": actual_filename,
                            "creator_id": session["profile"]["user"]["id"],
                        }),
                        True
                    )
                    if avatar_media[1] == 200:
                        _user = user.edit(
                            session["profile"]["user"]["id"],
                            json.dumps({
                                "avatar_media": avatar_media[0].get_json()['id']
                            }),
                            True
                        )
                        if _user[1] == 200:
                            session_setup(session["profile"]["user"]["id"])

                            return jsonify({
                                "status": True,
                                "avatar": str(saved_filename)
                            })

            return jsonify({
                "status": False,
            })

    if posted.get('user_id'):


       
        user_customer = user.fetch_one(posted.get('user_id'),True).get_json()['real_id']

        deactivate = user.deactivate(posted.get('user_id'))
        print("Deactivate",deactivate)

        if deactivate:
          user_rol = user_roles.fetch_by_user(user_customer).get_json()

          if user_rol != {}:
            user_rol_id = user_rol['id']
            deavtivate_user_rol = user_roles.deactivate(user_rol_id)

            print("Success",SUCCESS)

    if posted.get("action") == 'edit_account' and "account_name" and "account_phone_number" in posted:

       _account_name = posted.get("account_name").strip()
       _a_phone_number = posted.get("account_phone_number").strip()


    #    print("Account data",_account_name,_a_phone_number)
       if _account_name and _a_phone_number :
        first, *last = _account_name.split()
        fname = "{first}".format(first=first)
        lname = "{last}".format(last=" ".join(last))
        
        this_c_user = user.edit(
                   _creator_id,
                        json.dumps({
                                   "phone_number": _a_phone_number,
                                   "first_name" : fname,
                                   "last_name" : lname,
                                   })


                      )
        print(str(this_c_user[0].get_json()))


    if posted.get("action") == 'edit_org' and "org_name" and "phone_number" and "shipping_address"\
        and "billing_address" in posted:
        _org_name = posted.get("org_name").strip()
        _phone_number = posted.get("phone_number").strip()
        # _email_address = posted.get("email_address").strip()
        _billing_address = posted.get("billing_address").strip()
        _shipping_address = posted.get("shipping_address").strip()
        # _org_id = posted.get("org_id").strip()
        _igh_contatct = None
        # print("_org_name",_org_name,_phone_number,_shipping_address)

        if _org_name !='' and _billing_address != '' and _shipping_address != '' and  _phone_number:

              igh_contact_collect =  customer_user.fetch_one(_customery_id,True).get_json()

              if igh_contact_collect != {}:

                  user_contact = igh_contact_collect['igh_contact']['user']['id']
                  user_real = user.fetch_one(user_contact,True).get_json()['real_id']

                  _igh_cont_real = igh.fetch_by_user(user_real).get_json()['id']
                  print("IGHCONTACT",_igh_cont_real)
                #   _igh_cont_real = igh.fetch_one(igh_contact,True).get_json()['real_id']

                

                  new_customer = customer_user.edit(
                       _customery_id,
                        json.dumps({
                                   "name": _org_name,
                                   "billing_address":_billing_address,
                                   "shipping_address": _shipping_address,
                                   "igh_contact_id": _igh_cont_real,
                                   })
                    )

                  print(str(new_customer[0].get_json()))


                  this_customer_user = user.edit(


                        _creator_id,
                        json.dumps({
                                   "phone_number": _phone_number,
                                   })


                      )
                  print(str(this_customer_user[0].get_json()))

    if "password" in posted:
            password1 = posted.get("new_password").strip()
            password2 = posted.get("confirm_password").strip()
            if password1 and password2 and password1 == password2:
                _this_auth = auth.fetch_by_user(
                    session["profile"]["user"]["real_id"]).get_json()
                if _this_auth:
                    _password_reset = auth.edit(
                        _this_auth['id'],
                        json.dumps({
                            "password": password1
                        })
                    )
                    
                    del session['profile']
                    if _password_reset[1] == 200:
                        
                        return jsonify({
                            "status": True,
                        })
                    
                return redirect("./")
                    
                    
                    
                        

            return jsonify({
                "status": False,
            })




    # if posted:
    #     _org_name = posted.get("org_name").strip()
    #     _phone_number = posted.get("phone_number").strip()
    #     _email_address = posted.get("email_address").strip()
    #     _billing_address = posted.get("billing_address").strip()
    #     _shipping_address = posted.get("shipping_address").strip()
    #     _org_id = posted.get("org_id").strip()
    #
    #     if _org_id is not None and _org_name is not None \
    #         and _email_address is not None and _phone_number:
    #
    #         _customer = Customer\
    #             .query\
    #             .filter(Customer.uid == _org_id)\
    #             .first()
    #         if _customer:
    #             _customer.name = _org_name
    #             _customer.billing_address = _billing_address
    #             _customer.shipping_address = _shipping_address
    #
    #             session['profile']['org'] = _org_name
    #             session['profile']['org_billing_address'] = _billing_address
    #             session['profile']['org_shipping_address'] = _shipping_address
    #
    #             db.session.commit()
    #
    #             return jsonify({
    #                 "type": "success",
    #                 "message": "Customer details edited successfully."
    #             })
    #
    #         # exists = User\
    #         #     .query\
    #         #     .filter(User.email_address == _email_address)\
    #         #     .filter(User.status > config.STATUS_DELETED)\
    #         #     .first()
    #         # if not exists:
    #         #     role = Role\
    #         #         .query\
    #         #         .filter(Role.uid == _user_role)\
    #         #         .filter(Role.name.like('Customer%'))\
    #         #         .filter(Role.status > config.STATUS_DELETED)\
    #         #         .first()
    #         #
    #         #     user = User\
    #         #         .query\
    #         #         .filter(User.uid == session["profile"]["id"])\
    #         #         .filter(User.status > config.STATUS_DELETED)\
    #         #         .first()
    #         #     if user and role:
    #         #         _user = User(
    #         #             uid=uuid.uuid4(),
    #         #             first_name=_first_name,
    #         #             last_name=_last_name,
    #         #             email_address=_email_address,
    #         #             phone_number=_phone_number,
    #         #             creator_id=user.id
    #         #         )
    #         #         db.session.add(_user)
    #         #         db.session.commit()
    #         #
    #         #         if _user:
    #         #             _user_role = UserRole(
    #         #                 uid=uuid.uuid4(),
    #         #                 user_id=_user.id,
    #         #                 role_id=role.id,
    #         #                 creator_id=user.id
    #         #             )
    #         #             db.session.add(_user_role)
    #         #             db.session.commit()
    #         #
    #         #             _new_customer_user = CustomerUser(
    #         #                 uid=uuid.uuid4(),
    #         #                 id_number=_id_number,
    #         #                 customer_id=this_co["real_id"],
    #         #                 user_id=_user.id,
    #         #                 creator_id=user.id
    #         #             )
    #         #             db.session.add(_new_customer_user)
    #         #             db.session.commit()
    #         #
    #         #             activation_code = str(uuid.uuid4()).replace("-", "")
    #         #             _user_auth = Auth(
    #         #                 uid=uuid.uuid4(),
    #         #                 user_id=_user.id,
    #         #                 username=_email_address,
    #         #                 activation_code=activation_code,
    #         #                 creator_id=user.id
    #         #             )
    #         #             db.session.add(_user_auth)
    #         #             db.session.commit()
    #         #
    #         #             dispatch_resp = "not sent"
    #         #             _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
    #         #             if _dispatch.status_code == 200:
    #         #                 _obj = json.loads(_dispatch.content)
    #         #                 if "message" in _obj:
    #         #                     dispatch_resp = "sent"
    #         #
    #         #             action_response = {
    #         #                 "type": "success",
    #         #                 "message": "User added successfully, activation email " + dispatch_resp + "."
    #         #             }
    #         # else:
    #         #     action_response = {
    #         #         "type": "danger",
    #         #         "message": "Email address already in use, please try a different one."
    #         #     }
    #     else:
    #         action_response = {
    #             "type": "warning",
    #             "message": "User not added, invalid input."
    #         }
    #
        # if posted.get("action").strip().lower() == 'edit':
        #     _first_name = posted.get("first_name").strip()
        #     _last_name = posted.get("last_name").strip()
        #     _email_address = posted.get("email_address").strip()
        #     _phone_number = posted.get("phone_number").strip()
        #     _user_role = posted.get("user_role").strip()
        #     _id_number = posted.get("id_number").strip()
        #
        #     if _first_name is not None and _last_name is not None \
        #         and _email_address is not None and _phone_number is not None \
        #         and _user_role is not None:
        #
        #         exists = User\
        #             .query\
        #             .filter(User.email_address == _email_address)\
        #             .filter(User.status > config.STATUS_DELETED)\
        #             .first()
        #         if not exists:
        #             role = Role\
        #                 .query\
        #                 .filter(Role.uid == _user_role)\
        #                 .filter(Role.name.like('Customer%'))\
        #                 .filter(Role.status > config.STATUS_DELETED)\
        #                 .first()
        #
        #             user = User\
        #                 .query\
        #                 .filter(User.uid == session["profile"]["id"])\
        #                 .filter(User.status > config.STATUS_DELETED)\
        #                 .first()
        #             if user and role:
        #                 _user = User(
        #                     uid=uuid.uuid4(),
        #                     first_name=_first_name,
        #                     last_name=_last_name,
        #                     email_address=_email_address,
        #                     phone_number=_phone_number,
        #                     creator_id=user.id
        #                 )
        #                 db.session.add(_user)
        #                 db.session.commit()
        #
        #                 if _user:
        #                     _user_role = UserRole(
        #                         uid=uuid.uuid4(),
        #                         user_id=_user.id,
        #                         role_id=role.id,
        #                         creator_id=user.id
        #                     )
        #                     db.session.add(_user_role)
        #                     db.session.commit()
        #
        #                     _new_customer_user = CustomerUser(
        #                         uid=uuid.uuid4(),
        #                         id_number=_id_number,
        #                         customer_id=this_co["real_id"],
        #                         user_id=_user.id,
        #                         creator_id=user.id
        #                     )
        #                     db.session.add(_new_customer_user)
        #                     db.session.commit()
        #
        #                     activation_code = str(uuid.uuid4()).replace("-", "")
        #                     _user_auth = Auth(
        #                         uid=uuid.uuid4(),
        #                         user_id=_user.id,
        #                         username=_email_address,
        #                         activation_code=activation_code,
        #                         creator_id=user.id
        #                     )
        #                     db.session.add(_user_auth)
        #                     db.session.commit()
        #
        #                     dispatch_resp = "not sent"
        #                     _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
        #                     if _dispatch.status_code == 200:
        #                         _obj = json.loads(_dispatch.content)
        #                         if "message" in _obj:
        #                             dispatch_resp = "sent"
        #
        #                     action_response = {
        #                         "type": "success",
        #                         "message": "User added successfully, activation email " + dispatch_resp + "."
        #                     }
        #         else:
        #             action_response = {
        #                 "type": "danger",
        #                 "message": "Email address already in use, please try a different one."
        #             }
        #     else:
        #         action_response = {
        #             "type": "warning",
        #             "message": "User not added, invalid input."
        #         }
        # else:
        #     action_response = {
        #         "type": "danger",
        #         "message": "Invalid input."
        #     }

    users = []

    this_user = user.fetch_one(_creator_id,True).get_json()

    return render_template(
        'customers/settings/profile.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_PROFILE_ACTIVE='active',
        USERS=users,
        THISUSER = this_user,
        FARMS=farms,
        CUSTOMER_REAL_DETTAILS = _cutomer_real_id,
        FARM=swapped_firm_details,
        CUSTOMERDETAILS = _customery_details,
        SUBSCRIPTION = _subscription,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )


@customer_settings.route('/profile/<user_id>', methods=['POST', 'GET'])
def user_profile(user_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    user_detail = {}
    _user = []
    business_report = []
    revenue_report = []
    customer_report = []

    if valid_uuid(user_id):
        _staff_info = {}
        _role = {}
        _alert_config = {}

        # business_report = fetch_business_report(user_id)
        # revenue_report = fetch_revenue_report(user_id)
        # customer_report = fetch_customer_report(user_id,2022)

        user_detail = user.fetch_one(user_id, True).get_json()
        # user_real_id = user_detail['real_id']
        # user_all_details = user.fetch_by_id(user_real_id).get_json()
        print("User details",user_detail)
        if user_detail:
            # _staff_info = igh.fetch_by_user(user_detail["real_id"]).get_json()
            _role = user_roles.fetch_by_user(user_detail["real_id"]).get_json()
            _alert_config = alert.fetch_alert_config(
                user_detail["real_id"])[0].get_json()

        _user = {
            "staff_info": _staff_info,
            "detail": user_detail,
            "role": _role,
            "alert_config": _alert_config,
        }

    posted = request.form
    if posted:
        if "avatar" in posted:
            saved_filename = ""

            image_file = request.files.get("image_file")
            print("Filename",image_file)
            if image_file:
                actual_filename = secure_filename(image_file.filename)
                pref_filename = session["profile"]["user"]["id"] + \
                    "_" + str(uuid.uuid4())
                saved_filename = files.uploadFile(
                    request.files.get("image_file"),
                    "avatar",
                    pref_filename
                )

                if saved_filename:
                    avatar_media = media.add_new(
                        json.dumps({
                            "file_url": "avatar/" + saved_filename,
                            "caption": actual_filename,
                            "creator_id": session["profile"]["user"]["id"],
                        }),
                        True
                    )
                    if avatar_media[1] == 200:
                        _user = user.edit(
                            session["profile"]["user"]["id"],
                            json.dumps({
                                "avatar_media": avatar_media[0].get_json()['id']
                            }),
                            True
                        )
                        if _user[1] == 200:
                            session_setup(session["profile"]["user"]["id"])

                            return jsonify({
                                "status": True,
                                "avatar": str(saved_filename)
                            })

            return jsonify({
                "status": False,
            })
        if "password" in posted:
            password1 = posted.get("new_password").strip()
            password2 = posted.get("confirm_password").strip()
            if password1 and password2 and password1 == password2:
                user_real_id = user.fetch_one(user_id,True).get_json()['real_id']
                _this_auth = auth.fetch_by_user(user_real_id).get_json()
                if _this_auth:
                    _password_reset = auth.edit(
                        _this_auth['id'],
                        json.dumps({
                            "password": password1
                        })
                    )
                    if _password_reset[1] == 200:
                        return jsonify({
                            "status": True,
                        })

            return jsonify({
                "status": False,
            })

        # if "metric" in posted:
        #     _config = configs.fetch_by_user(
        #         session["profile"]["user"]["real_id"]).get_json()
        #     if _config:
        #         if "temp" in posted:
        #             if posted.get("temp").strip() == config.METRIC_TEMP_C or \
        #                     posted.get("temp").strip() == config.METRIC_TEMP_F:
        #                 _config_update = configs.edit(
        #                     _config["id"],
        #                     json.dumps({
        #                         "temp_metric": posted.get("temp").strip()
        #                     })
        #                 )

        #         if "liquid" in posted:
        #             if posted.get("liquid").strip() == config.METRIC_LIQUID_L or \
        #                     posted.get("liquid").strip() == config.METRIC_LIQUID_GAL:
        #                 _config_update = configs.edit(
        #                     _config["id"],
        #                     json.dumps({
        #                         "liquid_metric": posted.get("liquid").strip()
        #                     })
        #                 )

        #         if "length" in posted:
        #             if posted.get("length").strip() == config.METRIC_LENGTH_F or \
        #                     posted.get("length").strip() == config.METRIC_LENGTH_M:
        #                 _config_update = configs.edit(
        #                     _config["id"],
        #                     json.dumps({
        #                         "length_metric": posted.get("length").strip()
        #                     })
        #                 )

    return render_template(
        'customers/settings/user-profile.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_PROFILE_ACTIVE='active',
        PROFILE=session['profile'],
        USER=_user,
        BUSINESS_REPORT=business_report,
        REVENUE_REPORT=revenue_report,
        CUSTOMER_REPORT=customer_report,
        data={"date": str(datetime.now().year)}
    )

@customer_settings.route('/users', methods=['POST', 'GET'])
def users():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    #  print("_cutomer_real_id",_cutomer_real_id)
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    _invites = []
    _total_invites = invites.fetch_by_realm(config.USER_CUSTOMER, config.STATUS_ACTIVE).get_json()

    form_action = 'Add'
    action_response = {}
    invited_users = []
    # this_co = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()
    # invited_users = baseuser.fetch_by_creator(_this_org).get_json()
    # print("Invited users",invited_users)
    _invites = []
    invited_users = invites.fetch_by_customer(_customery_final_id).get_json()
    print("Invited users",invited_users)

    for _ti in invited_users:
        _role = {}
        _alert_config = {}
        _user = baseuser.fetch_one(_ti["user"]["id"], True).get_json()
        if _user:
            _role = user_roles.fetch_by_user(_user["real_id"]).get_json()
            _alert_config = alert.fetch_alert_config(_user["real_id"])[
                0].get_json()
        _invites.append({
            "user": _ti,
            "role": _role,
            "alert_config": _alert_config,
        })

    posted = request.form
    if posted:
        if posted.get("action").strip().lower() == 'add':
            _first_name = posted.get("first_name").strip()
            _last_name = posted.get("last_name").strip()
            _email_address = posted.get("email_address").strip()
            _phone_number = posted.get("phone_number").strip()
            _user_role = posted.get("user_role").strip()
            _id_number = posted.get("id_number").strip()

           

            if _first_name is not None and _last_name is not None \
                and _email_address is not None and _phone_number is not None \
                and _user_role is not None:

                exists = User\
                    .query\
                    .filter(User.email_address == _email_address)\
                    .filter(User.status > config.STATUS_DELETED)\
                    .first()
                if not exists:
                    role = Role\
                        .query\
                        .filter(Role.uid == _user_role)\
                        .filter(Role.name.like('Customer%'))\
                        .filter(Role.status > config.STATUS_DELETED)\
                        .first()

                    user = User\
                        .query\
                        .filter(User.uid == session["profile"]["id"])\
                        .filter(User.status > config.STATUS_DELETED)\
                        .first()
                    if user and role:
                        _user = User(
                            uid=uuid.uuid4(),
                            first_name=_first_name,
                            last_name=_last_name,
                            email_address=_email_address,
                            phone_number=_phone_number,
                            creator_id=user.id
                        )
                        db.session.add(_user)
                        db.session.commit()

                        if _user:
                            _user_role = UserRole(
                                uid=uuid.uuid4(),
                                user_id=_user.id,
                                role_id=role.id,
                                creator_id=user.id
                            )
                            db.session.add(_user_role)
                            db.session.commit()

                            _new_customer_user = CustomerUser(
                                uid=uuid.uuid4(),
                                id_number=_id_number,
                                customer_id=this_co["real_id"],
                                user_id=_user.id,
                                creator_id=user.id
                            )
                            db.session.add(_new_customer_user)
                            db.session.commit()

                            activation_code = str(uuid.uuid4()).replace("-", "")
                            _user_auth = Auth(
                                uid=uuid.uuid4(),
                                user_id=_user.id,
                                username=_email_address,
                                activation_code=activation_code,
                                creator_id=user.id
                            )
                            db.session.add(_user_auth)
                            db.session.commit()

                            dispatch_resp = "not sent"
                            _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
                            if _dispatch.status_code == 200:
                                _obj = json.loads(_dispatch.content)
                                if "message" in _obj:
                                    dispatch_resp = "sent"

                            action_response = {
                                "type": "success",
                                "message": "User added successfully, activation email " + dispatch_resp + "."
                            }
                else:
                    action_response = {
                        "type": "danger",
                        "message": "Email address already in use, please try a different one."
                    }
            else:
                action_response = {
                    "type": "warning",
                    "message": "User not added, invalid input."
                }
        else:
            action_response = {
                "type": "danger",
                "message": "Invalid input."
            }

    users = []

    return render_template(
        'customers/settings/users/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USERS_ACTIVE='active',
        FORM=[],
        FARM=swapped_firm_details,
        FARMS = farms,
        INVITEDUSERS = invited_users,
        INVITES=_invites,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        USERS=users,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )



@customer_settings.route('/deactivate/<string:id>', methods=['POST', 'GET'])
def deactivate_user(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
     print("_cutomer_real_id",_cutomer_real_id)
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    user_customer = user.fetch_one(id,True).get_json()['real_id']
    user_name = user.fetch_one(id,True).get_json()['first_name']

    deactivate = user.deactivate(id)

    if deactivate:
        user_rol = user_roles.fetch_by_user(user_customer).get_json()

        if user_rol != {}:
            user_rol_id = user_rol['id']
            deavtivate_user_rol = user_roles.deactivate(user_rol_id)


            _customer_d_log = logs.add_new(
                             json.dumps({
                                    "action": "Customer account is deactivated ",
                                    "ref": user_name,
                                    "ref_id": user_customer,
                                    "customer_id" : cutomers_uid,
                                    "creator_id" : session["profile"]["user"]["id"] 
                                    }),
                                    
                                    )
            print(str(_customer_d_log[0].get_json()))




    return render_template(
        'customers/settings/deactivate.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USERS_ACTIVE='active',
        USERS=users,
        FARM=swapped_firm_details,
        FARMS = farms,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )

@customer_settings.route('/activate/<string:id>', methods=['POST', 'GET'])
def activate_user(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
     print("_cutomer_real_id",_cutomer_real_id)
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    user_customer = user.fetch_one(id,True).get_json()['real_id']
    user_name = user.fetch_one(id,True).get_json()['first_name']


    deactivate = user.activate(id)

    if deactivate:
        user_rol = user_roles.fetch_by_user(user_customer).get_json()

        if user_rol != {}:
            user_rol_id = user_rol['id']
            deavtivate_user_rol = user_roles.activate(user_rol_id)


            _customer_a_log = logs.add_new(
                             json.dumps({
                                    "action": "Customer account is Activated ",
                                    "ref": user_name,
                                    "ref_id": user_customer,
                                    "customer_id" : cutomers_uid,
                                    "creator_id" : session["profile"]["user"]["id"] 
                                    }),
                                    
                                    )
            print(str(_customer_a_log[0].get_json()))





    return render_template(
        'customers/settings/activate.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USERS_ACTIVE='active',
        USERS=users,
        FARM=swapped_firm_details,
        FARMS = farms,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )

    

@customer_settings.route('/users/add', methods=['POST', 'GET'])
def add_users():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
     print("_cutomer_real_id",_cutomer_real_id)
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    form_action = 'Add'
    action_response = {}
    _multiple = []
    this_co = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()
    _roles = roles.fetch_by_customer(_customery_final_id).get_json()
    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:
            _full_name = posted.getlist("full_name[]")
            _employee_id = posted.getlist("employee_id[]")
            _email_address = posted.getlist("email_id[]")
            _password = posted.getlist("password[]")
        # _first_name = posted.get("full_name")
        # _last_name = posted.get("last_name")
        # _email_address = posted.get("email_id")
            # _phone_number = posted.get("phone_number[]")
            _user_role = posted.get("role[]")
        # _id_number = posted.get("employee_id")
            x = 0
            for _name in _full_name:
                _multiple.append({
                    "full_name": _full_name[x],
                    "employee_id": _employee_id[x],
                    "email_address": _email_address[x],
                    "password": _password[x],
                })
                x = x + 1
        else:
            _full_name = posted.getlist("full_name[]")
            _employee_id = posted.getlist("employee_id[]")
            _role = posted.getlist("role[]")
            _email_id = posted.getlist("email_id[]")
            _password = posted.getlist("password[]")

            x = 0
            for _f_n in _full_name:
                if _f_n and _email_id[x]:
                    y = 1
                    _first_name = ""
                    _last_name = ""
                    _names = _f_n.split(" ")
                    for _name in _names:
                        if y == 1:
                            _first_name = _name
                        else:
                            _last_name += _name + " "
                        y = y + 1

                    _new_user = user.add_new(
                        json.dumps({
                            "first_name": _first_name,
                            "last_name": _last_name,
                            "email_address": _email_id[x],
                            "employee_id": _employee_id[x],
                            "role_id": _role[x],
                            "user_realm": config.USER_CUSTOMER,
                            "customer_id": _customery_final_id,
                            "status" : config.STATUS_INACTIVE,
                            "creator_id" : session["profile"]["user"]["id"]
                        }),
                        True
                    )

                    print(str(_new_user[0].get_json()))
                    _user_id = user.fetch_by_email(_email_id[x]).get_json()['id']
                    print("User ID",_user_id)
                    if _new_user:
                         new_customer_id = customers.add_new(
                            json.dumps({
                                "employee_id": _employee_id[x],
                                "customer_id": _customery_final_id,
                                "user_id": _user_id,
                                "creator_id": session["profile"]["user"]["id"]
                            })
                        )
                         print(str(new_customer_id[0].get_json()))
                         _user_id = user.fetch_one(_user_id, True).get_json()['real_id']
                         _customer_log = logs.add_new(
                             json.dumps({
                                    "action": "New Customeruser is added",
                                    "ref": _first_name,
                                    "ref_id": _user_id,
                                    "customer_id" : cutomers_uid,
                                    "creator_id" : session["profile"]["user"]["id"] 
                                    }),
                                    
                                    )
                         print(str(_customer_log[0].get_json()))
                    if _new_user[1] == 200:
                        pass
                        # add password
                    x = x + 1

      
    users = []

    return render_template(
        'customers/settings/users/add.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USERS_ACTIVE='active',
        USERS=users,
        FARM=swapped_firm_details,
        FARMS = farms,
        ROLES = _roles,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )


@customer_settings.route('/users/edit/<user_id>', methods=['POST', 'GET'])
def edit_user_profile(user_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    action_response = {}

    _roles = roles.fetch_by_user_realm(config.USER_CUSTOMER).get_json()

    _user = []
    customer_report = []
    if valid_uuid(user_id):

        posted = request.form
        if posted:
            if "avatar" in posted:
                saved_filename = ""

                image_file = request.files.get("image_file")
                print("Imagefile",image_file)
                if image_file:
                    actual_filename = secure_filename(image_file.filename)
                    pref_filename = user_id + "_" + str(uuid.uuid4())
                    saved_filename = files.uploadFile(
                        request.files.get("image_file"),
                        "avatar",
                        pref_filename
                    )

                    if saved_filename:
                        avatar_media = media.add_new(
                            json.dumps({
                                "file_url": "avatar/" + saved_filename,
                                "caption": actual_filename,
                                "creator_id": user_id,
                            }),
                            True
                        )
                        if avatar_media[1] == 200:
                            _user = user.edit(
                                user_id,
                                json.dumps({
                                    "avatar_media": avatar_media[0].get_json()['id']
                                }),
                                True
                            )
                            if _user[1] == 200:

                                return jsonify({
                                    "status": True,
                                    "avatar": str(saved_filename)
                                })

                return jsonify({
                    "status": False,
                })

            if "password" in posted:
                password1 = posted.get("new_password").strip()
                password2 = posted.get("confirm_password").strip()
                if password1 and password2 and password1 == password2:
                    _this_auth = auth.fetch_by_user(session["profile"]["user"]["real_id"]).get_json()
                    if _this_auth:
                        _password_reset = auth.edit(
                            _this_auth['id'],
                            json.dumps({
                                "password": password1
                            })
                        )
                        if _password_reset[1] == 200:
                            return jsonify({
                                "status": True,
                            })

                return jsonify({
                    "status": False,
                })

            if "edit_profile" in posted:
                fullname = posted.get("fullname").strip()
                # email_address = posted.get("email_address").strip()
                phone_number = posted.get("phone_number").strip()
                role = posted.get("role_id").strip()
                print("Role",role)
                alerts_subscription = posted.get("alerts_subscription").strip()

                # User
                if fullname :
                    x = 1
                    first_name = ""
                    last_name = ""
                    _names = fullname.split(" ")
                    for _name in _names:
                        if x == 1:
                            first_name = _name
                        else:
                            last_name += _name + " "

                        x = x + 1

                    _user_update = user.edit(
                        user_id,
                        json.dumps({
                            "first_name": first_name.strip(),
                            "last_name": last_name.strip(),
                            "phone_number": phone_number.strip(),
                            # "email_address": email_address.strip(),
                        }),
                    )
                    if _user_update[1] != 200:
                        action_response = {
                            "type": "danger",
                            "message": str(_user_update[0].get_json()['error'])
                        }

                # Role
                if role:
                    user_real = user.fetch_one(user_id,True).get_json()['real_id']
                    user_role_id = user_roles.fetch_by_user(user_real).get_json()['id']
                    print("User ROle ID",user_role_id)
                    _user_role_update = user_roles.edit(
                       user_role_id,
                        json.dumps({
                            "role_id": role
                        })
                    )

                    if _user_role_update[1] != 200:
                        action_response = {
                            "type": "danger",
                            "message": str(_user_role_update[0].get_json()['error'])
                        }

                # Alerts Config
                if alerts_subscription:
                    _alerts_subscription_update = alert.set_alert_config(
                        user_id,
                        alerts_subscription,
                        session["profile"]["user"]["id"]
                    )

                if not action_response:
                    action_response = {
                        "type": "success",
                        "message": "Profile updated successfully."
                    }

        _staff_info = {}
        _role = {}
        _alert_config = {}

        # customer_report = fetch_customer_report(user_id,2022)

        user_detail = user.fetch_one(user_id, True).get_json()
        if user_detail:
            # _staff_info = igh.fetch_by_user(user_detail["real_id"]).get_json()
            _staff_info = None
            _role = user_roles.fetch_by_user(user_detail["real_id"]).get_json()
            _alert_config = alert.fetch_alert_config(
                user_detail["real_id"])[0].get_json()

        _user = {
            "staff_info": _staff_info,
            "detail": user_detail,
            "role": _role,
            "alert_config": _alert_config,
        }

    return render_template(
        'customers/settings/edit-user-profile.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_SETTINGS_ACTIVE='active',
        ADMIN_PORTAL_SETTINGS_USERS_ACTIVE='active',
        PROFILE=session['profile'],
        ROLES=_roles,
        ACTION_RESPONSE=action_response,
        USER=_user,
        CUSTOMER_REPORT=customer_report,
        data={"date": str(datetime.now().year)}
    )

@customer_settings.route('/users/bulk', methods=['POST', 'GET'])
def bulk_users():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    form_action = 'Add'
    action_response = {}

    this_co = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

    posted = request.form
    if posted:
        _first_name = posted.get("first_name")
        _last_name = posted.get("last_name")
        _email_address = posted.get("email_address")
        _phone_number = posted.get("phone_number")
        _user_role = posted.get("user_role")
        _id_number = posted.get("id_number")

        if _first_name is not None and _last_name is not None \
            and _email_address is not None and _phone_number is not None \
            and _user_role is not None:

            exists = User\
                .query\
                .filter(User.email_address == _email_address)\
                .filter(User.status > config.STATUS_DELETED)\
                .first()
            if not exists:
                role = Role\
                    .query\
                    .filter(Role.uid == _user_role)\
                    .filter(Role.name.like('Customer%'))\
                    .filter(Role.status > config.STATUS_DELETED)\
                    .first()

                user = User\
                    .query\
                    .filter(User.uid == session["profile"]["id"])\
                    .filter(User.status > config.STATUS_DELETED)\
                    .first()
                if user and role:
                    _user = User(
                        uid=uuid.uuid4(),
                        first_name=_first_name,
                        last_name=_last_name,
                        email_address=_email_address,
                        phone_number=_phone_number,
                        creator_id=user.id
                    )
                    db.session.add(_user)
                    db.session.commit()

                    if _user:
                        _user_role = UserRole(
                            uid=uuid.uuid4(),
                            user_id=_user.id,
                            role_id=role.id,
                            creator_id=user.id
                        )
                        db.session.add(_user_role)
                        db.session.commit()

                        _new_customer_user = CustomerUser(
                            uid=uuid.uuid4(),
                            id_number=_id_number,
                            customer_id=this_co["real_id"],
                            user_id=_user.id,
                            creator_id=user.id
                        )
                        db.session.add(_new_customer_user)
                        db.session.commit()

                        activation_code = str(uuid.uuid4()).replace("-", "")
                        _user_auth = Auth(
                            uid=uuid.uuid4(),
                            user_id=_user.id,
                            username=_email_address,
                            activation_code=activation_code,
                            creator_id=user.id
                        )
                        db.session.add(_user_auth)
                        db.session.commit()

                        dispatch_resp = "not sent"
                        _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
                        if _dispatch.status_code == 200:
                            _obj = json.loads(_dispatch.content)
                            if "message" in _obj:
                                dispatch_resp = "sent"

                        action_response = {
                            "type": "success",
                            "message": "User added successfully, activation email " + dispatch_resp + "."
                        }
            else:
                action_response = {
                    "type": "danger",
                    "message": "Email address already in use, please try a different one."
                }
        else:
            action_response = {
                "type": "warning",
                "message": "Emails not sent out, invalid input."
            }

    users = []

    return render_template(
        'customers/settings/users/bulk.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USERS_ACTIVE='active',
        USERS=users,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )




@customer_settings.route('/roles', methods=['POST', 'GET'])
def all_roles():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    #  print("_cutomer_real_id",_cutomer_real_id)
    else:
     cutomers_uid = None
    _customery_final_idz = customerfy.fetch_one(cutomers_uid, True).get_json()
    _customery_final_id = _customery_final_idz['real_id']
    _customery_uid = _customery_final_idz['id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    _roles = []
    action_response = {}
   
    _modules = []
    _actives = []
    _inactives = []

    posted = request.form
    if posted:
        if "action" in posted:
            if posted.get("action") == "role_users" and "role_id" in posted:
                _role_users = []

                _role = roles\
                    .fetch_one(posted.get("role_id"), True)\
                    .get_json()
                # print("Booommmmm",_role)
                if _role:
                    _role_users = user_roles\
                        .fetch_by_role(_role["real_id"])\
                        .get_json()

                return jsonify(_role_users)
            exists = roles.fetch_by_name_realm(posted.get("role_name"),2).get_json()

            if exists != {}:

                action_response = {
                            "type": "danger",
                            "message": "Role is already exists please add new role"
                              }

            if posted.get("action") == "add_role" and "role_name" in posted:
                return roles.add_new(
                    json.dumps({
                        "name": posted.get("role_name"),
                        "description": posted.get("role_name"),
                        "user_realm": config.USER_CUSTOMER,
                        "customer_id": _customery_uid,
                        "creator_id": session['profile']['user']['id'],
                    }),
                    True
                )

            else:

               action_response = {
                            "type": "danger",
                            "message": "Role is already exists please add new role"
                              }
            if posted.get("action") == "activate_role" and "role_id" in posted:
                return roles.activate(posted.get("role_id"))

            if posted.get("action") == "deactivate_role" and "role_id" in posted:
                return roles.deactivate(posted.get("role_id"))

            if posted.get("action") == "module_permissions" and \
                    "module_id" in posted and "role_id" in posted:

                _resp = []

                _module_id = posted.get("module_id")
                _role_id = posted.get("role_id")

                if valid_uuid(_module_id) and valid_uuid(_role_id):
                    _role = roles.fetch_one(_role_id, True).get_json()
                    _module = module.fetch_one(_module_id, True).get_json()
                    if _role and _module:
                        _feats = features.fetch_by_module(
                            _module['real_id'], True).get_json()
                        for _feat in _feats:
                            _mod_perm = {}
                            _perms = permissions.fetch_by_role_feature(
                                _role["real_id"], _feat['real_id']).get_json()
                            if _perms and _perms['role']['id'] == _role["id"]:
                                _mod_perm[str(_feat['id'])] = 1
                            else:
                                _mod_perm[str(_feat['id'])] = 0

                            _resp.append(_mod_perm)

                return jsonify(_resp)

            if posted.get("action") == "update_roles" and "roles" in posted:
                _roles = json.loads(posted.get("roles"))
                for role in _roles:
                    roles.edit(
                        role["role_id"],
                        json.dumps(
                            {
                                "name": role["role_name"]
                            }
                        )
                    )
                return jsonify(_roles)

            if posted.get("action") == "update_permission" \
                    and "add_remove" in posted \
                    and "module_id" in posted \
                    and "role_id" in posted\
                    and "feat_id" in posted:

                _module = module.fetch_one(
                    posted.get("module_id"), True).get_json()
                _role = roles.fetch_one(posted.get("role_id"), True).get_json()
                _feat = features.fetch_one(
                    posted.get("feat_id"), True).get_json()

                if _role and _feat:
                    if posted.get("add_remove") == "1":
                        _new_perm = permissions.add_new(
                            json.dumps({
                                "role_id": _role['id'],
                                "module_feature_id": _feat['id'],
                                "creator_id": session["profile"]["user"]["id"]
                            })
                        )
                        return jsonify({"message": _new_perm[0].get_json()})
                    else:
                        _permission = permissions.fetch_by_role_feature(
                            _role["real_id"], _feat["real_id"], True).get_json()
                        if _permission:
                            _deactivate = permissions.deactivate(
                                _permission["id"])

                        return jsonify({"message": "removed"})

    _mods = module.fetch_by_user_realm(config.USER_IGH, True).get_json()
    for _mod in _mods:
        _mod["feats"] = features.fetch_by_module(
            _mod['real_id'], True).get_json()

        _modules.append(_mod)

    _roles = roles.fetch_by_customer(_customery_final_id).get_json()






    return render_template(
        'customers/settings/roles.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS = farms,
        ROLES=_roles,
        FARM=swapped_firm_details,
        FORM_ACTION_RESPONSE=action_response,
        MODULES=_modules,
        ACTIVE_ROLES=_actives,
        INACTIVE_ROLES=_inactives,
        data={"date":str(datetime.now().year)}
    )

@customer_settings.route('/roles/edit', methods=['POST', 'GET'])
def edit_role():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    
    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
     
    else:
     cutomers_uid = None
    _customery_final_idz = customerfy.fetch_one(cutomers_uid, True).get_json()
    _customery_final_id = _customery_final_idz['real_id']
    _customery_uid = _customery_final_idz['id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()


    _roles = roles.fetch_by_customer(_customery_final_id).get_json()
    _modules = []
    _mods = module.fetch_by_user_realm(config.USER_CUSTOMER, True).get_json()
    for _mod in _mods:
        _mod["feats"] = features.fetch_by_module(
            _mod['real_id'], True).get_json()

        _modules.append(_mod)

    return render_template(
        'customers/settings/roles-edit.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS = farms,
        ROLES=_roles,
        FARM=swapped_firm_details,
        MODULES=_modules,
        data={"date":str(datetime.now().year)}
    )

@customer_settings.route('/roles/<role_id>/view', methods=['POST', 'GET'])
def view_role(role_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    role_real_id = roles.fetch_one(role_id,True).get_json()['real_id']
    role_real_idk = roles.fetch_one(role_id,True).get_json()
    
    users_by_role = []
    users_by_role = user_roles.fetch_by_role(role_real_id).get_json()

    return render_template(
        'customers/settings/role-view.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        USERROLES = users_by_role,
        ROLENAME = role_real_idk,
        # PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_settings.route('/activity-log', methods=['POST', 'GET'])
def activity_log():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
     print("_cutomer_real_id",_cutomer_real_id)
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    activities = []
    activities = alert.fetch_by_customer_all(_customery_final_id,config.ALERT_USER).get_json()
    logy = []
    # logy = logs.fetch_all().get_json()
    logy = logs.fetch_by_customer(_customery_final_id).get_json()
    print("Logs",logy)
    # print("All activities",activities)








    return render_template(
        'customers/settings/activity-logs.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS = farms,
        LOGS = logy,
        FARM=swapped_firm_details,
        ACTIVITIES = activities,
        data={"date":str(datetime.now().year)}
    )


@customer_settings.route('/download/invoice/<string:id>', methods=['POST', 'GET'])
def invoices(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
     print("_cutomer_real_id",_cutomer_real_id)
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = []
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    custmer_final_id = id
    customer_details = {}
    customer_details = customer_user.fetch_one(
        custmer_final_id, True).get_json()
    customer_real = customer_details['real_id']
    invoice_bill = {}
    invoice_bill = invoice.fetch_by_customer_id(customer_real).get_json()

    return render_template(
        'customers/settings/invoice.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS = farms,
        FARM=swapped_firm_details,
        INVOICE=invoice_bill,
        CUSTOMER=customer_details,
        # data={"date":str(datetime.now().year)}
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )
