from functools import wraps

import json
import os
import urllib.parse
from numpy import integer
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from datetime import timedelta
from app.core.user import customers as _customer
from app.core import customer as customerfy, greenhouse
from app.core.supplier import commodities
from app.core.customer import farms as _farms
from app.core import farm as ownfarms
from app.core.farm import actions
from app.core import supply
from app.core.supply import components
from app.core.customer import screenhouses
from app.core.customer import shadenets
from app.core.customer import shields
from app.core.customer import greenhouses
from app.models import CustomerFarmGreenhouse
from app.models import db
from app.core.customer import farm_greenhouses

from app.core.customer import greenhouse_shadenet
from app.core.customer import greenhouse_screenhouses
from app.core.customer import greenhouse_shields
from app.core.misc import soils
from app.models import SoilType
from app.core import planting
from app.core.planting import crops, suggested_plant, suggested_variety
from app.core.planting import plants as plnt
from app.core.planting import varieties as verty
from app.models import Plants
from app.models import Variety
from app.core import planting
from app.core.planting import production_stages
from app.core.planting import planting_production_stages
from app.models import PlantingProductionStage
from app.models import Planting
from app.models import ShieldPlantingSetting
from app.models import WaterTankSetting
from app.core.planting import setting
from app.core.planting.setting import water_tank
from app.models import Co2Setting
from app.models import AirTemperatureSetting
from app.models import AirHumiditySetting
from app.models import LightSetting
from app.models import SoilTemperatureSetting
from app.models import SoilMoistureSetting
from app.models import NitrogenSetting
from app.models import PhosphorusSetting
from app.models import PotassiumSetting
from app.core import shield
from app.config import activateConfig
from app.core.user import logs


from app.igh import portal_check

customer_inventory = Blueprint('customer_inventory', __name__,
                               static_folder='../../static', template_folder='.../../templates')
customer_inventory.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


@customer_inventory.route('/product-inventory', methods=['POST', 'GET'])
def product_inventory():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _customer_screenhouse = []
    _customer_shadnets = []
    _customer_shield = []
    _customer_greenhouse = []
    plant_details = []
    _customer_screenhouse = screenhouses.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_shadnets = shadenets.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_shield = shields.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_greenhouse = greenhouses.fetch_by_customer_all(
        _customery_final_id).get_json()
    _customer_screen_green = greenhouse_screenhouses.fetch_all().get_json()
    _customer_shadnet_green = greenhouse_shadenet.fetch_all().get_json()

    plant_details = crops.fetch_by_creator(_this_org).get_json()

    posted = request.form
    if posted:
        if "action" in posted:
            if posted.get("action") == "deactivate_plant" and "deactivate" in posted:

                deactivate_plant_id = posted.get('deactivate')
            #    print("Deactivate IDSSSSSSSSSSS",deactivate_plant_id)

                deactivate_pl = crops.deactivate(deactivate_plant_id)
                print(deactivate_pl)

            if posted.get("action") == "activate_plant" and "activate" in posted:

                activate_plant_id = posted.get('activate')
            #    print("Activate IDSSSSSSSSSSS",activate_plant_id)

                activate_pl = crops.activate(activate_plant_id)
                print(activate_pl)
            if posted.get("action") == "update_plant" and "pid" in posted:

                update_plant_id = posted.get('pid')
            #    print("Update id ",update_plant_id)

                plante_name = posted.get('plname')
                plante_variety = posted.get('plvariety')
                plante_start_d = posted.get('pstartdate')
                plante_no_plant = posted.get('pnoplant')
                plante_harv_esti = posted.get('pharvesti')
                plante_plant_period = posted.get('plantperiod')
                plante_prod_period = posted.get('plantprod')
                plante_harv_period = posted.get('plantharv')

                planty_id = posted.get('plname_id')
                veriety_idsp = posted.get('plvariety_id') 

                # plant_id_update = plnt.fetch_by_name(plante_name).get_json()
                # variety_id_update = verty.fetch_by_name(plante_variety).get_json()

                # if plant_id_update != {} :
                #     p_id = plant_id_update['id']

                #     print("PID",p_id)

                
                    

                # if variety_id_update != {}:

                #     v_id = variety_id_update['id']

                #     print("V_ID",v_id)

                

            #    print("Plan name and details",plante_name, plante_variety,plante_start_d,plante_no_plant,plante_harv_esti,plante_plant_period)
                edit_crops = crops.edit(
                          update_plant_id,
                                json.dumps({
                                
                                    "start_date": plante_start_d,
                                    "seedling_tally": plante_no_plant,
                                    "harvest_estimate": plante_harv_esti,
                                    "planting_period": plante_plant_period,
                                    "maturity_period": None,
                                    "production_period": plante_prod_period,
                                    "harvest_period": plante_harv_period,
                                    "crop_id": veriety_idsp,
                                    "planting_id": planty_id,
                                   
                                    
                                })
                            )
                print(str(edit_crops[0].get_json()))

    for planto in plant_details:

        plant_start_date = planto['start_date']
    #  print(type(plant_start_date))
        datetime_object = datetime.strptime(plant_start_date, '%Y-%m-%d')

    #  print("Plant start date",plant_start_date)

        plant_period = planto['planting_period']

        production_period = planto['production_period']

        harvest_period = planto['harvest_period']

        date_today = datetime.today()

        if planto['planting_id'] != {}:

         planting_uid = planto['planting_id']['id']


         print("Planting UID", planting_uid)
        else:
            planting_uid = None 

        date_planting = datetime_object + timedelta(days=plant_period)
        print("Date of planting", date_planting)

        date_harvesting = date_planting + timedelta(days=harvest_period)

        date_production = date_harvesting + timedelta(days=production_period)

        if date_today <= date_planting:

            print("This is planting period")
        elif date_today > date_planting and date_harvesting > date_today and date_harvesting > date_planting:
            h_stage = 'Harvesting'
            _maturity_period_harvest = production_stages.fetch_by_name(h_stage).get_json()[
                'id']

            _maturity_period_real_harvest = production_stages.fetch_one(
                _maturity_period_harvest, True).get_json()['real_id']

            planting_harv_stage = PlantingProductionStage(
                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                planting_id=None,
                production_stage_id=_maturity_period_real_harvest,
                creator_id=_this_org
            )
            db.session.add(planting_harv_stage)
            db.session.commit()

            if planting_harv_stage:

                planting_harv_stage_uid = planting_production_stages.fetch_by_id(
                    planting_harv_stage.id).get_json()['id']
                if planting_uid != None:

                 _main_plant_edit = planting.edit(
                    planting_uid,
                    json.dumps({
                        "customer_shield_id": None,
                        "current_production_stage_id": planting_harv_stage_uid,

                    })
                 )
                 print(str(_main_plant_edit[0].get_json()))

        elif date_today > date_harvesting:

            p_stage = 'Production'
            _maturity_period_prod = production_stages.fetch_by_name(p_stage).get_json()[
                'id']

            _maturity_period_real_prod = production_stages.fetch_one(
                _maturity_period_prod, True).get_json()['real_id']

            planting_prod_stage = PlantingProductionStage(
                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                planting_id=None,
                production_stage_id=_maturity_period_real_prod,
                creator_id=_this_org
            )
            db.session.add(planting_prod_stage)
            db.session.commit()

            if planting_prod_stage:

                planting_prod_stage_uid = planting_production_stages.fetch_by_id(
                    planting_prod_stage.id).get_json()['id']

                _main_plant_prod_edit = planting.edit(
                    planting_uid,
                    json.dumps({
                        "customer_shield_id": None,
                        "current_production_stage_id": planting_prod_stage_uid,

                    })
                )
                print(str(_main_plant_prod_edit[0].get_json()))

        elif date_today > date_production:
            print("This Planting is Closed or deactivated")

    return render_template(
        'customers/inventory/product-inventory.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        SCREENHOUSE=_customer_screenhouse,
        SHADNETS=_customer_shadnets,
        SHIELDS=_customer_shield,
        GHOUSES=_customer_greenhouse,
        GSIDS=_customer_screen_green,
        PLANTS=plant_details,
        GSSIDS=_customer_shadnet_green,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}

    )


@customer_inventory.route('/activate-screenhouse/<string:id>', methods=['POST', 'GET'])
def activate_screenhouse(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    farms = []
    _greenhouse = []
    _customer_screenh = {}
    action_response = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _greenhouse = greenhouses.fetch_by_customer_all(
        _customery_final_id).get_json()
    _customer_screenh = screenhouses.fetch_one(id, True).get_json()

    posted = request.form
    if posted:

        _green_sc = posted.get("green_S_house")
        _farm_sc = posted.get("farm_stype")

        if _green_sc != None and _farm_sc != None:
            farm_sc_real1 = _farms.fetch_one(_farm_sc, True).get_json()
            farm_sc_real = farm_sc_real1["id"]

            farm_sc_real_id = farm_sc_real1['real_id']
            print("Farm ID", farm_sc_real_id)
            # farm_greenid = farm_greenhouses.fetch_by_customer_farm(
            #     farm_sc_real).get_json()['id']
            # print("Farm Screen", farm_sc_real, farm_greenid)
            screenhouse_farm = {}
            screenhouse_farm = greenhouse_screenhouses.fetch_by_customer_farm_greenhouse(
                farm_sc_real_id).get_json()
            print("screeeeeeeeeeeeeeen farm", screenhouse_farm)
            creator = session["profile"]["user"]["id"]
            if screenhouse_farm == {}:
                _screen_green = greenhouse_screenhouses.add_new(
                    json.dumps({
                        "customer_farm_greenhouse_id": farm_sc_real,
                        "customer_screenhouse_id": id,
                        "creator_id": creator,


                    })
                )
                print(str(_screen_green[0].get_json()))
                if _screen_green:
                    _customer_screen = screenhouses.activate(id)
                    print(str(_customer_screen[0].get_json()))
                    action_response = {
                        "type": "success",
                                "message": "Screenhouse Activated successfully."
                    }
            else:

                action_response = {
                    "type": "warning",
                            "message": "Screenhouse is already assigned."
                }

    # _this_org = session["profile"]["user"]["real_id"]
    # _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    # cutomers_uid = _cutomer_real_id['customer']['id']
    # _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    # farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
    # swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    # farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    return render_template(
        'customers/inventory/activate-screenhouse.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FORM_ACTION_RESPONSE=action_response,
        FARM=swapped_firm_details,
        SCREENHOUSE=_customer_screenh,
        GHOUSES=_greenhouse,
        data={"date": str(datetime.now().year)}
    )


@customer_inventory.route('/deactivate-screenhouse-inventory/<id>', methods=['POST', 'GET'])
def deactivated_cinventory_screenhouse(id):

    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    farms = []
    _greenhouse = []
    _customer_screenh = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    _screenhouse_deactivate = screenhouses.deactivate(id)

    print("Deactivated", str(_screenhouse_deactivate[0].get_json()))
    if str(_screenhouse_deactivate[0].get_json()) != {}:
        _screen_d_msg = str(_screenhouse_deactivate[0].get_json())

    return render_template(
        'customers/inventory/deactivate.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        DEACTIVATE=_screen_d_msg,
        FARM=swapped_firm_details,
        data={"date": str(datetime.now().year)}

    )


@customer_inventory.route('/activate-greenhouse/<string:id>', methods=['POST', 'GET'])
def activate_greenhouse(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    farms = []
    _greenhouse = []
    _customer_screenh = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _greenhouse = greenhouses.fetch_by_customer_all(
        _customery_final_id).get_json()
    _greenhouse_fetch_id = {}
    _customer_shield = {}
    _greenhouse_fetch_id = greenhouses.fetch_one(id, True).get_json()
    _customer_shield = shields.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_screenh = screenhouses.fetch_one(id, True).get_json()
    action_response = {}
    farmz_greenid = {}
    customer_gid_real = greenhouses.fetch_one(id, True).get_json()['real_id']
    farmz_greenid = farm_greenhouses.fetch_by_customer_green(
        customer_gid_real).get_json()
    # print("Farm Green",farmz_greenid)

    posted = request.form
    if posted:

        _green_sc = id
        _farm_sc = posted.get("farmy_id")
        _status = posted.get("status")

        # print("FARM AND GREEN",_green_sc,_farm_sc)

        if _green_sc != None and _farm_sc != None:
            farm_sc_real = _farms.fetch_one(
                _farm_sc, True).get_json()['real_id']

            # farmz_greenid = farm_greenhouses.fetch_by_customer_green(customer_gid_real).get_json()
            # print("Farm Green",farmz_greenid)
            if farmz_greenid != {}:
                farm_greenid = farmz_greenid['id']
                creator = session["profile"]["user"]["id"]
                green_real_id = greenhouses.fetch_one(
                    id, True).get_json()['real_id']
                green_ref = greenhouses.fetch_one(id, True).get_json()[
                    'greenhouse']['greenhouse_ref']
                # print("FARM DETAILS",farm_greenid,_farm_sc,creator,green_real_id)
                _farm_green = farm_greenhouses.edit(
                    farm_greenid,
                    json.dumps({
                        "name": farm_greenid,
                        "customer_farm_id": _farm_sc,
                        "creator_id": creator,
                        "customer_greenhouse_id": green_real_id

                    })
                )
                print(str(_farm_green[0].get_json()))
            else:
                farm_greenid = 'Farm Greenhouse'
            # print("Farm Screen", farm_sc_real, farm_greenid)
                creator = session["profile"]["user"]["id"]
                green_real_id = greenhouses.fetch_one(
                    id, True).get_json()['real_id']
                _farm_green = farm_greenhouses.add_new(
                    json.dumps({
                        "name": farm_greenid,
                        "customer_farm_id": _farm_sc,
                        "creator_id": creator,
                        "customer_greenhouse_id": green_real_id

                    })
                )
                print(str(_farm_green[0].get_json()))
            if _farm_green:
                _customer_green = greenhouses.activate(id)
                print(str(_customer_green[0].get_json()))
                green_ref1 = greenhouses.fetch_one(id, True).get_json()['greenhouse']['greenhouse_ref']
                _activate_log = logs.add_new(
                    json.dumps({
                        "action": "Greenhouse is Activated",
                        "ref": green_ref1,
                        "ref_id": green_real_id,
                        "customer_id": cutomers_uid,
                        "creator_id": session["profile"]["user"]["id"]
                    }),

                )
                print(str(_activate_log[0].get_json()))

                action_response = {
                    "type": "success",
                            "message": "Greenhouse Activated successfully."
                }

    farmz_greenid = farm_greenhouses.fetch_by_customer_green(
        customer_gid_real).get_json()

    # _this_org = session["profile"]["user"]["real_id"]
    # _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    # cutomers_uid = _cutomer_real_id['customer']['id']
    # _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    # farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
    # swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    # farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    return render_template(
        'customers/inventory/activate-greenhouse.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        FARMGREEN=farmz_greenid,
        SCREENHOUSE=_customer_screenh,
        FORM_ACTION_RESPONSE=action_response,
        GHOUSES=_greenhouse,
        CSHIELD=_customer_shield,
        GREENFETCH=_greenhouse_fetch_id,
        data={"date": str(datetime.now().year)}
    )


@customer_inventory.route('/activate-shield/<string:id>', methods=['POST', 'GET'])
def activate_shield(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    farms = []
    _greenhouse = []
    _customer_screenh = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _greenhouse = greenhouses.fetch_by_customer_all(
        _customery_final_id).get_json()
    _greenhouse_fetch_id = {}
    _customer_shield = {}
    _greenhouse_fetch_id = greenhouses.fetch_one(id, True).get_json()
    _customer_shield = shields.fetch_one(id, True).get_json()
    _customer_shield_real = _customer_shield['real_id']
    _customer_screenh = screenhouses.fetch_one(id, True).get_json()
    _stages = production_stages.fetch_all().get_json()
    creator = session["profile"]["user"]["id"]
    action_response = {}
    action_response2 = {}
    action_response1 = {}

    posted = request.form
    if posted:

        # _green_sc = id
        _farm_sc = posted.get("farmoo_id")
        print("Farm ID",_farm_sc)
        _status = posted.get("status")
        _green_z_house = posted.get("green_z_house")

        _shield_long = posted.get("shield_long")
        _shield_lat = posted.get("shield_lat")
        _growth_area = posted.get("growth_area")
        _soil_type = posted.get("soil_type")

        _plant_name = posted.get("plant_name")
        _start_date = posted.get("start_date")
        _plant_variety = posted.get("plant_variety")
        _number_plant = posted.get("number_plant")
        _harv_estimate = posted.get("harv_estimate")
        _planting_period = posted.get("planting_period")
        _prod_time = posted.get("prod_time")
        _harvest_period = posted.get("harvest_period")
        _maturity_period = posted.get("maturity_period")

        _tot_lit = posted.get("tot_lit")
        _irrigation_max = posted.get("irrigation_max")
        _irrigation_min = posted.get("irrigation_min")
        _start_irr_time = posted.get("start_irr_time")
        _end_irr_time = posted.get("end_irr_time")
        _interval_time = posted.get("interval_time")

        _miminum_co2 = posted.get("miminum_co2")
        _maximum_co2 = posted.get("maximum_co2")

        _miminum_airt = posted.get("miminum_airt")
        _maximum_airt = posted.get("maximum_airt")

        _miminum_arh = posted.get("miminum_arh")
        _maximum_arh = posted.get("maximum_arh")

        _miminum_light = posted.get("miminum_light")
        _maximum_light = posted.get("maximum_light")

        _miminum_st = posted.get("miminum_st")
        _maximum_st = posted.get("maximum_st")

        _miminum_sm = posted.get("miminum_sm")
        _maximum_sm = posted.get("maximum_sm")

        _miminum_nitro = posted.get("miminum_nitro")
        _maximum_nitro = posted.get("maximum_nitro")

        _miminum_phos = posted.get("miminum_phos")
        _maximum_phos = posted.get("maximum_phos")

        _miminum_pota = posted.get("miminum_pota")
        _maximum_pota = posted.get("maximum_pota")

        print("Submit data", _soil_type, _plant_name,
              _plant_variety, _harvest_period)

        if _soil_type != "":

            _soil = SoilType(
                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                name=_soil_type,
                description=None,
                photo_media_id=None,
                creator_id=_this_org

            )
            db.session.add(_soil)
            db.session.commit()

            if _shield_long != None and _shield_lat != None and _growth_area != '':

                green_shield = greenhouse_shields.add_new(
                    json.dumps({
                        "latitude": _shield_lat,
                        "longitude": _shield_long,
                        "growth_area": _growth_area,
                        "soil_type_id": _soil.id,
                        "customer_farm_greenhouse_id": None,
                        "farm_id" : _farm_sc,
                        "customer_shield_id": id,
                        "creator_id": creator,



                    })
                )
                print("This is datttttttttttttttttttttttttttta",str(green_shield[0].get_json()))
            else:
                action_response = {
                    "type": "danger",
                    "message": "Please enter location and growth area data."
                }

        if _plant_name != '' and _plant_variety != '':

            _plant = Plants(
                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                name=_plant_name,
                creator_id=_this_org

            )
            db.session.add(_plant)
            db.session.commit()

            plant_real = plnt.fetch_by_id(_plant.id).get_json()['id']

            _planty_verieties = Variety(
                uid=uuid.uuid4(),
                status=config.STATUS_ACTIVE,
                name=_plant_variety,
                plant_id=_plant.id,
                creator_id=_this_org
            )
            db.session.add(_planty_verieties)
            db.session.commit()

            plant_real_vrty = verty.fetch_by_id(
                _planty_verieties.id).get_json()['id']

            if _start_date != None and _number_plant != None and _harv_estimate != '' and _planting_period != '' \
                    and _prod_time != '' and _harvest_period != '' and plant_real_vrty != None and plant_real != None:

                if _maturity_period:
                    _maturity_period_real = production_stages.fetch_one(
                        _maturity_period, True).get_json()['real_id']
                    planting_prod_stage = PlantingProductionStage(
                        uid=uuid.uuid4(),
                        status=config.STATUS_ACTIVE,
                        planting_id=None,
                        production_stage_id=_maturity_period_real,
                        creator_id=_this_org
                    )
                    db.session.add(planting_prod_stage)
                    db.session.commit()

                    if planting_prod_stage:

                        _main_plant = Planting(
                            uid=uuid.uuid4(),
                            status=config.STATUS_ACTIVE,
                            customer_shield_id=_customer_shield_real,
                            current_production_stage_id=planting_prod_stage.id,
                            creator_id=_this_org
                        )
                        db.session.add(_main_plant)
                        db.session.commit()
                        planting_crop_id = planting.fetch_by_id(
                            _main_plant.id).get_json()['id']
                        crops_type = crops.add_new(
                            json.dumps({
                                "start_date": _start_date,
                                "seedling_tally": _number_plant,
                                "harvest_estimate": _harv_estimate,
                                "planting_period": _planting_period,
                                "maturity_period": None,
                                "production_period": _prod_time,
                                "harvest_period": _harvest_period,
                                "crop_id": plant_real_vrty,
                                "planting_id": planting_crop_id,
                                "creator_id": creator,
                            })
                        )
                        print(str(crops_type[0].get_json()))

                        action_response = {
                            "type": "success",
                                    "message1": "Plant is Added successfully."
                        }

                        if _main_plant:
                            setting_plant = ShieldPlantingSetting(
                                uid=uuid.uuid4(),
                                status=config.STATUS_ACTIVE,
                                planting_id=_main_plant.id,
                                creator_id=_this_org
                            )
                            db.session.add(setting_plant)
                            db.session.commit()

                            if setting_plant and _tot_lit != None and _irrigation_min != None and _irrigation_max != None \
                                    and _start_irr_time != '' and _end_irr_time != '' and _interval_time != '':
                                water_cap = WaterTankSetting(

                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    capacity_litres=_tot_lit,
                                    irrigation_time_min=_irrigation_min,
                                    irrigation_time_max=_irrigation_max,
                                    irrigation_start_time=_start_irr_time,
                                    irrigation_end_time=_end_irr_time,
                                    irrigation_interval=_interval_time,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(water_cap)
                                db.session.commit()

                                action_response = {
                                    "type": "success",
                                    "message": "Water tank is added successfully."
                                }
                            else:

                                action_response = {
                                    "type": "danger",
                                    "message": "Please fill the all values."
                                }

                            if setting_plant and _miminum_co2 and _maximum_co2:

                                co2_setting = Co2Setting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_co2,
                                    max=_maximum_co2,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(co2_setting)
                                db.session.commit()

                            if setting_plant and _miminum_airt and _maximum_airt:

                                airt_setting = AirTemperatureSetting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_airt,
                                    max=_maximum_airt,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(airt_setting)
                                db.session.commit()

                            if setting_plant and _miminum_arh and _maximum_arh:

                                airh_setting = AirHumiditySetting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_arh,
                                    max=_maximum_arh,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(airh_setting)
                                db.session.commit()

                            if setting_plant and _miminum_light and _maximum_light:

                                light_setting = LightSetting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_light,
                                    max=_maximum_light,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(light_setting)
                                db.session.commit()

                            if setting_plant and _miminum_st and _maximum_st:

                                soiltemp_setting = SoilTemperatureSetting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_st,
                                    max=_maximum_st,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(soiltemp_setting)
                                db.session.commit()

                            if setting_plant and _miminum_sm and _maximum_sm:

                                soilmost_setting = SoilMoistureSetting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_sm,
                                    max=_maximum_sm,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(soilmost_setting)
                                db.session.commit()

                            if setting_plant and _miminum_nitro and _maximum_nitro:

                                nitro_setting = NitrogenSetting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_nitro,
                                    max=_maximum_nitro,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(nitro_setting)
                                db.session.commit()

                            if setting_plant and _miminum_phos and _maximum_phos:

                                phosposrus_setting = PhosphorusSetting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_phos,
                                    max=_maximum_phos,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(phosposrus_setting)
                                db.session.commit()

                            if setting_plant and _miminum_pota and _maximum_pota:

                                pottasium_setting = PotassiumSetting(
                                    uid=uuid.uuid4(),
                                    status=config.STATUS_ACTIVE,
                                    min=_miminum_pota,
                                    max=_maximum_pota,
                                    shield_planting_setting_id=setting_plant.id,
                                    creator_id=_this_org
                                )
                                db.session.add(pottasium_setting)
                                db.session.commit()
                            if id:
                                _customer_shield = shields.activate(id)
                                print(str(_customer_shield[0].get_json()))

                                if _customer_shield:

                                    _customer_shield = shields.fetch_one(
                                        id, True).get_json()

                                    action_response = {
                                        "type": "success",
                                        "message": "Shield is Activated successfully."
                                    }

                            # if setting_plant :
                            #    setting_uid = setting.fetch_by_id(setting_plant.id).get_json()['id']

                            #    water_setting = water_tank.add_new(
                            #                          json.dumps({
                            #        "capacity_litres": _tot_lit,
                            #        "irrigation_time_min": _irrigation_min,
                            #        "irrigation_time_max" :_irrigation_max,
                            #        "irrigation_start_time":_start_irr_time,
                            #        "irrigation_end_time":_end_irr_time,
                            #        "irrigation_interval": _interval_time,
                            #        "shield_planting_setting_id": setting_uid,
                            #        "creator_id": creator,
                            #        })
                            #    )
                            #    print(str(water_setting[0].get_json()))

        # if _green_sc != None and _farm_sc != None:
        #     farm_sc_real = _farms.fetch_one(_farm_sc, True).get_json()['real_id']
        #     farm_greenid = farm_greenhouses.fetch_by_customer_farm(farm_sc_real).get_json()['id']
        #     print("Farm Screen", farm_sc_real, farm_greenid)
        #     creator = session["profile"]["user"]["id"]
        #     green_real_id = greenhouses.fetch_one(id,True).get_json()['real_id']
        #     _farm_green = farm_greenhouses.add_new(
        #         json.dumps({
        #                    "name": farm_greenid,
        #                    "customer_farm_id": _farm_sc,
        #                    "creator_id": creator,
        #        "customer_greenhouse_id" : green_real_id

        #                    })
        #     )
        #     print(str(_farm_green[0].get_json()))
        #     if _farm_green:
        #         _customer_green = greenhouses.activate(id)
        #         print(str(_customer_green[0].get_json()))

    # _this_org = session["profile"]["user"]["real_id"]
    # _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    # cutomers_uid = _cutomer_real_id['customer']['id']
    # _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    # farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
    # swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    # farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    return render_template(
        'customers/inventory/activate-shield.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        SCREENHOUSE=_customer_screenh,
        GHOUSES=_greenhouse,
        CSHIELD=_customer_shield,
        STAGES=_stages,
        FORM_ACTION_RESPONSE=action_response,

        GREENFETCH=_greenhouse_fetch_id,
        data={"date": str(datetime.now().year)}
    )


@customer_inventory.route('/activate-shadenet/<string:id>', methods=['POST', 'GET'])
def activate_shadenet(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    farms = []
    _greenhouse = []
    _customer_shadnets = {}
    action_response = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _greenhouse = greenhouses.fetch_by_customer_all(
        _customery_final_id).get_json()
    _customer_shadnets = shadenets.fetch_one(id, True).get_json()

    posted = request.form
    if posted:

        _green_sh = posted.get("green_house")
        _farm_sh = posted.get("farm_type")

        if _green_sh != None and _farm_sh != None:
            green_sh_real = greenhouses.fetch_one(
                _green_sh, True).get_json()['real_id']
            farm_sh_details = _farms.fetch_one(_farm_sh, True).get_json()
            farm_sh_real = farm_sh_details['real_id']
            farm_sh_id = farm_sh_details['id']
            # farm_green_details = farm_greenhouses.fetch_by_customer_farm(farm_sh_real).get_json()
            farm_shadnet_details = {}

            farm_shadnet_details = greenhouse_shadenet.fetch_by_customer_farm_greenhouse(
                farm_sh_real).get_json()
            # print("ALl details", green_sh_real, farm_sh_real, farm_green_id)
            # if farm_green_details == {} and farm_shadnet_details == {}:

            #  _customery_details = {}
            #  _customery_details = customerfy.fetch_one(cutomers_uid, True).get_json()
            #  print("Name",_customery_details['name'])
            #  _farm_greenh = CustomerFarmGreenhouse(
            #             uid=uuid.uuid4(),
            #             status = config.STATUS_ACTIVE,
            #             name = _customery_details['name'],
            #             customer_farm_id = farm_sh_real,
            #             customer_greenhouse_id = green_sh_real,
            #             creator_id = _this_org

            #         )
            #  db.session.add(_farm_greenh)
            #  db.session.commit()
            #  if _farm_greenh:
            #  farm_green_new_id = farm_greenhouses.fetch_by_id(_farm_greenh.id).get_json()['id']
            if farm_shadnet_details == {}:
                creator = session["profile"]["user"]["id"]
                _shadnet_green = greenhouse_shadenet.add_new(
                    json.dumps({
                        "customer_farm_greenhouse_id": farm_sh_id,
                        "customer_shadenet_id": id,
                        "creator_id": creator,


                    })
                )
                print(str(_shadnet_green[0].get_json()))
                if _shadnet_green:

                    _customer_shad = shadenets.activate(id)
                    print(str(_customer_shad[0].get_json()))
                    action_response = {
                        "type": "Success",
                        "message": "Shadenet Activated Successfully"
                    }

            # elif farm_green_details != {} :

            #     farm_green_id = farm_green_details['id']

            #     creator = session["profile"]["user"]["id"]
            #     _shadnet_green = greenhouse_shadenet.add_new(
            #        json.dumps({
            #         "customer_farm_greenhouse_id": farm_green_id,
            #         "customer_shadenet_id": id,
            #         "creator_id": creator,

            #         })
            #     )
            #     print(str(_shadnet_green[0].get_json()))

            #     if _shadnet_green:

            #        _customer_shad = shadenets.activate(id)
            #        print(str(_customer_shad[0].get_json()))

            #        if _customer_shad:

            #            action_response = {
            #               "type": "Success",
            #               "message": "Shadenet Activated Successfully"
            #                       }

            else:

                action_response = {
                    "type": "warning",
                    "message": "Shadenet is already assigned "
                }

    return render_template(
        'customers/inventory/activate-shadenet.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        CGREEN=_greenhouse,
        SHADNET=_customer_shadnets,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year)}
    )


@customer_inventory.route('/deactivate-shadenet-inventory/<id>', methods=['POST', 'GET'])
def deactivated_cinventory_shadenet(id):

    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    farms = []
    _greenhouse = []
    _customer_screenh = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    _shadenet_deactivate = shadenets.deactivate(id)

    print("Deactivated", str(_shadenet_deactivate[0].get_json()))
    if str(_shadenet_deactivate[0].get_json()) != {}:
        _shadenet_d_msg = str(_shadenet_deactivate[0].get_json())

    return render_template(
        'customers/inventory/deactivate.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        DEACTIVATE=_shadenet_d_msg,
        FARM=swapped_firm_details,
        data={"date": str(datetime.now().year)}

    )


@customer_inventory.route('/activated-products', methods=['POST', 'GET'])
def active_product():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    _customer_screenhouse = []
    _customer_shadnets = []
    _customer_shield = []
    _customer_greenhouse = []
    plant_details = []

    _customer_screenhouse = screenhouses.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_shadnets = shadenets.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_shield = shields.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_greenhouse = greenhouses.fetch_by_customer_all(
        _customery_final_id).get_json()
    plant_details = crops.fetch_by_creator(_this_org).get_json()

    posted = request.form
    if posted:
        if "action" in posted:
            if posted.get("action") == "deactivate_plant" and "deactivate" in posted:

                deactivate_plant_id = posted.get('deactivate')
            #    print("Deactivate IDSSSSSSSSSSS",deactivate_plant_id)

                deactivate_pl = crops.deactivate(deactivate_plant_id)
                print(deactivate_pl)

            if posted.get("action") == "update_plant" and "pid" in posted:

                update_plant_id = posted.get('pid')
            #    print("Update id ",update_plant_id)

                plante_name = posted.get('plname')
                plante_variety = posted.get('plvariety')
                plante_start_d = posted.get('pstartdate')
                plante_no_plant = posted.get('pnoplant')
                plante_harv_esti = posted.get('pharvesti')
                plante_plant_period = posted.get('plantperiod')
                plante_prod_period = posted.get('plantprod')
                plante_harv_period = posted.get('plantharv')

                planty_id = posted.get('plname_id')
                veriety_idsp = posted.get('plvariety_id') 

                # plant_id_update = plnt.fetch_by_name(plante_name).get_json()
                # variety_id_update = verty.fetch_by_name(plante_variety).get_json()

                # if plant_id_update != {} :
                #     p_id = plant_id_update['id']

                #     print("PID",p_id)

                
                    

                # if variety_id_update != {}:

                #     v_id = variety_id_update['id']

                #     print("V_ID",v_id)

                

            #    print("Plan name and details",plante_name, plante_variety,plante_start_d,plante_no_plant,plante_harv_esti,plante_plant_period)
                edit_crops = crops.edit(
                          update_plant_id,
                                json.dumps({
                                
                                    "start_date": plante_start_d,
                                    "seedling_tally": plante_no_plant,
                                    "harvest_estimate": plante_harv_esti,
                                    "planting_period": plante_plant_period,
                                    "maturity_period": None,
                                    "production_period": plante_prod_period,
                                    "harvest_period": plante_harv_period,
                                    "crop_id": veriety_idsp,
                                    "planting_id": planty_id,
                                   
                                    
                                })
                            )
                print(str(edit_crops[0].get_json()))


    return render_template(
        'customers/inventory/active-products.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        PLANTS=plant_details,
        FARM=swapped_firm_details,
        SCREENHOUSE=_customer_screenhouse,
        SHADNETS=_customer_shadnets,
        SHIELDS=_customer_shield,
        GHOUSES=_customer_greenhouse,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_inventory.route('/deactivate-greenhouse-inventory/<id>', methods=['POST', 'GET'])
def deactivated_cinventory_greenhouse(id):

    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    farms = []
    _greenhouse = []
    _customer_screenh = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    green_real_id = greenhouses.fetch_one(id, True).get_json()['real_id']
    green_ref = greenhouses.fetch_one(id, True).get_json()[
        'greenhouse']['greenhouse_ref']
    _greenhouse_deactivate = greenhouses.deactivate(id)

    print("Deactivated", str(_greenhouse_deactivate[0].get_json()))
    if str(_greenhouse_deactivate[0].get_json()) != {}:
        _green_d_msg = str(_greenhouse_deactivate[0].get_json())
        _deactivate_log = logs.add_new(
            json.dumps({
                "action": "Greenhouse is Deactivated",
                "ref": green_ref,
                "ref_id": green_real_id,
                "customer_id": cutomers_uid,
                "creator_id": session["profile"]["user"]["id"]
            }),

        )
        print(str(_deactivate_log[0].get_json()))

    return render_template(
        'customers/inventory/deactivate.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        DEACTIVATE=_green_d_msg,
        FARM=swapped_firm_details,
        data={"date": str(datetime.now().year)}

    )


@customer_inventory.route('/edit-greengouse-inventory/<id>', methods=['POST', 'GET'])
def edit_cinventory_greenhouses(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    farms = []
    _greenhouse = []
    _customer_screenh = {}
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _greenhouse = greenhouses.fetch_by_customer_all(
        _customery_final_id).get_json()
    _greenhouse_fetch_id = {}
    _customer_shield = {}
    _greenhouse_fetch_id = greenhouses.fetch_one(id, True).get_json()
    _customer_shield = shields.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_screenh = screenhouses.fetch_one(id, True).get_json()
    action_response = {}
    customer_gid_real = greenhouses.fetch_one(id, True).get_json()['real_id']
    # farmz_greenid = farm_greenhouses.fetch_by_customer_green(customer_gid_real).get_json()

    posted = request.form
    if posted:

        _green_sc = id
        _farm_sc = posted.get("farm_id")
        _status = posted.get("status")

        if _green_sc != None and _farm_sc != None:
            farm_sc_real = _farms.fetch_one(
                _farm_sc, True).get_json()['real_id']
            if farm_sc_real != None:
                farm_greenz = farm_greenhouses.fetch_by_customer_green(
                    customer_gid_real).get_json()
                if farm_greenz != {}:
                    farm_greenid = farm_greenz['id']
                    creator = session["profile"]["user"]["id"]
                    green_real_id = greenhouses.fetch_one(
                        id, True).get_json()['real_id']
                    _farm_green = farm_greenhouses.edit(
                        farm_greenid,
                        json.dumps({
                            "name": farm_greenid,
                            "customer_farm_id": _farm_sc,
                            "creator_id": creator,
                            "customer_greenhouse_id": green_real_id

                        })
                    )
                    print(str(_farm_green[0].get_json()))

                    if _farm_green:
                        action_response = {
                            "type": "success",
                            "message": "Greenhouse Updated Successfully."
                        }
            #  else:
            #      farm_greenid = "Farm Greenhouse"
            # #  print("Farm Screen", farm_sc_real, farm_greenid)
            #      creator = session["profile"]["user"]["id"]
            #      green_real_id = greenhouses.fetch_one(id,True).get_json()['real_id']

            #      _farm_green = farm_greenhouses.add_new(
            #         json.dumps({
            #                "name": farm_greenid,
            #                "customer_farm_id": _farm_sc,
            #                "creator_id": creator,
            #                "customer_greenhouse_id" : green_real_id

            #                })
            #             )
            #      print(str(_farm_green[0].get_json()))
            #      if _farm_green:
            #         action_response = {
            #                 "type": "success",
            #                 "message": "Greenhouse Updated Successfully."
            #                  }

    farmz_greenid = farm_greenhouses.fetch_by_customer_green(
        customer_gid_real).get_json()

    return render_template(
        'customers/inventory/edit-greenhouse.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FORM_ACTION_RESPONSE=action_response,
        FARMS=farms,
        FARMGREEN=farmz_greenid,
        FARM=swapped_firm_details,
        SCREENHOUSE=_customer_screenh,
        GHOUSES=_greenhouse,
        CSHIELD=_customer_shield,
        GREENFETCH=_greenhouse_fetch_id,
        data={"date": str(datetime.now().year)}

    )


@customer_inventory.route('/edit-shield-inventory/<id>', methods=['POST', 'GET'])
def edit_cinventory_shield(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    _customer_screenhouse = []
    _customer_shadnets = []
    _customer_shield = []
    _customer_greenhouse = []

    _customer_screenhouse = screenhouses.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_shadnets = shadenets.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_shield = shields.fetch_by_customer(
        _customery_final_id).get_json()
    _customer_greenhouse = greenhouses.fetch_by_customer_all(
        _customery_final_id).get_json()

    # _this_org = session["profile"]["user"]["real_id"]
    # _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    # cutomers_uid = _cutomer_real_id['customer']['id']
    # _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    # farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
    # swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    # farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    return render_template(
        'customers/inventory/edit-shield-details.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        SCREENHOUSE=_customer_screenhouse,
        SHADNETS=_customer_shadnets,
        SHIELDS=_customer_shield,
        GHOUSES=_customer_greenhouse,
        data={"date": str(datetime.now().year)}

    )


@customer_inventory.route('/fertilizers-agrochems', methods=['POST', 'GET'])
def fertilizers_agrochems():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    _this_org1 = session["profile"]["user"]["id"]

    _components = components.fetch_all().get_json()
    _supplier_name = commodities.fetch_all().get_json()
    _supply = supply.fetch_by_customer(_customery_final_id).get_json()
    _ext_sh = shields.fetch_by_customer(_customery_final_id).get_json()
    # print("Supply", _supplier_name)
    action_response = {}

    # print("Comodities",request.form.get("supplier_id"))
    fertilizer_supply = []
    posted = request.form
    if posted:
        if "action" in posted:
            if posted.get("action") == "add_supplier" and "supplier_n_name" in posted:
                print("Comodities", posted.get("supplier_n_name"))
                exists = commodities.fetch_by_name(
                    posted.get("supplier_n_name")).get_json()
                if posted.get("supplier_n_name") != None and exists == {}:

                    _com = commodities.add_new(
                        json.dumps({
                            "name": posted.get("supplier_n_name"),
                            "commodity_id": None,
                            "supplier_id": None,
                            "creator_id": _this_org1
                        }),

                    )
                    print(str(_com[0].get_json()))

            if posted.get("action") == "add_ag_suppliers" and "supplier_agn_name" in posted:
                print("Comodities", posted.get("supplier_agn_name"))
                exists = commodities.fetch_by_name(
                    posted.get("supplier_agn_name")).get_json()
                if posted.get("supplier_agn_name") != None and exists == {}:

                    _com = commodities.add_new(
                        json.dumps({
                            "name": posted.get("supplier_agn_name"),
                            "commodity_id": None,
                            "supplier_id": None,
                            "creator_id": _this_org1
                        }),

                    )
                    print(str(_com[0].get_json()))

            if posted.get("action") == "add_fertilizer" and "name" in posted:

                # exists = commodities.fetch_by_name(posted.get("supplier_id")).get_json()
                # if posted.get("supplier_id") != None and exists == {} :

                #    _com =  commodities.add_new(
                #     json.dumps({
                #         "name": posted.get("supplier_id"),
                #         "commodity_id": None,
                #         "supplier_id":None,
                #         "creator_id" : _this_org1
                #          }),

                #     )
                #    print (str(_com[0].get_json()))
                if posted.get("name") != '' and posted.get('quantity') != '' and posted.get('notes') != '' and posted.get('active_component_id') != '':

                    print("Fertilizer", posted.get("name"))
                    exists = supply.fetch_by_name(
                        posted.get("name")).get_json()

                    if exists != {}:

                        action_response = {
                            "type": "warning",
                            "message": "Fertilizer name is exists.Please use other name. e.g Uera1,Urea2 in place Urea"
                        }

                    else:

                        add_fertilizer_det = supply.add_new(

                            json.dumps({
                                "name": posted.get("name"),
                                "commodity": config.COMMODITY_FERTILIZERS,
                                "metric": 'kg',
                                "quantity": posted.get('quantity'),
                                "minimum": posted.get('minimum'),
                                "notes": posted.get('notes'),
                                "remaining": posted.get('quantity'),
                                "active_component_id": posted.get('active_component_id'),
                                "supplier_commodity_id": posted.get("supplier_id"),
                                "customer_id": _customery_final_id,
                                "creator_id": _this_org1


                            })

                        )
                        print(str(add_fertilizer_det[0].get_json()))
                #  return supply.add_new(
                #     json.dumps({
                #         "name": posted.get("name"),
                #         "commodity": config.COMMODITY_FERTILIZERS,
                #         "metric": 'kg',
                #         "quantity": posted.get('quantity'),
                #         "minimum": posted.get('quantity'),
                #         "notes": posted.get('notes'),
                #         "active_component_id": posted.get('active_component_id'),
                #         "supplier_commodity_id": posted.get('supplier_commodity_id'),
                #         "customer_id": _customery_final_id,
                #         "creator_id": _this_org1
                #     }),
                #     True
                #  )

            if posted.get("action") == "update_fertilizer" and "name" in posted:

                fert_name = posted.get("name")
                fert_id = posted.get("id")
                fert_qty = posted.get("quantity")
                fert_min = posted.get("minimum")
                fert_notes = posted.get("notes")
                fert_cid = posted.get("active_component_id")
                fert_sid = posted.get("supplier_id")
                fert_rem = posted.get("remaining")

                fert_s_id = commodities.fetch_by_name(
                    fert_sid).get_json()['id']
                fert_c_id = components.fetch_by_component(
                    fert_cid).get_json()['id']

                # print("EDIt operation",fert_id,fert_name,fert_min,fert_cid,fert_notes,fert_qty,fert_sid,fert_rem)

                if fert_id != '' and fert_name != '' and fert_sid != '' and fert_qty != '' and fert_min != '' and fert_rem != '':

                    fertilizer_edit = supply.edit(
                        fert_id,

                        json.dumps({
                            "name": fert_name,
                            "commodity": config.COMMODITY_FERTILIZERS,
                            "metric": 'kg',
                            "quantity": fert_qty,
                            "minimum": fert_min,
                            "notes": fert_notes,
                            "remaining": fert_rem,
                            "active_component_id": fert_c_id,
                            "supplier_commodity_id": fert_s_id,
                            "customer_id": cutomers_uid,



                        })

                    )
                    print(str(fertilizer_edit[0].get_json()))

            if posted.get("action") == "update_agrochemical" and "name" in posted:

                agro_name = posted.get("name")
                agro_id = posted.get("id")
                agro_qty = posted.get("quantity")
                agro_min = posted.get("minimum")
                agro_notes = posted.get("notes")
                agro_cid = posted.get("active_component_id")
                agro_sid = posted.get("supplier_id")
                agro_rem = posted.get("remaining")

                agro_s_id = commodities.fetch_by_name(
                    agro_sid).get_json()['id']
                agro_c_id = components.fetch_by_component(
                    agro_cid).get_json()['id']

                print("EDIt operation", agro_id, agro_name, agro_min, agro_cid)

                if agro_id != '' and agro_name != '' and agro_sid != '' and agro_qty != '' and agro_min != '' and agro_rem != '':

                    agrochemical_edit = supply.edit(
                        agro_id,

                        json.dumps({
                            "name": agro_name,
                            "commodity": config.COMMODITY_AGROCHEMICALS,
                            "metric": 'kg',
                            "quantity": agro_qty,
                            "minimum": agro_min,
                            "notes": agro_notes,
                            "remaining": agro_rem,
                            "active_component_id": agro_c_id,
                            "supplier_commodity_id": agro_s_id,
                            "customer_id": cutomers_uid,



                        })

                    )
                    print(str(agrochemical_edit[0].get_json()))

            if posted.get("action") == "reorder_fert" and "refertid" in posted:

                reorder_id = posted.get("refertid")
                reorder_name = posted.get("refertname")

                if reorder_id != None:

                    reorder_details = supply.fetch_one(
                        reorder_id, True).get_json()
                    reorder_quanity = reorder_details["quantity"]
                    reorder_minimum = reorder_details["minimum"]
                    reorder_notes = reorder_details["notes"]
                    reorder_activecomp = reorder_details["active_component"]['id']
                    reorder_remaining = reorder_details["remaining"]
                    reorder_commodity_id = reorder_details["supplier_commodity"]['id']
                    reorder_customer_id = reorder_details["customer"]['id']

                    remaining_update = reorder_remaining + reorder_quanity

                    add_reorder_fert_det = supply.edit(
                        reorder_id,

                        json.dumps({
                            "name": reorder_name,
                            "commodity": config.COMMODITY_FERTILIZERS,
                            "metric": 'kg',
                            "quantity": reorder_quanity,
                            "minimum": reorder_minimum,
                            "notes": reorder_notes,
                            "remaining": remaining_update,
                            "active_component_id": reorder_activecomp,
                            "supplier_commodity_id": reorder_commodity_id,
                            "customer_id": cutomers_uid,



                        })

                    )
                    print(str(add_reorder_fert_det[0].get_json()))

            if posted.get("action") == "reorder_agro" and "reagroid" in posted:

                reorder_id = posted.get("reagroid")
                reorder_name = posted.get("reagroname")

                if reorder_id != None:

                    reorder_details = supply.fetch_one(
                        reorder_id, True).get_json()
                    reorder_quanity = reorder_details["quantity"]
                    reorder_minimum = reorder_details["minimum"]
                    reorder_notes = reorder_details["notes"]
                    reorder_activecomp = reorder_details["active_component"]['id']
                    reorder_remaining = reorder_details["remaining"]
                    reorder_commodity_id = reorder_details["supplier_commodity"]['id']
                    reorder_customer_id = reorder_details["customer"]['id']

                    remaining_update = reorder_remaining + reorder_quanity

                    add_reorder_agro_det = supply.edit(
                        reorder_id,

                        json.dumps({
                            "name": reorder_name,
                            "commodity": config.COMMODITY_AGROCHEMICALS,
                            "metric": 'kg',
                            "quantity": reorder_quanity,
                            "minimum": reorder_minimum,
                            "notes": reorder_notes,
                            "remaining": remaining_update,
                            "active_component_id": reorder_activecomp,
                            "supplier_commodity_id": reorder_commodity_id,
                            "customer_id": cutomers_uid,



                        })

                    )
                    print(str(add_reorder_agro_det[0].get_json()))

    posted = request.form
    if posted:
        if "action" in posted:
            if posted.get("action") == "add_agrochemicals" and "name" in posted:
                # print("All received data",posted.get("name"),posted.get('quantity'),posted.get('minimum'),posted.get('notes'),posted.get('active_component_id'),posted.get('supplier_commodity_id'))
                if posted.get("name") != '' and posted.get('quantity') != '' and posted.get('minimum') != '' and posted.get('notes') != ''\
                        and posted.get('active_component_id') != '' and posted.get('supplier_commodity_id') != '':
                    exists = supply.fetch_by_name(
                        posted.get("name")).get_json()
                    if exists != {}:
                        action_response = {
                            "type": "warning",
                            "message": "Agrochemical name is exists.Please use other name. e.g Uera1,Urea2 in place Urea"
                        }

                    else:

                        add_agrochemical_det = supply.add_new(

                            json.dumps({

                                #  return supply.add_new(
                                #     json.dumps({
                                "name": posted.get("name"),
                                "commodity": config.COMMODITY_AGROCHEMICALS,
                                "metric": 'kg',
                                "quantity": posted.get('quantity'),
                                "minimum": posted.get('minimum'),
                                "notes": posted.get('notes'),
                                "remaining": posted.get('quantity'),
                                "active_component_id": posted.get('active_component_id'),
                                "supplier_commodity_id": posted.get('supplier_commodity_id'),
                                "customer_id": _customery_final_id,
                                "creator_id": _this_org1
                            }),

                        )
                        print(str(add_agrochemical_det[0].get_json()))

            # if posted.get("action") == "update_types" and "types" in posted:
            #     _types = json.loads(posted.get("types"))
            #     for type in _types:
            #         type.edit(
            #             types["id"],
            #             json.dumps(
            #                 {
            #                     "name": types["name"],
            #                     "size": types["size"],
            #                     "description":types["description"],
            #                     "info_link":types["info_link"],
            #                     "photo_media_id": ""

            #                 }
            #             )
            #         )
            #     return jsonify(_types)

    # posted = request.form
    # if posted:
    #  _name = posted.get('name')
    #  _commodity = posted.get('commodity')
    #  _metric = posted.get('metric')
    #  _quantity = posted.get('quantity')
    #  _minimum = posted.get('minimum')
    #  _notes = posted.get('notes')
    #  _active_component_id = posted.get('active_component_id')
    #  _supplier_commodity_id = posted.get('supplier_commodity_id')
    #  _customer_id = _customery_final_id
    #  _creator_id = _this_org1

    #  _agri_fertilizer_supply = supply.add_new(
    #                          json.dumps({
    #                           "name": _name,
    #                           "commodity" : _commodity,
    #                           "metric" : _metric,
    #                           "quantity" : _quantity,
    #                           "minimum" : _minimum,
    #                           "notes" : _notes,
    #                           "active_component_id": _active_component_id,
    #                           "supplier_commodity_id" : _supplier_commodity_id,
    #                           "customer_id" : _customer_id,
    #                           "creator_id" : _creator_id

    #                          })

    #                    )
    #  print(str(_agri_fertilizer_supply[0].get_json()))

    return render_template(
        'customers/inventory/fertilizers-agrochems.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        COMPONENT=_components,
        FORM_ACTION_RESPONSE=action_response,
        SUPPLIER=_supplier_name,
        SUPPLY=_supply,
        SHIELDZ=_ext_sh,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_inventory.route('/greenhouse-customer-details/<string:id>', methods=['POST', 'GET'])
def view_greenhouse(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm
    _this_org = session["profile"]["user"]["real_id"]
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    _this_org1 = session["profile"]["user"]["id"]

    greenhouse_customer = {}
    greenhouse_customer = greenhouses.fetch_one(id, True).get_json()
    plant_details = {}
    customer_shield = {}
    shield_green_details = {}
    if greenhouse_customer['greenhouse'] != {}:
        green_id = greenhouse_customer['greenhouse']['greenhouse_ref']
        green_real_id = greenhouses.fetch_one(
            greenhouse_customer['id'], True).get_json()['real_id']
        # farm_green_idz = farm_greenhouses.fetch_by_customer_green(green_real_id).get_json()['id']
        # farm_green_real_id = farm_greenhouses.fetch_one(farm_green_idz,True).get_json()['real_id']
        # shield_green_details = greenhouse_shields.fetch_by_customer_farm_greenhouse(farm_green_real_id).get_json()
        shield_type_idi = shield.fetch_by_greenhouse(green_id).get_json()
        if shield_type_idi != {}:
            shield_type_id = shield_type_idi['id']
            shield_type_real_id = shield.fetch_one(
                shield_type_id, True).get_json()['real_id']
            print("Shield First ID", shield_type_real_id)
            customer_shield_id = shields.fetch_by_shield(
                shield_type_real_id).get_json()['id']
            customer_shield_real_id = shields.fetch_one(
                customer_shield_id, True).get_json()['real_id']
            print("Shield ID", customer_shield_real_id)
            shield_green_details = greenhouse_shields.fetch_by_customer_shield(
                customer_shield_real_id).get_json()

            plant_info = planting.fetch_by_customer_shield_id(
                customer_shield_real_id).get_json()
        #  print("Plant info",plant_info)
            if plant_info != {}:
                planting_info_id = plant_info['id']
                planting_info_real_id = planting.fetch_one(
                    planting_info_id, True).get_json()['real_id']
                production_stage = plant_info['current_production_stage']

                plant_details = crops.fetch_by_planting_id(
                    planting_info_real_id).get_json()

                print("Plant details", production_stage)

        #  print("Shield Details",shield_green_details)
    # print("Greenhouse",greenhouse_customer)

    return render_template(
        'customers/inventory/view-greenhouse.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        GREENHOUSE=greenhouse_customer,
        SHIELD=shield_green_details,
        PLANTS=plant_details,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_inventory.route('/plants/add', methods=['POST', 'GET'])
def add_plant():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    plant_suggest = suggested_plant.fetch_all().get_json()
    variety_suggest = suggested_variety.fetch_all().get_json()
    _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None:
        cutomers_uid = _cutomer_real_id['customer']['id']
    else:
        cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(
        cutomers_uid, True).get_json()['real_id']
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {}:
        farm_action_id = _farm_action_id['id']
        farm_id_by_action = actions.fetch_by_customer(
            _customery_final_id).get_json()['current_firm_uid']
        swapped_firm_details = ownfarms.fetch_by_id(
            farm_id_by_action).get_json()
    else:
        swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    action_response = {}
    _multiple = []
    _stages = production_stages.fetch_all().get_json()

    posted = request.form
    if posted:

        _plant_name = posted.getlist("plantname")
        _plant_date = posted.getlist("plantingdate[]")
        _plant_variety = posted.getlist("varietyofplant")
        _plant_number = posted.getlist("number_plant[]")
        _harvest_estimate = posted.getlist("harvest_estimate[]")
        _planting_period = posted.getlist("planting_period")
        _production_period = posted.getlist("production_period")
        _harvest_period = posted.getlist("harvest_period")
        _maturity_period = posted.getlist("maturity_period[]")

        x = 0
        for _name in _plant_name:

            if _plant_name[0]:
                _plant = Plants(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_plant_name[x],
                    creator_id=_this_org

                )
                db.session.add(_plant)
                db.session.commit()

                print("Plantttt", _plant.id, _plant_name[x])

                plant_real = plnt.fetch_by_id(_plant.id).get_json()['id']

                _planty_verieties = Variety(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    name=_plant_variety[x],
                    plant_id=_plant.id,
                    creator_id=_this_org
                )
                db.session.add(_planty_verieties)
                db.session.commit()

                plant_real_vrty = verty.fetch_by_id(
                    _planty_verieties.id).get_json()['id']

                if _plant_date[0] and _plant_number[0] and _harvest_estimate[0] and _planting_period[0] \
                        and _production_period[0] and _harvest_period[0] and plant_real_vrty and plant_real and _plant_variety[0]:

                    #  if  _maturity_period[x]:
                    #   maturity_id = "c99fbaa1-c3b2-4534-9733-6d707a5859eb"
                    #   _maturity_period_real = production_stages.fetch_one(maturity_id,True).get_json()['real_id']
                    p_stage = 'Planting'
                    _maturity_periodz = production_stages.fetch_by_name(p_stage).get_json()['id']

                    _maturity_period_real = production_stages.fetch_one(
                        _maturity_periodz, True).get_json()['real_id']

                    planting_prod_stage = PlantingProductionStage(
                        uid=uuid.uuid4(),
                        status=config.STATUS_ACTIVE,
                        planting_id=None,
                        production_stage_id=_maturity_period_real,
                        creator_id=_this_org
                    )
                    db.session.add(planting_prod_stage)
                    db.session.commit()

                    if planting_prod_stage:

                        _main_plant = Planting(
                            uid=uuid.uuid4(),
                            status=config.STATUS_ACTIVE,
                            customer_shield_id=None,
                            customer_id=_customery_final_id,
                            current_production_stage_id=planting_prod_stage.id,
                            creator_id=_this_org
                        )
                        db.session.add(_main_plant)
                        db.session.commit()
                        planting_crop_id = planting.fetch_by_id(
                            _main_plant.id).get_json()['id']
                        creator = session["profile"]["user"]["id"]
                        if _plant_date[x] and _plant_number[x] and _harvest_estimate[x] and _planting_period[x] and _production_period[x] and _harvest_period[x]:
                            print("Plant number", _plant_number[x])
                            crops_type = crops.add_new(
                                json.dumps({
                                    "start_date": _plant_date[x],
                                    "seedling_tally": _plant_number[x],
                                    "harvest_estimate": _harvest_estimate[x],
                                    "planting_period": _planting_period[x],
                                    "maturity_period": None,
                                    "production_period": _production_period[x],
                                    "harvest_period": _harvest_period[x],
                                    "crop_id": plant_real_vrty,
                                    "planting_id": planting_crop_id,
                                    "creator_id": creator,
                                })
                            )
                            print(str(crops_type[0].get_json()))

                            x = x + 1

                            action_response = {
                                "type": "success",
                                "message": "Plant has been added successfully."
                            }

                else:
                    action_response = {
                        "type": "danger",
                                "message": "Please add all required data"
                    }
            else:
                action_response = {
                    "type": "danger",
                            "message": "Please add plant name"
                }

                #  exists = greenh.fetch_by_green_ref(_greenhouse_ref[x]).get_json()
                #  if exists:
                #     action_response = {
                #         "type": "danger",
                #         "message": "Greenhouse ID already in use, please use a different one."
                #     }
                #  else:

                #    print("Types",_greenhouse_type_id[x])
                #    print("Size",_size[x])
                #    print("info link",_info_link[x])
                #    print("greenhouse ref",_greenhouse_ref[x])

                #    if _greenhouse_ref[0] and _size[0]  and _greenhouse_type_id[0] :
                #     _new_greenhouse = greenh.add_new(
                #      json.dumps({
                #         "greenhouse_ref": _greenhouse_ref[x],
                #         "size": _size[x],
                #         "info_link": _info_link[x],
                #         "assigned": config.NOT_ASSIGNED,
                #         "greenhouse_type_id": _greenhouse_type_id[x],
                #         "creator_id": session['profile']['user']['id'],

                #      }),

                #      )
                #     x = x + 1

                #     if _new_greenhouse:
                #      action_response = {
                #         "type": "success",
                #         "message": "Greenhouse added successfully."
                #      }

                #     else :

                #           action_response = {
                #                     "type": "danger",
                #                     "message": "Greenhouse is invalid."
                #                 }

                #    else :

                #     action_response = {
                #         "type": "danger",
                #         "message": "Please add Greenhouse ID, Size and Product Link."
                #     }

    return render_template(
        'customers/inventory/plant_add.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        MULTIGREEN=_multiple,
        PSUGGEST = plant_suggest,
        VSUGGEST = variety_suggest,
        STAGES=_stages,
        FORM_ACTION_RESPONSE=action_response,
        data={"date": str(datetime.now().year), "utc_date": datetime.utcnow()}
    )
