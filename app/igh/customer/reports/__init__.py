from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid
from sqlalchemy import null
from flask import Response
from werkzeug.exceptions import HTTPException
import io
import xlwt
from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from app.core.user import customers as _customer
from app.core import customer as customerfy
from app.core.customer import farms as _farms
from app.core import farm as ownfarms
from app.core.farm import actions, reportaction
from app.core.report import waste_pollution_management 
from app.core.report import fertilizer_application
from app.core.report import agrochemical_application
from app.core.report import preharvest_checklist
from app.core.report import harvest
from app.core.report import scouting
from app.core.report import other
from app.models import Report
from app.models import db
from app.core.customer import shields
from app.core.supply import components
from app.core.supply import application_methods
from app.core import supply
from app.core import pest
from app.core.planting import varieties
from app.core.planting import crops
from app.core.report import scouting_agronomist_recommendations
from app.core.user import customers as _customero
from app.core.report import waste_n_p_agronomy_recommend
from app.core.report import fertilizer_app_agronomist_rec
from app.core.report import agrochem_app_agronomist_rec
from app.core.report import preharv_agronomist_recom
from app.core.report import harvest_agronomy_recomend


from app.config import activateConfig

from app.igh import portal_check

from app.helper import valid_uuid, valid_date

customer_reports = Blueprint('customer_reports', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_reports.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@customer_reports.route('/daily-work', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    page = 2
    prev = page-1
    next = page+1

    no_of_posts = config.PAGE_LIMIT

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _wap = []
    _wap = waste_pollution_management.fetch_all().get_json()
    _shield_id = []
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _customery_final_id :
     _shield_id = shields.fetch_by_customer(_customery_final_id).get_json()
    _action_id = {}
    _components = []
    _fertilizers = []
    _pests = []
    _variety=[]
    planty_wnp = []
    _application_method = application_methods.fetch_all().get_json()

    _components = components.fetch_all().get_json()
    _fertilizers = supply.fetch_by_customer_comodity(_customery_final_id,1).get_json()
    _agrochems = supply.fetch_by_customer_comodity(_customery_final_id,2).get_json()
    _pests = pest.fetch_all().get_json()
    _variety = varieties.fetch_all().get_json()
    planty_wnp = crops.fetch_by_creator(_this_org).get_json()
    # print(" Cropsss",planty_wnp)

    _waste_pollution =[]
    _fertilizer_app =[]
    _agrochem_app =[]
    _preharv_check = []
    _harvest_details = []
    _scouting =[]
    _other_reports=[]
    _agronomist_recomendation = []
    _oreport_action =[]
    action_response = {}
    _wnp_recomendation = []
    _fetilizer_recomend = []
    _agrochem_recomend = []
    _preharv_recomend = []
    _harvest_recomend = []
    total = {}
    
    # print("Other actionssssss",_oreport_action)
   
 
    posted = request.form
    if posted:
        
        if "action" in posted:

           
            if posted.get("action") == "add_wap" and "residue" and "source" in posted:

                
                if _this_org :
                    _new_report = Report(
                               
                    uid=uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    type = config.REPORT_WASTE_POLLUTION_MANAGEMENT,
                    score = 1,
                    customer_id =  _customery_final_id,
                    creator_id = _this_org,   
               
                    )
                    db.session.add(_new_report)
                    db.session.commit()
            
                    if _new_report:
                     action_response = {
                        "type": "success",
                        "message": "Report added successfully."
                    }
                    print("report Id ",_new_report.id)

                # print("all details",posted.get("residue"),posted.get("source"),posted.get("eliminated_amount"))

                wnp_report = waste_pollution_management.add_new(
                    json.dumps({
                        "residue": posted.get("residue"),
                        "source": posted.get("source"),
                        "eliminated_amount" : posted.get("eliminated_amount"),
                        "reduced_amount" : posted.get("reduced_amount"),
                        "recycled_amount" : posted.get("recycled_amount"),
                        "customer_shield_id" : posted.get("customer_shield_id"),
                        "report_id" : _new_report.id,
                        "recommendations": None,
                        # "user_realm": config.USER_IGH,
                        "creator_id": session['profile']['user']['id'],
                        "customer_id" : cutomers_uid
                        
                    }),
                    True
                )

                # if wnp_report:

                #   wnp_report_details = waste_pollution_management.fetch_by_report(_new_report.id).get_json()
                #   print("All repo",wnp_report_details)
                #   wnp_main_id = wnp_report_details['id']
                #   wnp_report_id = wnp_report_details['report_id']['id']

                #   wnp_recomend = waste_n_p_agronomy_recommend.add_new(
                #       json.dumps({
                #            "wnprec": None,
                #            "wnp_id":  wnp_main_id,
                #            "report_id": wnp_report_id,
                #            "customer_id": cutomers_uid,
                #            "creator_id": session['profile']['user']['id'],

                #            }),
                
                #   )
                #   print(str(wnp_recomend[0].get_json()))
                
            # if posted.get("action") == "update_wap" and "waste_pollution_management" in posted:
            #     _types = json.loads(posted.get("waste_pollution_management"))
            #     for _wpm in waste_pollution_management:
            #         waste_pollution_management.edit(
            #             waste_pollution_management["id"],
            #             json.dumps(
            #                 {
            #                     "name": waste_pollution_management["name"],
            #                     "size": waste_pollution_management["size"],
            #                     "description":waste_pollution_management["description"],
            #                     "info_link":waste_pollution_management["info_link"],
            #                     "photo_media_id": ""
                                
            #                 }
            #             )
            #         )
            #     return jsonify(_types)

            if posted.get("action") == "add_fert" and "quantity_applied" in posted:

                
                if _this_org :
                    _new_f_report = Report(
                               
                    uid=uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    type = config.REPORT_FERTILIZER_APPLICATION,
                    score = 1,
                    customer_id =  _customery_final_id,
                    creator_id = _this_org,   
               
                    )
                    db.session.add(_new_f_report)
                    db.session.commit()
            
                    if _new_f_report:
                     action_response = {
                        "type": "success",
                        "message": "Report added successfully."
                    }
                    # print("report Id ",_new_f_report.id)
                    # print("supply application", posted.get("supply_application_method_id"))
                    # print("Fertilizer Alters", posted.get("fertilizer_id_alter"))
                    # print("supply method alter",posted.get("supply_application_alter"))
                    # print("Fertilizer component", posted.get("fertilizer_main_component"))
                    # print ("Fretilizer alters",posted.get("fertilizer_main_component_alter"))
                    # print("Shield ID",posted.get("customer_shield_id"))

                    if _new_f_report:

                        exists = application_methods.fetch_by_name(posted.get("supply_application_method_id")).get_json()

                        if exists == {}:

                           new_methods = application_methods.add_new(
                                          json.dumps({

                             "name" : posted.get("supply_application_method_id"),
                             "description": None,
                             "supply_id": None,    
                             "creator_id" :_creator_id,        

                                               }),
                                       True
                                  )

                           if new_methods:
                               method_id = application_methods.fetch_by_name(posted.get("supply_application_method_id")).get_json()['id']

                        else:
                            method_id = exists['id']

                        fertilizer_app = fertilizer_application.add_new(
                               json.dumps({
                                   "sources" : posted.get("sources"),
                                   "date_time_applied": posted.get("date_time_applied"),
                                   "quantity_applied": posted.get("quantity_applied"),
                                   "supply_application_method_id" : method_id,
                                   "fertilizer_id" : posted.get("fertilizer_id"),
                                   "fertilizer_main_component" : posted.get("fertilizer_main_component"),
                                   "customer_shield_id" : posted.get("customer_shield_id"),
                                   "report_id" : _new_f_report.id,
                                   # "user_realm": config.USER_IGH,
                                   "creator_id": session['profile']['user']['id'],
                                   "customer_id" : cutomers_uid
                        
                                }),
                              True
                            )
                        if fertilizer_app:
                            fertilizer_details = supply.fetch_one(posted.get("fertilizer_id"),True).get_json() 
                            fert_id = fertilizer_details['id']
                            fert_name = fertilizer_details['name']
                            fert_quantity = fertilizer_details['quantity']
                            fert_minimum = fertilizer_details['minimum']
                            fert_notes = fertilizer_details['notes']
                            fert_remaining = int(fertilizer_details['remaining']) - int(posted.get("quantity_applied"))
                            fert_active_component = fertilizer_details['active_component']['id']
                            fert_supplier_commodity_id = fertilizer_details['supplier_commodity']['id']


                            print("Fertilizer details",fertilizer_details)

                            _update_fert_det = supply.edit(
                                  fert_id,
               
                                 json.dumps({
                                     "name": fert_name,
                                     "commodity": config.COMMODITY_FERTILIZERS,
                                     "metric": 'kg',
                                     "quantity": fert_quantity,
                                     "minimum": fert_minimum,
                                     "notes": fert_notes,
                                     "remaining":fert_remaining,
                                     "active_component_id": fert_active_component,
                                     "supplier_commodity_id": fert_supplier_commodity_id,
                                     "customer_id": cutomers_uid,
                                 


                                   })

                                )
                            print(str(_update_fert_det[0].get_json()))

                        
                  


                # print("Fertilizer application details",posted.get("sources"),posted.get("date_time_applied"), posted.get("quantity_applied"),posted.get("supply_application_method_id"),posted.get("fertilizer_id"),posted.get("fertilizer_main_component"))

                # return fertilizer_application.add_new(
                #     json.dumps({
                #         "sources" : posted.get("sources"),
                #         "date_time_applied": posted.get("date_time_applied"),
                #         "quantity_applied": posted.get("quantity_applied"),
                #         "supply_application_method_id" : method_id,
                #         "fertilizer_id" : posted.get("fertilizer_id"),
                #         "fertilizer_main_component" : posted.get("fertilizer_main_component"),
                #         "customer_shield_id" : posted.get("customer_shield_id"),
                #         "report_id" : _new_f_report.id,
                #         # "user_realm": config.USER_IGH,
                #         "creator_id": session['profile']['user']['id'],
                #         "customer_id" : cutomers_uid
                        
                #     }),
                #     True
                # )
  



            if posted.get("action") == "add_agro" and "date_time_applied" in posted:

                
                if _this_org :
                    _new_a_report = Report(
                               
                    uid=uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    type = config.REPORT_AGROCHEMICAL_APPLICATION,
                    score = 1,
                    customer_id =  _customery_final_id,
                    creator_id = _this_org,   
               
                    )
                    db.session.add(_new_a_report)
                    db.session.commit()
            
                    if _new_a_report:
                     action_response = {
                        "type": "success",
                        "message": "Report added successfully."
                    }
                    print("report Id ",_new_a_report.id)


                    if _new_a_report:

                      exists = application_methods.fetch_by_name(posted.get("supply_application_method_id")).get_json()

                      if exists == {}:

                           new_methods = application_methods.add_new(
                                          json.dumps({

                             "name" : posted.get("supply_application_method_id"),
                             "description": None,
                             "supply_id": None,    
                             "creator_id" :_creator_id,        

                                               }),
                                       True
                                  )

                           if new_methods:
                               method_ag_id = application_methods.fetch_by_name(posted.get("supply_application_method_id")).get_json()['id']

                      else:
                            method_ag_id = exists['id']

                      print("Pest",posted.get("quantity"))

                      exist_pest = pest.fetch_by_name(posted.get("pest_id")).get_json()

                      if exist_pest == {}:

                          new_pests = pest.add_new(
                                          json.dumps({

                                         "name" : posted.get("pest_id"),
                                         "description": None,
                                         "creator_id" :_creator_id,        

                                               }),
                                           
                                            )
                          print(str(new_pests[0].get_json()))
                          if new_pests:

                            updated_pest_id = pest.fetch_by_name(posted.get("pest_id")).get_json()['id']
                      else:
                            updated_pest_id = exist_pest['id']

                     
                      print(posted.get("supply_application_method_id"),">>>>",posted.get("pest_id"),posted.get("agrochemical_id"),">>>",posted.get("agrochemical_main_component"), cutomers_uid)
                      agrochem_app = agrochemical_application.add_new(
                                json.dumps({
                                       "date_time_applied": posted.get("date_time_applied"),
                                       "rei": posted.get("rei"),
                                       "phi" : posted.get("phi"),
                                       "quantity" : posted.get("quantity"),
                                       "sources1": posted.get("sources1"),
                                       "re_entry_date" : posted.get("re_entry_date"),
                                       "preharvest_interval_expiration_date" : posted.get("preharvest_interval_expiration_date"),
                                       "customer_shield_id" : posted.get("customer_shield_id"),
                                       "pest_id" :  updated_pest_id,
                                       "supply_application_method_id" : method_ag_id,
                                       "agrochemical_id" : posted.get("agrochemical_id"),
                                       "agrochemical_main_component" : posted.get("agrochemical_main_component"),
                                       "report_id" : _new_a_report.id,
                                       "creator_id": session['profile']['user']['id'],
                                       "customer_id" : cutomers_uid

                        
                                          }),
                    
                      )

                      print(str(agrochem_app[0].get_json()))


                      if agrochem_app:
                            agrochem_details = supply.fetch_one(posted.get("agrochemical_id"),True).get_json() 
                            agro_id = agrochem_details['id']
                            agro_name = agrochem_details['name']
                            agro_quantity = agrochem_details['quantity']
                            agro_minimum = agrochem_details['minimum']
                            agro_notes = agrochem_details['notes']
                            agro_remaining = int(agrochem_details['remaining']) - int(posted.get("quantity"))
                            agro_active_component = agrochem_details['active_component']['id']
                            agro_supplier_commodity_id = agrochem_details['supplier_commodity']['id']


                            

                            _update_agro_det = supply.edit(
                                  agro_id,
               
                                 json.dumps({
                                     "name": agro_name,
                                     "commodity": config.COMMODITY_AGROCHEMICALS,
                                     "metric": 'kg',
                                     "quantity": agro_quantity,
                                     "minimum": agro_minimum,
                                     "notes": agro_notes,
                                     "remaining":agro_remaining,
                                     "active_component_id": agro_active_component,
                                     "supplier_commodity_id": agro_supplier_commodity_id,
                                     "customer_id": cutomers_uid,
                                 


                                   })

                                )
                            print(str(_update_agro_det[0].get_json()))

  


              
                # print(posted.get("supply_application_method_id1"),">>>>",posted.get("agrochemical_id"),">>>",posted.get("agrochemical_main_component"), cutomers_uid)
                # return agrochemical_application.add_new(
                #     json.dumps({
                #         "date_time_applied": posted.get("date_time_applied"),
                #         "rei": posted.get("rei"),
                #         "phi" : posted.get("phi"),
                #         "sources1": posted.get("sources1"),
                #         "re_entry_date" : posted.get("re_entry_date"),
                #         "preharvest_interval_expiration_date" : posted.get("preharvest_interval_expiration_date"),
                #         "customer_shield_id" : posted.get("customer_shield_id"),
                #         "pest_id" : posted.get("pest_id"),
                #         "supply_application_method_id" : posted.get("supply_application_method_id1"),
                #         "agrochemical_id" : posted.get("agrochemical_id"),
                #         "agrochemical_main_component" : posted.get("agrochemical_main_component"),
                #         "report_id" : _new_a_report.id,
                #         # "user_realm": config.USER_IGH,
                #         "creator_id": session['profile']['user']['id'],
                #         "customer_id" : cutomers_uid

                        
                #     }),
                #     True
                # )
  



            if posted.get("action") == "add_preharvest" and "animal_intrusion" in posted:

                
                if _this_org :
                    _new_p_report = Report(
                               
                    uid=uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    type = config.REPORT_PREHARVEST_CHECKLIST,
                    score = 1,
                    customer_id =  _customery_final_id,
                    creator_id = _this_org,   
               
                    )
                    db.session.add(_new_p_report)
                    db.session.commit()
            
                    if _new_p_report:
                     action_response = {
                        "type": "success",
                        "message": "Report added successfully."
                    }
                    print("report Id ",_new_p_report.id)

               

                return preharvest_checklist.add_new(
                    json.dumps({
                        "animal_intrusion": posted.get("animal_intrusion"),
                        "crew_washed_hands": posted.get("crew_washed_hands"),
                        "sources2": posted.get("sources2"),
                        "crew_clothing_is_clean" : posted.get("crew_clothing_is_clean"),
                        "crew_without_jewellery" : posted.get("crew_without_jewellery"),
                        "packing_material_clean" : posted.get("packing_material_clean"),
                        "customer_shield_id" : posted.get("customer_shield_id"),
                        "report_id" : _new_p_report.id,
                        "creator_id": session['profile']['user']['id'],
                        "customer_id" : cutomers_uid


                        
                    }),
                    True
                )
  

            if posted.get("action") == "add_harvest" and "harvest_date_time" in posted:

                
                if _this_org :
                    _new_h_report = Report(
                               
                    uid=uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    type = config.REPORT_HARVEST_DETAIL,
                    score = 1,
                    customer_id =  _customery_final_id,
                    creator_id = _this_org,   
               
                    )
                    db.session.add(_new_h_report)
                    db.session.commit()
            
                    if _new_h_report:
                     action_response = {
                        "type": "success",
                        "message": "Report added successfully."
                    }
                    print("report Id ",_new_h_report.id)

                    kilo_harvested = posted.get("kilos_harvested")
                    kilo_rejected = posted.get("kilos_rejected")
                    kilo_lost_in = posted.get("kilos_lost_in_transit")
                    kilo_sold = int(kilo_harvested) - (int(kilo_rejected)+ int(kilo_lost_in))
                    kilo_per_kg = posted.get("kilo_price")
                    total_income = int(kilo_sold) * int(kilo_per_kg)
                    # print("Total Income",total_income)

               

                return harvest.add_new(
                    json.dumps({
                        "harvest_date_time": posted.get("harvest_date_time"),
                        "kilos_harvested": kilo_harvested,
                        "kilos_rejected" : kilo_rejected,
                        "kilos_lost_in_transit" : kilo_lost_in,
                        "kilos_sold" : kilo_sold,
                        "kilo_price" : kilo_per_kg,
                        "total_income" : total_income,
                        "variety_id" : posted.get("variety_id"),
                        "customer_shield_id" : posted.get("customer_shield_id"),
                        "report_id" : _new_h_report.id,
                        "creator_id": session['profile']['user']['id'],
                        "customer_id" : cutomers_uid
                        
                    }),
                    True
                )


            if posted.get("action") == "add_scouting" and "leaf_miners" and "ball_worms" and  "thrips" and "caterpillars" and "mites" in posted:

                
                if _this_org :
                    _new_s_report = Report(
                               
                    uid=uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    type = config.REPORT_SCOUTING_REPORT,
                    score = 1,
                    customer_id =  _customery_final_id,
                    creator_id = _this_org,   
               
                    )
                    db.session.add(_new_s_report)
                    db.session.commit()
            
                    if _new_s_report:
                     action_response = {
                        "type": "success",
                        "message": "Report added successfully."
                    }
                    print("report Id ",_new_s_report.id)
                
                if posted.get("leaf_miners") != '' and posted.get("ball_worms") !='' and posted.get("thrips") != '' \
                    and posted.get("caterpillars") != '' and posted.get("mites") != '' and posted.get("scales") != ''\
                    and posted.get("bean_flies") != '' and posted.get("white_flies") != '' and posted.get("cut_worms") != ''\
                    and posted.get("other_insects") != '' and posted.get("aschochtya") != '' and posted.get("botrytis") != ''\
                    and posted.get("downy") != '' and posted.get("powdery") != '' and posted.get("leaf_spots") != ''\
                    and posted.get("halo_blight") != '' and posted.get("other_diseases") != '' and posted.get("weeds"):
                    

                 return scouting.add_new(
                    json.dumps({
                        "leaf_miners": posted.get("leaf_miners"),
                        "ball_worms": posted.get("ball_worms"),
                        "thrips" : posted.get("thrips"),
                        "caterpillars" : posted.get("caterpillars"),
                        "mites" : posted.get("mites"),
                        "scales" : posted.get("scales"),
                        "bean_flies" : posted.get("bean_flies"),
                        "white_flies" : posted.get("white_flies"),
                        "cut_worms" : posted.get("cut_worms"),
                        "other_insects" : posted.get("other_insects"),
                        "aschochtya" : posted.get("aschochtya"),
                        "botrytis" : posted.get("botrytis"),
                        "downy" : posted.get("downy"),
                        "powdery" : posted.get("powdery"),
                        "leaf_spots" : posted.get("leaf_spots"),
                        "halo_blight" : posted.get("halo_blight"),
                        "other_diseases" : posted.get("other_diseases"),
                        "weeds" : posted.get("weeds"),
                        "customer_shield_id" : posted.get("customer_shield_id"),
                        "report_id" : _new_s_report.id,
                        "creator_id": session['profile']['user']['id'],
                        "customer_id" : cutomers_uid                    
                    }),
                    True
                )
            else:
                 action_response = {
                        "type": "warning",
                        "message": "Empty value now allowed."
                    }

            if posted.get("action") == "add_orepots" and "notes"  in posted:

                
                if _this_org :
                    _new_o_report = Report(
                               
                    uid=uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    type = config.REPORT_OTHERS,
                    score = 1,
                    customer_id =  _customery_final_id,
                    creator_id = _this_org,   
               
                    )
                    db.session.add(_new_o_report)
                    db.session.commit()
            
                    if _new_o_report:
                     action_response = {
                        "type": "success",
                        "message": "Report added successfully."
                    }
                    print("report Id ",_new_o_report.id)
                    print("Action completed",posted.get("action_completed_ext"))
                    if posted.get("action_completed_ext") != "":
                        
                        oreps = reportaction.add_new(
                                json.dumps({
                                    "customer_id":_customery_final_id,
                                    
                                    "description": posted.get("action_completed_ext"),
                                    "creator_id" : session['profile']['user']['id']

                                     }),
                                   
                                   )
                        print (str(oreps[0].get_json()))

               
                print("Oth Reports",posted.get("action_completed"))
                return other.add_new(
                    json.dumps({
                        "notes": posted.get("notes"),
                        "farm_action_id": posted.get("action_completed"),
                        "customer_shield_id" : posted.get("customer_shield_id"),
                        "report_id" : _new_o_report.id,
                        "creator_id": session['profile']['user']['id'],
                        "customer_id" : cutomers_uid   
                
                    }),
                    True
                )

    _agronomist_recomendation = scouting_agronomist_recommendations.fetch_all().get_json()
    

    _waste_pollution = waste_pollution_management.fetch_by_customer(_customery_final_id).get_json()
    _fertilizer_app = fertilizer_application.fetch_by_customer(_customery_final_id).get_json()
    _agrochem_app = agrochemical_application.fetch_by_customer(_customery_final_id).get_json()
    _preharv_check = preharvest_checklist.fetch_by_customer(_customery_final_id).get_json()
    _harvest_details = harvest.fetch_by_customer(_customery_final_id).get_json()
    _scouting = scouting.fetch_by_customer(_customery_final_id).get_json()
    _other_reports = other.fetch_by_customer(_customery_final_id).get_json()
    _oreport_action = reportaction.fetch_all().get_json()
    _wnp_recomendation = waste_n_p_agronomy_recommend.fetch_by_customer(_customery_final_id).get_json()
    _fetilizer_recomend = fertilizer_app_agronomist_rec.fetch_by_customer(_customery_final_id).get_json()
    _agrochem_recomend = agrochem_app_agronomist_rec.fetch_by_customer(_customery_final_id).get_json()
    _preharv_recomend = preharv_agronomist_recom.fetch_by_customer(_customery_final_id).get_json()
    _harvest_recomend = harvest_agronomy_recomend.fetch_by_customer(_customery_final_id).get_json()


    














    return render_template(
        'customers/reports/daily-work.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        FARM=swapped_firm_details,
        FORM_ACTION_RESPONSE=action_response,
        SHIELDS = _shield_id,
        COMPONENT = _components,
        APPMETHOD = _application_method,
        FERTILIZER = _fertilizers,
        AGROCHEMS = _agrochems,
        CUSTOMER_REAL_DETTAILS = _cutomer_real_id,
        PEST = _pests,
        VARIETY = _variety,
        WNP = _waste_pollution,
        FERTAPP = _fertilizer_app,
        AGROAPP = _agrochem_app,
        PREHARV = _preharv_check,
        HARVEST = _harvest_details,
        SCOUT =_scouting,
        OTHERRE = _other_reports,
        RECOMEND = _agronomist_recomendation,
        WNPRECOMEND = _wnp_recomendation,
        FERTRECOMEND = _fetilizer_recomend,
        AGRORECOMEND = _agrochem_recomend,
        PREHARVRECOMEND = _preharv_recomend,
        HARVESTRECOMEND = _harvest_recomend,
        CROPSPLANTS = planty_wnp,
        OREP = _oreport_action,
        prev=prev, next=next,
        total=total,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_reports.route('/customer-archives', methods=['POST', 'GET'])
def creport_archives():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()
    _wap = []
    _wap = waste_pollution_management.fetch_all().get_json()
    _shield_id = []
    if _this_org :
     _cutomer_real_id = _customero.fetch_by_user(_this_org).get_json()
    if _customery_final_id :
     _shield_id = shields.fetch_by_customer(_customery_final_id).get_json()
    _action_id = {}
    _components = []
    _fertilizers = []
    _pests = []
    _variety=[]
    planty_wnp = []
    _application_method = application_methods.fetch_all().get_json()

    _components = components.fetch_all().get_json()
    _fertilizers = supply.fetch_by_customer(_customery_final_id).get_json()
    _pests = pest.fetch_all().get_json()
    _variety = varieties.fetch_all().get_json()
    planty_wnp = crops.fetch_by_creator(_this_org).get_json()
    # print(" Cropsss",planty_wnp)

    _waste_pollution =[]
    _fertilizer_app =[]
    _agrochem_app =[]
    _preharv_check = []
    _harvest_details = []
    _scouting =[]
    _other_reports=[]
    _agronomist_recomendation = []
    _oreport_action =[]

    _wnp_recomendation = []
    _fetilizer_recomend = []
    _agrochem_recomend = []
    _preharv_recomend = []
    _harvest_recomend = []

    
    
    
    
   
    
   
    
    action_response = {}



    _agronomist_recomendation = scouting_agronomist_recommendations.fetch_all().get_json()
    

    _waste_pollution = waste_pollution_management.fetch_by_customer(_customery_final_id).get_json()
    _fertilizer_app = fertilizer_application.fetch_by_customer(_customery_final_id).get_json()
    _agrochem_app = agrochemical_application.fetch_by_customer(_customery_final_id).get_json()
    _preharv_check = preharvest_checklist.fetch_by_customer(_customery_final_id).get_json()
    _harvest_details = harvest.fetch_by_customer(_customery_final_id).get_json()
    _scouting = scouting.fetch_by_customer(_customery_final_id).get_json()
    _other_reports = other.fetch_by_customer(_customery_final_id).get_json()
    _oreport_action = reportaction.fetch_all().get_json()
    _wnp_recomendation = waste_n_p_agronomy_recommend.fetch_by_customer(_customery_final_id).get_json()
    _fetilizer_recomend = fertilizer_app_agronomist_rec.fetch_by_customer(_customery_final_id).get_json()
    _agrochem_recomend = agrochem_app_agronomist_rec.fetch_by_customer(_customery_final_id).get_json()
    _preharv_recomend = preharv_agronomist_recom.fetch_by_customer(_customery_final_id).get_json()
    _harvest_recomend = harvest_agronomy_recomend.fetch_by_customer(_customery_final_id).get_json()
 
   
    
    
   
   



  
   
   
    
    
    
   
    
   

    _application_methods = application_methods.fetch_all().get_json()
    

    return render_template(
        'customers/reports/archives.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        FARMS=farms,
        FARM=swapped_firm_details,
        WNP=_waste_pollution,
        FERTAPP=_fertilizer_app,
        AGROAPP=_agrochem_app,
        PREHARV=_preharv_check,
        HARVEST=_harvest_details,
        SCOUT=_scouting,
        RECOMEND = _agronomist_recomendation,
        WNPRECOMEND = _wnp_recomendation,
        FERTRECOMEND = _fetilizer_recomend,
        AGRORECOMEND = _agrochem_recomend,
        PREHARVRECOMEND = _preharv_recomend,
        HARVESTRECOMEND = _harvest_recomend,
        OTHERRE=_other_reports,
        APPMETHOD=_application_methods,
        PROFILE=session['profile'],
        data={"date": str(datetime.now().year)}
    )



@customer_reports.route('daily-work/delete/<string:id>', methods=['POST', 'GET'])
def delete(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    waste_pollution_management.delete(id)

    


    return render_template(
        'customers/reports/wnpdelete.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_reports.route('daily-work/fertilizer/delete/<string:id>', methods=['POST', 'GET'])
def delete_fa(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    fertilizer_application.delete(id)

    


    return render_template(
        'customers/reports/fertilizer-report.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )




@customer_reports.route('daily-work/agrochemical/delete/<string:id>', methods=['POST', 'GET'])
def delete_agrochem(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    agrochemical_application.delete(id)

    


    return render_template(
        'customers/reports/agrochem-report.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )



@customer_reports.route('daily-work/preharvest/delete/<string:id>', methods=['POST', 'GET'])
def delete_preharv(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    preharvest_checklist.delete(id)

    


    return render_template(
        'customers/reports/preharv-report.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_reports.route('daily-work/harvest/delete/<string:id>', methods=['POST', 'GET'])
def delete_harv(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    harvest.delete(id)

    


    return render_template(
        'customers/reports/harvest-report.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_reports.route('daily-work/scouting/delete/<string:id>', methods=['POST', 'GET'])
def delete_scout(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    scouting.delete(id)

    


    return render_template(
        'customers/reports/scouting-report.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )


@customer_reports.route('daily-work/others/delete/<string:id>', methods=['POST', 'GET'])
def delete_oth(id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']
    _farm_action_id = {}
    _farm_action_id = actions.fetch_by_customer(_customery_final_id).get_json()
    if _farm_action_id != {} :
     farm_action_id = _farm_action_id['id']
     farm_id_by_action = actions.fetch_by_customer(_customery_final_id).get_json()['current_firm_uid']
     swapped_firm_details = ownfarms.fetch_by_id(farm_id_by_action).get_json()
    #  print("swapped farm details",swapped_firm_details)
    else: 
     swapped_firm_details = None
    farms = _farms.fetch_by_customer(_customery_final_id).get_json()

    other.delete(id)

    


    return render_template(
        'customers/reports/other-report.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        FARMS=farms,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )








@customer_reports.route('/download/report/excel')
def download_report():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']

    

    _waste_pollution = waste_pollution_management.fetch_by_customer(_customery_final_id).get_json()
    
   
    
    
   
    

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Waste and pollution Management')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'residue')
    sh.write(0, 2, 'source')
    sh.write(0, 3, 'eliminated_amount')
    sh.write(0, 4, 'reduced_amount')
    sh.write(0, 5, 'recycled_amount')

    idx = 0
    for row in _waste_pollution:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['residue'])
        sh.write(idx+1, 2, row['source'])
        sh.write(idx+1, 3, row['eliminated_amount'])
        sh.write(idx+1, 4, row['reduced_amount'])
        sh.write(idx+1, 5, row['recycled_amount'])

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=wnp_report.xls"})



@customer_reports.route('/download/fertilizerreport/excel')
def download_fertilizer_report():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']

    
    _fertilizer_app = fertilizer_application.fetch_by_customer(_customery_final_id).get_json()
    # print("Fert APP",_fertilizer_application)

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Fertilizer Application')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'date_time_applied')
    sh.write(0, 2, 'quantity_applied')
    sh.write(0, 3, 'sources')
    sh.write(0, 4, 'supply_application_method')
    sh.write(0, 5, 'fertilizer')
    sh.write(0, 6, 'fertilizer_main_component')
    sh.write(0, 7, 'customer_shield_id')
    sh.write(0, 8, 'report_id')
    sh.write(0, 9, 'customer_id')
  
    

    idx = 0
    for row in _fertilizer_app:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['date_time_applied'])
        sh.write(idx+1, 2, row['quantity_applied'])
        sh.write(idx+1, 3, row['sources'])
        if row['supply_application_method'] != {}:
         sh.write(idx+1, 4, row['supply_application_method']['name'])
        else:
         sh.write(idx+1, 4, None)  
        if row['fertilizer'] != {}:
         sh.write(idx+1, 5, row['fertilizer']['name'])
        else:
         sh.write(idx+1, 5, None)

        if row['fertilizer_main_component'] != {}:
         sh.write(idx+1, 6, row['fertilizer_main_component']['component'])
        else:
         sh.write(idx+1, 6, None)
        if row['customer_shield_id'] != {}:
         sh.write(idx+1, 7, row['customer_shield_id']['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 7, None)
        # sh.write(idx+1, 7, row['customer_shield_id']['shield']['serial']['boron_id'])
        sh.write(idx+1, 8, row['report_id']['main_id'])
        sh.write(idx+1, 9, row['customer_id']['main_id'])
       

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=fertilizer_report.xls"})



@customer_reports.route('/download/agrochemicalreport/excel')
def download_agrochemical_report():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']

   
    _agrochem_app = agrochemical_application.fetch_by_customer(_customery_final_id).get_json()
   

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Agrochemical Application')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'date_time_applied')
    sh.write(0, 2, 'rei')
    sh.write(0, 3, 'phi')
    sh.write(0, 4, 'sources1')
    sh.write(0, 5, 're_entry_date')
    sh.write(0, 6, 'preharvest_interval_expiration_date')
    sh.write(0, 7, 'pest')
    sh.write(0, 8, 'supply_application_method')
    sh.write(0, 9, 'agrochemical')
    sh.write(0,10, 'report_id')
    sh.write(0,11, 'customer_id')
  
    

    idx = 0
    for row in _agrochem_app:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['date_time_applied'])
        sh.write(idx+1, 2, row['rei'])
        sh.write(idx+1, 3, row['phi'])
        sh.write(idx+1, 4, row['sources1'])
        sh.write(idx+1, 5, row['re_entry_date'])
        sh.write(idx+1, 6, row['preharvest_interval_expiration_date'])
        if row['pest'] != {}:
         sh.write(idx+1, 7, row['pest']["name"])
        else:
         sh.write(idx+1, 7, None)
        if row['supply_application_method'] != {}:
         sh.write(idx+1, 8, row['supply_application_method']['name'])
        else:
            sh.write(idx+1, 8, None)
        if row['agrochemical'] != {}:
         sh.write(idx+1, 9, row['agrochemical']['name'])
        else:
         sh.write(idx+1, 9, None)
        sh.write(idx+1, 10, row['report_id']['main_id'])
        sh.write(idx+1, 11, row['customer_id']['main_id'])

       

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=agrochemical_report.xls"})




@customer_reports.route('/download/preharvestreport/excel')
def download_preharvest_report():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']

    
    _preharv_check = preharvest_checklist.fetch_by_customer(_customery_final_id).get_json()
   

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Preharvest Report')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'animal intrusion')
    sh.write(0, 2, 'crew washed hands')
    sh.write(0, 3, 'sources')
    sh.write(0, 4, 'crew clothing is clean')
    sh.write(0, 5, 'crew without jewellery')
    sh.write(0, 6, 'packing material clean')
    sh.write(0, 7, 'customer shield')
    sh.write(0,8, 'report_id')
    sh.write(0,9, 'customer_id')
  
    

    idx = 0
    for row in _preharv_check:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['animal_intrusion'])
        sh.write(idx+1, 2, row['crew_washed_hands'])
        sh.write(idx+1, 3, row['sources2'])
        sh.write(idx+1, 4, row['crew_clothing_is_clean'])
        sh.write(idx+1, 5, row['crew_without_jewellery'])
        sh.write(idx+1, 6, row['packing_material_clean'])
        if row['customer_shield_id'] != {}:
         sh.write(idx+1, 7, row['customer_shield_id']['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 7, None)
        # sh.write(idx+1, 7, row['customer_shield_id']['shield']['serial']['boron_id'])
        sh.write(idx+1, 8, row['report_id']['main_id'])
        sh.write(idx+1, 9, row['customer_id']['main_id'])

       

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=preharvest_report.xls"})





@customer_reports.route('/download/harvest/excel')
def download_harvest_report():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']

   
    _harvest_details = harvest.fetch_by_customer(_customery_final_id).get_json()
   

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Harvest Report')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'harvest date')
    sh.write(0, 2, 'Harvested in kilos')
    sh.write(0, 3, 'Rejected in kilos')
    sh.write(0, 4, 'Kilos lost in transit')
    sh.write(0, 5, 'kilos sold')
    sh.write(0, 6, 'kilo price')
    sh.write(0, 7, 'Total income')
    sh.write(0,8, 'variety')
    sh.write(0,9, 'customer shield')
    sh.write(0,10, 'report id')
    sh.write(0,11, 'customer id')
  
    

    idx = 0
    for row in _harvest_details:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['harvest_date_time'])
        sh.write(idx+1, 2, row['kilos_harvested'])
        sh.write(idx+1, 3, row['kilos_rejected'])
        sh.write(idx+1, 4, row['kilos_lost_in_transit'])
        sh.write(idx+1, 5, row['kilos_sold'])
        sh.write(idx+1, 6, row['kilo_price'])
        sh.write(idx+1, 7, row['total_income'])
        if row['variety'] != {}:
         sh.write(idx+1, 8, row['variety']['name'])
        else:
         sh.write(idx+1, 8, None)
        if row['customer_shield_id'] != {}:
         sh.write(idx+1, 9, row['customer_shield_id']['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 9, None)
        # sh.write(idx+1, 9, row['customer_shield_id']['shield']['serial']['boron_id'])
        sh.write(idx+1, 10, row['report_id']['main_id'])
        sh.write(idx+1, 11, row['customer_id']['main_id'])

       

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=harvest_report.xls"})


@customer_reports.route('/download/scoutreport/excel')
def download_scout_report():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']

     
    
    _scouting = scouting.fetch_by_customer(_customery_final_id).get_json()
   

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Preharvest Report')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'leaf miners')
    sh.write(0, 2, 'ball worms')
    sh.write(0,3, 'thrips')
    sh.write(0,4, 'caterpillars')
    sh.write(0,5, 'mites')
    sh.write(0,6, 'scales')
    sh.write(0,7, 'bean_flies')
    sh.write(0,8, 'white_flies')
    sh.write(0,9, 'cut_worms')
    sh.write(0,10, 'other_insects')
    sh.write(0,11, 'aschochtya')
    sh.write(0,12, 'botrytis')
    sh.write(0,13, 'downy')
    sh.write(0,14, 'powdery')
    sh.write(0,15, 'leaf_spots')
    sh.write(0,16, 'halo_blight')
    sh.write(0,17, 'other_diseases')
    sh.write(0,18, 'weeds')
    sh.write(0,19, 'customer_shield_id')
    sh.write(0,20, 'report_id')
    sh.write(0,21, 'customer_id')





     
    

    idx = 0
    for row in  _scouting:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row["leaf_miners"])
        sh.write(idx+1, 2, row["ball_worms"])
        sh.write(idx+1, 3, row["thrips"])
        sh.write(idx+1, 4, row["caterpillars"])
        sh.write(idx+1, 5, row["mites"])
        sh.write(idx+1, 6, row["scales"])
        sh.write(idx+1, 7, row["bean_flies"])
        sh.write(idx+1, 8, row["white_flies"])
        sh.write(idx+1, 9, row["cut_worms"])
        sh.write(idx+1, 10, row["other_insects"])
        sh.write(idx+1, 11, row["aschochtya"])
        sh.write(idx+1, 12, row["botrytis"])
        sh.write(idx+1, 13, row["downy"])
        sh.write(idx+1, 14, row["powdery"])
        sh.write(idx+1, 15, row["leaf_spots"])
        sh.write(idx+1, 16, row["halo_blight"])
        sh.write(idx+1, 17, row["other_diseases"])
        sh.write(idx+1, 18, row["weeds"])
        if row['customer_shield_id'] != {}:
         sh.write(idx+1, 19, row['customer_shield_id']['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 19, None)
        # sh.write(idx+1, 19, row['customer_shield_id']['shield']['serial']['boron_id'])
        sh.write(idx+1, 20, row['report_id']['main_id'])
        sh.write(idx+1, 21, row['customer_id']['main_id'])
        

       

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=scout_report.xls"})





@customer_reports.route('/download/otherreport/excel')
def download_other_report():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = session["profile"]["user"]["real_id"]
    _creator_id = session['profile']['user']['id']
    if _this_org :
     _cutomer_real_id = _customer.fetch_by_user(_this_org).get_json()
    if _cutomer_real_id != None :
     cutomers_uid = _cutomer_real_id['customer']['id']
    else:
     cutomers_uid = None
    _customery_final_id = customerfy.fetch_one(cutomers_uid, True).get_json()['real_id']

     
    _other_reports = other.fetch_by_customer(_customery_final_id).get_json()
    
   

    output = io.BytesIO()
    # create WorkBook object
    workbook = xlwt.Workbook()
    # add a sheet
    sh = workbook.add_sheet('Preharvest Report')

    # add headers
    sh.write(0, 0, 'id')
    sh.write(0, 1, 'notes')
    # sh.write(0, 2, 'farm action')
    sh.write(0, 3, 'customer shield')
    sh.write(0,4, 'report_id')
    sh.write(0,5, 'customer_id')
  
    

    idx = 0
    for row in  _other_reports:
        sh.write(idx+1, 0, str(row['id']))
        sh.write(idx+1, 1, row['notes'])
        # sh.write(idx+1, 2, row['farm_action'])
        if row['customer_shield_id'] != {}:
         sh.write(idx+1, 3, row['customer_shield_id']['shield']['serial']['boron_id'])
        else:
            sh.write(idx+1, 3, None) 
        sh.write(idx+1, 4, row['report_id']['main_id'])
        sh.write(idx+1, 5, row['customer_id']['main_id'])

       

        idx += 1

    workbook.save(output)
    output.seek(0)

    return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition": "attachment;filename=other_report.xls"})









   