# fetch_igh_user - uuid
# fetch_customer_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv
from werkzeug.security import check_password_hash, generate_password_hash

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Auth

from app.core import user
from app.core import mail

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def auth_obj(auth=False, real_id=False):
    _auth = {}
    if auth:
        if real_id:
            _auth["real_id"] = auth.id

        _auth["id"] = str(auth.uid)
        _auth["user"] = user.fetch_by_id(auth.user_id, True).get_json()
        _auth["username"] = auth.username
        _auth["password"] = auth.password
        _auth["activation_code"] = auth.activation_code
        _auth["created_by"] = creator_detail(auth.creator_id)
        _auth["status"] = status_name(auth.status)
        _auth["created"] = auth.created_at
        _auth["modified"] = auth.modified_at

    return _auth

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        auth = db\
            .session\
            .query(Auth)\
            .filter(Auth.uid == uid)\
            .filter(Auth.status > config.STATUS_DELETED)\
            .first()
        if auth:
            response = auth_obj(auth, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        auth = db\
            .session\
            .query(Auth)\
            .filter(Auth.id == id)\
            .filter(Auth.status > config.STATUS_DELETED)\
            .first()
        if auth:
            response = auth_obj(auth)

    return jsonify(response)

def fetch_by_user(user_id=0):
    response = {}
    if user_id:
        auth = db\
            .session\
            .query(Auth)\
            .filter(Auth.user_id == user_id)\
            .filter(Auth.status > config.STATUS_DELETED)\
            .first()
        if auth:
            response = auth_obj(auth)

    return jsonify(response)

def fetch_by_username(username=None):
    response = {}
    if username:
        auth = db\
            .session\
            .query(Auth)\
            .filter(Auth.username == username)\
            .filter(Auth.status > config.STATUS_DELETED)\
            .first()
        if auth:
            response = auth_obj(auth)

    return jsonify(response)

def fetch_by_activation_code(activation_code=0):
    response = {}
    if activation_code:
        auth = db\
            .session\
            .query(Auth)\
            .filter(Auth.activation_code == activation_code)\
            .filter(Auth.status > config.STATUS_DELETED)\
            .first()
        if auth:
            response = auth_obj(auth)

    return jsonify(response)



def add_new(auth_data=None, return_obj=False):

    try:
        data = json.loads(auth_data)
        if data:

            _activation_code = str(uuid.uuid4())

            if _activation_code and data['user_id'] and data['email_address'] \
                and data['creator_id']:
                new_auth = Auth(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    username = data['email_address'],
                    user_id = data['user_id'],
                    activation_code = _activation_code,
                    creator_id = data['creator_id']
                )
                db.session.add(new_auth)
                db.session.commit()
                # if new_auth:
                #     # Send activation mail
                #     _activation_mail = {
                #         "to": data['email_address'],
                #         "subject": "Welcome to FarmCloud",
                #         "activation": {
                #             "names": data['first_name'],
                #             "code": _activation_code,
                #         },
                #     }
                #     _dispatch = mail.send_user_activation(
                #         json.dumps(_activation_mail)
                #      )
                        
                    

                if return_obj:
                        return jsonify(fetch_one(new_auth.uid),True), 200

                return jsonify({"info": "Auth added successfully!"}), 200

        return jsonify({"error": "Auth not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(auth_id, auth_data=None, return_obj=False):

    try:
        data = json.loads(auth_data)
        if data:

            if valid_uuid(auth_id):
                _auth = Auth\
                    .query\
                    .filter(Auth.uid == auth_id)\
                    .first()
                if _auth:

                    if 'username' in data:
                        _username = data['username']

                        _exists = fetch_by_username(_username).get_json()
                        if _exists:
                            return jsonify({"error": "Auth username provided already exists."}), 409

                        _auth.username = _username

                    if 'password' in data:
                        _auth.password = generate_password_hash(data['password'])

                    if 'activation_code' in data:
                        _auth.activation_code = data['activation_code']

                    if 'reset_code' in data:
                        _auth.reset_code = data['reset_code']

                    db.session.commit()

                    return jsonify({"info": "Auth edited successfully!"}), 200

        return jsonify({"error": "Auth not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(auth_id):

    try:
        if valid_uuid(auth_id) :
            _auth = Auth\
                .query\
                .filter(Auth.uid == auth_id)\
                .first()
            if _auth:
                _auth.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Auth deactivated successfully!"}), 200

        return jsonify({"error": "Auth not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
