# fetch_realm_user - uuid
# fetch_customer_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import UserRealm

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def realm_name(realm_id=None):
    _realm_name = ""

    if realm_id == config.USER_IGH:
        _realm_name = "IGH"

    if realm_id == config.USER_CUSTOMER:
        _realm_name = "Customer"

    if realm_id == config.USER_PARTNER:
        _realm_name = "Partner"

    return(_realm_name)

def realm_obj(realm=False, real_id=False):
    _realm = {}
    if realm:
        if real_id:
            _realm["real_id"] = realm.id

        _realm["id"] = str(realm.uid)
        _realm["user"] = user.fetch_by_id(realm.user_id).get_json()
        _realm["realm"] = realm.realm
        _realm["created_by"] = creator_detail(realm.creator_id)
        _realm["status"] = status_name(realm.status)
        _realm["created"] = realm.created_at
        _realm["modified"] = realm.modified_at

    return _realm

def fetch_all():
    response = []

    realms = db\
        .session\
        .query(UserRealm)\
        .filter(UserRealm.status > config.STATUS_DELETED)\
        .all()

    for realm in realms:
        response.append(realm_obj(realm))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        realm = db\
            .session\
            .query(UserRealm)\
            .filter(UserRealm.uid == uid)\
            .filter(UserRealm.status > config.STATUS_DELETED)\
            .first()
        if realm:
            response = realm_obj(realm, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        realm = db\
            .session\
            .query(UserRealm)\
            .filter(UserRealm.id == id)\
            .filter(UserRealm.status > config.STATUS_DELETED)\
            .first()
        if realm:
            response = realm_obj(realm)

    return jsonify(response)

def fetch_by_user(id=0):
    response = []
    if id:
        realms = db\
            .session\
            .query(UserRealm)\
            .filter(UserRealm.user_id == id)\
            .filter(UserRealm.status > config.STATUS_DELETED)\
            .all()
        for realm in realms:
            response.append(realm_obj(realm))

    return jsonify(response)

def add_new(realm_data=None, return_obj=False):

    try:
        data = json.loads(realm_data)
        if data:

            _realm = None
            if 'realm' in data:
                _realm = data['realm']

            if data['user_id'] and data['creator_id']:
                new_realm = UserRealm(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    realm = _realm,
                    user_id = data['user_id'],
                    creator_id = data['creator_id']
                )
                db.session.add(new_realm)
                db.session.commit()
                if new_realm:
                    if return_obj:
                        return jsonify(fetch_one(new_realm.uid)), 200

                    return jsonify({"info": "User realm added successfully!"}), 200

        return jsonify({"error": "User realm not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error-realms": str(e)}), 406

def deactivate(realm_id):

    try:

        if valid_uuid(realm_id) :
            _realm = UserRealm\
                .query\
                .filter(UserRealm.uid == realm_id)\
                .first()
            if _realm:
                _realm.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "User realm deactivated successfully!"}), 200

        return jsonify({"error": "User realm not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
