# fetch_igh_user - uuid
# fetch_customer_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import UserRole

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def user_role_obj(user_role=False, real_id=False):
    _user_role = {}
    if user_role:
        if real_id:
            _user_role["real_id"] = user_role.id

        _user_role["id"] = str(user_role.uid)
        _user_role["realm"] = user.realms.realm_name(user_role.user_realm)
        _user_role["user"] = user.fetch_by_id(user_role.user_id).get_json()
        _user_role["role"] = user.roles.fetch_by_id(user_role.role_id).get_json()
        _user_role["created_by"] = creator_detail(user_role.creator_id)
        _user_role["status"] = status_name(user_role.status)
        _user_role["created"] = user_role.created_at
        _user_role["modified"] = user_role.modified_at

    return _user_role

def fetch_all():
    response = []

    user_roles = db\
        .session\
        .query(UserRole)\
        .filter(UserRole.status > config.STATUS_DELETED)\
        .all()

    for user_role in user_roles:
        response.append(user_role_obj(user_role))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        user_role = db\
            .session\
            .query(UserRole)\
            .filter(UserRole.uid == uid)\
            .filter(UserRole.status > config.STATUS_DELETED)\
            .first()
        if user_role:
            response = user_role_obj(user_role, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        user_role = db\
            .session\
            .query(UserRole)\
            .filter(UserRole.id == id)\
            .filter(UserRole.status > config.STATUS_DELETED)\
            .first()
        if user_role:
            response = user_role_obj(user_role)

    return jsonify(response)

def fetch_by_user_realm(user_realm=0):
    response = {}
    if user_realm:
        user_role = db\
            .session\
            .query(UserRole)\
            .filter(UserRole.user_realm == user_realm)\
            .filter(UserRole.status > config.STATUS_DELETED)\
            .first()
        if user_role:
            response = user_role_obj(user_role)

    return jsonify(response)

def fetch_by_user(user_id=0):
    response = {}
    if user_id:
        user_role = db\
            .session\
            .query(UserRole)\
            .filter(UserRole.user_id == user_id)\
            .filter(UserRole.status > config.STATUS_DELETED)\
            .first()
        if user_role:
            response = user_role_obj(user_role)

    return jsonify(response)

def fetch_by_role(role_id=0):
    response = []
    if role_id:
        user_roles = db\
            .session\
            .query(UserRole)\
            .filter(UserRole.role_id == role_id)\
            .filter(UserRole.status > config.STATUS_DELETED)\
            .order_by(UserRole.id)\
            .all()
        for user_role in user_roles:
            response.append(user_role_obj(user_role))

    return jsonify(response)

def add_new(user_role_data=None, return_obj=False):

    try:
        data = json.loads(user_role_data)
        if data:
            _user_realm = None
            if 'user_realm' in data:
                _user_realm = data['user_realm']

            _role_id = None
            if valid_uuid(data['role_id']):
                _role_id = user.roles.fetch_one(data['role_id'], True).get_json()['real_id']

            _user_id = data['user_id']
            _creator = data['creator_id']

            if _user_realm and _user_id and _role_id and _creator:
                new_user_role = UserRole(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    user_realm = _user_realm,
                    user_id = _user_id,
                    role_id = _role_id,
                    creator_id = _creator
                )
                db.session.add(new_user_role)
                db.session.commit()
                if new_user_role:
                    if return_obj:
                        return jsonify(fetch_one(new_user_role.uid)), 200

                    return jsonify({"info": "User role added successfully!"}), 200

        return jsonify({"error": "User role not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(user_role_id, user_role_data=None, return_obj=False):

    try:
        data = json.loads(user_role_data)
        if data:

            if valid_uuid(user_role_id):
                _user_role = UserRole\
                    .query\
                    .filter(UserRole.uid == user_role_id)\
                    .first()
                if _user_role:

                    if 'user_realm' in data:
                        _user_role.user_realm = data['user_realm']

                    if 'user_id' in data and valid_uuid(data['user_id']):
                        _user_id = user.fetch_one(data['user_id'], True).get_json()
                        if _user_id:
                            _user_role.user_id = _user_id["real_id"]

                    if 'role_id' in data and valid_uuid(data['role_id']):
                        _role_id = user.roles.fetch_one(data['role_id'], True).get_json()
                        if _role_id:
                            _user_role.role_id = _role_id["real_id"]

                    db.session.commit()

                    return jsonify({"info": "User role edited successfully!"}), 200

        return jsonify({"error": "User role not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(user_role_id):

    try:

        if valid_uuid(user_role_id) :
            _user_role = UserRole\
                .query\
                .filter(UserRole.uid == user_role_id)\
                .first()
            if _user_role:
                _user_role.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "User role deactivated successfully!"}), 200

        return jsonify({"error": "User role not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(user_role_id):

    try:

        if valid_uuid(user_role_id) :
            _user_role = UserRole\
                .query\
                .filter(UserRole.uid == user_role_id)\
                .first()
            if _user_role:
                _user_role.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "User role activated successfully!"}), 200

        return jsonify({"error": "User role not activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

