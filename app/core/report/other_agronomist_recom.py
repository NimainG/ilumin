import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import OtherReportRecommendation

from app.core import user

from app.core import customer
from app.core import report



from app.core.report import other

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def other_ar_obj(other_ar=False, real_id=False):
    _other_ar = {}
    if other_ar:
        if real_id:
            _other_ar["real_id"] = other_ar.id

        _other_ar["id"] = str(other_ar.uid)
        _other_ar["recomend"] = other_ar.recomend
        _other_ar["other_id"] = other.fetch_by_id(other_ar.other_id).get_json()
        _other_ar["report_id"] = report.fetch_by_id(other_ar.report_id).get_json()
        _other_ar["customer_id"] = customer.fetch_by_id(other_ar.customer_id).get_json()
        _other_ar["created_by"] = creator_detail(other_ar.creator_id)
        _other_ar["status"] = status_name(other_ar.status)
        _other_ar["created"] = other_ar.created_at.strftime('%d %B %Y')
        _other_ar["modified"] = other_ar.modified_at

    return _other_ar

def fetch_all():
    response = []

    other_ars = db\
        .session\
        .query(OtherReportRecommendation)\
        .filter(OtherReportRecommendation.status > config.STATUS_DELETED)\
        .all()

    for other_ar in other_ars:
        response.append(other_ar_obj(other_ar))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        other_ar = db\
            .session\
            .query(OtherReportRecommendation)\
            .filter(OtherReportRecommendation.uid == uid)\
            .filter(OtherReportRecommendation.status > config.STATUS_DELETED)\
            .first()
        if other_ar:
            response = other_ar_obj(other_ar, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        other_ar = db\
            .session\
            .query(OtherReportRecommendation)\
            .filter(OtherReportRecommendation.id == id)\
            .filter(OtherReportRecommendation.status > config.STATUS_DELETED)\
            .first()
        if other_ar:
            response = other_ar_obj(other_ar)

    return jsonify(response)

def fetch_by_scouting(scouting_id=0):
    response = {}
    if scouting_id:
        other_ars = db\
            .session\
            .query(OtherReportRecommendation)\
            .filter(OtherReportRecommendation.scouting_id == scouting_id)\
            .filter(OtherReportRecommendation.status > config.STATUS_DELETED)\
            .first()
        if other_ars:
            response = other_ar_obj(other_ars)

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        other_ars = db\
            .session\
            .query(OtherReportRecommendation)\
            .filter(OtherReportRecommendation.report_id == report_id)\
            .filter(OtherReportRecommendation.status > config.STATUS_DELETED)\
            .all()
        for other_ar in other_ars:
            response.append(other_ar_obj(other_ar))

    return jsonify(response)

def fetch_by_report_single(report_id=0):
    response = {}
    if report_id:
        other_ars = db\
            .session\
            .query(OtherReportRecommendation)\
            .filter(OtherReportRecommendation.report_id == report_id)\
            .filter(OtherReportRecommendation.status > config.STATUS_DELETED)\
            .first()
        if other_ars:
            response = other_ar_obj(other_ars)
       

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        other_ars = db\
            .session\
            .query(OtherReportRecommendation)\
            .filter(OtherReportRecommendation.customer_id == customer_id)\
            .filter(OtherReportRecommendation.status > config.STATUS_DELETED)\
            .all()
        for other_ar in other_ars:
            response.append(other_ar_obj(other_ar))

    return jsonify(response)

def add_new(other_ar_data=None, return_obj=False):

    try:
        data = json.loads(other_ar_data)
        if data:

           

            _recomend = None
            if 'recomend' in data:
                _recomend = data['recomend']

         
            _other_id = None
            if valid_uuid(data['other_id']):
                _other_id = other.fetch_one(data['other_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_other_ar = OtherReportRecommendation(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    recomend = _recomend,
                    other_id = _other_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_other_ar)
                db.session.commit()
                if new_other_ar:
                    if return_obj:
                        return jsonify(fetch_one(new_other_ar.uid)), 200

                    return jsonify({"info": "Others agronomist recommendation report added successfully!"}), 200

        return jsonify({"error": "Others agronomist recommendation report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(other_ar_id, other_ar_data=None, return_obj=False):

    try:
        data = json.loads(other_ar_data)
        if data:

            if valid_uuid(other_ar_id):

               

                _recomend = None
                if 'recomend' in data:
                    _recomend = data['recomend']

               
                _other_ar = OtherReportRecommendation\
                    .query\
                    .filter(OtherReportRecommendation.uid == str(other_ar_id))\
                    .first()
                if _other_ar:
                    
                    _other_ar.recomend = _recomend
                   
                    db.session.commit()

                    return jsonify({"info": "Others agronomist recommendation report edited successfully!"}), 200

        return jsonify({"error": "Others agronomist recommendation report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(other_ar_id):

    try:

        if valid_uuid(other_ar_id) :
            _other_ar = OtherReportRecommendation\
                .query\
                .filter(OtherReportRecommendation.uid == other_ar_id)\
                .first()
            if _other_ar:
                _other_ar.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Others agronomist recommendation report deactivated successfully!"}), 200

        return jsonify({"error": "Others agronomist recommendation report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def delete(other_ar_id):

    try:

        if valid_uuid(other_ar_id) :
            _other_ar = OtherReportRecommendation\
                .query\
                .filter(OtherReportRecommendation.uid == other_ar_id)\
                .first()
            if _other_ar:
                _other_ar.status = config.STATUS_DELETED
                db.session.commit()

                return jsonify({"info": "Others agronomist recommendation report deleted successfully!"}), 200

        return jsonify({"error": "Others agronomist recommendation report not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
