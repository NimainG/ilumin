import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import WastePollutionAgronomistRecommendation

from app.core import user

from app.core import customer
from app.core import report



from app.core.report import waste_pollution_management

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def wnp_ar_obj(wnp_ar=False, real_id=False):
    _wnp_ar = {}
    if wnp_ar:
        if real_id:
            _wnp_ar["real_id"] = wnp_ar.id

        _wnp_ar["id"] = str(wnp_ar.uid)
        _wnp_ar["wnprec"] = wnp_ar.wnprec
        _wnp_ar["wnp_id"] = waste_pollution_management.fetch_by_id(wnp_ar.wnp_id).get_json()
        _wnp_ar["report_id"] = report.fetch_by_id(wnp_ar.report_id).get_json()
        _wnp_ar["customer_id"] = customer.fetch_by_id(wnp_ar.customer_id).get_json()
        _wnp_ar["created_by"] = creator_detail(wnp_ar.creator_id)
        _wnp_ar["status"] = status_name(wnp_ar.status)
        _wnp_ar["created"] = wnp_ar.created_at.strftime('%d %B %Y')
        _wnp_ar["modified"] = wnp_ar.modified_at

    return _wnp_ar

def fetch_all():
    response = []

    wnp_ars = db\
        .session\
        .query(WastePollutionAgronomistRecommendation)\
        .filter(WastePollutionAgronomistRecommendation.status > config.STATUS_DELETED)\
        .all()

    for wnp_ar in wnp_ars:
        response.append(wnp_ar_obj(wnp_ar))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        wnp_ar = db\
            .session\
            .query(WastePollutionAgronomistRecommendation)\
            .filter(WastePollutionAgronomistRecommendation.uid == uid)\
            .filter(WastePollutionAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if wnp_ar:
            response = wnp_ar_obj(wnp_ar, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        wnp_ar = db\
            .session\
            .query(WastePollutionAgronomistRecommendation)\
            .filter(WastePollutionAgronomistRecommendation.id == id)\
            .filter(WastePollutionAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if wnp_ar:
            response = wnp_ar_obj(wnp_ar)

    return jsonify(response)

def fetch_by_wnp(wnp_id=0):
    response = {}
    if wnp_id:
        wnp_ars = db\
            .session\
            .query(WastePollutionAgronomistRecommendation)\
            .filter(WastePollutionAgronomistRecommendation.wnp_id == wnp_id)\
            .filter(WastePollutionAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if wnp_ars:
            response = wnp_ar_obj(wnp_ars)

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        wnp_ars = db\
            .session\
            .query(WastePollutionAgronomistRecommendation)\
            .filter(WastePollutionAgronomistRecommendation.report_id == report_id)\
            .filter(WastePollutionAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for wnp_ar in wnp_ars:
            response.append(wnp_ar_obj(wnp_ar))

    return jsonify(response)

def fetch_by_report_single(report_id=0):
    response = {}
    if report_id:
        wnp_ars = db\
            .session\
            .query(WastePollutionAgronomistRecommendation)\
            .filter(WastePollutionAgronomistRecommendation.report_id == report_id)\
            .filter(WastePollutionAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()

        if wnp_ars:
            response = wnp_ar_obj(wnp_ars)
        

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        wnp_ars = db\
            .session\
            .query(WastePollutionAgronomistRecommendation)\
            .filter(WastePollutionAgronomistRecommendation.customer_id == customer_id)\
            .filter(WastePollutionAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for wnp_ar in wnp_ars:
            response.append(wnp_ar_obj(wnp_ar))

    return jsonify(response)

def add_new(wnp_ar_data=None, return_obj=False):

    try:
        data = json.loads(wnp_ar_data)
        if data:

           

            _wnprec = None
            if 'wnprec' in data:
                _wnprec = data['wnprec']

         
            _wnp_id = None
            if valid_uuid(data['wnp_id']):
                _wnp_id = waste_pollution_management.fetch_one(data['wnp_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_wnp_ar = WastePollutionAgronomistRecommendation(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    wnprec = _wnprec,
                    wnp_id = _wnp_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_wnp_ar)
                db.session.commit()
                if new_wnp_ar:
                    if return_obj:
                        return jsonify(fetch_one(new_wnp_ar.uid)), 200

                    return jsonify({"info": "Waste and pollution agronomist recommendation report added successfully!"}), 200

        return jsonify({"error": "Waste and pollution agronomist recommendation report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(wnp_ar_id, wnp_ar_data=None, return_obj=False):

    try:
        data = json.loads(wnp_ar_data)
        if data:

            if valid_uuid(wnp_ar_id):

               

                _wnprec = None
                if 'wnprec' in data:
                    _wnprec = data['wnprec']

               
                _wnp_ar = WastePollutionAgronomistRecommendation\
                    .query\
                    .filter(WastePollutionAgronomistRecommendation.uid == str(wnp_ar_id))\
                    .first()
                if _wnp_ar:
                    
                    _wnp_ar.wnprec = _wnprec
                   
                    db.session.commit()

                    return jsonify({"info": "Waste and pollution agronomist recommendation report edited successfully!"}), 200

        return jsonify({"error": "Waste and pollution agronomist recommendation report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(wnp_ar_id):

    try:

        if valid_uuid(wnp_ar_id) :
            _wnp_ar = WastePollutionAgronomistRecommendation\
                .query\
                .filter(WastePollutionAgronomistRecommendation.uid == wnp_ar_id)\
                .first()
            if _wnp_ar:
                _wnp_ar.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Waste and pollution agronomist recommendation report deactivated successfully!"}), 200

        return jsonify({"error": "Waste and pollution agronomist recommendation report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def delete(wnp_ar_id):

    try:

        if valid_uuid(wnp_ar_id) :
            _wnp_ar = WastePollutionAgronomistRecommendation\
                .query\
                .filter(WastePollutionAgronomistRecommendation.uid == wnp_ar_id)\
                .first()
            if _wnp_ar:
                _wnp_ar.status = config.STATUS_DELETED
                db.session.commit()

                return jsonify({"info": "Waste and pollution agronomist recommendation report deleted successfully!"}), 200

        return jsonify({"error": "Waste and pollution agronomist recommendation report not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
