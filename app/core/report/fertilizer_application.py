import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import FertilizerApplication

from app.core import user
from app.core import media
from app.core import customer
from app.core import report
from app.core import supply
from app.core.supply import application_methods
from app.core.supply import components
from app.core.customer import shields

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def fa_obj(fa=False, real_id=False):
    _fa = {}
    if fa:
        if real_id:
            _fa["real_id"] = fa.id

        _fa["id"] = str(fa.uid)
        _fa["date_time_applied"] = fa.date_time_applied
        _fa["quantity_applied"] = fa.quantity_applied
        _fa["sources"] = fa.sources
        _fa["supply_application_method"] = application_methods.fetch_by_id(fa.supply_application_method_id).get_json()
        _fa["fertilizer"] = supply.fetch_by_id(fa.fertilizer_id).get_json()
        _fa["fertilizer_main_component"] = components.fetch_by_id(fa.fertilizer_main_component).get_json()
        _fa["customer_shield_id"] = shields.fetch_by_id(fa.customer_shield_id).get_json()
        _fa["report_id"] = report.fetch_by_id(fa.report_id).get_json()
        _fa["customer_id"] = customer.fetch_by_id(fa.customer_id).get_json()
        _fa["created_by"] = creator_detail(fa.creator_id)
        _fa["status"] = status_name(fa.status)
        _fa["created"] = fa.created_at.strftime('%d %B %Y')
        _fa["modified"] = fa.modified_at

    return _fa

def fetch_all():
    response = []

    fas = db\
        .session\
        .query(FertilizerApplication)\
        .filter(FertilizerApplication.status > config.STATUS_DELETED)\
        .order_by(FertilizerApplication.id.desc())\
        .all()

    for fa in fas:
        response.append(fa_obj(fa))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        fa = db\
            .session\
            .query(FertilizerApplication)\
            .filter(FertilizerApplication.uid == uid)\
            .filter(FertilizerApplication.status > config.STATUS_DELETED)\
            .first()
        if fa:
            response = fa_obj(fa, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        fa = db\
            .session\
            .query(FertilizerApplication)\
            .filter(FertilizerApplication.id == id)\
            .filter(FertilizerApplication.status > config.STATUS_DELETED)\
            .first()
        if fa:
            response = fa_obj(fa)

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=0):
    response = []
    if customer_shield_id:
        fas = db\
            .session\
            .query(FertilizerApplication)\
            .filter(FertilizerApplication.customer_shield_id == customer_shield_id)\
            .filter(FertilizerApplication.status > config.STATUS_DELETED)\
            .order_by(FertilizerApplication.id.desc())\
            .all()
        for fa in fas:
            response.append(fa_obj(fa))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        fas = db\
            .session\
            .query(FertilizerApplication)\
            .filter(FertilizerApplication.report_id == report_id)\
            .filter(FertilizerApplication.status > config.STATUS_DELETED)\
            .order_by(FertilizerApplication.id.desc())\
            .all()
        for fa in fas:
            response.append(fa_obj(fa))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        fas = db\
            .session\
            .query(FertilizerApplication)\
            .filter(FertilizerApplication.customer_id == customer_id)\
            .filter(FertilizerApplication.status > config.STATUS_DELETED)\
            .order_by(FertilizerApplication.id.desc())\
            .all()
        for fa in fas:
            response.append(fa_obj(fa))

    return jsonify(response)
def fetch_by_customer_recent(customer_id=0):
    response = {}
    if customer_id:
        fas = db\
            .session\
            .query(FertilizerApplication)\
            .filter(FertilizerApplication.customer_id == customer_id)\
            .filter(FertilizerApplication.status > config.STATUS_DELETED)\
            .first()
        if fas:
            response = fa_obj(fas)

    return jsonify(response)

def fetch_by_customer_date_range(customer_id,start_date,end_date):

    response = []
    if customer_id and start_date and end_date:
        fas = db\
            .session\
            .query(FertilizerApplication)\
            .filter(FertilizerApplication.customer_id == customer_id)\
            .filter(FertilizerApplication.created_at >= start_date)\
            .filter(FertilizerApplication.created_at <= end_date)\
            .filter(FertilizerApplication.status > config.STATUS_DELETED)\
            .order_by(FertilizerApplication.id.desc())\
            .all()
        for fa in fas:
            response.append(fa_obj(fa))

    return jsonify(response)

def add_new(fa_data=None, return_obj=False):

    try:
        data = json.loads(fa_data)
        if data:

            _date_time_applied = None
            if 'date_time_applied' in data:
                _date_time_applied = data['date_time_applied']

            _quantity_applied = None
            if 'quantity_applied' in data:
                _quantity_applied = data['quantity_applied']
            _sources = None
            if 'sources' in data:
                _sources = data['sources']
        

            _supply_application_method_id = None
            if valid_uuid(data['supply_application_method_id']):
                _supply_application_method_id = application_methods.fetch_one(data['supply_application_method_id'], True).get_json()['real_id']

            _fertilizer_id = None
            if valid_uuid(data['fertilizer_id']):
                _fertilizer_id = supply.fetch_one(data['fertilizer_id'], True).get_json()['real_id']

            _fertilizer_main_component_id = None
            if valid_uuid(data['fertilizer_main_component']):
                _fertilizer_main_component_id = components.fetch_one(data['fertilizer_main_component'], True).get_json()['real_id']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            # _report_id = None
            # if valid_uuid(data['report_id']):
            #     _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _report_id = None
            if 'report_id' in data:
                _report_id = data['report_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if  _customer_id and _creator_id:
                new_fa = FertilizerApplication(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    sources = _sources,
                    date_time_applied = _date_time_applied,
                    quantity_applied = _quantity_applied,
                    supply_application_method_id = _supply_application_method_id,
                    fertilizer_id = _fertilizer_id,
                    fertilizer_main_component = _fertilizer_main_component_id,
                    customer_shield_id = _customer_shield_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_fa)
                db.session.commit()
                if new_fa:
                    if return_obj:
                        return jsonify(fetch_one(new_fa.uid)), 200

                    return jsonify({"info": "Fertilizer application added successfully!"}), 200

        return jsonify({"error": "Fertilizer application not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(fa_id, fa_data=None, return_obj=False):

    try:
        data = json.loads(fa_data)
        if data:

            if valid_uuid(fa_id):

                _date_time_applied = None
                if 'date_time_applied' in data:
                    _date_time_applied = data['date_time_applied']

                _quantity_applied = None
                if 'quantity_applied' in data:
                    _quantity_applied = data['quantity_applied']

                _supply_application_method_id = None
                if valid_uuid(data['supply_application_method_id']):
                    _supply_application_method_id = supply.application_methods.fetch_one(data['supply_application_method_id'], True).get_json()['real_id']

                _fertilizer_id = None
                if valid_uuid(data['fertilizer_id']):
                    _fertilizer_id = supply.fetch_one(data['fertilizer_id'], True).get_json()['real_id']

                _fertilizer_main_component_id = None
                if valid_uuid(data['fertilizer_main_component_id']):
                    _fertilizer_main_component_id = supply.components.fetch_one(data['fertilizer_main_component_id'], True).get_json()['real_id']

                if _fertilizer_id:
                    _fa = FertilizerApplication\
                        .query\
                        .filter(FertilizerApplication.uid == str(fa_id))\
                        .first()
                    if _fa:
                        _fa.date_time_applied = _date_time_applied
                        _fa.quantity_applied = _quantity_applied
                        _fa.supply_application_method_id = _supply_application_method_id
                        _fa.fertilizer_id = _fertilizer_id
                        _fa.fertilizer_main_component = _fertilizer_main_component_id
                        db.session.commit()

                        return jsonify({"info": "Fertilizer application edited successfully!"}), 200

        return jsonify({"error": "Fertilizer application not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(fa_id):

    try:

        if valid_uuid(fa_id) :
            _fa = FertilizerApplication\
                .query\
                .filter(FertilizerApplication.uid == fa_id)\
                .first()
            if _fa:
                _fa.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Fertilizer application deactivated successfully!"}), 200

        return jsonify({"error": "Fertilizer application not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406



def delete(fa_id):

    try:

        if valid_uuid(fa_id) :
            _greenhouse_type = FertilizerApplication\
                .query\
                .filter(FertilizerApplication.uid == fa_id)\
                .first()
            if _greenhouse_type:
                db.session.delete(_greenhouse_type)
                db.session.commit()

                return jsonify({"info": " Report is deleted successfully!"}), 200

        return jsonify({"error": " Report is not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406