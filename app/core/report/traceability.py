import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import TraceabilityReport

from app.core import user
from app.core import media
from app.core import customer
from app.core import report
from app.core import planting
from app.core.customer import farms
from app.core.customer import shields

from app.core.maintenance import traceability_report_requests

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def traceability_obj(traceability=False, real_id=False):
    _traceability = {}
    if traceability:
        if real_id:
            _traceability["real_id"] = traceability.id

        _traceability["id"] = str(traceability.uid)
        _traceability["traceability_report_request_id"] = traceability_report_requests.fetch_by_id(traceability.traceability_report_request_id).get_json()
        _traceability["report_id"] = report.fetch_by_id(traceability.report_id).get_json()
        _traceability["plant_id"] = planting.plants.fetch_by_id(traceability.plant_id).get_json()
        _traceability["customer_shield_id"] = shields.fetch_by_id(traceability.customer_shield_id).get_json()
        _traceability["customer_farm_id"] = farms.fetch_by_id(traceability.customer_farm_id).get_json()
        _traceability["customer_id"] = customer.fetch_by_id(traceability.customer_id).get_json()
        _traceability["created_by"] = creator_detail(traceability.creator_id)
        _traceability["status"] = status_name(traceability.status)
        _traceability["created"] = traceability.created_at.strftime('%d %B %Y')
        _traceability["modified"] = traceability.modified_at

    return _traceability

def fetch_all():
    response = []

    traceabilities = db\
        .session\
        .query(TraceabilityReport)\
        .filter(TraceabilityReport.status > config.STATUS_DELETED)\
        .order_by(TraceabilityReport.id.desc())\
        .all()

    for traceability in traceabilities:
        response.append(traceability_obj(traceability))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        traceability = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.uid == uid)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .first()
        if traceability:
            response = traceability_obj(traceability, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        traceability = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.id == id)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .first()
        if traceability:
            response = traceability_obj(traceability)

    return jsonify(response)

def fetch_by_traceability_report(traceability_report_id=0):
    response = []
    if traceability_report_id:
        traceabilities = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.traceability_report_id == traceability_report_id)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .order_by(TraceabilityReport.id.desc())\
            .all()
        for traceability in traceabilities:
            response.append(traceability_obj(traceability))

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=0):
    response = []
    if customer_shield_id:
        traceabilities = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.customer_shield_id == customer_shield_id)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .order_by(TraceabilityReport.id.desc())\
            .all()
        for traceability in traceabilities:
            response.append(traceability_obj(traceability))

    return jsonify(response)

def fetch_by_plant(plant_id=0):
    response = []
    if plant_id:
        traceabilities = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.plant_id == plant_id)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .order_by(TraceabilityReport.id.desc())\
            .all()
        for traceability in traceabilities:
            response.append(traceability_obj(traceability))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        traceabilities = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.report_id == report_id)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .order_by(TraceabilityReport.id.desc())\
            .all()
        for traceability in traceabilities:
            response.append(traceability_obj(traceability))

    return jsonify(response)

def fetch_by_report_id(report_id=0):
    response = {}
    if report_id:
        traceability = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.report_id == report_id)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .first()
        if traceability:
            response = traceability_obj(traceability)
      

    return jsonify(response)

def fetch_by_customer_farm(customer_farm_id=0):
    response = []
    if customer_farm_id:
        traceabilities = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.customer_farm_id == customer_farm_id)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .order_by(TraceabilityReport.id.desc())\
            .all()
        for traceability in traceabilities:
            response.append(traceability_obj(traceability))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        traceabilities = db\
            .session\
            .query(TraceabilityReport)\
            .filter(TraceabilityReport.customer_id == customer_id)\
            .filter(TraceabilityReport.status > config.STATUS_DELETED)\
            .order_by(TraceabilityReport.id.desc())\
            .all()
        for traceability in traceabilities:
            response.append(traceability_obj(traceability))

    return jsonify(response)

def add_new(traceability_data=None, return_obj=False):

    try:
        data = json.loads(traceability_data)
        if data:

            _traceability_report_request_id = None
            if valid_uuid(data['traceability_report_request_id']):
                _traceability_report_request_id = traceability_report_requests.fetch_one(data['traceability_report_request_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _plant_id = None
            if valid_uuid(data['plant_id']):
                _plant_id = planting.plants.fetch_one(data['plant_id'], True).get_json()['real_id']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _customer_farm_id = None
            if valid_uuid(data['customer_farm_id']):
                _customer_farm_id = farms.fetch_one(data['customer_farm_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if  _report_id and _customer_id and _creator_id:
                new_traceability = TraceabilityReport(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    traceability_report_request_id = _traceability_report_request_id,
                    report_id = _report_id,
                    plant_id = _plant_id,
                    customer_shield_id = _customer_shield_id,
                    customer_farm_id = _customer_farm_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_traceability)
                db.session.commit()
                if new_traceability:
                    if return_obj:
                        return jsonify(fetch_one(new_traceability.uid)), 200

                    return jsonify({"info": "Traceability report added successfully!"}), 200

        return jsonify({"error": "Traceability report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(traceability_id, traceability_data=None, return_obj=False):

    try:
        data = json.loads(traceability_data)
        if data:

            if valid_uuid(traceability_id):

                _traceability_report_request_id = None
                if valid_uuid(data['traceability_report_request_id']):
                    _traceability_report_request_id = traceability_report_requests.fetch_one(data['traceability_report_request_id'], True).get_json()['real_id']

                _traceability = TraceabilityReport\
                    .query\
                    .filter(TraceabilityReport.uid == str(traceability_id))\
                    .first()
                if _traceability:
                    _traceability.traceability_report_request_id = _traceability_report_request_id
                    db.session.commit()

                    return jsonify({"info": "Traceability report edited successfully!"}), 200

        return jsonify({"error": "Traceability report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(traceability_id):

    try:

        if valid_uuid(traceability_id) :
            _traceability = TraceabilityReport\
                .query\
                .filter(TraceabilityReport.uid == traceability_id)\
                .first()
            if _traceability:
                _traceability.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Traceability report deactivated successfully!"}), 200

        return jsonify({"error": "Traceability report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
