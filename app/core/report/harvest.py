import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Harvest

from app.core import user
from app.core import media
from app.core import customer
from app.core import report
from app.core import planting
from app.core.customer import shields
from app.core.planting import varieties

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def harvest_obj(harvest=False, real_id=False):
    _harvest = {}
    if harvest:
        if real_id:
            _harvest["real_id"] = harvest.id

        _harvest["id"] = str(harvest.uid)
        _harvest["harvest_date_time"] = harvest.harvest_date_time
        _harvest["kilos_harvested"] = harvest.kilos_harvested
        _harvest["kilos_rejected"] = harvest.kilos_rejected
        _harvest["kilos_lost_in_transit"] = harvest.kilos_lost_in_transit
        _harvest["kilos_sold"] = harvest.kilos_sold
        _harvest["kilo_price"] = harvest.kilo_price
        _harvest["total_income"] = harvest.total_income
        _harvest["variety"] = varieties.fetch_by_id(harvest.variety_id).get_json()
        _harvest["customer_shield_id"] = shields.fetch_by_id(harvest.customer_shield_id).get_json()
        _harvest["report_id"] = report.fetch_by_id(harvest.report_id).get_json()
        _harvest["customer_id"] = customer.fetch_by_id(harvest.customer_id).get_json()
        _harvest["created_by"] = creator_detail(harvest.creator_id)
        _harvest["status"] = status_name(harvest.status)
        _harvest["created"] = harvest.created_at.strftime('%d %B %Y')
        _harvest["modified"] = harvest.modified_at

    return _harvest

def fetch_all():
    response = []

    harvests = db\
        .session\
        .query(Harvest)\
        .filter(Harvest.status > config.STATUS_DELETED)\
        .order_by(Harvest.id.desc())\
        .all()

    for harvest in harvests:
        response.append(harvest_obj(harvest))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        harvest = db\
            .session\
            .query(Harvest)\
            .filter(Harvest.uid == uid)\
            .filter(Harvest.status > config.STATUS_DELETED)\
            .first()
        if harvest:
            response = harvest_obj(harvest, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        harvest = db\
            .session\
            .query(Harvest)\
            .filter(Harvest.id == id)\
            .filter(Harvest.status > config.STATUS_DELETED)\
            .first()
        if harvest:
            response = harvest_obj(harvest)

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=0):
    response = []
    if customer_shield_id:
        harvests = db\
            .session\
            .query(Harvest)\
            .filter(Harvest.customer_shield_id == customer_shield_id)\
            .filter(Harvest.status > config.STATUS_DELETED)\
            .order_by(Harvest.id.desc())\
            .all()
        for harvest in harvests:
            response.append(harvest_obj(harvest))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        harvests = db\
            .session\
            .query(Harvest)\
            .filter(Harvest.report_id == report_id)\
            .filter(Harvest.status > config.STATUS_DELETED)\
            .order_by(Harvest.id.desc())\
            .all()
        for harvest in harvests:
            response.append(harvest_obj(harvest))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        harvests = db\
            .session\
            .query(Harvest)\
            .filter(Harvest.customer_id == customer_id)\
            .filter(Harvest.status > config.STATUS_DELETED)\
            .order_by(Harvest.id.desc())\
            .all()
        for harvest in harvests:
            response.append(harvest_obj(harvest))

    return jsonify(response)

def fetch_by_customer_recent(customer_id=0):
    response = {}
    if customer_id:
        harvests = db\
            .session\
            .query(Harvest)\
            .filter(Harvest.customer_id == customer_id)\
            .filter(Harvest.status > config.STATUS_DELETED)\
            .first()
        if harvests:
            response = harvest_obj(harvests)
        

    return jsonify(response)

def fetch_by_customer_date_range(customer_id, start_date, end_date):

    response = []
    if customer_id and start_date and end_date:
        fas = db\
            .session\
            .query(Harvest)\
            .filter(Harvest.customer_id == customer_id)\
            .filter(Harvest.created_at <= end_date)\
            .filter(Harvest.created_at >= start_date)\
            .filter(Harvest.status > config.STATUS_DELETED)\
            .order_by(Harvest.id.desc())\
            .all()
        for fa in fas:
            response.append(harvest_obj(fa))

    return jsonify(response)

def add_new(harvest_data=None, return_obj=False):

    try:
        data = json.loads(harvest_data)
        if data:

            _harvest_date_time = None
            if 'harvest_date_time' in data:
                _harvest_date_time = data['harvest_date_time']

            _kilos_harvested = None
            if 'kilos_harvested' in data:
                _kilos_harvested = data['kilos_harvested']

            _kilos_rejected = None
            if 'kilos_rejected' in data:
                _kilos_rejected = data['kilos_rejected']

            _kilos_lost_in_transit = None
            if 'kilos_lost_in_transit' in data:
                _kilos_lost_in_transit = data['kilos_lost_in_transit']

            _kilos_sold = None
            if 'kilos_sold' in data:
                _kilos_sold = data['kilos_sold']

            _kilo_price = None
            if 'kilo_price' in data:
                _kilo_price = data['kilo_price']

            _total_income = None
            if 'total_income' in data:
                _total_income = data['total_income']

            _variety_id = None
            if valid_uuid(data['variety_id']):
                _variety_id = varieties.fetch_one(data['variety_id'], True).get_json()['real_id']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _report_id = None
            if 'report_id' in data:
                _report_id = data['report_id']
            # _report_id = None
            # if valid_uuid(data['report_id']):
            #     _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_harvest = Harvest(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    harvest_date_time = _harvest_date_time,
                    kilos_harvested = _kilos_harvested,
                    kilos_rejected = _kilos_rejected,
                    kilos_lost_in_transit = _kilos_lost_in_transit,
                    kilos_sold = _kilos_sold,
                    kilo_price = _kilo_price,
                    total_income = _total_income,
                    variety_id = _variety_id,
                    customer_shield_id = _customer_shield_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_harvest)
                db.session.commit()
                if new_harvest:
                    if return_obj:
                        return jsonify(fetch_one(new_harvest.uid)), 200

                    return jsonify({"info": "Harvest added successfully!"}), 200

        return jsonify({"error": "Harvest not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(harvest_id, harvest_data=None, return_obj=False):

    try:
        data = json.loads(harvest_data)
        if data:

            if valid_uuid(harvest_id):

                _harvest_date_time = None
                if 'harvest_date_time' in data:
                    _harvest_date_time = data['harvest_date_time']

                _kilos_harvested = None
                if 'kilos_harvested' in data:
                    _kilos_harvested = data['kilos_harvested']

                _kilos_rejected = None
                if 'kilos_rejected' in data:
                    _kilos_rejected = data['kilos_rejected']

                _kilos_lost_in_transit = None
                if 'kilos_lost_in_transit' in data:
                    _kilos_lost_in_transit = data['kilos_lost_in_transit']

                _kilos_sold = None
                if 'kilos_sold' in data:
                    _kilos_sold = data['kilos_sold']

                _kilo_price = None
                if 'kilo_price' in data:
                    _kilo_price = data['kilo_price']

                _total_income = None
                if 'total_income' in data:
                    _total_income = data['total_income']

                _variety_id = None
                if valid_uuid(data['variety_id']):
                    _variety_id = planting.varieties.fetch_one(data['variety_id'], True).get_json()['real_id']

                if _variety_id:
                    _harvest = Harvest\
                        .query\
                        .filter(Harvest.uid == str(harvest_id))\
                        .first()
                    if _harvest:
                        _harvest.harvest_date_time = _harvest_date_time
                        _harvest.kilos_harvested = _kilos_harvested
                        _harvest.kilos_rejected = _kilos_rejected
                        _harvest.kilos_lost_in_transit = _kilos_lost_in_transit
                        _harvest.kilos_sold = _kilos_sold
                        _harvest.kilo_price = _kilo_price
                        _harvest.total_income = _total_income
                        _harvest.variety_id = _variety_id
                        db.session.commit()

                        return jsonify({"info": "Harvest edited successfully!"}), 200

        return jsonify({"error": "Harvest not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(harvest_id):

    try:

        if valid_uuid(harvest_id) :
            _harvest = Harvest\
                .query\
                .filter(Harvest.uid == harvest_id)\
                .first()
            if _harvest:
                _harvest.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Harvest deactivated successfully!"}), 200

        return jsonify({"error": "Harvest not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406




def delete(harvest_id):

    try:

        if valid_uuid(harvest_id) :
            _greenhouse_type = Harvest\
                .query\
                .filter(Harvest.uid == harvest_id)\
                .first()
            if _greenhouse_type:
                db.session.delete(_greenhouse_type)
                db.session.commit()

                return jsonify({"info": " Report is deleted successfully!"}), 200

        return jsonify({"error": " Report is not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

