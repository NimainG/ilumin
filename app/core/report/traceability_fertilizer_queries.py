import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import TraceabilityFertilizerQuery

from app.core import user

from app.core.report import traceability

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def traceability_fq_obj(traceability_fq=False, real_id=False):
    _traceability_fq = {}
    if traceability_fq:
        if real_id:
            _traceability_fq["real_id"] = traceability_fq.id

        _traceability_fq["id"] = str(traceability_fq.uid)
        _traceability_fq["name"] = traceability_fq.name
        _traceability_fq["component"] = traceability_fq.component
        _traceability_fq["application_date"] = traceability_fq.application_date
        _traceability_fq["traceability_report_id"] = traceability.fetch_by_id(traceability_fq.traceability_report_id).get_json()
        _traceability_fq["created_by"] = creator_detail(traceability_fq.creator_id)
        _traceability_fq["status"] = status_name(traceability_fq.status)
        _traceability_fq["created"] = traceability_fq.created_at.strftime('%d %B %Y')
        _traceability_fq["modified"] = traceability_fq.modified_at

    return _traceability_fq

def fetch_all():
    response = []

    traceabilities = db\
        .session\
        .query(TraceabilityFertilizerQuery)\
        .filter(TraceabilityFertilizerQuery.status > config.STATUS_DELETED)\
        .all()

    for traceability_fq in traceabilities:
        response.append(traceability_fq_obj(traceability_fq))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        traceability_fq = db\
            .session\
            .query(TraceabilityFertilizerQuery)\
            .filter(TraceabilityFertilizerQuery.uid == uid)\
            .filter(TraceabilityFertilizerQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_fq:
            response = traceability_fq_obj(traceability_fq, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        traceability_fq = db\
            .session\
            .query(TraceabilityFertilizerQuery)\
            .filter(TraceabilityFertilizerQuery.id == id)\
            .filter(TraceabilityFertilizerQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_fq:
            response = traceability_fq_obj(traceability_fq)

    return jsonify(response)

def fetch_by_traceability_report(traceability_report_id=0):
    response = []
    if traceability_report_id:
        traceabilities = db\
            .session\
            .query(TraceabilityFertilizerQuery)\
            .filter(TraceabilityFertilizerQuery.traceability_report_id == traceability_report_id)\
            .filter(TraceabilityFertilizerQuery.status > config.STATUS_DELETED)\
            .all()
        for traceability_fq in traceabilities:
            response.append(traceability_fq_obj(traceability_fq))

    return jsonify(response)

def add_new(traceability_fq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_fq_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _component = None
            if 'component' in data:
                _component = data['component']

            _application_date = None
            if 'application_date' in data:
                _application_date = data['application_date']

            _traceability_report_id = None
            if valid_uuid(data['traceability_report_id']):
                _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _traceability_report_id and _creator_id:
                new_traceability_fq = TraceabilityFertilizerQuery(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    component = _component,
                    application_date = _application_date,
                    traceability_report_id = _traceability_report_id,
                    creator_id = _creator_id
                )
                db.session.add(new_traceability_fq)
                db.session.commit()
                if new_traceability_fq:
                    if return_obj:
                        return jsonify(fetch_one(new_traceability_fq.uid)), 200

                    return jsonify({"info": "Traceability fertilizer queries report added successfully!"}), 200

        return jsonify({"error": "Traceability fertilizer queries report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(traceability_fq_id, traceability_fq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_fq_data)
        if data:

            if valid_uuid(traceability_fq_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _component = None
                if 'component' in data:
                    _component = data['component']

                _application_date = None
                if 'application_date' in data:
                    _application_date = data['application_date']

                _traceability_report_id = None
                if valid_uuid(data['traceability_report_id']):
                    _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

                if _traceability_report_id:
                    _traceability_fq = TraceabilityFertilizerQuery\
                        .query\
                        .filter(TraceabilityFertilizerQuery.uid == str(traceability_fq_id))\
                        .first()
                    if _traceability_fq:
                        _traceability_fq.name = _name
                        _traceability_fq.component = _component
                        _traceability_fq.application_date = _application_date
                        _traceability_fq.traceability_report_id = _traceability_report_id
                        db.session.commit()

                        return jsonify({"info": "Traceability fertilizer queries report edited successfully!"}), 200

        return jsonify({"error": "Traceability fertilizer queries report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(traceability_fq_id):

    try:

        if valid_uuid(traceability_fq_id) :
            _traceability_fq = TraceabilityFertilizerQuery\
                .query\
                .filter(TraceabilityFertilizerQuery.uid == traceability_fq_id)\
                .first()
            if _traceability_fq:
                _traceability_fq.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Traceability fertilizer queries report deactivated successfully!"}), 200

        return jsonify({"error": "Traceability fertilizer queries report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
