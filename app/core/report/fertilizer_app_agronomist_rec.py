import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import FertilizerAgronomistRecommendation

from app.core import user

from app.core import customer
from app.core import report



from app.core.report import fertilizer_application

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def fertapp_ar_obj(fertapp_ar=False, real_id=False):
    _fertapp_ar = {}
    if fertapp_ar:
        if real_id:
            _fertapp_ar["real_id"] = fertapp_ar.id

        _fertapp_ar["id"] = str(fertapp_ar.uid)
        _fertapp_ar["recomend"] = fertapp_ar.recomend
        _fertapp_ar["fertilize_id"] = fertilizer_application.fetch_by_id(fertapp_ar.fertilize_id).get_json()
        _fertapp_ar["report_id"] = report.fetch_by_id(fertapp_ar.report_id).get_json()
        _fertapp_ar["customer_id"] = customer.fetch_by_id(fertapp_ar.customer_id).get_json()
        _fertapp_ar["created_by"] = creator_detail(fertapp_ar.creator_id)
        _fertapp_ar["status"] = status_name(fertapp_ar.status)
        _fertapp_ar["created"] = fertapp_ar.created_at.strftime('%d %B %Y')
        _fertapp_ar["modified"] = fertapp_ar.modified_at

    return _fertapp_ar

def fetch_all():
    response = []

    fertapp_ars = db\
        .session\
        .query(FertilizerAgronomistRecommendation)\
        .filter(FertilizerAgronomistRecommendation.status > config.STATUS_DELETED)\
        .all()

    for fertapp_ar in fertapp_ars:
        response.append(fertapp_ar_obj(fertapp_ar))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        fertapp_ar = db\
            .session\
            .query(FertilizerAgronomistRecommendation)\
            .filter(FertilizerAgronomistRecommendation.uid == uid)\
            .filter(FertilizerAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if fertapp_ar:
            response = fertapp_ar_obj(fertapp_ar, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        fertapp_ar = db\
            .session\
            .query(FertilizerAgronomistRecommendation)\
            .filter(FertilizerAgronomistRecommendation.id == id)\
            .filter(FertilizerAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if fertapp_ar:
            response = fertapp_ar_obj(fertapp_ar)

    return jsonify(response)

def fetch_by_scouting(scouting_id=0):
    response = {}
    if scouting_id:
        fertapp_ars = db\
            .session\
            .query(FertilizerAgronomistRecommendation)\
            .filter(FertilizerAgronomistRecommendation.scouting_id == scouting_id)\
            .filter(FertilizerAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if fertapp_ars:
            response = fertapp_ar_obj(fertapp_ars)

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        fertapp_ars = db\
            .session\
            .query(FertilizerAgronomistRecommendation)\
            .filter(FertilizerAgronomistRecommendation.report_id == report_id)\
            .filter(FertilizerAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for fertapp_ar in fertapp_ars:
            response.append(fertapp_ar_obj(fertapp_ar))

    return jsonify(response)

def fetch_by_report_single(report_id=0):
    response = {}
    if report_id:
        fertapp_ars = db\
            .session\
            .query(FertilizerAgronomistRecommendation)\
            .filter(FertilizerAgronomistRecommendation.report_id == report_id)\
            .filter(FertilizerAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()

        if fertapp_ars:
            response = fertapp_ar_obj(fertapp_ars)

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        fertapp_ars = db\
            .session\
            .query(FertilizerAgronomistRecommendation)\
            .filter(FertilizerAgronomistRecommendation.customer_id == customer_id)\
            .filter(FertilizerAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for fertapp_ar in fertapp_ars:
            response.append(fertapp_ar_obj(fertapp_ar))

    return jsonify(response)

def add_new(fertapp_ar_data=None, return_obj=False):

    try:
        data = json.loads(fertapp_ar_data)
        if data:

           

            _recomend = None
            if 'recomend' in data:
                _recomend = data['recomend']

         
            _fertilize_id = None
            if valid_uuid(data['fertilize_id']):
                _fertilize_id = fertilizer_application.fetch_one(data['fertilize_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_fertapp_ar = FertilizerAgronomistRecommendation(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    recomend = _recomend,
                    fertilize_id = _fertilize_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_fertapp_ar)
                db.session.commit()
                if new_fertapp_ar:
                    if return_obj:
                        return jsonify(fetch_one(new_fertapp_ar.uid)), 200

                    return jsonify({"info": "Fertilization Application agronomist recommendation report added successfully!"}), 200

        return jsonify({"error": "Fertilization Application agronomist recommendation report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(fertapp_ar_id, fertapp_ar_data=None, return_obj=False):

    try:
        data = json.loads(fertapp_ar_data)
        if data:

            if valid_uuid(fertapp_ar_id):

               

                _recomend = None
                if 'recomend' in data:
                    _recomend = data['recomend']


                # _fertilize_id = None
                # if valid_uuid(data['fertilize_id']):
                #  _fertilize_id = fertilizer_application.fetch_one(data['fertilize_id'], True).get_json()['real_id']

                # _report_id = None
                # if valid_uuid(data['report_id']):
                #  _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

                # _customer_id = None
                # if valid_uuid(data['customer_id']):
                #  _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

                # _creator_id = None
                # if valid_uuid(data['creator_id']):
                #  _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']


               
                _fertapp_ar = FertilizerAgronomistRecommendation\
                    .query\
                    .filter(FertilizerAgronomistRecommendation.uid == str(fertapp_ar_id))\
                    .first()
                if _fertapp_ar:
                    
                    _fertapp_ar.recomend = _recomend
                    # _fertapp_ar.fertilize_id = _fertilize_id
                    # _fertapp_ar.report_id =   _report_id
                    # _fertapp_ar.customer_id = _customer_id
                    # _fertapp_ar.creator_id = _creator_id
                   

                   
                    db.session.commit()

                    return jsonify({"info": "Fertilization  agronomist recommendation report edited successfully!"}), 200

        return jsonify({"error": "Fertilization Application agronomist recommendation report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(fertapp_ar_id):

    try:

        if valid_uuid(fertapp_ar_id) :
            _fertapp_ar = FertilizerAgronomistRecommendation\
                .query\
                .filter(FertilizerAgronomistRecommendation.uid == fertapp_ar_id)\
                .first()
            if _fertapp_ar:
                _fertapp_ar.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Fertilization Application agronomist recommendation report deactivated successfully!"}), 200

        return jsonify({"error": "Fertilization Application agronomist recommendation report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def delete(fertapp_ar_id):

    try:

        if valid_uuid(fertapp_ar_id) :
            _fertapp_ar = FertilizerAgronomistRecommendation\
                .query\
                .filter(FertilizerAgronomistRecommendation.uid == fertapp_ar_id)\
                .first()
            if _fertapp_ar:
                _fertapp_ar.status = config.STATUS_DELETED
                db.session.commit()

                return jsonify({"info": "Fertilization Application agronomist recommendation report deleted successfully!"}), 200

        return jsonify({"error": "Fertilization Application agronomist recommendation report not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
