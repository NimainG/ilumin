import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import SaleType

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def sale_type_obj(sale_type=False, real_id=False):
    _sale_type = {}
    if sale_type:
        if real_id:
            _sale_type["real_id"] = sale_type.id

        _sale_type["id"] = str(sale_type.uid)
        _sale_type["name"] = sale_type.name
        _sale_type["description"] = sale_type.description
        _sale_type["created_by"] = creator_detail(sale_type.creator_id)
        _sale_type["status"] = status_name(sale_type.status)
        _sale_type["created"] = sale_type.created_at
        _sale_type["modified"] = sale_type.modified_at

    return _sale_type

def fetch_all():
    response = []

    sale_types = db\
        .session\
        .query(SaleType)\
        .filter(SaleType.status > config.STATUS_DELETED)\
        .all()

    for sale_type in sale_types:
        response.append(sale_type_obj(sale_type))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        sale_type = db\
            .session\
            .query(SaleType)\
            .filter(SaleType.uid == uid)\
            .filter(SaleType.status > config.STATUS_DELETED)\
            .first()
        if sale_type:
            response = sale_type_obj(sale_type, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        sale_type = db\
            .session\
            .query(SaleType)\
            .filter(SaleType.id == id)\
            .filter(SaleType.status > config.STATUS_DELETED)\
            .first()
        if sale_type:
            response = sale_type_obj(sale_type)

    return jsonify(response)

def add_new(sale_type_data=None, return_obj=False):

    try:
        data = json.loads(sale_type_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _description = None
            if 'description' in data:
                _description = data['description']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _creator_id:
                new_sale_type = SaleType(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    description = _description,
                    creator_id = _creator_id
                )
                db.session.add(new_sale_type)
                db.session.commit()
                if new_sale_type:
                    if return_obj:
                        return jsonify(fetch_one(new_sale_type.uid)), 200

                    return jsonify({"info": "Sale type added successfully!"}), 200

        return jsonify({"error": "Sale type not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(sale_type_id, sale_type_data=None, return_obj=False):

    try:
        data = json.loads(sale_type_data)
        if data:

            if valid_uuid(sale_type_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _description = None
                if 'description' in data:
                    _description = data['description']

                if _name:
                    _sale_type = SaleType\
                        .query\
                        .filter(SaleType.uid == str(sale_type_id))\
                        .first()
                    if _sale_type:
                        _sale_type.name = _name
                        _sale_type.description = _description
                        db.session.commit()

                        return jsonify({"info": "Sale type edited successfully!"}), 200

        return jsonify({"error": "Sale type not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(sale_type_id):

    try:
        if valid_uuid(sale_type_id) :
            _sale_type = SaleType\
                .query\
                .filter(SaleType.uid == sale_type_id)\
                .first()
            if _sale_type:
                _sale_type.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Sale type deactivated successfully!"}), 200

        return jsonify({"error": "Sale type not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
