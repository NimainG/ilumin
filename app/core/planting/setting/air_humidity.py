import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import AirHumiditySetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def air_humidity_obj(air_humidity=False, real_id=False):
    _air_humidity = {}
    if air_humidity:
        if real_id:
            _air_humidity["real_id"] = air_humidity.id

        _air_humidity["id"] = str(air_humidity.uid)
        _air_humidity["min"] = air_humidity.min
        _air_humidity["max"] = air_humidity.max
        _air_humidity["shield_setting_id"] = setting.fetch_by_id(air_humidity.shield_planting_setting_id).get_json()['uid']
        _air_humidity["created_by"] = creator_detail(air_humidity.creator_id)
        _air_humidity["status"] = status_name(air_humidity.status)
        _air_humidity["created"] = air_humidity.created_at
        _air_humidity["modified"] = air_humidity.modified_at

    return _air_humidity

def fetch_all():
    response = []

    air_humiditys = db\
        .session\
        .query(AirHumiditySetting)\
        .filter(AirHumiditySetting.status > config.STATUS_DELETED)\
        .all()

    for air_humidity in air_humiditys:
        response.append(air_humidity_obj(air_humidity))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        air_humidity = db\
            .session\
            .query(AirHumiditySetting)\
            .filter(AirHumiditySetting.uid == uid)\
            .filter(AirHumiditySetting.status > config.STATUS_DELETED)\
            .first()
        if air_humidity:
            response = air_humidity_obj(air_humidity, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        air_humidity = db\
            .session\
            .query(AirHumiditySetting)\
            .filter(AirHumiditySetting.id == id)\
            .filter(AirHumiditySetting.status > config.STATUS_DELETED)\
            .first()
        if air_humidity:
            response = air_humidity_obj(air_humidity)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        air_humiditys = db\
            .session\
            .query(AirHumiditySetting)\
            .filter(AirHumiditySetting.shield_planting_setting_id == shield_setting_id)\
            .filter(AirHumiditySetting.status > config.STATUS_DELETED)\
            .all()
        for air_humidity in air_humiditys:
            response.append(air_humidity_obj(air_humidity))

    return jsonify(response)

def add_new(air_humidity_data=None, return_obj=False):

    try:
        data = json.loads(air_humidity_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_air_humidity = AirHumiditySetting(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_air_humidity)
                db.session.commit()
                if new_air_humidity:
                    if return_obj:
                        return jsonify(fetch_one(new_air_humidity.uid)), 200

                    return jsonify({"info": "Air humidity setting added successfully!"}), 200

        return jsonify({"error": "Air humidity setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(air_humidity_id, air_humidity_data=None, return_obj=False):

    try:
        data = json.loads(air_humidity_data)
        if data:
            if valid_uuid(air_humidity_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _air_humidity = AirHumiditySetting\
                    .query\
                    .filter(AirHumiditySetting.uid == air_humidity_id)\
                    .first()
                if _air_humidity:
                    _air_humidity.min = _min
                    _air_humidity.max = _max
                    db.session.commit()

                    return jsonify({"info": "Air humidity setting edited successfully!"}), 200

        return jsonify({"error": "Air humidity setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(air_humidity_id):

    try:

        if valid_uuid(air_humidity_id) :
            _air_humidity = AirHumiditySetting\
                .query\
                .filter(AirHumiditySetting.uid == air_humidity_id)\
                .first()
            if _air_humidity:
                _air_humidity.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Air humidity setting deactivated successfully!"}), 200

        return jsonify({"error": "Air humidity setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
