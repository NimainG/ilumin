import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import SoilTemperatureSetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def soil_temp_obj(soil_temp=False, real_id=False):
    _soil_temp = {}
    if soil_temp:
        if real_id:
            _soil_temp["real_id"] = soil_temp.id

        _soil_temp["id"] = str(soil_temp.uid)
        _soil_temp["min"] = soil_temp.min
        _soil_temp["max"] = soil_temp.max
        _soil_temp["shield_setting_id"] = setting.fetch_by_id(soil_temp.shield_planting_setting_id).get_json()['uid']
        _soil_temp["created_by"] = creator_detail(soil_temp.creator_id)
        _soil_temp["status"] = status_name(soil_temp.status)
        _soil_temp["created"] = soil_temp.created_at
        _soil_temp["modified"] = soil_temp.modified_at

    return _soil_temp

def fetch_all():
    response = []

    soil_temps = db\
        .session\
        .query(SoilTemperatureSetting)\
        .filter(SoilTemperatureSetting.status > config.STATUS_DELETED)\
        .all()

    for soil_temp in soil_temps:
        response.append(soil_temp_obj(soil_temp))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        soil_temp = db\
            .session\
            .query(SoilTemperatureSetting)\
            .filter(SoilTemperatureSetting.uid == uid)\
            .filter(SoilTemperatureSetting.status > config.STATUS_DELETED)\
            .first()
        if soil_temp:
            response = soil_temp_obj(soil_temp, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        soil_temp = db\
            .session\
            .query(SoilTemperatureSetting)\
            .filter(SoilTemperatureSetting.id == id)\
            .filter(SoilTemperatureSetting.status > config.STATUS_DELETED)\
            .first()
        if soil_temp:
            response = soil_temp_obj(soil_temp)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        soil_temps = db\
            .session\
            .query(SoilTemperatureSetting)\
            .filter(SoilTemperatureSetting.shield_planting_setting_id == shield_setting_id)\
            .filter(SoilTemperatureSetting.status > config.STATUS_DELETED)\
            .all()
        for soil_temp in soil_temps:
            response.append(soil_temp_obj(soil_temp))

    return jsonify(response)

def add_new(soil_temp_data=None, return_obj=False):

    try:
        data = json.loads(soil_temp_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_soil_temp = SoilTemperatureSetting(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_soil_temp)
                db.session.commit()
                if new_soil_temp:
                    if return_obj:
                        return jsonify(fetch_one(new_soil_temp.uid)), 200

                    return jsonify({"info": "Soil temperature setting added successfully!"}), 200

        return jsonify({"error": "Soil temperature setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(soil_temp_id, soil_temp_data=None, return_obj=False):

    try:
        data = json.loads(soil_temp_data)
        if data:
            if valid_uuid(soil_temp_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _soil_temp = SoilTemperatureSetting\
                    .query\
                    .filter(SoilTemperatureSetting.uid == soil_temp_id)\
                    .first()
                if _soil_temp:
                    _soil_temp.min = _min
                    _soil_temp.max = _max
                    db.session.commit()

                    return jsonify({"info": "Soil temperature setting edited successfully!"}), 200

        return jsonify({"error": "Soil temperature setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(soil_temp_id):

    try:

        if valid_uuid(soil_temp_id) :
            _soil_temp = SoilTemperatureSetting\
                .query\
                .filter(SoilTemperatureSetting.uid == soil_temp_id)\
                .first()
            if _soil_temp:
                _soil_temp.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Soil temperature setting deactivated successfully!"}), 200

        return jsonify({"error": "Soil temperature setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
