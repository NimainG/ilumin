import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import NitrogenSetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def nitrogen_obj(nitrogen=False, real_id=False):
    _nitrogen = {}
    if nitrogen:
        if real_id:
            _nitrogen["real_id"] = nitrogen.id

        _nitrogen["id"] = str(nitrogen.uid)
        _nitrogen["min"] = nitrogen.min
        _nitrogen["max"] = nitrogen.max
        _nitrogen["shield_setting_id"] = setting.fetch_by_id(nitrogen.shield_planting_setting_id).get_json()['uid']
        _nitrogen["created_by"] = creator_detail(nitrogen.creator_id)
        _nitrogen["status"] = status_name(nitrogen.status)
        _nitrogen["created"] = nitrogen.created_at
        _nitrogen["modified"] = nitrogen.modified_at

    return _nitrogen

def fetch_all():
    response = []

    nitrogens = db\
        .session\
        .query(NitrogenSetting)\
        .filter(NitrogenSetting.status > config.STATUS_DELETED)\
        .all()

    for nitrogen in nitrogens:
        response.append(nitrogen_obj(nitrogen))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        nitrogen = db\
            .session\
            .query(NitrogenSetting)\
            .filter(NitrogenSetting.uid == uid)\
            .filter(NitrogenSetting.status > config.STATUS_DELETED)\
            .first()
        if nitrogen:
            response = nitrogen_obj(nitrogen, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        nitrogen = db\
            .session\
            .query(NitrogenSetting)\
            .filter(NitrogenSetting.id == id)\
            .filter(NitrogenSetting.status > config.STATUS_DELETED)\
            .first()
        if nitrogen:
            response = nitrogen_obj(nitrogen)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        nitrogens = db\
            .session\
            .query(NitrogenSetting)\
            .filter(NitrogenSetting.shield_planting_setting_id == shield_setting_id)\
            .filter(NitrogenSetting.status > config.STATUS_DELETED)\
            .all()
        for nitrogen in nitrogens:
            response.append(nitrogen_obj(nitrogen))

    return jsonify(response)

def add_new(nitrogen_data=None, return_obj=False):

    try:
        data = json.loads(nitrogen_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_nitrogen = NitrogenSetting(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_nitrogen)
                db.session.commit()
                if new_nitrogen:
                    if return_obj:
                        return jsonify(fetch_one(new_nitrogen.uid)), 200

                    return jsonify({"info": "Nitrogen setting added successfully!"}), 200

        return jsonify({"error": "Nitrogen setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(nitrogen_id, nitrogen_data=None, return_obj=False):

    try:
        data = json.loads(nitrogen_data)
        if data:
            if valid_uuid(nitrogen_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _nitrogen = NitrogenSetting\
                    .query\
                    .filter(NitrogenSetting.uid == nitrogen_id)\
                    .first()
                if _nitrogen:
                    _nitrogen.min = _min
                    _nitrogen.max = _max
                    db.session.commit()

                    return jsonify({"info": "Nitrogen setting edited successfully!"}), 200

        return jsonify({"error": "Nitrogen setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(nitrogen_id):

    try:

        if valid_uuid(nitrogen_id) :
            _nitrogen = NitrogenSetting\
                .query\
                .filter(NitrogenSetting.uid == nitrogen_id)\
                .first()
            if _nitrogen:
                _nitrogen.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Nitrogen setting deactivated successfully!"}), 200

        return jsonify({"error": "Nitrogen setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
