import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import SuggestedPlants
from datetime import datetime
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def plant_obj(plant=False, real_id=False):
    _plant = {}
    if plant:
        if real_id:
            _plant["real_id"] = plant.id

        _plant["id"] = str(plant.uid)
        _plant["plant_name"] = plant.plant_name
       
        _plant["created_by"] = creator_detail(plant.creator_id)
        _plant["status"] = status_name(plant.status)
        _plant["created"] = plant.created_at.strftime('%d %B %Y')
        _plant["modified"] =plant.modified_at

    return _plant

def fetch_all():
    response = []

    plants = db\
        .session\
        .query(SuggestedPlants)\
        .filter(SuggestedPlants.status > config.STATUS_DELETED)\
        .all()

    for plant in plants:
        response.append(plant_obj(plant))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        plant = db\
            .session\
            .query(SuggestedPlants)\
            .filter(SuggestedPlants.uid == uid)\
            .filter(SuggestedPlants.status > config.STATUS_DELETED)\
            .first()
        if plant:
            response = plant_obj(plant, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        plant = db\
            .session\
            .query(SuggestedPlants)\
            .filter(SuggestedPlants.id == id)\
            .filter(SuggestedPlants.status > config.STATUS_DELETED)\
            .first()
        if plant:
            response = plant_obj(plant)

    return jsonify(response)

def fetch_by_name(name):
    response = {}
    if name:
        plant = db\
            .session\
            .query(SuggestedPlants)\
            .filter(SuggestedPlants.plant_name == name)\
            .filter(SuggestedPlants.status > config.STATUS_DELETED)\
            .first()
        if plant:
            response = plant_obj(plant)

    return jsonify(response)

def add_new(plant_data=None, return_obj=False):

    try:
        data = json.loads(plant_data)
        if data:

            if 'name' in data and 'creator_id' in data:

                _plant_name = data['plant_name']
                _variety_name = data['variety_name']
                _planting_period = data['planting_period']
                _production_period = data['production_period']
                _harvest_period = data['harvest_period']

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _plant_name:
                    new_plant = SuggestedPlants(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        plant_name = _plant_name,
                        variety_name = _variety_name,
                        planting_period = _planting_period,
                        production_period = _production_period,
                        harvest_period = _harvest_period,
                        creator_id = _creator
                    )
                    db.session.add(new_plant)
                    db.session.commit()
                    if new_plant:
                        if return_obj:
                            return jsonify(fetch_one(new_plant.uid)), 200

                        return jsonify({"info": "Plant suggest added successfully!"}), 200

        return jsonify({"error": "Plant suggest not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(plant_id, plant_data=None, return_obj=False):

    try:
        data = json.loads(plant_data)
        if data:
            if valid_uuid(plant_id) and 'plant_name' in data:

                _name = data['plant_name']

                if _name:
                    _plant = SuggestedPlants\
                        .query\
                        .filter(SuggestedPlants.uid == plant_id)\
                        .first()
                    if _plant:
                        _plant.name = _name
                        db.session.commit()

                        return jsonify({"info": "Plant suggest edited successfully!"}), 200

        return jsonify({"error": "Plant suggest not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(plant_id):

    try:

        if valid_uuid(plant_id) :
            _plant = SuggestedPlants\
                .query\
                .filter(SuggestedPlants.uid == plant_id)\
                .first()
            if _plant:
                _plant.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Plant deactivated successfully!"}), 200

        return jsonify({"error": "Plant not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
