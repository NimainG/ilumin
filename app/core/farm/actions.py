import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import FarmActions
from app.core import farm as farms


from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def farm_action_obj(farm_action=False, real_id=False):
    _farm_action = {}
    if farm_action:
        if real_id:
            _farm_action["real_id"] = farm_action.id

        _farm_action["id"] = str(farm_action.uid)
        _farm_action["customer_id"] = farm_action.customer_id
        _farm_action["current_firm_uid"]=farm_action.current_firm_uid
        # _farm_action["description"] = farm_action.description
        _farm_action["created_by"] = creator_detail(farm_action.creator_id)
        _farm_action["status"] = status_name(farm_action.status)
        _farm_action["created"] = farm_action.created_at
        _farm_action["modified"] = farm_action.modified_at

    return _farm_action

def fetch_all():
    response = []

    farm_actions = db\
        .session\
        .query(FarmActions)\
        .filter(FarmActions.status > config.STATUS_DELETED)\
        .all()

    for farm_action in farm_actions:
        response.append(farm_action_obj(farm_action))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        farm_action = db\
            .session\
            .query(FarmActions)\
            .filter(FarmActions.uid == uid)\
            .filter(FarmActions.status > config.STATUS_DELETED)\
            .first()
        if farm_action:
            response = farm_action_obj(farm_action, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        farm_action = db\
            .session\
            .query(FarmActions)\
            .filter(FarmActions.id == id)\
            .filter(FarmActions.status > config.STATUS_DELETED)\
            .first()
        if farm_action:
            response = farm_action_obj(farm_action)

    return jsonify(response)


def fetch_by_customer(customer_id=0):
    response = {}
    if customer_id:
        customer_farm = db\
            .session\
            .query(FarmActions)\
            .filter(FarmActions.customer_id == customer_id)\
            .first()
        if customer_farm:
            response = farm_action_obj(customer_farm)
        

    return jsonify(response)

def add_new(farm_action_data=None, return_obj=False):

    try:
        data = json.loads(farm_action_data)
        if data:

            customer_id = None
            if 'customer_id' in data:
                _customer_id = data['customer_id']

            _current_firm_uid = None
            if 'current_firm_uid' in data:
                _current_firm_uid = data['current_firm_uid']
            # _description = None
            # if 'description' in data:
            #     _description = data['description']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if  _current_firm_uid and _creator_id:
                new_farm_action = FarmActions(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    customer_id = _customer_id,
                    current_firm_uid = _current_firm_uid,
                    # description = _description,
                    creator_id = _creator_id
                )
                db.session.add(new_farm_action)
                db.session.commit()
                if new_farm_action:
                    if return_obj:
                        return jsonify(fetch_one(new_farm_action.uid)), 200

                    return jsonify({"info": "Farm actions added successfully!"}), 200

        return jsonify({"error": "Farm actions not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(farm_action_id, farm_action_data=None, return_obj=False):

    try:
        data = json.loads(farm_action_data)
        if data:

            if valid_uuid(farm_action_id):

                _customer_id = None
                if 'customer_id' in data:
                    _customer_id = data['customer_id']
                _current_firm_uid = None
                if valid_uuid(data['current_firm_uid']):
                 _current_firm_uid = farms.fetch_one(data['current_firm_uid'], True).get_json()['real_id']
               
                # if 'current_firm_uid' in data:
                #     _current_firm_uid = data['current_firm_uid']
              
                # _description = None
                # if 'description' in data:
                #     _description = data['description']

                if _customer_id:
                    _farm_action = FarmActions\
                        .query\
                        .filter(FarmActions.uid == str(farm_action_id))\
                        .first()
                    if _farm_action:
                        _farm_action.customer_id = _customer_id
                        _farm_action.current_firm_uid =  _current_firm_uid
                        # _farm_action.description = _description
                        db.session.commit()

                        return jsonify({"info": "Farm actions edited successfully!"}), 200

        return jsonify({"error": "Farm actions not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(farm_action_id):

    try:

        if valid_uuid(farm_action_id) :
            _farm_action = FarmActions\
                .query\
                .filter(FarmActions.uid == farm_action_id)\
                .first()
            if _farm_action:
                _farm_action.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Farm Actions deactivated successfully!"}), 200

        return jsonify({"error": "Farm Actions not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
