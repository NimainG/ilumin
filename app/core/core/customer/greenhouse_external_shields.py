import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerExternalShield

from app.core import customer
from app.core import user
from app.core.misc import soils as soil_types

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouse_external_shield_obj(greenhouse_external_shield=False, real_id=False):
    _greenhouse_external_shield = {}
    if greenhouse_external_shield:
        if real_id:
            _greenhouse_external_shield["real_id"] = greenhouse_external_shield.id

        _greenhouse_external_shield["id"] = str(greenhouse_external_shield.uid)
        _greenhouse_external_shield["latitude"] = greenhouse_external_shield.latitude
        _greenhouse_external_shield["longitude"] = greenhouse_external_shield.longitude
        _greenhouse_external_shield["growth_area"] = greenhouse_external_shield.growth_area
        _greenhouse_external_shield["farm_id"] = customer.farms.fetch_by_id(greenhouse_external_shield.farm_id).get_json()['uid']
        _greenhouse_external_shield["soil_type"] = soil_types.fetch_by_id(greenhouse_external_shield.soil_type_id).get_json()
        _greenhouse_external_shield["customer_shield"] = customer.shields.fetch_by_id(greenhouse_external_shield.customer_shield_id).get_json()['uid']
        _greenhouse_external_shield["created_by"] = creator_detail(greenhouse_external_shield.creator_id)
        _greenhouse_external_shield["status"] = status_name(greenhouse_external_shield.status)
        _greenhouse_external_shield["created"] = greenhouse_external_shield.created_at
        _greenhouse_external_shield["modified"] = greenhouse_external_shield.modified_at

    return _greenhouse_external_shield

def fetch_all():
    response = []

    greenhouse_external_shields = db\
        .session\
        .query(CustomerExternalShield)\
        .filter(CustomerExternalShield.status > config.STATUS_DELETED)\
        .all()

    for greenhouse_external_shield in greenhouse_external_shields:
        response.append(greenhouse_external_shield_obj(greenhouse_external_shield))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        greenhouse_external_shield = db\
            .session\
            .query(CustomerExternalShield)\
            .filter(CustomerExternalShield.uid == uid)\
            .filter(CustomerExternalShield.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_external_shield:
            response = greenhouse_external_shield_obj(greenhouse_external_shield, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        greenhouse_external_shield = db\
            .session\
            .query(CustomerExternalShield)\
            .filter(CustomerExternalShield.id == id)\
            .filter(CustomerExternalShield.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_external_shield:
            response = greenhouse_external_shield_obj(greenhouse_external_shield)

    return jsonify(response)

def fetch_by_customer_farm(customer_farm_id=None):
    response = []
    if customer_farm_id:
        greenhouse_external_shields = db\
            .session\
            .query(CustomerExternalShield)\
            .filter(CustomerExternalShield.customer_farm_id == customer_farm_id)\
            .filter(CustomerExternalShield.status > config.STATUS_DELETED)\
            .all()
        for greenhouse_external_shield in greenhouse_external_shields:
            response = greenhouse_external_shield_obj(greenhouse_external_shield)

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=None):
    response = []
    if customer_shield_id:
        greenhouse_external_shields = db\
            .session\
            .query(CustomerExternalShield)\
            .filter(CustomerExternalShield.customer_shield_id == customer_shield_id)\
            .filter(CustomerExternalShield.status > config.STATUS_DELETED)\
            .all()
        for greenhouse_external_shield in greenhouse_external_shields:
            response = greenhouse_external_shield_obj(greenhouse_external_shield)

    return jsonify(response)

def add_new(greenhouse_external_shield_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_external_shield_data)
        if data:

            _latitude = None
            if 'latitude' in data:
                _latitude = data['latitude']

            _longitude = None
            if 'longitude' in data:
                _longitude = data['longitude']

            _growth_area = None
            if 'growth_area' in data:
                _growth_area = data['growth_area']

            _soil_type_id = None
            if valid_uuid(data['soil_type_id']):
                _soil_type_id = soil_types.fetch_one(data['soil_type_id'], True).get_json()['real_id']

            _customer_farm_greenhouse_id = None
            if valid_uuid(data['customer_farm_greenhouse_id']):
                _customer_farm_greenhouse_id = customer.farm_greenhouses.fetch_one(data['customer_farm_greenhouse_id'], True).get_json()['real_id']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_farm_greenhouse_id and _customer_shield_id and _creator_id:
                new_greenhouse_external_shield = CustomerExternalShield(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    latitude = _latitude,
                    longitude = _longitude,
                    growth_area = _growth_area,
                    soil_type_id = _soil_type_id,
                    # customer_farm_greenhouse_id = _customer_farm_greenhouse_id,
                    customer_shield_id = _customer_shield_id,
                    creator_id = _creator_id
                )
                db.session.add(new_greenhouse_external_shield)
                db.session.commit()
                if new_greenhouse_external_shield:
                    if return_obj:
                        return jsonify(fetch_one(new_greenhouse_external_shield.uid)), 200

                    return jsonify({"info": "Customer greenhouse shield added successfully!"}), 200

        return jsonify({"error": "Customer greenhouse shield not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(greenhouse_external_shield_id, greenhouse_external_shield_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_external_shield_data)
        if data:

            if valid_uuid(greenhouse_external_shield_id):

                _latitude = None
                if 'latitude' in data:
                    _latitude = data['latitude']

                _longitude = None
                if 'longitude' in data:
                    _longitude = data['longitude']

                _growth_area = None
                if 'growth_area' in data:
                    _growth_area = data['growth_area']

                _soil_type_id = None
                if valid_uuid(data['soil_type_id']):
                    _soil_type_id = soil_types.fetch_one(data['soil_type_id'], True).get_json()['real_id']

                _greenhouse_external_shield = CustomerExternalShield\
                    .query\
                    .filter(CustomerExternalShield.uid == str(greenhouse_external_shield_id))\
                    .first()
                if _greenhouse_external_shield:
                    _greenhouse_external_shield.latitude = _latitude
                    _greenhouse_external_shield.longitude = _longitude
                    _greenhouse_external_shield.growth_area = _growth_area
                    _greenhouse_external_shield.growth_area = _growth_area
                    _greenhouse_external_shield.soil_type_id = _soil_type_id
                    db.session.commit()

                    return jsonify({"info": "Customer greenhouse shield edited successfully!"}), 200

        return jsonify({"error": "Customer greenhouse shield not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(greenhouse_external_shield_id):

    try:
        if valid_uuid(greenhouse_external_shield_id) :
            _greenhouse_external_shield = CustomerExternalShield\
                .query\
                .filter(CustomerExternalShield.uid == greenhouse_external_shield_id)\
                .first()
            if _greenhouse_external_shield:
                _greenhouse_external_shield.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer greenhouse shield deactivated successfully!"}), 200

        return jsonify({"error": "Customer greenhouse shield not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
