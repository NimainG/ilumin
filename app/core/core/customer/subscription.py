# customer_subscription_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerSubscription

from app.core import customer
from app.core import subscription
from app.core import invoice
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def customer_subscription_obj(customer_subscription=False, real_id=False):
    _customer_subscription = {}
    if customer_subscription:
        if real_id:
            _customer_subscription["real_id"] = customer_subscription.id

        _customer_subscription["id"] = str(customer_subscription.uid)
        _customer_subscription["starts"] = customer_subscription.starts
        _customer_subscription["ends"] = customer_subscription.ends
        _customer_subscription["is_active"] = customer_subscription.is_active
        _customer_subscription["invoice"] = invoice.fetch_by_id(customer_subscription.invoice_id).get_json()
        _customer_subscription["sale_type"] = subscription.sale_types.fetch_by_id(customer_subscription.sale_type_id).get_json()
        _customer_subscription["subscription"] = subscription.fetch_by_id(customer_subscription.subscription_id).get_json()
        _customer_subscription["customer"] = customer.fetch_by_id(customer_subscription.customer_id).get_json()['uid']
        _customer_subscription["created_by"] = creator_detail(customer_subscription.creator_id)
        _customer_subscription["status"] = status_name(customer_subscription.status)
        _customer_subscription["created"] = customer_subscription.created_at
        _customer_subscription["modified"] = customer_subscription.modified_at

    return _customer_subscription

def fetch_all():
    response = []

    customer_subscriptions = db\
        .session\
        .query(CustomerSubscription)\
        .filter(CustomerSubscription.status > config.STATUS_DELETED)\
        .all()

    for customer_subscription in customer_subscriptions:
        response.append(customer_subscription_obj(customer_subscription))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer_subscription = db\
            .session\
            .query(CustomerSubscription)\
            .filter(CustomerSubscription.uid == uid)\
            .filter(CustomerSubscription.status > config.STATUS_DELETED)\
            .first()
        if customer_subscription:
            response = customer_subscription_obj(customer_subscription, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        customer_subscription = db\
            .session\
            .query(CustomerSubscription)\
            .filter(CustomerSubscription.id == id)\
            .filter(CustomerSubscription.status > config.STATUS_DELETED)\
            .first()
        if customer_subscription:
            response = customer_subscription_obj(customer_subscription)

    return jsonify(response)

def fetch_by_invoice(invoice_id):
    response = []
    if invoice_id:
        customer_subscriptions = db\
            .session\
            .query(CustomerSubscription)\
            .filter(CustomerSubscription.invoice_id == invoice_id)\
            .filter(CustomerSubscription.status > config.STATUS_DELETED)\
            .all()
        for customer_subscription in customer_subscriptions:
            response.append(customer_subscription_obj(customer_subscription))

    return jsonify(response)

def fetch_by_sale_type(sale_type_id):
    response = []
    if sale_type_id:
        customer_sale_types = db\
            .session\
            .query(CustomerSubscription)\
            .filter(CustomerSubscription.sale_type_id == sale_type_id)\
            .filter(CustomerSubscription.status > config.STATUS_DELETED)\
            .all()
        for customer_subscription in customer_sale_types:
            response.append(customer_subscription_obj(customer_subscription))

    return jsonify(response)

def fetch_by_subscription(subscription_id):
    response = []
    if subscription_id:
        customer_subscriptions = db\
            .session\
            .query(CustomerSubscription)\
            .filter(CustomerSubscription.subscription_id == subscription_id)\
            .filter(CustomerSubscription.status > config.STATUS_DELETED)\
            .all()
        for customer_subscription in customer_subscriptions:
            response.append(customer_subscription_obj(customer_subscription))

    return jsonify(response)

def fetch_by_customer(customer_id):
    response = []
    if customer_id:
        customer_subscriptions = db\
            .session\
            .query(CustomerSubscription)\
            .filter(CustomerSubscription.customer_id == customer_id)\
            .filter(CustomerSubscription.status > config.STATUS_DELETED)\
            .all()
        for customer_subscription in customer_subscriptions:
            response.append(customer_subscription_obj(customer_subscription))

    return jsonify(response)

def add_new(customer_subscription_data=None, return_obj=False):

    try:
        data = json.loads(customer_subscription_data)
        if data:

            _starts = None
            if 'starts' in data:
                _starts = data['starts']

            _ends = None
            if 'ends' in data:
                _ends = data['ends']

            _is_active = None
            if 'is_active' in data:
                _is_active = data['is_active']

            _invoice_id = None
            if valid_uuid(data['invoice_id']):
                _invoice_id = invoice.fetch_one(data['invoice_id'], True).get_json()['real_id']

            _sale_type_id = None
            if valid_uuid(data['sale_type_id']):
                _sale_type_id = subscription.sale_types.fetch_one(data['sale_type_id'], True).get_json()['real_id']

            _subscription_id = None
            if valid_uuid(data['subscription_id']):
                _subscription_id = subscription.fetch_one(data['subscription_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _subscription_id and _customer_id and _creator_id:
                new_customer_subscription = CustomerSubscription(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    starts = _starts,
                    ends = _ends,
                    is_active = _is_active,
                    invoice_id = _invoice_id,
                    sale_type_id = _sale_type_id,
                    subscription_id = _subscription_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_customer_subscription)
                db.session.commit()
                if new_customer_subscription:
                    if return_obj:
                        return jsonify(fetch_one(new_customer_subscription.uid)), 200

                    return jsonify({"info": "Customer subscription added successfully!"}), 200

        return jsonify({"error": "Customer subscription not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_subscription_id):

    try:
        if valid_uuid(customer_subscription_id) :
            _customer_subscription = CustomerSubscription\
                .query\
                .filter(CustomerSubscription.uid == customer_subscription_id)\
                .first()
            if _customer_subscription:
                _customer_subscription.is_active = 0
                _customer_subscription.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer subscription deactivated successfully!"}), 200

        return jsonify({"error": "Customer subscription not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
