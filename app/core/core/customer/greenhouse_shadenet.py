import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerGreenhouseShadenet

from app.core import customer
from app.core import user
from app.core.misc import soils as soil_types

from app.core.customer import shadenets
from app.core.customer import farms

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouse_shadenet_obj(greenhouse_shadenet=False, real_id=False):
    _greenhouse_shadenet = {}
    if greenhouse_shadenet:
        if real_id:
            _greenhouse_shadenet["real_id"] = greenhouse_shadenet.id

        _greenhouse_shadenet["customer_farm_greenhouse"] = farms.fetch_by_id(greenhouse_shadenet.customer_farm_greenhouse_id).get_json()
        _greenhouse_shadenet["customer_shadenet"] = shadenets.fetch_by_id(greenhouse_shadenet.customer_shadenet_id).get_json()
        _greenhouse_shadenet["created_by"] = creator_detail(greenhouse_shadenet.creator_id)
        _greenhouse_shadenet["status"] = status_name(greenhouse_shadenet.status)
        _greenhouse_shadenet["created"] = greenhouse_shadenet.created_at
        _greenhouse_shadenet["modified"] = greenhouse_shadenet.modified_at

    return _greenhouse_shadenet

def fetch_all():
    response = []

    greenhouse_shadenets = db\
        .session\
        .query(CustomerGreenhouseShadenet)\
        .filter(CustomerGreenhouseShadenet.status > config.STATUS_DELETED)\
        .all()

    for greenhouse_shadenet in greenhouse_shadenets:
        response.append(greenhouse_shadenet_obj(greenhouse_shadenet))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        greenhouse_shadenet = db\
            .session\
            .query(CustomerGreenhouseShadenet)\
            .filter(CustomerGreenhouseShadenet.uid == uid)\
            .filter(CustomerGreenhouseShadenet.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_shadenet:
            response = greenhouse_shadenet_obj(greenhouse_shadenet, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        greenhouse_shadenet = db\
            .session\
            .query(CustomerGreenhouseShadenet)\
            .filter(CustomerGreenhouseShadenet.id == id)\
            .filter(CustomerGreenhouseShadenet.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_shadenet:
            response = greenhouse_shadenet_obj(greenhouse_shadenet)

    return jsonify(response)

def fetch_by_customer_farm_greenhouses(customer_farm_greenhouse_id=None):
    response = []
    if customer_farm_greenhouse_id:
        greenhouse_shadenets = db\
            .session\
            .query(CustomerGreenhouseShadenet)\
            .filter(CustomerGreenhouseShadenet.customer_farm_greenhouse_id == customer_farm_greenhouse_id)\
            .filter(CustomerGreenhouseShadenet.status > config.STATUS_DELETED)\
            .all()
        for greenhouse_shadenet in greenhouse_shadenets:
            response = greenhouse_shadenet_obj(greenhouse_shadenet)

    return jsonify(response)

def fetch_by_customer_farm_greenhouse(customer_farm_greenhouse_id=None):
    response = {}
    if customer_farm_greenhouse_id:
        greenhouse_shadenets = db\
            .session\
            .query(CustomerGreenhouseShadenet)\
            .filter(CustomerGreenhouseShadenet.customer_farm_greenhouse_id == customer_farm_greenhouse_id)\
            .filter(CustomerGreenhouseShadenet.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_shadenets:
            response = greenhouse_shadenet_obj(greenhouse_shadenets)

    return jsonify(response)

def fetch_by_customer_shadenet(customer_shadenet_id=None):
    response = {}
    if customer_shadenet_id:
        greenhouse_shadenets = db\
            .session\
            .query(CustomerGreenhouseShadenet)\
            .filter(CustomerGreenhouseShadenet.customer_shadenet_id == customer_shadenet_id)\
            .filter(CustomerGreenhouseShadenet.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_shadenets:
            response = greenhouse_shadenet_obj(greenhouse_shadenets)


    return jsonify(response)

def add_new(greenhouse_shadenet_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_shadenet_data)
        if data:

            # _customer_farm_greenhouse_id = None
            # if 'customer_farm_greenhouse_id' in data:
            #     _customer_farm_greenhouse_id = data['customer_farm_greenhouse_id']

            _customer_farm_greenhouse_id = None
            if valid_uuid(data['customer_farm_greenhouse_id']):
                _customer_farm_greenhouse_id = farms.fetch_one(data['customer_farm_greenhouse_id'], True).get_json()['real_id']

            _customer_shadenet_id = None
            if valid_uuid(data['customer_shadenet_id']):
                _customer_shadenet_id = shadenets.fetch_one(data['customer_shadenet_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_farm_greenhouse_id and _creator_id:
                new_greenhouse_shadenet = CustomerGreenhouseShadenet(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    customer_farm_greenhouse_id = _customer_farm_greenhouse_id,
                    customer_shadenet_id = _customer_shadenet_id,
                    creator_id = _creator_id
                )
                db.session.add(new_greenhouse_shadenet)
                db.session.commit()
                if new_greenhouse_shadenet:
                    if return_obj:
                        return jsonify(fetch_one(new_greenhouse_shadenet.uid)), 200

                    return jsonify({"info": "Customer greenhouse shadenet added successfully!"}), 200

        return jsonify({"error": "Customer greenhouse shadenet not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(greenhouse_shadenet_id):

    try:
        if valid_uuid(greenhouse_shadenet_id) :
            _greenhouse_shadenet = CustomerGreenhouseShadenet\
                .query\
                .filter(CustomerGreenhouseShadenet.uid == greenhouse_shadenet_id)\
                .first()
            if _greenhouse_shadenet:
                _greenhouse_shadenet.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer greenhouse shadenet deactivated successfully!"}), 200

        return jsonify({"error": "Customer greenhouse shadenet not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
