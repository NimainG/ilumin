import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import ModulePermission

from app.core import user
from app.core import module

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def module_permission_obj(module_permission=False, real_id=False):
    _module_permission = {}
    if module_permission:
        if real_id:
            _module_permission["real_id"] = module_permission.id

        _module_permission["id"] = str(module_permission.uid)
        _module_permission["role"] = user.roles.fetch_by_id(module_permission.role_id).get_json()
        _module_permission["feature"] = module.features.fetch_by_id(module_permission.module_feature_id).get_json()
        _module_permission["created_by"] = creator_detail(module_permission.creator_id)
        _module_permission["status"] = status_name(module_permission.status)
        _module_permission["created"] = module_permission.created_at
        _module_permission["modified"] = module_permission.modified_at

    return _module_permission

def fetch_all():
    response = []

    module_permissions = db\
        .session\
        .query(ModulePermission)\
        .filter(ModulePermission.status == config.STATUS_ACTIVE)\
        .all()

    for module_permission in module_permissions:
        response.append(module_permission_obj(module_permission))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        module_permission = db\
            .session\
            .query(ModulePermission)\
            .filter(ModulePermission.uid == uid)\
            .filter(ModulePermission.status == config.STATUS_ACTIVE)\
            .first()
        if module_permission:
            response = module_permission_obj(module_permission, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        module_permission = db\
            .session\
            .query(ModulePermission)\
            .filter(ModulePermission.id == id)\
            .filter(ModulePermission.status == config.STATUS_ACTIVE)\
            .first()
        if module_permission:
            response = module_permission_obj(module_permission)

    return jsonify(response)

def fetch_by_feature(feature_id=0):
    response = {}
    if feature_id:
        module_permission = db\
            .session\
            .query(ModulePermission)\
            .filter(ModulePermission.module_feature_id == feature_id)\
            .filter(ModulePermission.status == config.STATUS_ACTIVE)\
            .first()
        if module_permission:
            response = module_permission_obj(module_permission)

    return jsonify(response)

def fetch_by_role_feature(role_id=None, feature_id=0, real_id=False):
    response = {}
    if role_id and feature_id:
        module_permission = db\
            .session\
            .query(ModulePermission)\
            .filter(ModulePermission.role_id == role_id)\
            .filter(ModulePermission.module_feature_id == feature_id)\
            .filter(ModulePermission.status == config.STATUS_ACTIVE)\
            .first()
        if module_permission:
            response = module_permission_obj(module_permission, real_id)

    return jsonify(response)

def add_new(module_permission_data=None, return_obj=False):

    try:
        data = json.loads(module_permission_data)
        if data:

            if 'role_id' in data and 'module_feature_id' in data and 'creator_id' in data:

                _role_id = None
                if valid_uuid(data['role_id']):
                    _role_id = user.roles.fetch_one(data['role_id'], True).get_json()['real_id']

                _module_feature_id = None
                if valid_uuid(data['module_feature_id']):
                    _module_feature_id = module.features.fetch_one(data['module_feature_id'], True).get_json()['real_id']

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _role_id and _module_feature_id and _creator:
                    new_module_permission = ModulePermission(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        role_id = _role_id,
                        module_feature_id = _module_feature_id,
                        creator_id = _creator
                    )
                    db.session.add(new_module_permission)
                    db.session.commit()
                    if new_module_permission:
                        if return_obj:
                            return fetch_one(new_module_permission.uid), 200

                        return jsonify({"info": "Module permission added successfully!"}), 200

        return jsonify({"error": "Module permission not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(module_permission_id, module_permission_data=None, return_obj=False):

    try:
        data = json.loads(module_permission_data)
        if data:

            if valid_uuid(module_permission_id) and 'role_id' in data and 'module_feature_id' in data:

                _role_id = None
                if valid_uuid(data['role_id']):
                    _role_id = user.roles.fetch_one(data['role_id'], True).get_json()['real_id']

                _module_feature_id = None
                if valid_uuid(data['module_feature_id']):
                    _module_feature_id = module.features.fetch_one(data['module_feature_id'], True).get_json()['real_id']

                if _name and _code and _module_id:
                    _module_permission = ModulePermission\
                        .query\
                        .filter(ModulePermission.uid == module_permission_id)\
                        .first()
                    if _module_permission:
                        _module_permission.role_id = _role_id
                        _module_permission.module_feature_id = _module_feature_id
                        db.session.commit()

                        return jsonify({"info": "Module permission edited successfully!"}), 200

        return jsonify({"error": "Module permission not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(module_permission_id):

    try:

        if valid_uuid(module_permission_id) :
            _module_permission = ModulePermission\
                .query\
                .filter(ModulePermission.uid == module_permission_id)\
                .first()
            if _module_permission:
                _module_permission.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Module permission deactivated successfully!"}), 200

        return jsonify({"error": "Module permission not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
