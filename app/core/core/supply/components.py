import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import SupplyComponents

from app.core import user
from app.core import supply

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def component_obj(component=False, real_id=False):
    _component = {}
    if component:
        if real_id:
            _component["real_id"] = component.id

        _component["id"] = str(component.uid)
        _component["component"] = component.component
        _component["supply"] = supply.fetch_by_id(component.supply_id).get_json()
        _component["created_by"] = creator_detail(component.creator_id)
        _component["status"] = status_name(component.status)
        _component["commodity"] = component.commodity
        _component["created"] = component.created_at
        _component["modified"] = component.modified_at

    return _component

def fetch_all():
    response = []

    components = db\
        .session\
        .query(SupplyComponents)\
        .filter(SupplyComponents.status > config.STATUS_DELETED)\
        .all()

    for component in components:
        response.append(component_obj(component))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        component = db\
            .session\
            .query(SupplyComponents)\
            .filter(SupplyComponents.uid == uid)\
            .filter(SupplyComponents.status > config.STATUS_DELETED)\
            .first()
        if component:
            response = component_obj(component, real_id)

    return jsonify(response)

def fetch_by_component(component):
    response = {}

    
    component = db\
            .session\
            .query(SupplyComponents)\
            .filter(SupplyComponents.component == component)\
            .filter(SupplyComponents.status > config.STATUS_DELETED)\
            .first()
    if component:
            response = component_obj(component)

    return jsonify(response)


def fetch_by_id(id=0):
    response = {}
    if id:
        component = db\
            .session\
            .query(SupplyComponents)\
            .filter(SupplyComponents.id == id)\
            .filter(SupplyComponents.status > config.STATUS_DELETED)\
            .first()
        if component:
            response = component_obj(component)

    return jsonify(response)

def fetch_by_supply(supply_id=None):
    response = []

    components = db\
        .session\
        .query(SupplyComponents)\
        .filter(SupplyComponents.supply_id == supply_id)\
        .filter(SupplyComponents.status > config.STATUS_DELETED)\
        .all()

    for component in components:
        response.append(component_obj(component))

    return jsonify(response)

def add_new(component_data=None, return_obj=False):

    try:
        data = json.loads(component_data)
        if data:

            _component = None
            if 'component' in data:
                _component = data['component']

            _commodity = None
            if 'commodity' in data:
                _commodity = data['commodity']

            _supply_id = None
            if valid_uuid(data['supply_id']):
                _supply_id = supply.fetch_one(data['supply_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if  _creator_id:
                new_component = SupplyComponents(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    component = _component,
                    commodity = _commodity,
                    supply_id = _supply_id,
                    creator_id = _creator_id
                )
                db.session.add(new_component)
                db.session.commit()
                if new_component:
                    if return_obj:
                        return jsonify(fetch_one(new_component.uid)), 200

                    return jsonify({"info": "Supply component added successfully!"}), 200

        return jsonify({"error": "Supply component not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(component_id, component_data=None, return_obj=False):

    try:
        data = json.loads(component_data)
        if data:

            if valid_uuid(component_id):

                _component = None
                if 'component' in data:
                    _component = data['component']
                _commodity = None
                if 'commodity' in data:
                 _commodity = data['commodity']

                _supply_id = None
                if valid_uuid(data['supply_id']):
                    _supply_id = supply.fetch_one(data['supply_id'], True).get_json()['real_id']

                if _component and _supply_id:
                    _component = SupplyComponents\
                        .query\
                        .filter(SupplyComponents.uid == str(component_id))\
                        .first()
                    if _component:
                        _component.component = _component
                        _component.supply_id = _supply_id
                        _component.commodity = _commodity
                        db.session.commit()

                        return jsonify({"info": "Supply component edited successfully!"}), 200

        return jsonify({"error": "Supply component not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(component_id):

    try:

        if valid_uuid(component_id) :
            _component = SupplyComponents\
                .query\
                .filter(SupplyComponents.uid == component_id)\
                .first()
            if _component:
                _component.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Supply component deactivated successfully!"}), 200

        return jsonify({"error": "Supply component not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
