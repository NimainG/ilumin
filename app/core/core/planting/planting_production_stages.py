import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import PlantingProductionStage

from app.core import user
from app.core import planting
from app.core.planting import production_stages

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def planting_production_stage_obj(planting_production_stage=False, real_id=False):
    _planting_production_stage = {}
    if planting_production_stage:
        if real_id:
            _planting_production_stage["real_id"] = planting_production_stage.id

        _planting_production_stage["id"] = str(planting_production_stage.uid)
        _planting_production_stage["production_stage"] = planting.production_stages.fetch_by_id(planting_production_stage.production_stage_id).get_json()
        _planting_production_stage["created_by"] = creator_detail(planting_production_stage.creator_id)
        _planting_production_stage["status"] = status_name(planting_production_stage.status)
        _planting_production_stage["created"] = planting_production_stage.created_at.strftime('%d %B %Y')
        _planting_production_stage["modified"] = planting_production_stage.modified_at

    return _planting_production_stage

def fetch_all():
    response = []

    planting_production_stages = db\
        .session\
        .query(PlantingProductionStage)\
        .filter(PlantingProductionStage.status > config.STATUS_DELETED)\
        .all()

    for planting_production_stage in planting_production_stages:
        response.append(planting_production_stage_obj(planting_production_stage))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        planting_production_stage = db\
            .session\
            .query(PlantingProductionStage)\
            .filter(PlantingProductionStage.uid == uid)\
            .filter(PlantingProductionStage.status > config.STATUS_DELETED)\
            .first()
        if planting_production_stage:
            response = planting_production_stage_obj(planting_production_stage, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        planting_production_stage = db\
            .session\
            .query(PlantingProductionStage)\
            .filter(PlantingProductionStage.id == id)\
            .filter(PlantingProductionStage.status > config.STATUS_DELETED)\
            .first()
        if planting_production_stage:
            response = planting_production_stage_obj(planting_production_stage)

    return jsonify(response)

def fetch_by_planting_id(planting_id=0):
    response = {}
    if planting_id:
        planting_production_stage = db\
            .session\
            .query(PlantingProductionStage)\
            .filter(PlantingProductionStage.planting_id == planting_id)\
            .filter(PlantingProductionStage.status > config.STATUS_DELETED)\
            .first()
        if planting_production_stage:
            response = planting_production_stage_obj(planting_production_stage)

    return jsonify(response)

def add_new(planting_production_stage_data=None, return_obj=False):

    try:
        data = json.loads(planting_production_stage_data)
        if data:

            if  'production_stage_id' in data and 'creator_id' in data:

                _planting_id = None
                if valid_uuid(data['planting_id']):
                    _planting_id = planting.fetch_one(data['planting_id'], True).get_json()['real_id']

                _production_stage_id = None
                if valid_uuid(data['production_stage_id']):
                    _production_stage_id = planting.production_stages.fetch_one(data['production_stage_id'], True).get_json()['real_id']

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if  _production_stage_id and _creator:
                    new_planting_production_stage = PlantingProductionStage(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        planting_id = _planting_id,
                        production_stage_id = _production_stage_id,
                        creator_id = _creator
                    )
                    db.session.add(new_planting_production_stage)
                    db.session.commit()
                    if new_planting_production_stage:
                        if return_obj:
                            return jsonify(fetch_one(new_planting_production_stage.uid)), 200

                        return jsonify({"info": "Planting production stage added successfully!"}), 200

        return jsonify({"error": "Planting production stage not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(planting_production_stage_id, planting_production_stage_data=None, return_obj=False):

    try:
        data = json.loads(planting_production_stage_data)
        if data:
            if valid_uuid(planting_production_stage_id) and 'planting_id' in data and 'production_stage_id' in data:

                _planting_id = None
                if valid_uuid(data['planting_id']):
                    _planting_id = planting.fetch_one(data['planting_id'], True).get_json()['real_id']

                _production_stage_id = None
                if valid_uuid(data['production_stage_id']):
                    _production_stage_id = planting.production_stages.fetch_one(data['production_stage_id'], True).get_json()['real_id']

                if _planting_id and _production_stage_id :
                    _planting_production_stage = PlantingProductionStage\
                        .query\
                        .filter(PlantingProductionStage.uid == planting_production_stage_id)\
                        .first()
                    if _planting_production_stage:
                        _planting_production_stage.planting_id = _planting_id
                        _planting_production_stage.production_stage_id = _production_stage_id
                        db.session.commit()

                        return jsonify({"info": "Planting production stage edited successfully!"}), 200

        return jsonify({"error": "Planting production stage not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(planting_production_stage_id):

    try:

        if valid_uuid(planting_production_stage_id) :
            _planting_production_stage = PlantingProductionStage\
                .query\
                .filter(PlantingProductionStage.uid == planting_production_stage_id)\
                .first()
            if _planting_production_stage:
                _planting_production_stage.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Planting production stage deactivated successfully!"}), 200

        return jsonify({"error": "Planting production stage not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
