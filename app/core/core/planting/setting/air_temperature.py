import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import AirTemperatureSetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def air_temp_obj(air_temp=False, real_id=False):
    _air_temp = {}
    if air_temp:
        if real_id:
            _air_temp["real_id"] = air_temp.id

        _air_temp["id"] = str(air_temp.uid)
        _air_temp["min"] = air_temp.min
        _air_temp["max"] = air_temp.max
        _air_temp["shield_setting_id"] = setting.fetch_by_id(air_temp.shield_planting_setting_id).get_json()['uid']
        _air_temp["created_by"] = creator_detail(air_temp.creator_id)
        _air_temp["status"] = status_name(air_temp.status)
        _air_temp["created"] = air_temp.created_at
        _air_temp["modified"] = air_temp.modified_at

    return _air_temp

def fetch_all():
    response = []

    air_temps = db\
        .session\
        .query(AirTemperatureSetting)\
        .filter(AirTemperatureSetting.status > config.STATUS_DELETED)\
        .all()

    for air_temp in air_temps:
        response.append(air_temp_obj(air_temp))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        air_temp = db\
            .session\
            .query(AirTemperatureSetting)\
            .filter(AirTemperatureSetting.uid == uid)\
            .filter(AirTemperatureSetting.status > config.STATUS_DELETED)\
            .first()
        if air_temp:
            response = air_temp_obj(air_temp, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        air_temp = db\
            .session\
            .query(AirTemperatureSetting)\
            .filter(AirTemperatureSetting.id == id)\
            .filter(AirTemperatureSetting.status > config.STATUS_DELETED)\
            .first()
        if air_temp:
            response = air_temp_obj(air_temp)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        air_temps = db\
            .session\
            .query(AirTemperatureSetting)\
            .filter(AirTemperatureSetting.shield_planting_setting_id == shield_setting_id)\
            .filter(AirTemperatureSetting.status > config.STATUS_DELETED)\
            .all()
        for air_temp in air_temps:
            response.append(air_temp_obj(air_temp))

    return jsonify(response)

def add_new(air_temp_data=None, return_obj=False):

    try:
        data = json.loads(air_temp_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_air_temp = AirTemperatureSetting(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_air_temp)
                db.session.commit()
                if new_air_temp:
                    if return_obj:
                        return jsonify(fetch_one(new_air_temp.uid)), 200

                    return jsonify({"info": "Air temperature setting added successfully!"}), 200

        return jsonify({"error": "Air temperature setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(air_temp_id, air_temp_data=None, return_obj=False):

    try:
        data = json.loads(air_temp_data)
        if data:
            if valid_uuid(air_temp_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _air_temp = AirTemperatureSetting\
                    .query\
                    .filter(AirTemperatureSetting.uid == air_temp_id)\
                    .first()
                if _air_temp:
                    _air_temp.min = _min
                    _air_temp.max = _max
                    db.session.commit()

                    return jsonify({"info": "Air temperature setting edited successfully!"}), 200

        return jsonify({"error": "Air temperature setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(air_temp_id):

    try:

        if valid_uuid(air_temp_id) :
            _air_temp = AirTemperatureSetting\
                .query\
                .filter(AirTemperatureSetting.uid == air_temp_id)\
                .first()
            if _air_temp:
                _air_temp.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Air temperature setting deactivated successfully!"}), 200

        return jsonify({"error": "Air temperature setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
