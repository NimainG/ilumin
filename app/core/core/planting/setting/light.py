import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import LightSetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def light_obj(light=False, real_id=False):
    _light = {}
    if light:
        if real_id:
            _light["real_id"] = light.id

        _light["id"] = str(light.uid)
        _light["min"] = light.min
        _light["max"] = light.max
        _light["shield_setting_id"] = setting.fetch_by_id(light.shield_planting_setting_id).get_json()['uid']
        _light["created_by"] = creator_detail(light.creator_id)
        _light["status"] = status_name(light.status)
        _light["created"] = light.created_at
        _light["modified"] = light.modified_at

    return _light

def fetch_all():
    response = []

    lights = db\
        .session\
        .query(LightSetting)\
        .filter(LightSetting.status > config.STATUS_DELETED)\
        .all()

    for light in lights:
        response.append(light_obj(light))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        light = db\
            .session\
            .query(LightSetting)\
            .filter(LightSetting.uid == uid)\
            .filter(LightSetting.status > config.STATUS_DELETED)\
            .first()
        if light:
            response = light_obj(light, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        light = db\
            .session\
            .query(LightSetting)\
            .filter(LightSetting.id == id)\
            .filter(LightSetting.status > config.STATUS_DELETED)\
            .first()
        if light:
            response = light_obj(light)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        lights = db\
            .session\
            .query(LightSetting)\
            .filter(LightSetting.shield_planting_setting_id == shield_setting_id)\
            .filter(LightSetting.status > config.STATUS_DELETED)\
            .all()
        for light in lights:
            response.append(light_obj(light))

    return jsonify(response)

def add_new(light_data=None, return_obj=False):

    try:
        data = json.loads(light_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_light = LightSetting(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_light)
                db.session.commit()
                if new_light:
                    if return_obj:
                        return jsonify(fetch_one(new_light.uid)), 200

                    return jsonify({"info": "Light setting added successfully!"}), 200

        return jsonify({"error": "Light setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(light_id, light_data=None, return_obj=False):

    try:
        data = json.loads(light_data)
        if data:
            if valid_uuid(light_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _light = LightSetting\
                    .query\
                    .filter(LightSetting.uid == light_id)\
                    .first()
                if _light:
                    _light.min = _min
                    _light.max = _max
                    db.session.commit()

                    return jsonify({"info": "Light setting edited successfully!"}), 200

        return jsonify({"error": "Light setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(light_id):

    try:

        if valid_uuid(light_id) :
            _light = LightSetting\
                .query\
                .filter(LightSetting.uid == light_id)\
                .first()
            if _light:
                _light.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Light setting deactivated successfully!"}), 200

        return jsonify({"error": "Light setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
