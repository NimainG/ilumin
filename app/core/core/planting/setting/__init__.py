import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import ShieldPlantingSetting

from app.core import user
from app.core import planting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def setting_obj(setting=False, real_id=False):
    _setting = {}
    if setting:
        if real_id:
            _setting["real_id"] = setting.id

        _setting["id"] = str(setting.uid)
        _setting["planting"] = planting.fetch_by_id(setting.planting_id).get_json()
        _setting["created_by"] = creator_detail(setting.creator_id)
        _setting["status"] = status_name(setting.status)
        _setting["created"] = setting.created_at
        _setting["modified"] = setting.modified_at

    return _setting

def fetch_all():
    response = []

    settings = db\
        .session\
        .query(ShieldPlantingSetting)\
        .filter(ShieldPlantingSetting.status > config.STATUS_DELETED)\
        .all()

    for setting in settings:
        response.append(setting_obj(setting))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        setting = db\
            .session\
            .query(ShieldPlantingSetting)\
            .filter(ShieldPlantingSetting.uid == uid)\
            .filter(ShieldPlantingSetting.status > config.STATUS_DELETED)\
            .first()
        if setting:
            response = setting_obj(setting, real_id)

    return jsonify(response)

def fetch_by_plant(planting):
    response = []

    if planting:
        setting = db\
            .session\
            .query(ShieldPlantingSetting)\
            .filter(ShieldPlantingSetting.planting_id == planting)\
            .filter(ShieldPlantingSetting.status > config.STATUS_DELETED)\
            .all()
        for set in setting:
          response.append(setting_obj(set))
        

    return jsonify(response)



def fetch_by_id(id=0):
    response = {}
    if id:
        setting = db\
            .session\
            .query(ShieldPlantingSetting)\
            .filter(ShieldPlantingSetting.id == id)\
            .filter(ShieldPlantingSetting.status > config.STATUS_DELETED)\
            .first()
        if setting:
            response = setting_obj(setting)

    return jsonify(response)

def add_new(setting_data=None, return_obj=False):

    try:
        data = json.loads(setting_data)
        if data:

            _planting_id = None
            if valid_uuid(data['planting_id']):
                _planting_id = planting.fetch_one(data['planting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _planting_id and _creator_id:

                new_setting = ShieldPlantingSetting(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    planting_id = _planting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_setting)
                db.session.commit()
                if new_setting:
                    if return_obj:
                        return jsonify(fetch_one(new_setting.uid)), 200

                    return jsonify({"info": "Shield planting setting added successfully!"}), 200

        return jsonify({"error": "Shield planting setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(setting_id):

    try:

        if valid_uuid(setting_id) :
            _setting = ShieldPlantingSetting\
                .query\
                .filter(ShieldPlantingSetting.uid == setting_id)\
                .first()
            if _setting:
                _setting.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shield planting setting deactivated successfully!"}), 200

        return jsonify({"error": "Shield planting setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
