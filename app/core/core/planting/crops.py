import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import PlantingCrops

from app.core import user
from app.core import planting
from app.core.planting import plants
from app.core.planting import varieties

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def planting_crops_obj(planting_crops=False, real_id=False):
    _planting_crops = {}
    if planting_crops:
        if real_id:
            _planting_crops["real_id"] = planting_crops.id

        _planting_crops["id"] = str(planting_crops.uid)
        _planting_crops["start_date"] = planting_crops.start_date.strftime('%Y-%m-%d')
        _planting_crops["seedling_tally"] = planting_crops.seedling_tally
        _planting_crops["harvest_estimate"] = planting_crops.harvest_estimate
        _planting_crops["planting_period"] = planting_crops.planting_period
        _planting_crops["maturity_period"] = planting_crops.maturity_period
        _planting_crops["production_period"] = planting_crops.production_period
        _planting_crops["harvest_period"] = planting_crops.harvest_period
        _planting_crops["crop"] = varieties.fetch_by_id(
            planting_crops.crop_id).get_json()
        _planting_crops["planting_id"] = planting.fetch_by_id(
            planting_crops.planting_id).get_json()
        _planting_crops["created_by"] = creator_detail(
            planting_crops.creator_id)
        _planting_crops["status"] = status_name(planting_crops.status)
        _planting_crops["created"] = planting_crops.created_at
        _planting_crops["modified"] = planting_crops.modified_at

    return _planting_crops


def fetch_all():
    response = []

    planting_cropss = db\
        .session\
        .query(PlantingCrops)\
        .filter(PlantingCrops.status > config.STATUS_DELETED)\
        .order_by(PlantingCrops.id.desc())\
        .all()

    for planting_crops in planting_cropss:
        response.append(planting_crops_obj(planting_crops))

    return jsonify(response)


def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        planting_crops = db\
            .session\
            .query(PlantingCrops)\
            .filter(PlantingCrops.uid == uid)\
            .filter(PlantingCrops.status > config.STATUS_DELETED)\
            .first()
        if planting_crops:
            response = planting_crops_obj(planting_crops, real_id)

    return jsonify(response)


def fetch_by_id(id=0):
    response = {}
    if id:
        planting_crops = db\
            .session\
            .query(PlantingCrops)\
            .filter(PlantingCrops.id == id)\
            .filter(PlantingCrops.status > config.STATUS_DELETED)\
            .first()
        if planting_crops:
            response = planting_crops_obj(planting_crops)

    return jsonify(response)

def fetch_by_customer_date_range(creator_id,start_date,end_date):

    response = []
    if creator_id and start_date and end_date:
        fas = db\
            .session\
            .query(PlantingCrops)\
            .filter(PlantingCrops.creator_id == creator_id)\
            .filter(PlantingCrops.created_at <= end_date)\
            .filter(PlantingCrops.created_at >= start_date)\
            .filter(PlantingCrops.status > config.STATUS_DELETED)\
            .order_by(PlantingCrops.id.desc())\
            .all()
        for fa in fas:
            response.append(planting_crops_obj(fa))

    return jsonify(response)


def fetch_by_crop_id(crop_id=0):
    response = {}
    if crop_id:
        planting_crops = db\
            .session\
            .query(PlantingCrops)\
            .filter(PlantingCrops.crop_id == crop_id)\
            .filter(PlantingCrops.status > config.STATUS_DELETED)\
            .first()
        if planting_crops:
            response = planting_crops_obj(planting_crops)

    return jsonify(response)


def fetch_by_planting_id(planting_id=0):
    response = {}
    if planting_id:
        planting_crops = db\
            .session\
            .query(PlantingCrops)\
            .filter(PlantingCrops.planting_id == planting_id)\
            .filter(PlantingCrops.status > config.STATUS_DELETED)\
            .first()
        if planting_crops:
            response = planting_crops_obj(planting_crops)

    return jsonify(response)


def fetch_by_creator(creator_id):
    response = []
    if creator_id:
        planting_cropss = db\
        .session\
        .query(PlantingCrops)\
        .filter(PlantingCrops.creator_id == creator_id)\
        .filter(PlantingCrops.status > config.STATUS_DELETED)\
        .order_by(PlantingCrops.id.desc())\
        .all()

    for planting_crops in planting_cropss:
        response.append(planting_crops_obj(planting_crops))

    return jsonify(response)



def add_new(planting_crops_data=None, return_obj=False):

    try:
        data = json.loads(planting_crops_data)
        if data:

            if 'start_date' in data and 'creator_id' in data:

                _start_date = data['start_date']
                _seedling_tally = data['seedling_tally'] if 'seedling_tally' in data else 0
                _harvest_estimate = data['harvest_estimate'] if 'harvest_estimate' in data else 0
                _planting_period = data['planting_period'] if 'planting_period' in data else 0
                _maturity_period = data['maturity_period'] if 'maturity_period' in data else 0
                _production_period = data['production_period'] if 'production_period' in data else 0
                _harvest_period = data['harvest_period'] if 'harvest_period' in data else 0

                _crop_id = None
                if valid_uuid(data['crop_id']):
                    _crop_id = varieties.fetch_one(
                        data['crop_id'], True).get_json()['real_id']

                _planting_id = None
                if valid_uuid(data['planting_id']):
                    _planting_id = planting.fetch_one(
                        data['planting_id'], True).get_json()['real_id']

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(
                        data['creator_id'], True).get_json()['real_id']

                if _start_date and _crop_id and _planting_id and _creator:
                    new_planting_crops = PlantingCrops(
                        uid=uuid.uuid4(),
                        status=config.STATUS_ACTIVE,
                        start_date=_start_date,
                        seedling_tally=_seedling_tally,
                        harvest_estimate=_harvest_estimate,
                        planting_period=_planting_period,
                        maturity_period=_maturity_period,
                        production_period=_production_period,
                        harvest_period=_harvest_period,
                        crop_id=_crop_id,
                        planting_id=_planting_id,
                        creator_id=_creator
                    )
                    db.session.add(new_planting_crops)
                    db.session.commit()
                    if new_planting_crops:
                        if return_obj:
                            return jsonify(fetch_one(new_planting_crops.uid)), 200

                        return jsonify({"info": "Planting crops added successfully!"}), 200

        return jsonify({"error": "Planting crops not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def edit(planting_crops_id, planting_crops_data=None, return_obj=False):

    try:
        data = json.loads(planting_crops_data)
        if data:

            if valid_uuid(planting_crops_id) and 'start_date' in data:

                _start_date = data['start_date']
                _seedling_tally = data['seedling_tally'] if 'seedling_tally' in data else 0
                _harvest_estimate = data['harvest_estimate'] if 'harvest_estimate' in data else 0
                _planting_period = data['planting_period'] if 'planting_period' in data else 0
                _maturity_period = data['maturity_period'] if 'maturity_period' in data else 0
                _production_period = data['production_period'] if 'production_period' in data else 0
                _harvest_period = data['harvest_period'] if 'harvest_period' in data else 0

                _crop_id = None
                if valid_uuid(data['crop_id']):
                    _crop_id = varieties.fetch_one(
                        data['crop_id'], True).get_json()['real_id']

                _planting_id = None
                if valid_uuid(data['planting_id']):
                    _planting_id = plants.fetch_one(
                        data['planting_id'], True).get_json()['real_id']

                if _start_date and _crop_id and _planting_id:
                    _planting_crops = PlantingCrops\
                        .query\
                        .filter(PlantingCrops.uid == planting_crops_id)\
                        .first()
                    if _planting_crops:
                        # _planting_crops.name = _name
                        _planting_crops.seedling_tally = _seedling_tally
                        _planting_crops.harvest_estimate = _harvest_estimate
                        _planting_crops.planting_period = _planting_period
                        _planting_crops.maturity_period = _maturity_period
                        _planting_crops.production_period = _production_period
                        _planting_crops.harvest_period = _harvest_period
                        _planting_crops.crop_id = _crop_id
                        _planting_crops.planting_id = _planting_id
                        db.session.commit()

                        return jsonify({"info": "Planting crops edited successfully!"}), 200

        return jsonify({"error": "Planting crops not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def deactivate(planting_crops_id):

    try:

        if valid_uuid(planting_crops_id):
            _planting_crops = PlantingCrops\
                .query\
                .filter(PlantingCrops.uid == planting_crops_id)\
                .first()
            if _planting_crops:
                _planting_crops.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Planting crops deactivated successfully!"}), 200

        return jsonify({"error": "Planting crops not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def activate(planting_crops_id):

    try:

        if valid_uuid(planting_crops_id):
            _planting_crops = PlantingCrops\
                .query\
                .filter(PlantingCrops.uid == planting_crops_id)\
                .first()
            if _planting_crops:
                _planting_crops.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "Planting crops activated successfully!"}), 200

        return jsonify({"error": "Planting crops not activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406