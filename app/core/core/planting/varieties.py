import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Variety

from app.core import user
from app.core import planting
from app.core.planting import plants

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def variety_obj(variety=False, real_id=False):
    _variety = {}
    if variety:
        if real_id:
            _variety["real_id"] = variety.id

        _variety["id"] = str(variety.uid)
        _variety["name"] = variety.name
        _variety["plant"] = planting.plants.fetch_by_id(variety.plant_id).get_json()
        _variety["created_by"] = creator_detail(variety.creator_id)
        _variety["status"] = status_name(variety.status)
        _variety["created"] = variety.created_at
        _variety["modified"] = variety.modified_at

    return _variety

def fetch_all():
    response = []

    varieties = db\
        .session\
        .query(Variety)\
        .filter(Variety.status > config.STATUS_DELETED)\
        .all()

    for variety in varieties:
        response.append(variety_obj(variety))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        variety = db\
            .session\
            .query(Variety)\
            .filter(Variety.uid == uid)\
            .filter(Variety.status > config.STATUS_DELETED)\
            .first()
        if variety:
            response = variety_obj(variety, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        variety = db\
            .session\
            .query(Variety)\
            .filter(Variety.id == id)\
            .filter(Variety.status > config.STATUS_DELETED)\
            .first()
        if variety:
            response = variety_obj(variety)

    return jsonify(response)

def fetch_by_name(name):
    response = {}
    if name:
        variety = db\
            .session\
            .query(Variety)\
            .filter(Variety.name == name)\
            .filter(Variety.status > config.STATUS_DELETED)\
            .first()
        if variety:
            response = variety_obj(variety)

    return jsonify(response)

def add_new(variety_data=None, return_obj=False):

    try:
        data = json.loads(variety_data)
        if data:

            if 'name' in data and 'creator_id' in data:

                _name = data['name']

                _plant_id = None
                if valid_uuid(data['planting_id']):
                    _plant_id = plants.fetch_one(data['planting_id'], True).get_json()['real_id']

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _name and _plant_id and _creator:
                    new_variety = Variety(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        plant_id = _plant_id,
                        creator_id = _creator
                    )
                    db.session.add(new_variety)
                    db.session.commit()
                    if new_variety:
                        if return_obj:
                            return jsonify(fetch_one(new_variety.uid)), 200

                        return jsonify({"info": "Variety added successfully!"}), 200

        return jsonify({"error": "Variety not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(variety_id, variety_data=None, return_obj=False):

    try:
        data = json.loads(variety_data)
        if data:
            if valid_uuid(variety_id) and 'name' in data:

                _name = data['name']

                _plant_id = None
                if valid_uuid(data['planting_id']):
                    _plant_id = planting.plants.fetch_one(data['planting_id'], True).get_json()['real_id']

                if _name and _plant_id:
                    _variety = Variety\
                        .query\
                        .filter(Variety.uid == variety_id)\
                        .first()
                    if _variety:
                        _variety.name = _name
                        _variety.plant_id = _plant_id
                        db.session.commit()

                        return jsonify({"info": "Variety edited successfully!"}), 200

        return jsonify({"error": "Variety not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(variety_id):

    try:

        if valid_uuid(variety_id) :
            _variety = Variety\
                .query\
                .filter(Variety.uid == variety_id)\
                .first()
            if _variety:
                _variety.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Variety deactivated successfully!"}), 200

        return jsonify({"error": "Variety not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
