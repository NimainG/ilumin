import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Media

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def media_obj(media=False, real_id=False):
    _media = {}
    if media:
        if real_id:
            _media["real_id"] = media.id

        _media["id"] = str(media.uid)
        _media["file_url"] = media.file_url
        _media["caption"] = media.caption
        _media["created_by"] = creator_detail(media.creator_id)
        _media["status"] = status_name(media.status)
        _media["created"] = media.created_at
        _media["modified"] = media.modified_at

    return _media

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        media = db\
            .session\
            .query(Media)\
            .filter(Media.uid == uid)\
            .filter(Media.status > config.STATUS_DELETED)\
            .first()
        if media:
            response = media_obj(media, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        media = db\
            .session\
            .query(Media)\
            .filter(Media.id == id)\
            .filter(Media.status > config.STATUS_DELETED)\
            .first()
        if media:
            response = media_obj(media)

    return jsonify(response)

def add_new(media_data=None, return_obj=False):

    try:
        data = json.loads(media_data)
        if data:

            if 'file_url' in data and 'caption' in data and 'creator_id' in data :

                _photo_media = data['file_url']
                _caption = data['caption']
                _creator = None

                if "creator_id" in data and valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _photo_media and _caption and _creator:
                    new_media = Media(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        file_url = _photo_media,
                        caption = _caption,
                        creator_id = _creator
                    )
                    db.session.add(new_media)
                    db.session.commit()
                    if new_media:
                        if return_obj:
                            return fetch_one(new_media.uid), 200

                        return jsonify({"info": "Media added successfully!"}), 200

        return jsonify({"error": "Media not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(media_id):

    try:
        if valid_uuid(media_id) :
            _media = Media\
                .query\
                .filter(Media.uid == media_id)\
                .first()
            if _media:
                _media.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Media deactivated successfully!"}), 200

        return jsonify({"error": "Media not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
