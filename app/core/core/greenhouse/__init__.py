import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv
from werkzeug.wrappers import response

from app.config import activateConfig
from app.core.customer import greenhouses

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Greenhouse

from app.core import user

from app.core.greenhouse import types 

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouse_obj(greenhouse=False, real_id=False):
    _greenhouse = {}
    if greenhouse:
        if real_id:
            _greenhouse["real_id"] = greenhouse.id

        _greenhouse["id"] = str(greenhouse.uid)
        _greenhouse["greenhouse_ref"] = greenhouse.greenhouse_ref
        _greenhouse["size"] = greenhouse.size
        _greenhouse["info_link"] = greenhouse.info_link
        _greenhouse["greenhouse_type_id"] = types.fetch_by_id(greenhouse.greenhouse_type_id).get_json()
        _greenhouse["created_by"] = creator_detail(greenhouse.creator_id)
        _greenhouse["assigned"] = greenhouse.assigned
        _greenhouse["status"] = status_name(greenhouse.status)
        _greenhouse["created"] = greenhouse.created_at.strftime('%d %B %Y') 
        _greenhouse["modified"] = greenhouse.modified_at

    return _greenhouse

def fetch_all():
    response = []

    greenhouses = db\
        .session\
        .query(Greenhouse)\
        .filter(Greenhouse.status > config.STATUS_DELETED)\
        .all()

    for greenhouse in greenhouses:
        response.append(greenhouse_obj(greenhouse))

    return jsonify(response)


def fetch_all1(page):
    response = []
    page_limit = config.PAGE_LIMIT
    greenhouses = db.session\
        .query(Greenhouse)\
        .limit(page_limit)\
        .offset((page-1)*page_limit)
        
    # greenhouses = db\
    #     .session\
    #     .query(Greenhouse)\
    #     .order_by(Greenhouse.id.asc())\
    #     .paginate(page,10,error_out=False)\
    #     .items()\
    #     .fetch_all()
    for greenhouse in greenhouses:
        response.append(greenhouse_obj(greenhouse))

    return jsonify(response)

# def fetch_pagingate():
#     response ={}
#     page = page
#     pages = 10
#     greenhouses = db\
#         .session\
#             .query(Greenhouse)\
#             .paginate(page,pages,error_out=False)\
#             .items()
        
#     if greenhouses:
#             response = greenhouse_obj(greenhouses)
#     return jsonify(response)
         

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        greenhouse = db\
            .session\
            .query(Greenhouse)\
            .filter(Greenhouse.uid == uid)\
            .filter(Greenhouse.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        greenhouse = db\
            .session\
            .query(Greenhouse)\
            .filter(Greenhouse.id == id)\
            .filter(Greenhouse.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse)

    return jsonify(response)

def fetch_by_type_id(id=0):
    response = {}
    if id:
        greenhouse = db\
            .session\
            .query(Greenhouse)\
            .filter(Greenhouse.greenhouse_type_id == id)\
            .filter(Greenhouse.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse)

    return jsonify(response)

def fetch_by_green_ref(greenhouse_ref):
    response = {}
    if greenhouse_ref:
        greenhouse = db\
            .session\
            .query(Greenhouse)\
            .filter(Greenhouse.greenhouse_ref == greenhouse_ref)\
            .filter(Greenhouse.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse)

    return jsonify(response)

def add_new(greenhouse_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_data)
        if data:

            if 'greenhouse_ref' in data and 'creator_id' in data:

                _greenhouse_ref = data['greenhouse_ref']
                _size = data['size']  if 'size' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                _greenhouse_type = None
                _creator = None

                if valid_uuid(data['greenhouse_type_id']):
                    _greenhouse_type = types.fetch_one(data['greenhouse_type_id'], True).get_json()['real_id']
                
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _greenhouse_ref and _creator:
                    new_greenhouse = Greenhouse(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        assigned = config.NOT_ASSIGNED,
                        greenhouse_ref = _greenhouse_ref,
                        size = _size,
                        info_link = _info_link,
                        greenhouse_type_id = _greenhouse_type,
                        creator_id = _creator
                    )
                    db.session.add(new_greenhouse)
                    db.session.commit()
                    if new_greenhouse:
                        if return_obj:
                            return jsonify(fetch_one(new_greenhouse.uid)), 200

                        return jsonify({"info": "Greenhouse added successfully!"}), 200

        return jsonify({"error": "Greenhouse not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(greenhouse_id, greenhouse_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_data)
        if data:

            if valid_uuid(greenhouse_id):

                _size = data['size']  if 'size' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None
                _assigned = data['assigned'] if 'assigned' in data else None

                _greenhouse_type = None

                if valid_uuid(data['greenhouse_type_id']):
                    _greenhouse_type = types.fetch_one(data['greenhouse_type_id'], True).get_json()['real_id']

                # if _size:
                _greenhouse = Greenhouse\
                        .query\
                        .filter(Greenhouse.uid == str(greenhouse_id))\
                        .first()
                if _greenhouse:
                        _greenhouse.size = _size
                        _greenhouse.info_link = _info_link
                        _greenhouse.assigned = _assigned
                        _greenhouse.greenhouse_type_id = _greenhouse_type
                        db.session.commit()

                        return jsonify({"info": "Greenhouse edited successfully!"}), 200

        return jsonify({"error": "Greenhouse not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(greenhouse_id):

    try:

        if valid_uuid(greenhouse_id):
            _greenhouse = Greenhouse\
                .query\
                .filter(Greenhouse.uid == greenhouse_id)\
                .first()
            if _greenhouse:
                _greenhouse.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "Greenhouse activated successfully!"}), 200

        return jsonify({"error": "Greenhouse not activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def deactivate(greenhouse_id):

    try:

        if valid_uuid(greenhouse_id) :
            _greenhouse = Greenhouse\
                .query\
                .filter(Greenhouse.uid == greenhouse_id)\
                .first()
            if _greenhouse:
                _greenhouse.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Greenhouse deactivated successfully!"}), 200

        return jsonify({"error": "Greenhouse not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
