# fetch_invite_user - uuid
# fetch_customer_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import UserInvite

from app.core import user
from app.core import customer
from app.core import partner
from app.core import mail

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def invite_obj(invite=False, real_id=False):
    _invite = {}
    if invite:
        if real_id:
            _invite["real_id"] = invite.id

        _invite["id"] = str(invite.uid)
        _invite["accept"] = invite.accept
        _invite["igh"] = invite.igh
        _invite["customer"] = customer.fetch_by_id(invite.customer_id).get_json()
        _invite["partner"] = partner.fetch_by_id(invite.partner_id).get_json()
        _invite["user"] = user.fetch_by_id(invite.user_id).get_json()
        _invite["staff"] = user.igh.fetch_by_user(invite.user_id).get_json()
        _invite["created_by"] = creator_detail(invite.creator_id)
        _invite["status"] = status_name(invite.status)
        _invite["created"] = invite.created_at.strftime('%d %B %Y')
        _invite["modified"] = invite.modified_at

    return _invite

def fetch_all():
    response = []

    invites = db\
        .session\
        .query(UserInvite)\
        .filter(UserInvite.status > config.STATUS_DELETED)\
        .all()

    for invite in invites:
        response.append(invite_obj(invite))

    return jsonify(response)

def fetch_by_onlyrealm(realm):
    response = []

    invites = db\
        .session\
        .query(UserInvite)\
        .filter(UserInvite.igh == realm)\
        .filter(UserInvite.status > config.STATUS_DELETED)\
        .all()

    for invite in invites:
        response.append(invite_obj(invite))

    return jsonify(response)

def fetch_by_realm(realm=None, status=1, the_id=None):
    response = []

    if realm:
        invites = []

        if realm == config.USER_IGH:
            invites = db\
                .session\
                .query(UserInvite)\
                .filter(UserInvite.accept == 0)\
                .filter(UserInvite.igh == 1)\
                .filter(UserInvite.status == status)\
                .all()

        if realm == config.USER_CUSTOMER:
            invites = db\
                .session\
                .query(UserInvite)\
                .filter(UserInvite.accept == 0)\
                .filter(UserInvite.customer_id == the_id)\
                .filter(UserInvite.status == status)\
                .all()

        if realm == config.USER_PARTNER:
            invites = db\
                .session\
                .query(UserInvite)\
                .filter(UserInvite.accept == 0)\
                .filter(UserInvite.partner_id == the_id)\
                .filter(UserInvite.status == status)\
                .all()

        for invite in invites:
            response.append(invite_obj(invite))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        invite = db\
            .session\
            .query(UserInvite)\
            .filter(UserInvite.uid == uid)\
            .filter(UserInvite.status > config.STATUS_DELETED)\
            .first()
        if invite:
            response = invite_obj(invite, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        invite = db\
            .session\
            .query(UserInvite)\
            .filter(UserInvite.id == id)\
            .filter(UserInvite.status > config.STATUS_DELETED)\
            .first()
        if invite:
            response = invite_obj(invite)

    return jsonify(response)

def fetch_by_user(user_id=0):
    response = {}
    if user_id:
        invites = db\
            .session\
            .query(UserInvite)\
            .filter(UserInvite.user_id == user_id)\
            .filter(UserInvite.status > config.STATUS_DELETED)\
            .first()
        if invites:
        # for invite in invites:
            response=(invite_obj(invites))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        invites = db\
            .session\
            .query(UserInvite)\
            .filter(UserInvite.customer_id == customer_id)\
            .filter(UserInvite.status > config.STATUS_DELETED)\
            .all()
        # if invites:
        for invite in invites:
            response.append(invite_obj(invite))
            # response=(invite_obj(invites))

    return jsonify(response)

def add_new(invite_data=None, return_obj=False):

    try:
        data = json.loads(invite_data)
        if data:

            _invite_org = config.COMPANY_NAME

            _igh = None
            if 'igh' in data:
                _igh = data['igh']

            _customer_id = None
            _customer_id = data['customer_id']
            # if 'customer_id' in data and valid_uuid(data['customer_id']):
            _customer = customer.fetch_by_id(data['customer_id'], True).get_json()
            if _customer:
                    _invite_org = _customer['name']
                    _customer_id = _customer['real_id']

            _partner_id = None
            if 'partner_id' in data and valid_uuid(data['partner_id']):
                _partner = partner.fetch_one(data['partner_id'], True).get_json()
                if _partner:
                    _invite_org = _partner['name']
                    _partner_id = _partner['real_id']

            if data['user_id'] and data['creator_id']:
                new_invite = UserInvite(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    accept = 0,
                    igh = _igh,
                    customer_id = _customer_id,
                    partner_id = _partner_id,
                    user_id = data['user_id'],
                    creator_id = data['creator_id']
                )
                db.session.add(new_invite)
                db.session.commit()
                if new_invite:
                    _invite_mail = {
                        "to": data['email_address'],
                        "subject": "FarmCloud Team Invitation",
                        "invite": {
                            "names": data['first_name'],
                            "code": str(new_invite.uid),
                            "org": _invite_org,
                        },
                    }
                    mail.send_user_invite(
                    
                        json.dumps(_invite_mail)
                    )

                    if return_obj:
                        return jsonify(fetch_one(new_invite.uid)), 200

                    return jsonify({"info": "User invite added successfully!"}), 200

        return jsonify({"error": "User invite not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(invite_id, invite_data=None, return_obj=False):

    try:
        data = json.loads(invite_data)
        if data:

            if valid_uuid(invite_id):

                _accept = None
                if 'accept' in data:
                    _accept = data['accept']

                _invite = UserInvite\
                    .query\
                    .filter(UserInvite.uid == invite_id)\
                    .first()
                if _invite:
                    _invite.accept = _accept
                    db.session.commit()

                    return jsonify({"info": "User invite edited successfully!"}), 200

        return jsonify({"error": "User invite not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(invite_id):

    try:

        if valid_uuid(invite_id) :
            _invite = UserInvite\
                .query\
                .filter(UserInvite.uid == invite_id)\
                .first()
            if _invite:
                _invite.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "User invite deactivated successfully!"}), 200

        return jsonify({"error": "User invite not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
