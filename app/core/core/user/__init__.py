import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import User

from app.core import media
from app.core import alert
from app.core.user import roles
from app.core.user import user_roles
from app.core.user import igh as igh_users
from app.core.user import customers as customer_users
from app.core import customer
from app.core.user import partners as partner_users
from app.core.user import configs as user_configs
from app.core.user import auth as user_auth
from app.core.user import invites as user_invites
from app.core.user import realms as user_realms
from app.core.user import logs

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def user_obj(user=False, real_id=False):
    _user = {}
    if user:
        if real_id:
            _user["real_id"] = user.id

        _user["id"] = str(user.uid)
        _user["email_address"] = user.email_address
        _user["phone_number"] = user.phone_number
        _user["first_name"] = user.first_name
        _user["last_name"] = user.last_name
        _user["avatar_media"] = media.fetch_by_id(user.avatar_media_id).get_json()
        _user["created_by"] = creator_detail(user.creator_id)
        _user["status"] = status_name(user.status)
        _user["created"] = user.created_at.strftime('%d %B %Y')
        _user["modified"] = user.modified_at

    return _user

def fetch_all():
    response = []

    users = db\
        .session\
        .query(User)\
        .filter(User.status > config.STATUS_DELETED)\
        .all()

    for user in users:
        response.append(user_obj(user))

    return jsonify(response)


def fetch_by_creator(created_by):
    response = []

    users = db\
        .session\
        .query(User)\
        .filter(User.creator_id == created_by)\
        .filter(User.status > config.STATUS_DELETED)\
        .all()

    for user in users:
        response.append(user_obj(user))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        user = db\
            .session\
            .query(User)\
            .filter(User.uid == uid)\
            .filter(User.status > config.STATUS_DELETED)\
            .first()
        if user:
            response = user_obj(user, real_id)

    return jsonify(response)

def fetch_by_id(id=0, real_id=False):
    response = {}
    if id:
        user = db\
            .session\
            .query(User)\
            .filter(User.id == id)\
            .filter(User.status > config.STATUS_DELETED)\
            .first()
        if user:
            response = user_obj(user, real_id)

    return jsonify(response)

def fetch_by_email(email_address=None):
    response = {}
    if email_address:
        user = db\
            .session\
            .query(User)\
            .filter(User.email_address == email_address)\
            .filter(User.status > config.STATUS_DELETED)\
            .first()
        if user:
            response = user_obj(user)

    return jsonify(response)

def fetch_by_phone(phone_number=None):
    response = {}
    if phone_number:
        user = db\
            .session\
            .query(User)\
            .filter(User.phone_number == phone_number)\
            .filter(User.status > config.STATUS_DELETED)\
            .first()
        if user:
            response = user_obj(user)

    return jsonify(response)

def add_new(user_data=None, return_obj=False):

    try:
        data = json.loads(user_data)
        if data:

            _email_address = None
            if 'email_address' in data:
                _email_address = data['email_address']

                exists = fetch_by_email(_email_address).get_json()
                if exists:
                    return jsonify({"error": "Email address provided already exists."}), 409

            _phone_number = None
            if 'phone_number' in data:
                _phone_number = data['phone_number']

            _first_name = None
            if 'first_name' in data:
                _first_name = data['first_name']

            _last_name = None
            if 'last_name' in data:
                _last_name = data['last_name']

            _user_realm = None
            if 'user_realm' in data:
                _user_realm = data['user_realm']

            _employee_id = None
            if 'employee_id' in data:
                _employee_id = data['employee_id']

            _customer_id = None
            if 'customer_id' in data:
                _customer_id = data['customer_id']

            _partner_id = None
            if 'partner_id' in data:
                _partner_id = data['partner_id']

            _avatar_media_id = None
            if 'avatar_media' in data and valid_uuid(data['avatar_media']):
                _avatar_media_id = media.fetch_one(data['avatar_media'], True).get_json()['real_id']

            _creator = None
            if 'creator_id' in data and valid_uuid(data['creator_id']):
                _creator = fetch_one(data['creator_id'], True).get_json()['real_id']

            if _email_address and _user_realm:
                new_user = User(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    email_address = _email_address,
                    phone_number = _phone_number,
                    first_name = _first_name,
                    last_name = _last_name,
                    avatar_media_id = _avatar_media_id,
                    creator_id = _creator
                )
                db.session.add(new_user)
                db.session.commit()
                if new_user:
                    if _creator is None:
                        _creator = new_user.id

                    # Role
                    if 'role_id' in data and valid_uuid(data['role_id']):
                        _user_role_data = {
                            "user_realm": _user_realm,
                            "user_id": new_user.id,
                            "role_id": data['role_id'],
                            "creator_id": _creator
                        }
                        print("User Role data",_user_role_data)
                        _new_user_role = user_roles.add_new(
                            json.dumps(_user_role_data)
                        )

                    # Realm User
                    _invitation_data = {}

                    _realm_data = {}

                    _realm_user_data = {
                        "employee_id": _employee_id,
                        "customer_id": _customer_id,
                        "user_id": new_user.id,
                        "creator_id": _creator
                    }
                    print("User Ro---le data",_realm_user_data)

                    # _realm_user_data1 = {
                    #     "employee_id": _employee_id,
                    #     "user_id": new_user.id,
                    #     "creator_id": _creator
                    # }

                    if _user_realm == config.USER_IGH:
                        _new_igh_user = igh_users.add_new(
                            json.dumps(_realm_user_data)
                        )
                        print(str(_new_igh_user[0].get_json()))

                        _invitation_data = {
                            "igh": 1,
                            "user_id": new_user.id,
                            "customer_id": _customer_id,
                            "first_name": new_user.first_name,
                            "email_address": new_user.email_address,
                            "creator_id": _creator
                        }

                        _realm_data = {
                            "realm": config.USER_IGH,
                            "user_id": new_user.id,
                            "creator_id": _creator
                        }

                       
                        # _user_log = logs.add_new(
                        # json.dumps({
                        #     "action": "Added a new Admin User",
                        #     "ref": new_user.first_name,
                        #     "ref_id": new_user.id,
                        #     "creator_id" : _creator
                        # }),
                        
                        #  )

                        # print(str(_user_log[0].get_json()))

                    if _user_realm == config.USER_CUSTOMER:
                        _new_customer_user = customer_users.add_new(
                            json.dumps(_realm_user_data)
                        )
                        
                        _invitation_data = {
                            "igh": 2,
                            "customer_id": _customer_id,
                            "user_id": new_user.id,
                            "first_name": new_user.first_name,
                            "email_address": new_user.email_address,
                            "creator_id": _creator
                        }

                        _realm_data = {
                            "realm": config.USER_CUSTOMER,
                            "user_id": new_user.id,
                            "creator_id": _creator
                        }
                        if _customer_id != None :
                         _customer_idzz = customer.fetch_by_id(_customer_id).get_json()['id']
                         print("customer",_customer_idzz)
                         _user_log = logs.add_new(
                         json.dumps({
                            "action": "Onboarded a New Customer",
                            "ref": new_user.first_name,
                            "ref_id": new_user.id,
                            "customer_id": _customer_idzz,
                            "creator_id" : _creator
                         }),
                         True
                         )
                        else: 

                         _user_log = logs.add_new(
                         json.dumps({
                            "action": "Onboarded a New Farm Owner",
                            "ref": new_user.first_name,
                            "customer_id": None,
                            "ref_id": new_user.id,
                            "creator_id" : _creator
                         }),
                         True
                         )


                    # if _user_realm == config.USER_CUSTOMER:
                    #     _new_customer_user = customer.add_new(
                    #         json.dumps(_realm_user_data)
                    #     )
                    #     # print("All Customersssssssssssssssssssssssssssssss",str(_new_customer_user[0].get_json()))
                    #     _invitation_data = {
                    #         "igh": 2,
                    #         "customer_id": _customer_id,
                    #         "first_name" : new_user.first_name,
                    #         "user_id": new_user.id,
                    #         "email_address": new_user.email_address,
                    #         "creator_id": _creator
                    #     }

                    #     _realm_data = {
                    #         "realm": config.USER_CUSTOMER,
                    #         "user_id": new_user.id,
                    #         "creator_id": _creator
                    #     }


                    if _user_realm == config.USER_PARTNER:
                        _new_partner_user = partner_users.add_new(
                            json.dumps(_realm_user_data)
                        )

                        _invitation_data = {
                            "partner_id": _partner_id,
                            "user_id": new_user.id,
                            "first_name": new_user.first_name,
                            "email_address": new_user.email_address,
                            "creator_id": _creator
                        }

                        _realm_data = {
                            "realm": config.USER_PARTNER,
                            "user_id": new_user.id,
                            "creator_id": _creator
                        }

                        

                    # Attach realms
                    _user_realm = user_realms.add_new(
                        json.dumps(_realm_data)
                    )

                    # Default user configs
                    _user_configs = {
                        "temp_metric": config.METRIC_TEMP_C,
                        "liquid_metric": config.METRIC_LIQUID_L,
                        "length_metric": config.METRIC_LENGTH_F,
                        "user_id": new_user.id,
                        "creator_id": _creator
                    }
                    _new_user_config = user_configs.add_new(
                        json.dumps(_user_configs)
                    )

                    _alert_config = alert.set_alert_config(
                        str(new_user.uid),
                        config.NOTIFICATION_MODE_ALL,
                        _creator
                    )

                    # User auth
                    _auth_data = {
                        "email_address": _email_address,
                        "user_id": new_user.id,
                        "first_name": new_user.first_name,
                        "email_address": new_user.email_address,
                        "creator_id": _creator
                    }
                    _new_user_auth = user_auth.add_new(
                        json.dumps(_auth_data)
                    )

                    # Dispatch invitation
                    _user_invite = user_invites.add_new(
                        json.dumps(_invitation_data)
                    )
                    print(str(_user_invite[0].get_json()))

                   

                    if return_obj:
                        return jsonify({"user_id": new_user.uid}), 200

                    return jsonify({"info": "User added successfully!"}), 200

        return jsonify({"error": "User not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(user_id, user_data=None, return_obj=False):

    try:
        data = json.loads(user_data)
        if data:

            if valid_uuid(user_id):

                _user = User\
                    .query\
                    .filter(User.uid == str(user_id))\
                    .first()
                if _user:

                    if 'email_address' in data:
                        _email_address = data['email_address']

                        exists = fetch_by_email(_email_address).get_json()
                        if exists and exists['id'] != user_id:
                            return jsonify({"error": "Email address provided already exists."}), 409

                        _user.email_address = _email_address

                    if 'phone_number' in data:
                        _user.phone_number = data['phone_number']

                    if 'first_name' in data:
                        _user.first_name = data['first_name']

                    if 'last_name' in data:
                        _user.last_name = data['last_name']

                    if "avatar_media" in data and valid_uuid(data['avatar_media']):
                        _avatar_media_id = media.fetch_one(data['avatar_media'], True).get_json()['real_id']

                        if _avatar_media_id:
                            _user.avatar_media_id = _avatar_media_id

                    db.session.commit()

                    return jsonify({"info": "User edited successfully!"}), 200

        return jsonify({"error": "User not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(user_id):

    try:

        if valid_uuid(user_id) :
            _user = User\
                .query\
                .filter(User.uid == user_id)\
                .first()
            if _user:
                _user.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "User deactivated successfully!"}), 200

        return jsonify({"error": "User not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(user_id):

    try:

        if valid_uuid(user_id) :
            _user = User\
                .query\
                .filter(User.uid == user_id)\
                .first()
            if _user:
                _user.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "User deactivated successfully!"}), 200

        return jsonify({"error": "User not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
