# fetch_igh_user - uuid
# fetch_customer_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import IghUser

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def next_employee_id():
    _employee_id = config.EMPLOYEE_ID_START

    igh = db\
        .session\
        .query(IghUser)\
        .order_by(IghUser.employee_id.desc())\
        .first()
    if igh:
        _employee_id = igh.employee_id + 1

    return _employee_id

def igh_obj(igh=False, real_id=False):
    _igh = {}
    if igh:
        if real_id:
            _igh["real_id"] = igh.id

        _igh["id"] = str(igh.uid)
        _igh["id_number"] = igh.id_number
        _igh["employee_id"] = igh.employee_id if igh.employee_id else "-"
        _igh["rank"] = igh.rank
        _igh["user"] = user.fetch_by_id(igh.user_id).get_json()
        _igh["created_by"] = creator_detail(igh.creator_id)
        _igh["status"] = status_name(igh.status)
        _igh["created"] = igh.created_at.strftime('%d %B %Y')
        _igh["modified"] = igh.modified_at

    return _igh

def fetch_all():
    response = []

    ighs = db\
        .session\
        .query(IghUser)\
        .filter(IghUser.status > config.STATUS_DELETED)\
        .all()
    for igh in ighs:
        response.append(igh_obj(igh))

    return jsonify(response)

def fetch_all_active():
    response = []

    ighs = db\
        .session\
        .query(IghUser)\
        .filter(IghUser.status == config.STATUS_ACTIVE)\
        .all()
    for igh in ighs:
        response.append(igh_obj(igh))

    return jsonify(response)

def fetch_all_inactive():
    response = []

    ighs = db\
        .session\
        .query(IghUser)\
        .filter(IghUser.status == config.STATUS_INACTIVE)\
        .all()
    for igh in ighs:
        response.append(igh_obj(igh))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        igh = db\
            .session\
            .query(IghUser)\
            .filter(IghUser.uid == uid)\
            .filter(IghUser.status > config.STATUS_DELETED)\
            .first()
        if igh:
            response = igh_obj(igh, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        igh = db\
            .session\
            .query(IghUser)\
            .filter(IghUser.id == id)\
            .filter(IghUser.status > config.STATUS_DELETED)\
            .first()
        if igh:
            response = igh_obj(igh)

    return jsonify(response)

def fetch_by_user(user_id=0):
    response = {}
    if user_id:
        igh = db\
            .session\
            .query(IghUser)\
            .filter(IghUser.user_id == user_id)\
            .filter(IghUser.status > config.STATUS_DELETED)\
            .first()
        if igh:
            response = igh_obj(igh)

    return jsonify(response)

def add_new(igh_data=None, return_obj=False):

    try:
        data = json.loads(igh_data)
        if data:

            _id_number = None
            if 'id_number' in data:
                _id_number = data['id_number']

            _employee_id = None
            if 'employee_id' in data:
                _employee_id = data['employee_id']

            _rank = None
            if 'rank' in data:
                _rank = data['rank']

            if data['user_id'] and data['creator_id']:
                new_igh = IghUser(
                    uid = uuid.uuid4(),
                    status = config.STATUS_INACTIVE,
                    id_number = _id_number,
                    employee_id = _employee_id,
                    rank = _rank,
                    user_id = data['user_id'],
                    creator_id = data['creator_id'],
                )
                db.session.add(new_igh)
                db.session.commit()
                if new_igh:
                    if return_obj:
                        return jsonify(fetch_one(new_igh.uid)), 200

                    return jsonify({"info": "IGH user added successfully!"}), 200

        return jsonify({"error": "IGH user not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(igh_id, igh_data=None, return_obj=False):

    try:
        data = json.loads(igh_data)
        if data:

            if valid_uuid(igh_id):

                _id_number = None
                if 'id_number' in data:
                    _id_number = data['id_number']

                _rank = None
                if 'rank' in data:
                    _rank = data['rank']

                _igh = IghUser\
                    .query\
                    .filter(IghUser.uid == igh_id)\
                    .first()
                if _igh:
                    _igh.id_number = _id_number
                    _igh.rank = _rank
                    db.session.commit()

                    return jsonify({"info": "IGH user edited successfully!"}), 200

        return jsonify({"error": "IGH user not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(igh_id):

    try:

        if valid_uuid(igh_id) :
            _igh = IghUser\
                .query\
                .filter(IghUser.uid == igh_id)\
                .first()
            if _igh:
                _igh.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "IGH user deactivated successfully!"}), 200

        return jsonify({"error": "IGH user not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
