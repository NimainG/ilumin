import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Pest

from app.core import user

from app.core.pest import photos as pest_photos


load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def pest_obj(pest=False, real_id=False):
    _pest = {}
    if pest:
        if real_id:
            _pest["real_id"] = pest.id

        _pest["id"] = str(pest.uid)
        _pest["name"] = pest.name
        _pest["description"] = pest.description
        _pest["photos"] = pest_photos.fetch_by_pest(pest.id).get_json()
        _pest["created_by"] = creator_detail(pest.creator_id)
        _pest["status"] = status_name(pest.status)
        _pest["created"] = pest.created_at

    return _pest

def fetch_all():
    response = []

    pests = db\
        .session\
        .query(Pest)\
        .filter(Pest.status > config.STATUS_DELETED)\
        .all()

    for pest in pests:
        response.append(pest_obj(pest))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        pest = db\
            .session\
            .query(Pest)\
            .filter(Pest.uid == uid)\
            .filter(Pest.status > config.STATUS_DELETED)\
            .first()
        if pest:
            response = pest_obj(pest, real_id)

    return jsonify(response)



def fetch_by_id(id=0):
    response = {}
    if id:
        pest = db\
            .session\
            .query(Pest)\
            .filter(Pest.id == id)\
            .filter(Pest.status > config.STATUS_DELETED)\
            .first()
        if pest:
            response = pest_obj(pest)

    return jsonify(response)

def fetch_by_name(name):
    response = {}
    if name:
        pest = db\
            .session\
            .query(Pest)\
            .filter(Pest.name == name)\
            .filter(Pest.status > config.STATUS_DELETED)\
            .first()
        if pest:
           response = pest_obj(pest)

    return jsonify(response)

def add_new(pest_data=None, return_obj=False):

    try:
        data = json.loads(pest_data)
        if data:

            if 'name' in data and 'creator_id' in data:

                _name = data['name']
                _description = data['description']
                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _name and _creator:
                    new_pest = Pest(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        description = _description,
                        creator_id = _creator
                    )
                    db.session.add(new_pest)
                    db.session.commit()
                    if new_pest:
                        if return_obj:
                            return jsonify(fetch_one(new_pest.uid)), 200

                        return jsonify({"info": "Pest added successfully!"}), 200

        return jsonify({"error": "Pest not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(pest_id, pest_data=None, return_obj=False):

    try:
        data = json.loads(pest_data)
        if data:

            if valid_uuid(pest_id) and 'name' in data:

                _name = data['name']
                _description = data['description']

                # if _name and _description and _creator:
                if _name and _description:
                    _pest = Pest\
                        .query\
                        .filter(Pest.uid == str(pest_id))\
                        .first()
                    if _pest:
                        _pest.name = _name
                        _pest.description = _description
                        db.session.commit()

                        return jsonify({"info": "Pest edited successfully!"}), 200

        return jsonify({"error": "Pest not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(pest_id):

    try:

        if valid_uuid(pest_id) :
            _pest = Pest\
                .query\
                .filter(Pest.uid == pest_id)\
                .first()
            if _pest:
                _pest.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Pest deactivated successfully!"}), 200

        return jsonify({"error": "Pest not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
