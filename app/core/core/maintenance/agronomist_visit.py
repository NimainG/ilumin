import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv
from sqlalchemy import desc
from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import AgronomistVisit

from app.core import user
from app.core import maintenance
from app.core import supply
from app.core import customer
from app.core.supply import components

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def agronomist_visit_obj(agronomist_visit=False, real_id=False):
    _agronomist_visit = {}
    if agronomist_visit:
        if real_id:
            _agronomist_visit["real_id"] = agronomist_visit.id

        _agronomist_visit["id"] = str(agronomist_visit.uid)
        _agronomist_visit["visit_date"] = agronomist_visit.visit_date
        _agronomist_visit["visit_time"] = agronomist_visit.visit_time
        _agronomist_visit["reorder_fertilizer"] = agronomist_visit.reorder_fertilizer
        _agronomist_visit["reorder_agrochemical"] = agronomist_visit.reorder_agrochemical
        _agronomist_visit["fertilizer"] = supply.fetch_by_id(agronomist_visit.fertilizer_id).get_json()
        _agronomist_visit["fertilizer_main_component"] = components.fetch_by_id(agronomist_visit.fertilizer_main_component_id).get_json()
        _agronomist_visit["fertilizer_amount"] = agronomist_visit.fertilizer_amount
        _agronomist_visit["agrochemical"] = supply.fetch_by_id(agronomist_visit.agrochemical_id).get_json()
        _agronomist_visit["agrochemical_main_component"] = components.fetch_by_id(agronomist_visit.agrochemical_main_component_id).get_json()
        _agronomist_visit["agrochemical_amount"] = agronomist_visit.agrochemical_amount
        _agronomist_visit["notes"] = agronomist_visit.notes
        _agronomist_visit["observations"] = agronomist_visit.observations
        _agronomist_visit["risk_observations"] = agronomist_visit.risk_observations
        _agronomist_visit["recommendations"] = agronomist_visit.recommendations
        _agronomist_visit["maintenance_id"] = maintenance.fetch_by_id(agronomist_visit.maintenance_id).get_json()
        _agronomist_visit["customer"] = customer.fetch_by_id(agronomist_visit.customer_id).get_json()
        _agronomist_visit["created_by"] = creator_detail(agronomist_visit.creator_id)
        _agronomist_visit["status"] = status_name(agronomist_visit.status)
        _agronomist_visit["created"] = agronomist_visit.created_at.strftime('%d %B %Y')
        _agronomist_visit["modified"] = agronomist_visit.modified_at

    return _agronomist_visit



def fetch_all():
    response = []

    visit = db\
        .session\
        .query(AgronomistVisit)\
        .filter(AgronomistVisit.status > config.STATUS_DELETED)\
        .order_by(AgronomistVisit.id.desc())\
        .all()

    for ag_visit in visit:
        response.append(agronomist_visit_obj(ag_visit))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        agronomist_visit = db\
            .session\
            .query(AgronomistVisit)\
            .filter(AgronomistVisit.uid == uid)\
            .filter(AgronomistVisit.status > config.STATUS_DELETED)\
            .first()
        if agronomist_visit:
            response = agronomist_visit_obj(agronomist_visit, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        agronomist_visit = db\
            .session\
            .query(AgronomistVisit)\
            .filter(AgronomistVisit.id == id)\
            .filter(AgronomistVisit.status > config.STATUS_DELETED)\
            .first()
        if agronomist_visit:
            response = agronomist_visit_obj(agronomist_visit)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        agronomist_visit = db\
            .session\
            .query(AgronomistVisit)\
            .filter(AgronomistVisit.maintenance_id == id)\
            .filter(AgronomistVisit.status > config.STATUS_DELETED)\
            .first()
        if agronomist_visit:
            response = agronomist_visit_obj(agronomist_visit)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = []
    if id:
        agronomist_visit = db\
            .session\
            .query(AgronomistVisit)\
            .filter(AgronomistVisit.customer_id == id)\
            .filter(AgronomistVisit.status > config.STATUS_DELETED)\
            .all()
        for ag_visit in agronomist_visit:
         response.append(agronomist_visit_obj(ag_visit))
        

    return jsonify(response)

def add_new(agronomist_visit_data=None, return_obj=False):

    try:
        data = json.loads(agronomist_visit_data)
        if data:

            _visit_date = None
            if 'visit_date' in data and data['visit_date']:
                _visit_date = data['visit_date']

            _visit_time = None
            if 'visit_time' in data and data['visit_time']:
                _visit_time = data['visit_time']

            _reorder_fertilizer = None
            if 'reorder_fertilizer' in data and data['reorder_fertilizer']:
                _reorder_fertilizer = data['reorder_fertilizer']

            _reorder_agrochemical = None
            if 'reorder_agrochemical' in data and data['reorder_agrochemical']:
                _reorder_agrochemical = data['reorder_agrochemical']

            _fertilizer_id = None
            if valid_uuid(data['fertilizer_id']):
                _fertilizer_id = supply.fetch_one(data['fertilizer_id'], True).get_json()['real_id']

            _fertilizer_main_component_id = None
            if valid_uuid(data['fertilizer_main_component_id']):
                _fertilizer_main_component_id = components.fetch_one(data['fertilizer_main_component_id'], True).get_json()['real_id']

            _fertilizer_amount = None
            if 'fertilizer_amount' in data and data['fertilizer_amount']:
                _fertilizer_amount = data['fertilizer_amount']

            _agrochemical_id = None
            if valid_uuid(data['agrochemical_id']):
                _agrochemical_id = supply.fetch_one(data['agrochemical_id'], True).get_json()['real_id']

            _agrochemical_main_component_id = None
            if valid_uuid(data['agrochemical_main_component_id']):
                _agrochemical_main_component_id = components.fetch_one(data['agrochemical_main_component_id'], True).get_json()['real_id']

            _agrochemical_amount = None
            if 'agrochemical_amount' in data and data['agrochemical_amount']:
                _agrochemical_amount = data['agrochemical_amount']

            _notes = None
            if 'notes' in data and data['notes']:
                _notes = data['notes']

            _observations = None
            if 'observations' in data and data['observations']:
                _observations = data['observations']

            _risk_observations = None
            if 'risk_observations' in data and data['risk_observations']:
                _risk_observations = data['risk_observations']

            _recommendations = None
            if 'recommendations' in data and data['recommendations']:
                _recommendations = data['recommendations']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            # _customer_id = None
            # if valid_uuid(data['customer_id']):
            #     _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']
            _customer_id  = None
            if 'customer_id' in data and data['customer_id']:
                _customer_id = data['customer_id']
            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _visit_date and _visit_time and _maintenance_id and _creator:
                new_agronomist_visit = AgronomistVisit(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    visit_date = _visit_date,
                    visit_time = _visit_time,
                    reorder_fertilizer = _reorder_fertilizer,
                    reorder_agrochemical = _reorder_agrochemical,
                    fertilizer_id = _fertilizer_id,
                    fertilizer_main_component_id = _fertilizer_main_component_id,
                    fertilizer_amount = _fertilizer_amount,
                    agrochemical_id = _agrochemical_id,
                    agrochemical_main_component_id = _agrochemical_main_component_id,
                    agrochemical_amount = _agrochemical_amount,
                    notes = _notes,
                    observations = _observations,
                    risk_observations = _risk_observations,
                    recommendations = _recommendations,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_agronomist_visit)
                db.session.commit()
                if new_agronomist_visit:
                    if return_obj:
                        return jsonify(fetch_one(new_agronomist_visit.uid)), 200

                    return jsonify({"info": "Agronomist visit added successfully!"}), 200

        return jsonify({"error": "Agronomist visit not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(agronomist_visit_id, agronomist_visit_data=None, return_obj=False):

    try:
        data = json.loads(agronomist_visit_data)
        if data:

            if valid_uuid(agronomist_visit_id):

                _notes = None
                if 'notes' in data and data['notes']:
                    _notes = data['notes']

                # _agronomist_visit_id = None
                # if valid_uuid(data['agronomist_visit_id']):
                #     _agronomist_visit_id = agronomist_visit.fetch_one(data['agronomist_visit_id'], True).get_json()['real_id']

                _visit_date = None
            if 'visit_date' in data and data['visit_date']:
                _visit_date = data['visit_date']

            _visit_time = None
            if 'visit_time' in data and data['visit_time']:
                _visit_time = data['visit_time']

            _reorder_fertilizer = None
            if 'reorder_fertilizer' in data and data['reorder_fertilizer']:
                _reorder_fertilizer = data['reorder_fertilizer']

            _reorder_agrochemical = None
            if 'reorder_agrochemical' in data and data['reorder_agrochemical']:
                _reorder_agrochemical = data['reorder_agrochemical']

            _fertilizer_id = None
            if valid_uuid(data['fertilizer_id']):
                _fertilizer_id = supply.fetch_one(data['fertilizer_id'], True).get_json()['real_id']

            _fertilizer_main_component_id = None
            if valid_uuid(data['fertilizer_main_component_id']):
                _fertilizer_main_component_id = components.fetch_one(data['fertilizer_main_component_id'], True).get_json()['real_id']

            _fertilizer_amount = None
            if 'fertilizer_amount' in data and data['fertilizer_amount']:
                _fertilizer_amount = data['fertilizer_amount']

            _agrochemical_id = None
            if valid_uuid(data['agrochemical_id']):
                _agrochemical_id = supply.fetch_one(data['agrochemical_id'], True).get_json()['real_id']

            _agrochemical_main_component_id = None
            if valid_uuid(data['agrochemical_main_component_id']):
                _agrochemical_main_component_id = components.fetch_one(data['agrochemical_main_component_id'], True).get_json()['real_id']

            _agrochemical_amount = None
            if 'agrochemical_amount' in data and data['agrochemical_amount']:
                _agrochemical_amount = data['agrochemical_amount']

            _notes = None
            if 'notes' in data and data['notes']:
                _notes = data['notes']

            _observations = None
            if 'observations' in data and data['observations']:
                _observations = data['observations']
            _risk_observations = None
            if 'risk_observations' in data and data['risk_observations']:
                _risk_observations = data['risk_observations']

            _recommendations = None
            if 'recommendations' in data and data['recommendations']:
                _recommendations = data['recommendations']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            # _customer_id = None
            # if valid_uuid(data['customer_id']):
            #     _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']
            _customer_id  = None
            if 'customer_id' in data and data['customer_id']:
                _customer_id = data['customer_id']
            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if agronomist_visit_id :
                    _agronomist_visit = AgronomistVisit\
                        .query\
                        .filter(AgronomistVisit.uid == str(agronomist_visit_id))\
                        .first()
                    if _agronomist_visit:
                        _agronomist_visit.notes = _notes
                        _agronomist_visit.visit_date = _visit_date
                        _agronomist_visit.visit_time = _visit_time
                        _agronomist_visit.reorder_fertilizer = _reorder_fertilizer
                        _agronomist_visit.reorder_agrochemical = _reorder_agrochemical
                        _agronomist_visit.fertilizer_id = _fertilizer_id
                        _agronomist_visit.fertilizer_main_component_id = _fertilizer_main_component_id
                        _agronomist_visit.fertilizer_amount = _fertilizer_amount
                        _agronomist_visit.agrochemical_id = _agrochemical_id
                        _agronomist_visit.agrochemical_main_component_id = _agrochemical_main_component_id
                        _agronomist_visit.agrochemical_amount = _agrochemical_amount
                        _agronomist_visit.observations = _observations
                        _agronomist_visit.risk_observations = _risk_observations
                        _agronomist_visit.recommendations = _recommendations
                        _agronomist_visit.maintenance_id = _maintenance_id
                        _agronomist_visit.customer_id = _customer_id
                        _agronomist_visit.creator_id = _creator
                        db.session.commit()




                        return jsonify({"info": "Agronomist visit edited successfully!"}), 200

        return jsonify({"error": "Agronomist visit not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(agronomist_visit_id):

    try:
        if valid_uuid(agronomist_visit_id) :
            _agronomist_visit = AgronomistVisit\
                .query\
                .filter(AgronomistVisit.uid == agronomist_visit_id)\
                .first()
            if _agronomist_visit:
                _agronomist_visit.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Agronomist visit deactivated successfully!"}), 200

        return jsonify({"error": "Agronomist visit not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
