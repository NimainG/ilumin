import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import ShieldMaintenanceIssue

from app.core import user
from app.core import maintenance
from app.core import customer
from app.core.maintenance import issues
from app.core.customer import shields

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def shield_issue_obj(shield_issue=False, real_id=False):
    _shield_issue = {}
    if shield_issue:
        if real_id:
            _shield_issue["real_id"] = shield_issue.id

        _shield_issue["id"] = str(shield_issue.uid)
        _shield_issue["main_id"] = shield_issue.id
        _shield_issue["notes"] = shield_issue.notes
        _shield_issue["issue"] = issues.fetch_by_id(shield_issue.issue_id).get_json()
        _shield_issue["shield_maintenance"] = shields.fetch_by_id(shield_issue.shield_id).get_json()
        _shield_issue["maintenance"] = maintenance.fetch_by_id(shield_issue.maintenance_id).get_json()
        _shield_issue["customer"] = customer.fetch_one(shield_issue.customer_id).get_json()
        _shield_issue["created_by"] = creator_detail(shield_issue.creator_id)
        _shield_issue["status"] = status_name(shield_issue.status)
        _shield_issue["created"] = shield_issue.created_at.strftime('%d %B %Y')
        _shield_issue["modified"] = shield_issue.modified_at

    return _shield_issue

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield_issue = db\
            .session\
            .query(ShieldMaintenanceIssue)\
            .filter(ShieldMaintenanceIssue.uid == uid)\
            .filter(ShieldMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shield_issue:
            response = shield_issue_obj(shield_issue, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shield_issue = db\
            .session\
            .query(ShieldMaintenanceIssue)\
            .filter(ShieldMaintenanceIssue.id == id)\
            .filter(ShieldMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shield_issue:
            response = shield_issue_obj(shield_issue)

    return jsonify(response)

def fetch_all():
    response = []

    _shield = db\
        .session\
        .query(ShieldMaintenanceIssue)\
        .filter(ShieldMaintenanceIssue.status > config.STATUS_DELETED)\
        .all()

    for _sh in  _shield:
        response.append(shield_issue_obj(_sh))

    return jsonify(response)

def fetch_by_shield_maintenance(id=0):
    response = {}
    if id:
        shield_issue = db\
            .session\
            .query(ShieldMaintenanceIssue)\
            .filter(ShieldMaintenanceIssue.shield_maintenance_id == id)\
            .filter(ShieldMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shield_issue:
            response = shield_issue_obj(shield_issue)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        shield_issue = db\
            .session\
            .query(ShieldMaintenanceIssue)\
            .filter(ShieldMaintenanceIssue.maintenance_id == id)\
            .filter(ShieldMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shield_issue:
            response = shield_issue_obj(shield_issue)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        shield_issue = db\
            .session\
            .query(ShieldMaintenanceIssue)\
            .filter(ShieldMaintenanceIssue.customer_id == id)\
            .filter(ShieldMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shield_issue:
            response = shield_issue_obj(shield_issue)

    return jsonify(response)


def fetch_by_customers(id=0):
    response = []
    if id:
        shield_issue = db\
            .session\
            .query(ShieldMaintenanceIssue)\
            .filter(ShieldMaintenanceIssue.customer_id == id)\
            .filter(ShieldMaintenanceIssue.status > config.STATUS_DELETED)\
            .all()
        
        for s_h in shield_issue:
            response.append(shield_issue_obj(s_h))

    return jsonify(response)




      

def add_new(shield_issue_data=None, return_obj=False):

    try:
        data = json.loads(shield_issue_data)
        if data:

            _notes = None
            if 'notes' in data and data['notes']:
                _notes = data['notes']

            _issue_id = None
            if valid_uuid(data['issue_id']):
                _issue_id = issues.fetch_one(data['issue_id'], True).get_json()['real_id']

            _shield_maintenance_id = None
            if valid_uuid(data['shield_id']):
                _shield_maintenance_id = shields.fetch_one(data['shield_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            # _customer_id = None
            # if valid_uuid(data['customer_id']):
            #     _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']
            _customer_id  = None
            if 'customer_id' in data and data['customer_id']:
                _customer_id = data['customer_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _issue_id  and _maintenance_id and _customer_id and _creator:
                new_shield_issue = ShieldMaintenanceIssue(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    issue_id = _issue_id,
                    shield_id = _shield_maintenance_id,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_shield_issue)
                db.session.commit()
                if new_shield_issue:
                    if return_obj:
                        return jsonify(fetch_one(new_shield_issue.uid)), 200

                    return jsonify({"info": "Shield maintenance issue added successfully!"}), 200

        return jsonify({"error": "Shield maintenance issue not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(shield_issue_id, shield_issue_data=None, return_obj=False):

    try:
        data = json.loads(shield_issue_data)
        if data:

             if valid_uuid(shield_issue_id):

            #  _issue_id = None
            #     if valid_uuid(data['issue_id']):
            #         _issue_id = customer.shield_issues.fetch_one(data['issue_id'], True).get_json()['real_id']
                _notes = None
                if 'notes' in data and data['notes']:
                 _notes = data['notes']
                _customer_id = None
                if 'customer_id' in data and data['customer_id']:
                    _customer_id = data['customer_id']

                _issue_id = None
                if valid_uuid(data['issue_id']):
                  _issue_id = issues.fetch_one(data['issue_id'], True).get_json()['real_id']

                _shield_maintenance_id = None
                if valid_uuid(data['shield_id']):
                    _shield_maintenance_id = shields.fetch_one(data['shield_id'], True).get_json()['real_id']

                if _issue_id and _shield_maintenance_id:
                    _shield_issue = ShieldMaintenanceIssue\
                        .query\
                        .filter(ShieldMaintenanceIssue.uid == str(shield_issue_id))\
                        .first()
                    if _shield_issue:
                        _shield_issue.issue_id = _issue_id
                        _shield_issue.shield_maintenance_id = _shield_maintenance_id
                        _shield_issue.notes = _notes
                        _shield_issue.customer_id  = _customer_id
                        db.session.commit()

                        return jsonify({"info": "Shield maintenance issue edited successfully!"}), 200

        return jsonify({"error": "Shield maintenance issue not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(shield_issue_id):

    try:
        if valid_uuid(shield_issue_id) :
            _shield_issue = ShieldMaintenanceIssue\
                .query\
                .filter(ShieldMaintenanceIssue.uid == shield_issue_id)\
                .first()
            if _shield_issue:
                _shield_issue.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shield maintenance issue deactivated successfully!"}), 200

        return jsonify({"error": "Shield maintenance issue not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
