import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import ScreenhouseMaintenanceIssue

from app.core import user
from app.core import maintenance
from app.core import customer
from app.core.customer import screenhouses
from app.core.maintenance import issues

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def screenhouse_issue_obj(screenhouse_issue=False, real_id=False):
    _screenhouse_issue = {}
    if screenhouse_issue:
        if real_id:
            _screenhouse_issue["real_id"] = screenhouse_issue.id

        _screenhouse_issue["id"] = str(screenhouse_issue.uid)
        _screenhouse_issue["notes"] = screenhouse_issue.notes
        _screenhouse_issue["issue"] = issues.fetch_by_id(screenhouse_issue.issue_id).get_json()
        _screenhouse_issue["screenhouse_maintenance"] = screenhouses.fetch_by_id(screenhouse_issue.screenhouse_maintenance_id).get_json()
        _screenhouse_issue["maintenance"] = maintenance.fetch_by_id(screenhouse_issue.maintenance_id).get_json()
        _screenhouse_issue["customer"] = customer.fetch_by_id(screenhouse_issue.customer_id).get_json()
        _screenhouse_issue["created_by"] = creator_detail(screenhouse_issue.creator_id)
        _screenhouse_issue["status"] = status_name(screenhouse_issue.status)
        _screenhouse_issue["created"] = screenhouse_issue.created_at.strftime('%d %B %Y')
        _screenhouse_issue["modified"] = screenhouse_issue.modified_at

    return _screenhouse_issue

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        screenhouse_issue = db\
            .session\
            .query(ScreenhouseMaintenanceIssue)\
            .filter(ScreenhouseMaintenanceIssue.uid == uid)\
            .filter(ScreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if screenhouse_issue:
            response = screenhouse_issue_obj(screenhouse_issue, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        screenhouse_issue = db\
            .session\
            .query(ScreenhouseMaintenanceIssue)\
            .filter(ScreenhouseMaintenanceIssue.id == id)\
            .filter(ScreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if screenhouse_issue:
            response = screenhouse_issue_obj(screenhouse_issue)

    return jsonify(response)

def fetch_all():
    response = []

    _screenissue = db\
        .session\
        .query(ScreenhouseMaintenanceIssue)\
        .filter(ScreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
        .all()

    for _sc in  _screenissue:
        response.append(screenhouse_issue_obj(_sc))

    return jsonify(response)

def fetch_by_screenhouse_maintenance(id=0):
    response = {}
    if id:
        screenhouse_issue = db\
            .session\
            .query(ScreenhouseMaintenanceIssue)\
            .filter(ScreenhouseMaintenanceIssue.screenhouse_maintenance_id == id)\
            .filter(ScreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if screenhouse_issue:
            response = screenhouse_issue_obj(screenhouse_issue)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        screenhouse_issue = db\
            .session\
            .query(ScreenhouseMaintenanceIssue)\
            .filter(ScreenhouseMaintenanceIssue.maintenance_id == id)\
            .filter(ScreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if screenhouse_issue:
            response = screenhouse_issue_obj(screenhouse_issue)

    return jsonify(response)


def fetch_by_customer(id=0):
    response = []
    if id:
        screenhouse_issue = db\
            .session\
            .query(ScreenhouseMaintenanceIssue)\
            .filter(ScreenhouseMaintenanceIssue.customer_id == id)\
            .filter(ScreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .all()
        for sc_i in screenhouse_issue:
            response.append(screenhouse_issue_obj(sc_i))

    return jsonify(response)

# def fetch_by_customer(id=0):
#     response = {}
#     if id:
#         screenhouse_issue = db\
#             .session\
#             .query(ScreenhouseMaintenanceIssue)\
#             .filter(ScreenhouseMaintenanceIssue.customer_id == id)\
#             .filter(ScreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
#             .first()
#         if screenhouse_issue:
#             response = screenhouse_issue_obj(screenhouse_issue)

#     return jsonify(response)

def add_new(screenhouse_issue_data=None, return_obj=False):

    try:
        data = json.loads(screenhouse_issue_data)
        if data:

            _notes = None
            if 'notes' in data and data['notes']:
                _notes = data['notes']

            _issue_id = None
            if valid_uuid(data['issue_id']):
                _issue_id = issues.fetch_one(data['issue_id'], True).get_json()['real_id']

            _screenhouse_maintenance_id = None
            if valid_uuid(data['screenhouse_maintenance_id']):
                _screenhouse_maintenance_id = screenhouses.fetch_one(data['screenhouse_maintenance_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            # _customer_id = None
            # if valid_uuid(data['customer_id']):
            #     _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']
            _customer_id  = None
            if 'customer_id' in data and data['customer_id']:
                _customer_id = data['customer_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _issue_id and _screenhouse_maintenance_id and _maintenance_id and _customer_id and _creator:
                new_screenhouse_issue = ScreenhouseMaintenanceIssue(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    issue_id = _issue_id,
                    screenhouse_maintenance_id = _screenhouse_maintenance_id,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_screenhouse_issue)
                db.session.commit()
                if new_screenhouse_issue:
                    if return_obj:
                        return jsonify(fetch_one(new_screenhouse_issue.uid)), 200

                    return jsonify({"info": "Screenhouse maintenance issue added successfully!"}), 200

        return jsonify({"error": "Screenhouse maintenance issue not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(screenhouse_issue_id, screenhouse_issue_data=None, return_obj=False):

    try:
        data = json.loads(screenhouse_issue_data)
        if data:

            if valid_uuid(screenhouse_issue_id):

              

                _notes = None
                if 'notes' in data and data['notes']:
                 _notes = data['notes']

                _issue_id = None
                if valid_uuid(data['issue_id']):
                 _issue_id = issues.fetch_one(data['issue_id'], True).get_json()['real_id']

                _screenhouse_maintenance_id = None
                if valid_uuid(data['screenhouse_maintenance_id']):
                 _screenhouse_maintenance_id = screenhouses.fetch_one(data['screenhouse_maintenance_id'], True).get_json()['real_id']

                _maintenance_id = None
                if valid_uuid(data['maintenance_id']):
                 _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']
                
                _customer_id  = None
                if 'customer_id' in data and data['customer_id']:
                 _customer_id = data['customer_id']

                _creator = None
                if valid_uuid(data['creator_id']):
                 _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

              

                if  _maintenance_id:
                    _screenhouse_issue = ScreenhouseMaintenanceIssue\
                        .query\
                        .filter(ScreenhouseMaintenanceIssue.uid == str(screenhouse_issue_id))\
                        .first()
                    if _screenhouse_issue:
                        _screenhouse_issue.issue_id = _issue_id
                        _screenhouse_issue.notes = _notes
                        _screenhouse_issue.screenhouse_maintenance_id = _screenhouse_maintenance_id
                        _screenhouse_issue.maintenance_id = _maintenance_id
                        _screenhouse_issue.customer_id = _customer_id
                        _screenhouse_issue.creator_id = _creator


                  
                        db.session.commit()

                        return jsonify({"info": "Screenhouse maintenance issue edited successfully!"}), 200

        return jsonify({"error": "Screenhouse maintenance issue not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(screenhouse_issue_id):

    try:
        if valid_uuid(screenhouse_issue_id) :
            _screenhouse_issue = ScreenhouseMaintenanceIssue\
                .query\
                .filter(ScreenhouseMaintenanceIssue.uid == screenhouse_issue_id)\
                .first()
            if _screenhouse_issue:
                _screenhouse_issue.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Screenhouse maintenance issue deactivated successfully!"}), 200

        return jsonify({"error": "Screenhouse maintenance issue not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
