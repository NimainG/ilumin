import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import MaintenanceClosure

from app.core import user
from app.core import maintenance
from app.core.user import igh

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def closure_obj(closure=False, real_id=False):
    _closure = {}
    if closure:
        if real_id:
            _closure["real_id"] = closure.id

        _closure["id"] = str(closure.uid)
        _closure["notes"] = closure.notes
        _closure["igh_user"] = igh.fetch_by_id(closure.igh_user_id).get_json()
        _closure["maintenance"] = maintenance.fetch_by_id(closure.maintenance_id,True).get_json()
        _closure["created_by"] = creator_detail(closure.creator_id)
        _closure["status"] = status_name(closure.status)
        _closure["created"] = closure.created_at.strftime('%d %B %Y')
        _closure["modified"] = closure.modified_at

    return _closure

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        closure = db\
            .session\
            .query(MaintenanceClosure)\
            .filter(MaintenanceClosure.uid == uid)\
            .filter(MaintenanceClosure.status > config.STATUS_DELETED)\
            .first()
        if closure:
            response = closure_obj(closure, real_id)

    return jsonify(response)

def fetch_all():
    response = []

    closre = db\
        .session\
        .query(MaintenanceClosure)\
        .filter(MaintenanceClosure.status > config.STATUS_DELETED)\
        .all()

    for cl in closre:
        response.append(closure_obj(cl))

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        closure = db\
            .session\
            .query(MaintenanceClosure)\
            .filter(MaintenanceClosure.id == id)\
            .filter(MaintenanceClosure.status > config.STATUS_DELETED)\
            .first()
        if closure:
            response = closure_obj(closure)

    return jsonify(response)

def fetch_by_igh_user(id=0):
    response = {}
    if id:
        closure = db\
            .session\
            .query(MaintenanceClosure)\
            .filter(MaintenanceClosure.igh_user_id == id)\
            .filter(MaintenanceClosure.status > config.STATUS_DELETED)\
            .first()
        if closure:
            response = closure_obj(closure)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        closure = db\
            .session\
            .query(MaintenanceClosure)\
            .filter(MaintenanceClosure.maintenance_id == id)\
            .filter(MaintenanceClosure.status > config.STATUS_DELETED)\
            .first()
        if closure:
            response = closure_obj(closure)

    return jsonify(response)

def add_new(closure_data=None, return_obj=False):

    try:
        data = json.loads(closure_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _igh_user_id = None
            if valid_uuid(data['igh_user_id']):
                _igh_user_id = igh.fetch_one(data['igh_user_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _igh_user_id and _maintenance_id and _creator:
                new_closure = MaintenanceClosure(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    igh_user_id = _igh_user_id,
                    maintenance_id = _maintenance_id,
                    creator_id = _creator
                )
                db.session.add(new_closure)
                db.session.commit()
                if new_closure:
                    if return_obj:
                        return jsonify(fetch_one(new_closure.uid)), 200

                    return jsonify({"info": "Maintenance closure added successfully!"}), 200

        return jsonify({"error": "Maintenance closure not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(closure_id, closure_data=None, return_obj=False):

    try:
        data = json.loads(closure_data)
        if data:

            if valid_uuid(closure_id):

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                if _notes:
                    _closure = MaintenanceClosure\
                        .query\
                        .filter(MaintenanceClosure.uid == str(closure_id))\
                        .first()
                    if _closure:
                        _closure.notes = _notes
                        db.session.commit()

                        return jsonify({"info": "Maintenance closure edited successfully!"}), 200

        return jsonify({"error": "Maintenance closure not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(closure_id):

    try:
        if valid_uuid(closure_id) :
            _closure = MaintenanceClosure\
                .query\
                .filter(MaintenanceClosure.uid == closure_id)\
                .first()
            if _closure:
                _closure.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Maintenance closure deactivated successfully!"}), 200

        return jsonify({"error": "Maintenance closure not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
