import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import ScreenhouseMaintenance

from app.core import user
from app.core import maintenance
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def screenhouse_obj(screenhouse=False, real_id=False):
    _screenhouse = {}
    if screenhouse:
        if real_id:
            _screenhouse["real_id"] = screenhouse.id

        _screenhouse["id"] = str(screenhouse.uid)
        _screenhouse["notes"] = screenhouse.notes
        _screenhouse["customer_screenhouse"] = customer.screenhouses.fetch_by_id(screenhouse.customer_screenhouse_id).get_json()
        _screenhouse["customer"] = customer.fetch_by_id(screenhouse.customer_id).get_json()
        _screenhouse["issues"] = maintenance.screenhouse_issues.fetch_by_screenhouse_maintenance(screenhouse.id).get_json()
        _screenhouse["created_by"] = creator_detail(screenhouse.creator_id)
        _screenhouse["status"] = status_name(screenhouse.status)
        _screenhouse["created"] = screenhouse.created_at.strftime('%d %B %Y')
        _screenhouse["modified"] = screenhouse.modified_at

    return _screenhouse

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        screenhouse = db\
            .session\
            .query(ScreenhouseMaintenance)\
            .filter(ScreenhouseMaintenance.uid == uid)\
            .filter(ScreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if screenhouse:
            response = screenhouse_obj(screenhouse, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        screenhouse = db\
            .session\
            .query(ScreenhouseMaintenance)\
            .filter(ScreenhouseMaintenance.id == id)\
            .filter(ScreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if screenhouse:
            response = screenhouse_obj(screenhouse)

    return jsonify(response)

def fetch_all():
    response = []

    _screenh = db\
        .session\
        .query(ScreenhouseMaintenance)\
        .filter(ScreenhouseMaintenance.status > config.STATUS_DELETED)\
        .all()

    for _sc in  _screenh:
        response.append(screenhouse_obj(_sc))

    return jsonify(response)

def fetch_by_customer_screenhouse(id=0):
    response = {}
    if id:
        screenhouse = db\
            .session\
            .query(ScreenhouseMaintenance)\
            .filter(ScreenhouseMaintenance.customer_screenhouse_id == id)\
            .filter(ScreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if screenhouse:
            response = screenhouse_obj(screenhouse)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        screenhouse = db\
            .session\
            .query(ScreenhouseMaintenance)\
            .filter(ScreenhouseMaintenance.maintenance_id == id)\
            .filter(ScreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if screenhouse:
            response = screenhouse_obj(screenhouse)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        screenhouse = db\
            .session\
            .query(ScreenhouseMaintenance)\
            .filter(ScreenhouseMaintenance.customer_id == id)\
            .filter(ScreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if screenhouse:
            response = screenhouse_obj(screenhouse)

    return jsonify(response)

def add_new(screenhouse_data=None, return_obj=False):

    try:
        data = json.loads(screenhouse_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _customer_screenhouse_id = None
            if valid_uuid(data['customer_screenhouse_id']):
                _customer_screenhouse_id = customer.screenhouses.fetch_one(data['customer_screenhouse_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_screenhouse_id and _maintenance_id and _customer_id and _creator:
                new_screenhouse = ScreenhouseMaintenance(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    customer_screenhouse_id = _customer_screenhouse_id,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_screenhouse)
                db.session.commit()
                if new_screenhouse:
                    if return_obj:
                        return jsonify(fetch_one(new_screenhouse.uid)), 200

                    return jsonify({"info": "Screenhouse maintenance added successfully!"}), 200

        return jsonify({"error": "Screenhouse maintenance not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(screenhouse_id, screenhouse_data=None, return_obj=False):

    try:
        data = json.loads(screenhouse_data)
        if data:

            if valid_uuid(screenhouse_id):

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                if _notes:
                    _screenhouse = ScreenhouseMaintenance\
                        .query\
                        .filter(ScreenhouseMaintenance.uid == str(screenhouse_id))\
                        .first()
                    if _screenhouse:
                        _screenhouse.notes = _notes
                        db.session.commit()

                        return jsonify({"info": "Screenhouse maintenance edited successfully!"}), 200

        return jsonify({"error": "Screenhouse maintenance not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(screenhouse_id):

    try:
        if valid_uuid(screenhouse_id) :
            _screenhouse = ScreenhouseMaintenance\
                .query\
                .filter(ScreenhouseMaintenance.uid == screenhouse_id)\
                .first()
            if _screenhouse:
                _screenhouse.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Screenhouse maintenance deactivated successfully!"}), 200

        return jsonify({"error": "Screenhouse maintenance not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
