import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import ShieldMaintenance

from app.core import user
from app.core import maintenance
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def shield_obj(shield=False, real_id=False):
    _shield = {}
    if shield:
        if real_id:
            _shield["real_id"] = shield.id

        _shield["id"] = str(shield.uid)
        _shield["notes"] = shield.notes
        _shield["customer_shield"] = customer.shields.fetch_by_id(shield.customer_shield_id).get_json()
        _shield["customer"] = customer.fetch_by_id(shield.customer_id).get_json()
        _shield["issues"] = maintenance.shield_issues.fetch_by_shield_maintenance(shield.id).get_json()
        _shield["created_by"] = creator_detail(shield.creator_id)
        _shield["status"] = status_name(shield.status)
        _shield["created"] = shield.created_at.strftime('%d %B %Y')
        _shield["modified"] = shield.modified_at

    return _shield

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield = db\
            .session\
            .query(ShieldMaintenance)\
            .filter(ShieldMaintenance.uid == uid)\
            .filter(ShieldMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shield = db\
            .session\
            .query(ShieldMaintenance)\
            .filter(ShieldMaintenance.id == id)\
            .filter(ShieldMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)

def fetch_all():
    response = []

    _shields = db\
        .session\
        .query(ShieldMaintenance)\
        .filter(ShieldMaintenance.status > config.STATUS_DELETED)\
        .all()

    for _sh in  _shields:
        response.append(shield_obj(_sh))

    return jsonify(response)

def fetch_by_customer_shield(id=0):
    response = {}
    if id:
        shield = db\
            .session\
            .query(ShieldMaintenance)\
            .filter(ShieldMaintenance.customer_shield_id == id)\
            .filter(ShieldMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        shield = db\
            .session\
            .query(ShieldMaintenance)\
            .filter(ShieldMaintenance.maintenance_id == id)\
            .filter(ShieldMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        shield = db\
            .session\
            .query(ShieldMaintenance)\
            .filter(ShieldMaintenance.customer_id == id)\
            .filter(ShieldMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)

def add_new(shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_shield_id and _maintenance_id and _customer_id and _creator:
                new_shield = ShieldMaintenance(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    customer_shield_id = _customer_shield_id,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_shield)
                db.session.commit()
                if new_shield:
                    if return_obj:
                        return jsonify(fetch_one(new_shield.uid)), 200

                    return jsonify({"info": "Shield maintenance added successfully!"}), 200

        return jsonify({"error": "Shield maintenance not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(shield_id, shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:

            if valid_uuid(shield_id):

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                if _notes:
                    _shield = ShieldMaintenance\
                        .query\
                        .filter(ShieldMaintenance.uid == str(shield_id))\
                        .first()
                    if _shield:
                        _shield.notes = _notes
                        db.session.commit()

                        return jsonify({"info": "Shield maintenance edited successfully!"}), 200

        return jsonify({"error": "Shield maintenance not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(shield_id):

    try:
        if valid_uuid(shield_id) :
            _shield = ShieldMaintenance\
                .query\
                .filter(ShieldMaintenance.uid == shield_id)\
                .first()
            if _shield:
                _shield.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shield maintenance deactivated successfully!"}), 200

        return jsonify({"error": "Shield maintenance not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
