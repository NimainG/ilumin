import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv
from datetime import datetime
from app.config import activateConfig
from app.core.customer import shields
from sqlalchemy import extract

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import ShieldCompleteData

from app.core import user
from app.core.shield import types as shield_types
from app.core.shield import spears as shield_spears

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def shield_comcplte_data_obj(data=False, real_id=False):
    _data = {}
    if data:
        if real_id:
            _data["real_id"] = data.id

        _data["id"] = str(data.uid)
        _data["boron_id"] = data.boron_id
        _data["msg_counter"] = data.msg_counter
        _data["shield_actual_id"] = data.shield_actual_id
        _data["store_timestamp"] = data.store_timestamp
        _data["shield_fw_version"] = data.shield_fw_version

        _data["shield_battery_level"] = data.shield_battery_level
        _data["valave_positon"] = data.valave_positon
        _data["water_dispensed"] = data.water_dispensed
        _data["spear_actual_id"] = data.spear_actual_id

        _data["spear_fw_version"] = data.spear_fw_version
        _data["spear_battery_level"] = data.spear_battery_level
        _data["light_intensity"] = data.light_intensity
        _data["soil_moisture"] = data.soil_moisture

        _data["soil_humidity"] = data.soil_humidity
        _data["soil_temperature"] = data.soil_temperature
        _data["air_humidity"] = data.air_humidity
        _data["air_temperature"] = data.air_temperature
        _data["carbon_dioxide"] = data.carbon_dioxide

        _data["status"] = status_name(data.status)
        _data["created"] = data.created_at

    return _data

def fetch_all():
    response = []

    shield_data = db\
        .session\
        .query(ShieldCompleteData)\
        .filter(ShieldCompleteData.status > config.STATUS_DELETED)\
        .all()

    for sh in shield_data:
        response.append(shield_comcplte_data_obj(sh))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield_data = db\
            .session\
            .query(ShieldCompleteData)\
            .filter(ShieldCompleteData.uid == uid)\
            .filter(ShieldCompleteData.status > config.STATUS_DELETED)\
            .first()
        if shield_data:
            response = shield_comcplte_data_obj(shield_data, real_id)

    return jsonify(response)

def fetch_by_boron(id):
    response = {}
    if id:
      shield_data = db\
            .session\
            .query(ShieldCompleteData)\
            .filter(ShieldCompleteData.boron_id == id)\
            .filter(ShieldCompleteData.status > config.STATUS_DELETED)\
            .first()
      if shield_data:
            response = shield_comcplte_data_obj(shield_data)

    return jsonify(response)

def fetch_by_boron_all(id):
    response = []
    _data_date = datetime(2020, 3, 31, 0, 0, 0)
    if id:
      shield_data = db\
            .session\
            .query(ShieldCompleteData)\
            .filter(ShieldCompleteData.boron_id == id)\
            .filter(ShieldCompleteData.status > config.STATUS_DELETED)\
            .all()
      for sh in shield_data:
        response.append(shield_comcplte_data_obj(sh))

      return jsonify(response)


def fetch_by_date_boron_id(id,start_date,end_date):
    response = []
    if id:
      shield_data = db\
            .session\
            .query(ShieldCompleteData)\
            .filter(ShieldCompleteData.boron_id == id)\
            .filter(ShieldCompleteData.store_timestamp >= start_date)\
            .filter(ShieldCompleteData.store_timestamp <= end_date)\
            .filter(ShieldCompleteData.status > config.STATUS_DELETED)\
            .all()
      for sh in shield_data:
        response.append(shield_comcplte_data_obj(sh))

      return jsonify(response)






# def fetch_by_date_boron_id(start_date,end_date):
#     response = []
#     if start_date:
#       shield_data = db\
#             .session\
#             .query(ShieldCompleteData)\
#             .filter(ShieldCompleteData.store_timestamp <= start_date)\
#             .filter(ShieldCompleteData.store_timestamp >= end_date)\
#             .filter(ShieldCompleteData.status > config.STATUS_DELETED)\
#             .all()
#       for sh in shield_data:
#         response.append(shield_comcplte_data_obj(sh))

#       return jsonify(response)
 # .filter(extract(year, ShieldCompleteData.created_at)==_data_date.strftime('%Y'))\
            # .filter(extract(month, ShieldCompleteData.created_at)==_data_date.strftime('%m'))\
def fetch_by_id(id=0):
    response = {}
    if id:
        shield_data = db\
            .session\
            .query(ShieldCompleteData)\
            .filter(ShieldCompleteData.id == id)\
            .filter(ShieldCompleteData.status > config.STATUS_DELETED)\
            .first()
        if shield_data:
            response = shield_comcplte_data_obj(shield_data)

    return jsonify(response)

# def fetch_by_customer_shield(customer_shield_id):
#     response = {}
#     if customer_shield_id:
#         shield_data = db\
#             .session\
#             .query(ShieldCompleteData)\
#             .filter(ShieldCompleteData.customer_shield_id == customer_shield_id)\
#             .filter(ShieldCompleteData.status > config.STATUS_DELETED)\
#             .first()
#         if shield_data:
#             response = shield_comcplte_data_obj(shield_data)

#     return jsonify(response)

def add_new(shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:
            if 'boron_id' in data : 
                _boron_id = data['boron_id']
                _msg_counter = data['msg_counter']
                _shield_actual_id = data['shield_actual_id']
                _store_timestamp = data['store_timestamp']
                _shield_fw_version = data['shield_fw_version']
                _shield_battery_level = data['shield_battery_level']
                _valave_positon = data['valave_positon']
                _water_dispensed = data['water_dispensed']
                _spear_actual_id = data['spear_actual_id']
                _spear_fw_version = data['spear_fw_version']
                _spear_battery_level = data['spear_battery_level']
                _light_intensity = data['light_intensity']
                _soil_moisture = data['soil_moisture']
                _soil_humidity = data['soil_humidity']
                _soil_temperature = data['soil_temperature']
                _air_humidity = data['air_humidity']
                _air_temperature = data['air_temperature']
                _carbon_dioxide = data['carbon_dioxide']






                # if valid_uuid(data['customer_shield_id']):
                #     _customer_shield = shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

                if _boron_id:
                    new_shield_data = ShieldCompleteData(
                        uid = uuid.uuid4(),
                        boron_id = _boron_id,
                        status = config.STATUS_ACTIVE,
                        msg_counter = _msg_counter,
                        shield_actual_id = _shield_actual_id,
                        shield_fw_version = _shield_fw_version,
                        store_timestamp = _store_timestamp,
                        shield_battery_level = _shield_battery_level,
                        valave_positon = _valave_positon,
                        water_dispensed = _water_dispensed,
                        spear_actual_id = _spear_actual_id,
                        spear_fw_version = _spear_fw_version,
                        spear_battery_level = _spear_battery_level,
                        light_intensity = _light_intensity,
                        soil_moisture = _soil_moisture,
                        soil_humidity = _soil_humidity,
                        soil_temperature = _soil_temperature,
                        air_humidity = _air_humidity,
                        air_temperature = _air_temperature,
                        carbon_dioxide = _carbon_dioxide

                    )
                    db.session.add(new_shield_data)
                    db.session.commit()
                    if new_shield_data:
                        if return_obj:
                            return jsonify(fetch_one(new_shield_data.uid)), 200

                        return jsonify({"info": "Shield complete data added successfully!"}), 200

        return jsonify({"error": "Shield complete data not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
