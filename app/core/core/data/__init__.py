import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info

from app.models import db
from app.models import ShieldData

from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def data_source_name(source_id):
    _source = ""

    if source_id == config.DATA_CSV:
        _source = "CSV Upload"

    if source_id == config.DATA_AT:
        _source = "AfricasTalking"

    if source_id == config.DATA_G_SHEET:
        _source = "Google Sheet"

    if source_id == config.DATA_MANUAL:
        _source = "Manual"

    if source_id == config.DATA_MQTT:
        _source = "MQTT"

    return _source


def shield_data_obj(data=False, real_id=False):
    _data = {}
    if data:
        if real_id:
            _data["real_id"] = data.id

        _data["id"] = str(data.uid)
        _data["batch"] = data.batch
        _data["data_source"] = data_source_name(data.data_source)
        _data["customer_shield_id"] = customer.shields.fetch_by_id(data.customer_shield_id).get_json()
        _data["status"] = status_name(data.status)
        _data["created"] = data.created_at

    return _data

def fetch_all():
    response = []

    shield_data = db\
        .session\
        .query(ShieldData)\
        .filter(ShieldData.status > config.STATUS_DELETED)\
        .all()

    for sh in shield_data:
        response.append(shield_data_obj(sh))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield_data = db\
            .session\
            .query(ShieldData)\
            .filter(ShieldData.uid == uid)\
            .filter(ShieldData.status > config.STATUS_DELETED)\
            .first()
        if shield_data:
            response = shield_data_obj(shield_data, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shield_data = db\
            .session\
            .query(ShieldData)\
            .filter(ShieldData.id == id)\
            .filter(ShieldData.status > config.STATUS_DELETED)\
            .first()
        if shield_data:
            response = shield_data_obj(shield_data)

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id):
    response = {}
    if customer_shield_id:
        shield_data = db\
            .session\
            .query(ShieldData)\
            .filter(ShieldData.customer_shield_id == customer_shield_id)\
            .filter(ShieldData.status > config.STATUS_DELETED)\
            .first()
        if shield_data:
            response = shield_data_obj(shield_data)

    return jsonify(response)

def add_new(shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:
            if 'batch' in data and 'data_source' in data and 'customer_shield_id'\
             in data:

                _batch = data['batch']
                _data_source = data['data_source']
                _customer_shield = None

                if valid_uuid(data['customer_shield_id']):
                    _customer_shield = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

                if _batch and _data_source and _customer_shield:
                    new_shield_data = ShieldData(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        batch = _batch,
                        data_source = _data_source,
                        customer_shield_id = _customer_shield
                    )
                    db.session.add(new_shield_data)
                    db.session.commit()
                    if new_shield_data:
                        if return_obj:
                            return jsonify(fetch_one(new_shield_data.uid)), 200

                        return jsonify({"info": "Shield data added successfully!"}), 200

        return jsonify({"error": "Shield data not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
