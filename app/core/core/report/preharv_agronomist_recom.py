import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import PreharvestAgronomistRecommendation

from app.core import user

from app.core import customer
from app.core import report



from app.core.report import preharvest_checklist

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def preharv_ar_obj(preharv_ar=False, real_id=False):
    _preharv_ar = {}
    if preharv_ar:
        if real_id:
            _preharv_ar["real_id"] = preharv_ar.id

        _preharv_ar["id"] = str(preharv_ar.uid)
        _preharv_ar["recomend"] = preharv_ar.recomend
        _preharv_ar["preharvest_id"] = preharvest_checklist.fetch_by_id(preharv_ar.preharvest_id).get_json()
        _preharv_ar["report_id"] = report.fetch_by_id(preharv_ar.report_id).get_json()
        _preharv_ar["customer_id"] = customer.fetch_by_id(preharv_ar.customer_id).get_json()
        _preharv_ar["created_by"] = creator_detail(preharv_ar.creator_id)
        _preharv_ar["status"] = status_name(preharv_ar.status)
        _preharv_ar["created"] = preharv_ar.created_at.strftime('%d %B %Y')
        _preharv_ar["modified"] = preharv_ar.modified_at

    return _preharv_ar

def fetch_all():
    response = []

    preharv_ars = db\
        .session\
        .query(PreharvestAgronomistRecommendation)\
        .filter(PreharvestAgronomistRecommendation.status > config.STATUS_DELETED)\
        .all()

    for preharv_ar in preharv_ars:
        response.append(preharv_ar_obj(preharv_ar))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        preharv_ar = db\
            .session\
            .query(PreharvestAgronomistRecommendation)\
            .filter(PreharvestAgronomistRecommendation.uid == uid)\
            .filter(PreharvestAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if preharv_ar:
            response = preharv_ar_obj(preharv_ar, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        preharv_ar = db\
            .session\
            .query(PreharvestAgronomistRecommendation)\
            .filter(PreharvestAgronomistRecommendation.id == id)\
            .filter(PreharvestAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if preharv_ar:
            response = preharv_ar_obj(preharv_ar)

    return jsonify(response)

def fetch_by_scouting(scouting_id=0):
    response = {}
    if scouting_id:
        preharv_ars = db\
            .session\
            .query(PreharvestAgronomistRecommendation)\
            .filter(PreharvestAgronomistRecommendation.scouting_id == scouting_id)\
            .filter(PreharvestAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if preharv_ars:
            response = preharv_ar_obj(preharv_ars)

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        preharv_ars = db\
            .session\
            .query(PreharvestAgronomistRecommendation)\
            .filter(PreharvestAgronomistRecommendation.report_id == report_id)\
            .filter(PreharvestAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for preharv_ar in preharv_ars:
            response.append(preharv_ar_obj(preharv_ar))

    return jsonify(response)

def fetch_by_report_single(report_id=0):
    response = {}
    if report_id:
        preharv_ars = db\
            .session\
            .query(PreharvestAgronomistRecommendation)\
            .filter(PreharvestAgronomistRecommendation.report_id == report_id)\
            .filter(PreharvestAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if preharv_ars:
            response = preharv_ar_obj(preharv_ars)
       

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        preharv_ars = db\
            .session\
            .query(PreharvestAgronomistRecommendation)\
            .filter(PreharvestAgronomistRecommendation.customer_id == customer_id)\
            .filter(PreharvestAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for preharv_ar in preharv_ars:
            response.append(preharv_ar_obj(preharv_ar))

    return jsonify(response)

def add_new(preharv_ar_data=None, return_obj=False):

    try:
        data = json.loads(preharv_ar_data)
        if data:

           

            _recomend = None
            if 'recomend' in data:
                _recomend = data['recomend']

         
            _preharvest_id = None
            if valid_uuid(data['preharvest_id']):
                _preharvest_id = preharvest_checklist.fetch_one(data['preharvest_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_preharv_ar = PreharvestAgronomistRecommendation(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    recomend = _recomend,
                    preharvest_id = _preharvest_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_preharv_ar)
                db.session.commit()
                if new_preharv_ar:
                    if return_obj:
                        return jsonify(fetch_one(new_preharv_ar.uid)), 200

                    return jsonify({"info": "Preharvest agronomist recommendation report added successfully!"}), 200

        return jsonify({"error": "Preharvest agronomist recommendation report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(preharv_ar_id, preharv_ar_data=None, return_obj=False):

    try:
        data = json.loads(preharv_ar_data)
        if data:

            if valid_uuid(preharv_ar_id):

               

                _recomend = None
                if 'recomend' in data:
                    _recomend = data['recomend']

               
                _preharv_ar = PreharvestAgronomistRecommendation\
                    .query\
                    .filter(PreharvestAgronomistRecommendation.uid == str(preharv_ar_id))\
                    .first()
                if _preharv_ar:
                    
                    _preharv_ar.recomend = _recomend
                   
                    db.session.commit()

                    return jsonify({"info": "Preharvest agronomist recommendation report edited successfully!"}), 200

        return jsonify({"error": "Preharvest agronomist recommendation report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(preharv_ar_id):

    try:

        if valid_uuid(preharv_ar_id) :
            _preharv_ar = PreharvestAgronomistRecommendation\
                .query\
                .filter(PreharvestAgronomistRecommendation.uid == preharv_ar_id)\
                .first()
            if _preharv_ar:
                _preharv_ar.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Preharvest agronomist recommendation report deactivated successfully!"}), 200

        return jsonify({"error": "Preharvest agronomist recommendation report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def delete(preharv_ar_id):

    try:

        if valid_uuid(preharv_ar_id) :
            _preharv_ar = PreharvestAgronomistRecommendation\
                .query\
                .filter(PreharvestAgronomistRecommendation.uid == preharv_ar_id)\
                .first()
            if _preharv_ar:
                _preharv_ar.status = config.STATUS_DELETED
                db.session.commit()

                return jsonify({"info": "Preharvest agronomist recommendation report deleted successfully!"}), 200

        return jsonify({"error": "Preharvest agronomist recommendation report not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
