import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import AgrochemicalAgronomistRecommendation

from app.core import user

from app.core import customer
from app.core import report


from app.core.report import agrochemical_application

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def agrochem_ar_obj(agrochem_ar=False, real_id=False):
    _agrochem_ar = {}
    if agrochem_ar:
        if real_id:
            _agrochem_ar["real_id"] = agrochem_ar.id

        _agrochem_ar["id"] = str(agrochem_ar.uid)
        _agrochem_ar["recomend"] = agrochem_ar.recomend
        _agrochem_ar["agronomy_id"] = agrochemical_application.fetch_by_id(
            agrochem_ar.agronomy_id).get_json()
        _agrochem_ar["report_id"] = report.fetch_by_id(
            agrochem_ar.report_id).get_json()
        _agrochem_ar["customer_id"] = customer.fetch_by_id(
            agrochem_ar.customer_id).get_json()
        _agrochem_ar["created_by"] = creator_detail(agrochem_ar.creator_id)
        _agrochem_ar["status"] = status_name(agrochem_ar.status)
        _agrochem_ar["created"] = agrochem_ar.created_at.strftime('%d %B %Y')
        _agrochem_ar["modified"] = agrochem_ar.modified_at

    return _agrochem_ar


def fetch_all():
    response = []

    agrochem_ars = db\
        .session\
        .query(AgrochemicalAgronomistRecommendation)\
        .filter(AgrochemicalAgronomistRecommendation.status > config.STATUS_DELETED)\
        .all()

    for agrochem_ar in agrochem_ars:
        response.append(agrochem_ar_obj(agrochem_ar))

    return jsonify(response)


def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        agrochem_ar = db\
            .session\
            .query(AgrochemicalAgronomistRecommendation)\
            .filter(AgrochemicalAgronomistRecommendation.uid == uid)\
            .filter(AgrochemicalAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if agrochem_ar:
            response = agrochem_ar_obj(agrochem_ar, real_id)

    return jsonify(response)


def fetch_by_id(id=0):
    response = {}
    if id:
        agrochem_ar = db\
            .session\
            .query(AgrochemicalAgronomistRecommendation)\
            .filter(AgrochemicalAgronomistRecommendation.id == id)\
            .filter(AgrochemicalAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if agrochem_ar:
            response = agrochem_ar_obj(agrochem_ar)

    return jsonify(response)


def fetch_by_scouting(scouting_id=0):
    response = {}
    if scouting_id:
        agrochem_ars = db\
            .session\
            .query(AgrochemicalAgronomistRecommendation)\
            .filter(AgrochemicalAgronomistRecommendation.scouting_id == scouting_id)\
            .filter(AgrochemicalAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if agrochem_ars:
            response = agrochem_ar_obj(agrochem_ars)

    return jsonify(response)


def fetch_by_report(report_id=0):
    response = []
    if report_id:
        agrochem_ars = db\
            .session\
            .query(AgrochemicalAgronomistRecommendation)\
            .filter(AgrochemicalAgronomistRecommendation.report_id == report_id)\
            .filter(AgrochemicalAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for agrochem_ar in agrochem_ars:
            response.append(agrochem_ar_obj(agrochem_ar))

    return jsonify(response)


def fetch_by_report_single(report_id=0):
    response = {}
    if report_id:
        agrochem_ars = db\
            .session\
            .query(AgrochemicalAgronomistRecommendation)\
            .filter(AgrochemicalAgronomistRecommendation.report_id == report_id)\
            .filter(AgrochemicalAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if agrochem_ars:
            response = agrochem_ar_obj(agrochem_ars)

    return jsonify(response)


def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        agrochem_ars = db\
            .session\
            .query(AgrochemicalAgronomistRecommendation)\
            .filter(AgrochemicalAgronomistRecommendation.customer_id == customer_id)\
            .filter(AgrochemicalAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for agrochem_ar in agrochem_ars:
            response.append(agrochem_ar_obj(agrochem_ar))

    return jsonify(response)


def add_new(agrochem_ar_data=None, return_obj=False):

    try:
        data = json.loads(agrochem_ar_data)
        if data:

            _recomend = None
            if 'recomend' in data:
                _recomend = data['recomend']

            _agronomy_id = None
            if valid_uuid(data['agronomy_id']):
                _agronomy_id = agrochemical_application.fetch_one(
                    data['agronomy_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(
                    data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(
                    data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(
                    data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_agrochem_ar = AgrochemicalAgronomistRecommendation(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    recomend=_recomend,
                    agronomy_id=_agronomy_id,
                    report_id=_report_id,
                    customer_id=_customer_id,
                    creator_id=_creator_id
                )
                db.session.add(new_agrochem_ar)
                db.session.commit()
                if new_agrochem_ar:
                    if return_obj:
                        return jsonify(fetch_one(new_agrochem_ar.uid)), 200

                    return jsonify({"info": "Agrochemical Application agronomist recommendation report added successfully!"}), 200

        return jsonify({"error": "Agrochemical Application agronomist recommendation report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def edit(agrochem_ar_id, agrochem_ar_data=None, return_obj=False):

    try:
        data = json.loads(agrochem_ar_data)
        if data:

            if valid_uuid(agrochem_ar_id):

                _recomend = None
                if 'recomend' in data:
                    _recomend = data['recomend']

                _agrochem_ar = AgrochemicalAgronomistRecommendation\
                    .query\
                    .filter(AgrochemicalAgronomistRecommendation.uid == str(agrochem_ar_id))\
                    .first()
                if _agrochem_ar:

                    _agrochem_ar.recomend = _recomend

                    db.session.commit()

                    return jsonify({"info": "Agrochemical   agronomist recommendation report edited successfully!"}), 200

        return jsonify({"error": "Agrochemical Application agronomist recommendation report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def deactivate(agrochem_ar_id):

    try:

        if valid_uuid(agrochem_ar_id):
            _agrochem_ar = AgrochemicalAgronomistRecommendation\
                .query\
                .filter(AgrochemicalAgronomistRecommendation.uid == agrochem_ar_id)\
                .first()
            if _agrochem_ar:
                _agrochem_ar.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Agrochemical Application agronomist recommendation report deactivated successfully!"}), 200

        return jsonify({"error": "Agrochemical Application agronomist recommendation report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def delete(agrochem_ar_id):

    try:

        if valid_uuid(agrochem_ar_id):
            _agrochem_ar = AgrochemicalAgronomistRecommendation\
                .query\
                .filter(AgrochemicalAgronomistRecommendation.uid == agrochem_ar_id)\
                .first()
            if _agrochem_ar:
                _agrochem_ar.status = config.STATUS_DELETED
                db.session.commit()

                return jsonify({"info": "Agrochemical Application agronomist recommendation report deleted successfully!"}), 200

        return jsonify({"error": "Agrochemical Application agronomist recommendation report not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
