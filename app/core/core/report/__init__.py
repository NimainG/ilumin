import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Report

from app.core import user
from app.core import media
from app.core import customer
from app.core.report import scouting_agronomist_recommendations
from app.core.report import waste_n_p_agronomy_recommend

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def report_type_name(report_type):
    _report_type = ""

    if report_type:
        if report_type == config.REPORT_WASTE_POLLUTION_MANAGEMENT:
            _report_type = 'Waste pollution management'

        if report_type == config.REPORT_FERTILIZER_APPLICATION:
            _report_type = 'Fertilizer application'

        if report_type == config.REPORT_AGROCHEMICAL_APPLICATION:
            _report_type = 'Aggrochemical application'

        if report_type == config.REPORT_PREHARVEST_CHECKLIST:
            _report_type = 'Pre-harvest checklist'

        if report_type == config.REPORT_HARVEST_DETAIL:
            _report_type = 'Harvest detail'

        if report_type == config.REPORT_SCOUTING_REPORT:
            _report_type = 'Scouting report'

        if report_type == config.REPORT_OTHERS:
            _report_type = 'Others'
        if report_type == config.REPORT_TRACEABILITY:
            _report_type = 'Traceability Report'

    return _report_type


def report_obj(report=False, real_id=False):
    _report = {}
    if report:
        if real_id:
            _report["real_id"] = report.id

        _report["id"] = str(report.uid)
        _report["main_id"]= report.id
        _report["type"] = report_type_name(report.type)
        _report["score"] = report.score
        # _report["recomend_id"] = scouting_agronomist_recommendations.fetch_one(
        #     report.recomend_id, True).get_json()
        _report["recomend_id"] =  report.recomend_id
        _report["customer_id"] = customer.fetch_by_id(
            report.customer_id).get_json()
        _report["created_by"] = creator_detail(report.creator_id)
        _report["status"] = status_name(report.status)
        _report["created"] = report.created_at.strftime('%d %B %Y')
        if report.modified_at != None:
         _report["modified"] = report.modified_at.strftime('%d %B %Y')  
        else:
          _report["modified"] = report.modified_at

    return _report


def fetch_all():
    response = []

    reports = db\
        .session\
        .query(Report)\
        .filter(Report.status > config.STATUS_DELETED)\
        .order_by(Report.id.desc())\
        .all()

    for report in reports:
        response.append(report_obj(report))

    return jsonify(response)


def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        report = db\
            .session\
            .query(Report)\
            .filter(Report.uid == uid)\
            .filter(Report.status > config.STATUS_DELETED)\
            .first()
        if report:
            response = report_obj(report, real_id)

    return jsonify(response)


def fetch_by_id(id=0):
    response = {}
    if id:
        report = db\
            .session\
            .query(Report)\
            .filter(Report.id == id)\
            .filter(Report.status > config.STATUS_DELETED)\
            .first()
        if report:
            response = report_obj(report)

    return jsonify(response)


def fetch_by_type(type=0):
    response = []
    if type:
        reports = db\
            .session\
            .query(Report)\
            .filter(Report.type == type)\
            .filter(Report.status > config.STATUS_DELETED)\
            .order_by(Report.id.desc())\
            .all()
        for report in reports:
            response.append(report_obj(report))

    return jsonify(response)


def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        reports = db\
            .session\
            .query(Report)\
            .filter(Report.customer_id == customer_id)\
            .filter(Report.status > config.STATUS_DELETED)\
            .order_by(Report.id.desc())\
            .all()
        for report in reports:
            response.append(report_obj(report))

    return jsonify(response)
def fetch_by_customer_recent(customer_id=0):
    response = {}
    if customer_id:
        reports = db\
            .session\
            .query(Report)\
            .filter(Report.customer_id == customer_id)\
            .filter(Report.status > config.STATUS_DELETED)\
            .first()
        if reports:
            response = report_obj(reports)
       
    return jsonify(response)


def add_new(report_data=None, return_obj=False):

    try:
        data = json.loads(report_data)
        if data:

            _type = None
            if 'type' in data:
                _type = data['type']

            _score = None
            if 'score' in data:
                _score = data['score']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(
                    data['customer_id'], True).get_json()['real_id']

            _recomend_id = None
            if 'recomend_id' in data:
                    _recomend_id = data['recomend_id']


            # _recomend_id = None
            # if valid_uuid(data['recomend_id']):
            #     _recomend_id = scouting_agronomist_recommendations.fetch_one(
            #         data['recomend_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(
                    data['creator_id'], True).get_json()['real_id']

            if _type and _customer_id and _creator_id:
                new_report = Report(
                    uid=uuid.uuid4(),
                    status=config.STATUS_ACTIVE,
                    type=_type,
                    score=_score,
                    recomend_id=_recomend_id,
                    # recomend_id_wnp = None,
                    customer_id=_customer_id,
                    creator_id=_creator_id
                )
                db.session.add(new_report)
                db.session.commit()
                if new_report:
                    if return_obj:
                        return jsonify(fetch_one(new_report.uid)), 200

                    return jsonify({"info": "Report added successfully!"}), 200

        return jsonify({"error": "Report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def edit(report_id, report_data=None, return_obj=False):

    try:
        data = json.loads(report_data)
        if data:

            if valid_uuid(report_id):

                _type = None
                if 'type' in data:
                    _type = data['type']

                _score = None
                if 'score' in data:
                    _score = data['score']

                _creator_id = None
                if valid_uuid(data['creator_id']):
                    _creator_id = user.fetch_one(
                        data['creator_id'], True).get_json()['real_id']
                _recomend_id = None
                if 'recomend_id' in data:
                    _recomend_id = data['recomend_id']

               

                    # _recomend_id = scouting_agronomist_recommendations.fetch_one(
                    #     data['recomend_id'], True).get_json()['real_id']

                if _type and _score:
                    _report = Report\
                        .query\
                        .filter(Report.uid == str(report_id))\
                        .first()
                    if _report:
                        _report.type = _type
                        _report.score = _score
                        _report.creator_id = _creator_id
                        _report.recomend_id = _recomend_id
                        db.session.commit()

                        return jsonify({"info": "Report edited successfully!"}), 200

        return jsonify({"error": "Report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def deactivate(report_id):

    try:

        if valid_uuid(report_id):
            _report = Report\
                .query\
                .filter(Report.uid == report_id)\
                .first()
            if _report:
                _report.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Report deactivated successfully!"}), 200

        return jsonify({"error": "Report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
