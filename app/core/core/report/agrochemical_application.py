import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import AgrochemicalApplication

from app.core import user
from app.core import media
from app.core import customer
from app.core import report
from app.core import supply
from app.core import pest
from app.core.supply import application_methods
from app.core.supply import components
from app.core.customer import shields

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def aa_obj(aa=False, real_id=False):
    _aa = {}
    if aa:
        if real_id:
            _aa["real_id"] = aa.id

        _aa["id"] = str(aa.uid)
        _aa["date_time_applied"] = aa.date_time_applied
        _aa["rei"] = aa.rei
        _aa["phi"] = aa.phi
        _aa["quantity"] = aa.quantity
        _aa["sources1"]= aa.sources1
        _aa["re_entry_date"] = aa.re_entry_date
        _aa["preharvest_interval_expiration_date"] = aa.preharvest_interval_expiration_date
        _aa["pest"] = pest.fetch_by_id(aa.pest_id).get_json()
        _aa["supply_application_method"] = application_methods.fetch_by_id(aa.supply_application_method_id).get_json()
        _aa["agrochemical"] = supply.fetch_by_id(aa.agrochemical_id).get_json()
        _aa["agrochemical_main_component"] = supply.components.fetch_by_id(aa.agrochemical_main_component).get_json()
        _aa["customer_shield_id"] = customer.shields.fetch_by_id(aa.customer_shield_id).get_json()
        _aa["report_id"] = report.fetch_by_id(aa.report_id).get_json()
        _aa["customer_id"] = customer.fetch_by_id(aa.customer_id).get_json()
        _aa["created_by"] = creator_detail(aa.creator_id)
        _aa["status"] = status_name(aa.status)
        _aa["created"] = aa.created_at.strftime('%d %B %Y')
        _aa["modified"] = aa.modified_at

    return _aa

def fetch_all():
    response = []

    aas = db\
        .session\
        .query(AgrochemicalApplication)\
        .filter(AgrochemicalApplication.status > config.STATUS_DELETED)\
        .order_by(AgrochemicalApplication.id.desc())\
        .all()

    for aa in aas:
        response.append(aa_obj(aa))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        aa = db\
            .session\
            .query(AgrochemicalApplication)\
            .filter(AgrochemicalApplication.uid == uid)\
            .filter(AgrochemicalApplication.status > config.STATUS_DELETED)\
            .first()
        if aa:
            response = aa_obj(aa, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        aa = db\
            .session\
            .query(AgrochemicalApplication)\
            .filter(AgrochemicalApplication.id == id)\
            .filter(AgrochemicalApplication.status > config.STATUS_DELETED)\
            .first()
        if aa:
            response = aa_obj(aa)

    return jsonify(response)

def fetch_by_customer_recent(customer_id=0):
    response = {}
    if customer_id:
        aas = db\
            .session\
            .query(AgrochemicalApplication)\
            .filter(AgrochemicalApplication.customer_id == customer_id)\
            .filter(AgrochemicalApplication.status > config.STATUS_DELETED)\
            .first()
        if aas:
            response = aa_obj(aas)

    return jsonify(response)


def fetch_by_customer_date_range(customer_id, start_date, end_date):

    response = []
    if customer_id and start_date and end_date:
        fas = db\
            .session\
            .query(AgrochemicalApplication)\
            .filter(AgrochemicalApplication.customer_id == customer_id)\
            .filter(AgrochemicalApplication.created_at <= end_date)\
            .filter(AgrochemicalApplication.created_at >= start_date)\
            .filter(AgrochemicalApplication.status > config.STATUS_DELETED)\
            .order_by(AgrochemicalApplication.id.desc())\
            .all()
        for fa in fas:
            response.append(aa_obj(fa))

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=0):
    response = []
    if customer_shield_id:
        aas = db\
            .session\
            .query(AgrochemicalApplication)\
            .filter(AgrochemicalApplication.customer_shield_id == customer_shield_id)\
            .filter(AgrochemicalApplication.status > config.STATUS_DELETED)\
            .order_by(AgrochemicalApplication.id.desc())\
            .all()
        for aa in aas:
            response.append(aa_obj(aa))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        aas = db\
            .session\
            .query(AgrochemicalApplication)\
            .filter(AgrochemicalApplication.report_id == report_id)\
            .filter(AgrochemicalApplication.status > config.STATUS_DELETED)\
            .order_by(AgrochemicalApplication.id.desc())\
            .all()
        for aa in aas:
            response.append(aa_obj(aa))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        aas = db\
            .session\
            .query(AgrochemicalApplication)\
            .filter(AgrochemicalApplication.customer_id == customer_id)\
            .filter(AgrochemicalApplication.status > config.STATUS_DELETED)\
            .order_by(AgrochemicalApplication.id.desc())\
            .all()
        for aa in aas:
            response.append(aa_obj(aa))

    return jsonify(response)

def add_new(aa_data=None, return_obj=False):

    try:
        data = json.loads(aa_data)
        if data:

            _date_time_applied = None
            if 'date_time_applied' in data:
                _date_time_applied = data['date_time_applied']

            _rei = None
            if 'rei' in data:
                _rei = data['rei']
            _sources1 = None
            if 'sources1' in data:
                _sources1 = data['sources1']

            _phi = None
            if 'phi' in data:
                _phi = data['phi']

            _quantity = None
            if 'quantity' in data:
             _quantity = data['quantity']


            _re_entry_date = None
            if 're_entry_date' in data:
                _re_entry_date = data['re_entry_date']

            _preharvest_interval_expiration_date = None
            if 'preharvest_interval_expiration_date' in data:
                _preharvest_interval_expiration_date = data['preharvest_interval_expiration_date']

            _pest_id = None
            if valid_uuid(data['pest_id']):
                _pest_id = pest.fetch_one(data['pest_id'], True).get_json()['real_id']

            _supply_application_method_id = None
            if valid_uuid(data['supply_application_method_id']):
                _supply_application_method_id = supply.application_methods.fetch_one(data['supply_application_method_id'], True).get_json()['real_id']

            _agrochemical_id = None
            if valid_uuid(data['agrochemical_id']):
                _agrochemical_id = supply.fetch_one(data['agrochemical_id'], True).get_json()['real_id']

            _agrochemical_main_component_id = None
            if valid_uuid(data['agrochemical_main_component']):
                _agrochemical_main_component_id = components.fetch_one(data['agrochemical_main_component'], True).get_json()['real_id']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _report_id = None
            if 'report_id' in data:
                _report_id = data['report_id']
            # _report_id = None
            # if valid_uuid(data['report_id']):
            #     _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_aa = AgrochemicalApplication(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    date_time_applied = _date_time_applied,
                    sources1 = _sources1,
                    rei = _rei,
                    phi = _phi,
                    quantity = _quantity,
                    re_entry_date = _re_entry_date,
                    preharvest_interval_expiration_date = _preharvest_interval_expiration_date,
                    pest_id = _pest_id,
                    supply_application_method_id = _supply_application_method_id,
                    agrochemical_id = _agrochemical_id,
                    agrochemical_main_component = _agrochemical_main_component_id,
                    customer_shield_id = _customer_shield_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_aa)
                db.session.commit()
                if new_aa:
                    if return_obj:
                        return jsonify(fetch_one(new_aa.uid)), 200

                    return jsonify({"info": "Agrochemical application added successfully!"}), 200

        return jsonify({"error": "Agrochemical application not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(aa_id, aa_data=None, return_obj=False):

    try:
        data = json.loads(aa_data)
        if data:

            if valid_uuid(aa_id):

                _date_time_applied = None
                if 'date_time_applied' in data:
                    _date_time_applied = data['date_time_applied']

                _rei = None
                if 'rei' in data:
                    _rei = data['rei']

                _phi = None
                if 'phi' in data:
                    _phi = data['phi']

                _quantity = None
                if 'quantity' in data:
                 _quantity = data['quantity']


                _re_entry_date = None
                if 're_entry_date' in data:
                    _re_entry_date = data['re_entry_date']

                _preharvest_interval_expiration_date = None
                if 'preharvest_interval_expiration_date' in data:
                    _preharvest_interval_expiration_date = data['preharvest_interval_expiration_date']

                _pest_id = None
                if valid_uuid(data['pest_id']):
                    _pest_id = pest.fetch_one(data['pest_id'], True).get_json()['real_id']

                _supply_application_method_id = None
                if valid_uuid(data['supply_application_method_id']):
                    _supply_application_method_id = supply.application_methods.fetch_one(data['supply_application_method_id'], True).get_json()['real_id']

                _agrochemical_id = None
                if valid_uuid(data['agrochemical_id']):
                    _agrochemical_id = supply.fetch_one(data['agrochemical_id'], True).get_json()['real_id']

                _agrochemical_main_component_id = None
                if valid_uuid(data['agrochemical_main_component_id']):
                    _agrochemical_main_component_id = supply.components.fetch_one(data['agrochemical_main_component_id'], True).get_json()['real_id']

                if _agrochemical_id:
                    _aa = AgrochemicalApplication\
                        .query\
                        .filter(AgrochemicalApplication.uid == str(aa_id))\
                        .first()
                    if _aa:
                        _aa.date_time_applied = _date_time_applied
                        _aa.rei = _rei
                        _aa.phi = _phi
                        _aa.quantity = _quantity
                        _aa.re_entry_date = _re_entry_date
                        _aa.preharvest_interval_expiration_date = _preharvest_interval_expiration_date
                        _aa.pest_id = _pest_id
                        _aa.supply_application_method_id = _supply_application_method_id
                        _aa.agrochemical_id = _agrochemical_id
                        _aa.agrochemical_main_component = _agrochemical_main_component_id
                        db.session.commit()

                        return jsonify({"info": "Agrochemical application edited successfully!"}), 200

        return jsonify({"error": "Agrochemical application not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(aa_id):

    try:

        if valid_uuid(aa_id) :
            _aa = AgrochemicalApplication\
                .query\
                .filter(AgrochemicalApplication.uid == aa_id)\
                .first()
            if _aa:
                _aa.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Agrochemical application deactivated successfully!"}), 200

        return jsonify({"error": "Agrochemical application not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def delete(aa_id):

    try:

        if valid_uuid(aa_id) :
            _greenhouse_type = AgrochemicalApplication\
                .query\
                .filter(AgrochemicalApplication.uid == aa_id)\
                .first()
            if _greenhouse_type:
                db.session.delete(_greenhouse_type)
                db.session.commit()

                return jsonify({"info": " Report is deleted successfully!"}), 200

        return jsonify({"error": " Report is not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
