import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import TraceabilityHarvestQuery

from app.core import user

from app.core.report import traceability

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def traceability_hq_obj(traceability_hq=False, real_id=False):
    _traceability_hq = {}
    if traceability_hq:
        if real_id:
            _traceability_hq["real_id"] = traceability_hq.id

        _traceability_hq["id"] = str(traceability_hq.uid)
        _traceability_hq["harvest_date"] = traceability_hq.harvest_date
        _traceability_hq["harvest_time"] = traceability_hq.harvest_time
        _traceability_hq["traceability_report_id"] = traceability.fetch_by_id(traceability_hq.traceability_report_id).get_json()
        _traceability_hq["created_by"] = creator_detail(traceability_hq.creator_id)
        _traceability_hq["status"] = status_name(traceability_hq.status)
        _traceability_hq["created"] = traceability_hq.created_at.strftime('%d %B %Y')
        _traceability_hq["modified"] = traceability_hq.modified_at

    return _traceability_hq

def fetch_all():
    response = []

    traceabilities = db\
        .session\
        .query(TraceabilityHarvestQuery)\
        .filter(TraceabilityHarvestQuery.status > config.STATUS_DELETED)\
        .all()

    for traceability_hq in traceabilities:
        response.append(traceability_hq_obj(traceability_hq))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        traceability_hq = db\
            .session\
            .query(TraceabilityHarvestQuery)\
            .filter(TraceabilityHarvestQuery.uid == uid)\
            .filter(TraceabilityHarvestQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_hq:
            response = traceability_hq_obj(traceability_hq, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        traceability_hq = db\
            .session\
            .query(TraceabilityHarvestQuery)\
            .filter(TraceabilityHarvestQuery.id == id)\
            .filter(TraceabilityHarvestQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_hq:
            response = traceability_hq_obj(traceability_hq)

    return jsonify(response)

def fetch_by_traceability_report(traceability_report_id=0):
    response = []
    if traceability_report_id:
        traceabilities = db\
            .session\
            .query(TraceabilityHarvestQuery)\
            .filter(TraceabilityHarvestQuery.traceability_report_id == traceability_report_id)\
            .filter(TraceabilityHarvestQuery.status > config.STATUS_DELETED)\
            .all()
        for traceability_hq in traceabilities:
            response.append(traceability_hq_obj(traceability_hq))

    return jsonify(response)

def add_new(traceability_hq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_hq_data)
        if data:

            _harvest_date = None
            if 'harvest_date' in data:
                _harvest_date = data['harvest_date']

            _harvest_time = None
            if 'harvest_time' in data:
                _harvest_time = data['harvest_time']

            _traceability_report_id = None
            if valid_uuid(data['traceability_report_id']):
                _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _traceability_report_id and _creator_id:
                new_traceability_hq = TraceabilityHarvestQuery(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    harvest_date = _harvest_date,
                    harvest_time = _harvest_time,
                    traceability_report_id = _traceability_report_id,
                    creator_id = _creator_id
                )
                db.session.add(new_traceability_hq)
                db.session.commit()
                if new_traceability_hq:
                    if return_obj:
                        return jsonify(fetch_one(new_traceability_hq.uid)), 200

                    return jsonify({"info": "Traceability harvest queries report added successfully!"}), 200

        return jsonify({"error": "Traceability harvest queries report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(traceability_hq_id, traceability_hq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_hq_data)
        if data:

            if valid_uuid(traceability_hq_id):

                _harvest_date = None
                if 'harvest_date' in data:
                    _harvest_date = data['harvest_date']

                _harvest_time = None
                if 'harvest_time' in data:
                    _harvest_time = data['harvest_time']

                _traceability_report_id = None
                if valid_uuid(data['traceability_report_id']):
                    _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

                if _traceability_report_id:
                    _traceability_hq = TraceabilityHarvestQuery\
                        .query\
                        .filter(TraceabilityHarvestQuery.uid == str(traceability_hq_id))\
                        .first()
                    if _traceability_hq:
                        _traceability_hq.harvest_date = _harvest_date
                        _traceability_hq.harvest_time = _harvest_time
                        _traceability_hq.traceability_report_id = _traceability_report_id
                        db.session.commit()

                        return jsonify({"info": "Traceability harvest queries report edited successfully!"}), 200

        return jsonify({"error": "Traceability harvest queries report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(traceability_hq_id):

    try:

        if valid_uuid(traceability_hq_id) :
            _traceability_hq = TraceabilityHarvestQuery\
                .query\
                .filter(TraceabilityHarvestQuery.uid == traceability_hq_id)\
                .first()
            if _traceability_hq:
                _traceability_hq.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Traceability harvest queries report deactivated successfully!"}), 200

        return jsonify({"error": "Traceability harvest queries report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
