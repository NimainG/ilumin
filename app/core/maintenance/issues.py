import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Issue

from app.core import user
from app.core import maintenance

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def issue_obj(issue=False, real_id=False):
    _issue = {}
    if issue:
        if real_id:
            _issue["real_id"] = issue.id

        _issue["id"] = str(issue.uid)
        _issue["name"] = issue.name
        _issue["description"] = issue.description
        _issue["applies_to"] = maintenance.maintenance_type_name(issue.applies_to)
        _issue["created_by"] = creator_detail(issue.creator_id)
        _issue["status"] = status_name(issue.status)
        _issue["created"] = issue.created_at.strftime('%d %B %Y')
        _issue["modified"] = issue.modified_at

    return _issue

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        issue = db\
            .session\
            .query(Issue)\
            .filter(Issue.uid == uid)\
            .filter(Issue.status > config.STATUS_DELETED)\
            .first()
        if issue:
            response = issue_obj(issue, real_id)

    return jsonify(response)


def fetch_all():
    response = []

    _issue = db\
        .session\
        .query(Issue)\
        .filter(Issue.status > config.STATUS_DELETED)\
        .all()

    for _iss in _issue:
        response.append(issue_obj(_iss))

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        issue = db\
            .session\
            .query(Issue)\
            .filter(Issue.id == id)\
            .filter(Issue.status > config.STATUS_DELETED)\
            .first()
        if issue:
            response = issue_obj(issue)

    return jsonify(response)
def fetch_by_name(name):
    response = {}
    if name:
        issue = db\
            .session\
            .query(Issue)\
            .filter(Issue.name == name)\
            .filter(Issue.status > config.STATUS_DELETED)\
            .first()
        if issue:
            response = issue_obj(issue)

    return jsonify(response)

def fetch_by_applies_to(id=0):
    response = []
    if id:
        _issue = db\
            .session\
            .query(Issue)\
            .filter(Issue.applies_to == id)\
            .filter(Issue.status > config.STATUS_DELETED)\
            .all()
        for _iss in _issue:
            response.append(issue_obj(_iss))

    return jsonify(response)
       

def add_new(issue_data=None, return_obj=False):

    try:
        data = json.loads(issue_data)
        if data:

            if 'name' in data and 'applies_to' in data and 'creator_id' in data :

                _name = data['name']
                _applies_to = data['applies_to']
                _description = None
                if 'description' in data and data['description']:
                    _description = data['description']

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _name and _applies_to and _creator:
                    new_issue = Issue(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        description = _description,
                        applies_to = _applies_to,
                        creator_id = _creator
                    )
                    db.session.add(new_issue)
                    db.session.commit()
                    if new_issue:
                        if return_obj:
                            return jsonify(fetch_one(new_issue.uid)), 200

                        return jsonify({"info": "Issue added successfully!"}), 200

        return jsonify({"error": "Issue not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(issue_id, issue_data=None, return_obj=False):

    try:
        data = json.loads(issue_data)
        if data:

            if valid_uuid(issue_id) and 'type' in data and 'name' in data and \
            'applies_to' in data:

                _name = data['name']
                _applies_to = data['applies_to']
                _description = None
                if 'description' in data and data['description']:
                    _description = data['description']

                if _name and _applies_to:
                    _issue = Issue\
                        .query\
                        .filter(Issue.uid == str(issue_id))\
                        .first()
                    if _issue:
                        _issue.name = _name
                        _issue.description = _description
                        _issue.applies_to = _applies_to
                        db.session.commit()

                        return jsonify({"info": "Issue edited successfully!"}), 200

        return jsonify({"error": "Issue not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(issue_id):

    try:
        if valid_uuid(issue_id) :
            _issue = Issue\
                .query\
                .filter(Issue.uid == issue_id)\
                .first()
            if _issue:
                _issue.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Issue deactivated successfully!"}), 200

        return jsonify({"error": "Issue not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
