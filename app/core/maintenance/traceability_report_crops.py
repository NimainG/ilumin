import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import TraceabilityReportCrop

from app.core import user
from app.core import planting
from app.core import maintenance
from app.core import customer
from app.core.planting import crops
from app.core.maintenance import traceability_report_requests

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def tr_crop_obj(tr_crop=False, real_id=False):
    _tr_crop = {}
    if tr_crop:
        if real_id:
            _tr_crop["real_id"] = tr_crop.id

        _tr_crop["id"] = str(tr_crop.uid)
        _tr_crop["crop_planting"] = crops.fetch_by_id(tr_crop.crop_planting_id).get_json()
        _tr_crop["traceability_report_request"] = traceability_report_requests.fetch_by_id(tr_crop.crop_planting_id).get_json()
        _tr_crop["maintenance"] = maintenance.fetch_by_id(tr_crop.maintenance_id).get_json()
        _tr_crop["customer"] = customer.fetch_by_id(tr_crop.customer_id).get_json()
        _tr_crop["created_by"] = creator_detail(tr_crop.creator_id)
        _tr_crop["status"] = status_name(tr_crop.status)
        _tr_crop["created"] = tr_crop.created_at.strftime('%d %B %Y')
        _tr_crop["modified"] = tr_crop.modified_at

    return _tr_crop

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        tr_crop = db\
            .session\
            .query(TraceabilityReportCrop)\
            .filter(TraceabilityReportCrop.uid == uid)\
            .filter(TraceabilityReportCrop.status > config.STATUS_DELETED)\
            .first()
        if tr_crop:
            response = tr_crop_obj(tr_crop, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        tr_crop = db\
            .session\
            .query(TraceabilityReportCrop)\
            .filter(TraceabilityReportCrop.id == id)\
            .filter(TraceabilityReportCrop.status > config.STATUS_DELETED)\
            .first()
        if tr_crop:
            response = tr_crop_obj(tr_crop)

    return jsonify(response)

def fetch_all():
    response = []

    _trace = db\
        .session\
        .query(TraceabilityReportCrop)\
        .filter(TraceabilityReportCrop.status > config.STATUS_DELETED)\
        .all()

    for _tr in  _trace:
        response.append(tr_crop_obj(_tr))

    return jsonify(response)


def fetch_by_crop_planting(id=0):
    response = {}
    if id:
        tr_crop = db\
            .session\
            .query(TraceabilityReportCrop)\
            .filter(TraceabilityReportCrop.crop_planting_id == id)\
            .filter(TraceabilityReportCrop.status > config.STATUS_DELETED)\
            .first()
        if tr_crop:
            response = tr_crop_obj(tr_crop)

    return jsonify(response)

def fetch_by_traceability_report_request(id=0):
    response = {}
    if id:
        tr_crop = db\
            .session\
            .query(TraceabilityReportCrop)\
            .filter(TraceabilityReportCrop.traceability_report_request_id == id)\
            .filter(TraceabilityReportCrop.status > config.STATUS_DELETED)\
            .first()
        if tr_crop:
            response = tr_crop_obj(tr_crop)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        tr_crop = db\
            .session\
            .query(TraceabilityReportCrop)\
            .filter(TraceabilityReportCrop.maintenance_id == id)\
            .filter(TraceabilityReportCrop.status > config.STATUS_DELETED)\
            .first()
        if tr_crop:
            response = tr_crop_obj(tr_crop)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        tr_crop = db\
            .session\
            .query(TraceabilityReportCrop)\
            .filter(TraceabilityReportCrop.customer_id == id)\
            .filter(TraceabilityReportCrop.status > config.STATUS_DELETED)\
            .first()
        if tr_crop:
            response = tr_crop_obj(tr_crop)

    return jsonify(response)

def fetch_by_customer_all(id=0):
    response = []
    if id:
        tr_crop = db\
            .session\
            .query(TraceabilityReportCrop)\
            .filter(TraceabilityReportCrop.customer_id == id)\
            .filter(TraceabilityReportCrop.status > config.STATUS_DELETED)\
            .all()


        for _tr in  tr_crop:
         response.append(tr_crop_obj(_tr))
        
    return jsonify(response)

def add_new(tr_crop_data=None, return_obj=False):

    try:
        data = json.loads(tr_crop_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _crop_planting_id = None
            if valid_uuid(data['crop_planting_id']):
                _crop_planting_id = crops.fetch_one(data['crop_planting_id'], True).get_json()['real_id']

            _traceability_report_request_id = None
            if valid_uuid(data['traceability_report_request_id']):
                _traceability_report_request_id = traceability_report_requests.fetch_one(data['traceability_report_request_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if  _maintenance_id and _customer_id and _creator:

                if _creator:
                    new_tr_crop = TraceabilityReportCrop(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        crop_planting_id = _crop_planting_id,
                        traceability_report_request_id = _traceability_report_request_id,
                        maintenance_id = _maintenance_id,
                        customer_id = _customer_id,
                        creator_id = _creator
                    )
                    db.session.add(new_tr_crop)
                    db.session.commit()
                    if new_tr_crop:
                        if return_obj:
                            return jsonify(fetch_one(new_tr_crop.uid)), 200

                        return jsonify({"info": "Traceability report crop added successfully!"}), 200

        return jsonify({"error": "Traceability report crop not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(tr_crop_id):

    try:
        if valid_uuid(tr_crop_id) :
            _tr_crop = TraceabilityReportCrop\
                .query\
                .filter(TraceabilityReportCrop.uid == tr_crop_id)\
                .first()
            if _tr_crop:
                _tr_crop.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Traceability report crop deactivated successfully!"}), 200

        return jsonify({"error": "Traceability report crop not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
