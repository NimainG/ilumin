import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import OtherMaintenanceReport

from app.core import user
from app.core import maintenance
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def other_obj(other=False, real_id=False):
    _other = {}
    if other:
        if real_id:
            _other["real_id"] = other.id

        _other["id"] = str(other.uid)
        _other["notes"] = other.notes
        _other["maintenance"] = maintenance.fetch_by_id(other.maintenance_id).get_json()
        _other["customer"] = customer.fetch_by_id(other.customer_id).get_json()
        _other["created_by"] = creator_detail(other.creator_id)
        _other["status"] = status_name(other.status)
        _other["created"] = other.created_at.strftime('%d %B %Y')
        _other["modified"] = other.modified_at

    return _other

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        other = db\
            .session\
            .query(OtherMaintenanceReport)\
            .filter(OtherMaintenanceReport.uid == uid)\
            .filter(OtherMaintenanceReport.status > config.STATUS_DELETED)\
            .first()
        if other:
            response = other_obj(other, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        other = db\
            .session\
            .query(OtherMaintenanceReport)\
            .filter(OtherMaintenanceReport.id == id)\
            .filter(OtherMaintenanceReport.status > config.STATUS_DELETED)\
            .first()
        if other:
            response = other_obj(other)

    return jsonify(response)

def fetch_all():
    response = []

    _other = db\
        .session\
        .query(OtherMaintenanceReport)\
        .filter(OtherMaintenanceReport.status > config.STATUS_DELETED)\
        .all()

    for _iss in _other:
        response.append(other_obj(_iss))

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        other = db\
            .session\
            .query(OtherMaintenanceReport)\
            .filter(OtherMaintenanceReport.maintenance_id == id)\
            .filter(OtherMaintenanceReport.status > config.STATUS_DELETED)\
            .first()
        if other:
            response = other_obj(other)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        other = db\
            .session\
            .query(OtherMaintenanceReport)\
            .filter(OtherMaintenanceReport.customer_id == id)\
            .filter(OtherMaintenanceReport.status > config.STATUS_DELETED)\
            .first()
        if other:
            response = other_obj(other)

    return jsonify(response)


def fetch_by_customer_all(id=0):
    response = []
    if id:
        others = db\
            .session\
            .query(OtherMaintenanceReport)\
            .filter(OtherMaintenanceReport.customer_id == id)\
            .filter(OtherMaintenanceReport.status > config.STATUS_DELETED)\
            .all()
        for sh in others:
         response.append(other_obj(sh))

    return jsonify(response)

def add_new(other_data=None, return_obj=False):

    try:
        data = json.loads(other_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            # _customer_id = None
            # if valid_uuid(data['customer_id']):
            #     _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _customer_id  = None
            if 'customer_id' in data and data['customer_id']:
                _customer_id = data['customer_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _maintenance_id and _customer_id and _creator:
                new_other = OtherMaintenanceReport(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_other)
                db.session.commit()
                if new_other:
                    if return_obj:
                        return jsonify(fetch_one(new_other.uid)), 200

                    return jsonify({"info": "Other maintenance added successfully!"}), 200

        return jsonify({"error": "Other maintenance not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(other_id, other_data=None, return_obj=False):

    try:
        data = json.loads(other_data)
        if data:

            if valid_uuid(other_id):

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                if _notes:
                    _other = OtherMaintenanceReport\
                        .query\
                        .filter(OtherMaintenanceReport.uid == str(other_id))\
                        .first()
                    if _other:
                        _other.notes = _notes
                        db.session.commit()

                        return jsonify({"info": "Other maintenance edited successfully!"}), 200

        return jsonify({"error": "Other maintenance not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(other_id):

    try:
        if valid_uuid(other_id) :
            _other = OtherMaintenanceReport\
                .query\
                .filter(OtherMaintenanceReport.uid == other_id)\
                .first()
            if _other:
                _other.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Other maintenance deactivated successfully!"}), 200

        return jsonify({"error": "Other maintenance not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
