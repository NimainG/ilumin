import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Maintenance

from app.core import user
from app.core import media
from app.core import customer
from app.core.maintenance import photos as maintenance_photos

from app.core.maintenance import pests_diseases
from app.core.maintenance import agronomist_visit
from app.core.maintenance import shields
from app.core.maintenance import greenhouses
from app.core.maintenance import screenhouses
from app.core.maintenance import shadenets
from app.core.maintenance import traceability_report_requests
from app.core.maintenance import others

from app.core.maintenance import assignment
from app.core.maintenance import closure

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def get_next_maintenance_serial():
    _serial = config.MAINTENANCE_SERIAL_START

    _latest_serial = db\
        .session\
        .query(Maintenance)\
        .filter(Maintenance.status > config.STATUS_DELETED)\
        .order_by(Maintenance.serial.desc())\
        .first()
    if _latest_serial:
        _serial = _latest_serial.serial + 1

    return _serial


def maintenance_type_name(maintenance_type):
    _maintenance_type = ""

    if maintenance_type:
        if maintenance_type == config.MAINTENANCE_PESTS_DISEASES:
            _maintenance_type = 'Pests and diseases'

        if maintenance_type == config.MAINTENANCE_AGRONOMIST_VISIT:
            _maintenance_type = 'Agronomist visit'

        if maintenance_type == config.MAINTENANCE_SHIELD_ISSUE:
            _maintenance_type = 'Shield issue'

        if maintenance_type == config.MAINTENANCE_GREENHOUSE_ISSUE:
            _maintenance_type = 'Greenhouse issue'

        if maintenance_type == config.MAINTENANCE_SCREENHOUSE_ISSUE:
            _maintenance_type = 'Screenhouse issue'

        if maintenance_type == config.MAINTENANCE_SHADENET_ISSUE:
            _maintenance_type = 'Shadenet issue'

        if maintenance_type == config.MAINTENANCE_TRACEABILITY_REPORT:
            _maintenance_type = 'Traceability report'

        if maintenance_type == config.MAINTENANCE_OTHERS:
            _maintenance_type = 'Others'

    return _maintenance_type


def maintenance_detail(maintenance_id, maintenance_type):
    _maintenance_detail = {}

    if maintenance_type:
        if maintenance_type == config.MAINTENANCE_PESTS_DISEASES:
            _maintenance_detail = pests_diseases.fetch_by_maintenance(
                maintenance_id).get_json()

        if maintenance_type == config.MAINTENANCE_AGRONOMIST_VISIT:
            _maintenance_detail = agronomist_visit.fetch_by_maintenance(
                maintenance_id).get_json()

        if maintenance_type == config.MAINTENANCE_SHIELD_ISSUE:
            _maintenance_detail = shields.fetch_by_maintenance(
                maintenance_id).get_json()

        if maintenance_type == config.MAINTENANCE_GREENHOUSE_ISSUE:
            _maintenance_detail = greenhouses.fetch_by_maintenance(
                maintenance_id).get_json()

        if maintenance_type == config.MAINTENANCE_SCREENHOUSE_ISSUE:
            _maintenance_detail = screenhouses.fetch_by_maintenance(
                maintenance_id).get_json()

        if maintenance_type == config.MAINTENANCE_SHADENET_ISSUE:
            _maintenance_detail = shadenets.fetch_by_maintenance(
                maintenance_id).get_json()

        if maintenance_type == config.MAINTENANCE_TRACEABILITY_REPORT:
            _maintenance_detail = traceability_report_requests.fetch_by_maintenance(
                maintenance_id).get_json()

        if maintenance_type == config.MAINTENANCE_OTHERS:
            _maintenance_detail = others.fetch_by_maintenance(
                maintenance_id).get_json()

    return _maintenance_detail


def maintenance_obj(maintenance=False, real_id=False):
    _maintenance = {}
    if maintenance:
        if real_id:
            _maintenance["real_id"] = maintenance.id

        _maintenance["id"] = str(maintenance.uid)
        _maintenance["serial"] = maintenance.serial
        _maintenance["type"] = maintenance_type_name(maintenance.type)
        _maintenance["customer"] = customer.fetch_by_id(
            maintenance.customer_id).get_json()
        _maintenance["customer_id"] = customer.fetch_by_id(
            maintenance.customer_id).get_json()
        _maintenance["photos"] = maintenance_photos.fetch_by_maintenance(
            maintenance.id).get_json()
        # _maintenance["detail"] = maintenance_detail(maintenance.type, maintenance.id).get_json()
        _maintenance["assignment"] = assignment.fetch_by_maintenance(
            maintenance.id).get_json()
        # _maintenance["closure"] = closure.fetch_by_maintenance(maintenance.id).get_json()
        _maintenance["created_by"] = creator_detail(maintenance.creator_id)
        _maintenance["status"] = status_name(maintenance.status)
        _maintenance["created"] = maintenance.created_at.strftime('%d %B %Y')
        _maintenance["modified"] = maintenance.modified_at

    return _maintenance


def fetch_all():
    response = []

    maintenances = db\
        .session\
        .query(Maintenance)\
        .filter(Maintenance.status > config.STATUS_DELETED)\
        .order_by(Maintenance.id.desc())\
        .all()

    for maintenance in maintenances:
        response.append(maintenance_obj(maintenance))

    return jsonify(response)


def fetch_all1(page):
    response = []
    page_limit = config.PAGE_LIMIT
    maintenances = db\
        .session\
        .query(Maintenance)\
        .order_by(Maintenance.id.desc())\
        .limit(page_limit)\
        .offset((page-1)*page_limit)

    for maintenance in maintenances:
        response.append(maintenance_obj(maintenance))

    return jsonify(response)


def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        maintenance = db\
            .session\
            .query(Maintenance)\
            .filter(Maintenance.uid == uid)\
            .filter(Maintenance.status > config.STATUS_DELETED)\
            .first()
        if maintenance:
            response = maintenance_obj(maintenance, real_id)

    return jsonify(response)


def fetch_by_id(id=0, real_id=False):
    response = {}
    if id:
        maintenance = db\
            .session\
            .query(Maintenance)\
            .filter(Maintenance.id == id)\
            .filter(Maintenance.status > config.STATUS_DELETED)\
            .first()
        if maintenance:
            response = maintenance_obj(maintenance, real_id)

    return jsonify(response)


def fetch_by_customer_id(customer_id=0):
    response = {}
    if customer_id:
        maintenance = db\
            .session\
            .query(Maintenance)\
            .order_by(Maintenance.id.desc())\
            .filter(Maintenance.customer_id == customer_id)\
            .filter(Maintenance.status > config.STATUS_DELETED)\
            .order_by(Maintenance.id.desc()).first()
        if maintenance:
            response = maintenance_obj(maintenance)

    return jsonify(response)


def fetch_by_type(type):
    response = {}
    if type:
        maintenance = db\
            .session\
            .query(Maintenance)\
            .order_by(Maintenance.id.desc())\
            .filter(Maintenance.type == type)\
            .filter(Maintenance.status > config.STATUS_DELETED)\
            .order_by(Maintenance.id.desc()).first()
        if maintenance:
            response = maintenance_obj(maintenance)

    return jsonify(response)


def fetch_all_by_customer(customer_id=0):
    response = []
    if customer_id:
        maintenance = db\
            .session\
            .query(Maintenance)\
            .order_by(Maintenance.id.desc())\
            .filter(Maintenance.customer_id == customer_id)\
            .filter(Maintenance.status > config.STATUS_DELETED)\
            .all()
        for maint in maintenance:
            response.append(maintenance_obj(maint))

    return jsonify(response)


def fetch_all_by_customer1(page, customer_id=0):
    response = []
    if customer_id:
        page_limit = config.PAGE_LIMIT
        maintenance = db.session\
            .query(Maintenance)\
            .order_by(Maintenance.id.desc())\
            .filter(Maintenance.customer_id == customer_id)\
            .limit(page_limit)\
            .offset((page-1)*page_limit)

        for cstmr in maintenance:
            response.append(maintenance_obj(cstmr))

    return jsonify(response)


# def fetch_all_by_customer(cust_id):
#     response = []

#     maintenances = db\
#         .session\
#         .query(Maintenance.customer_id == cust_id)\
#         .filter(Maintenance.status > config.STATUS_DELETED)\
#         .all()

#     for maintenance in maintenances:
#         response.append(maintenance_obj(maintenance))

#     return jsonify(response)


def add_new(maintenance_data=None, return_obj=False):

    try:
        data = json.loads(maintenance_data)
        if data:

            if 'customer_id' in data and 'creator_id' in data:

                _serial = get_next_maintenance_serial()
                _type = data['type']
                _customer_id = data['customer_id']

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(
                        data['creator_id'], True).get_json()['real_id']

                if _serial and _customer_id and _creator:
                    new_maintenance = Maintenance(
                        uid=uuid.uuid4(),
                        status=config.STATUS_ACTIVE,
                        serial=_serial,
                        type=_type,
                        customer_id=_customer_id,
                        creator_id=_creator
                    )
                    db.session.add(new_maintenance)
                    db.session.commit()

                    if new_maintenance:
                        if return_obj:
                            return jsonify(fetch_one(new_maintenance.uid)), 200

                        return jsonify({"info": "Maintenance added successfully!"}), 200

        return jsonify({"error": "Maintenance not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def edit(maintenance_id, maintenance_data=None, return_obj=False):

    try:
        data = json.loads(maintenance_data)
        if data:

            if valid_uuid(maintenance_id) and 'type' in data:

                _type = data['type']

                if _type:
                    _maintenance = Maintenance\
                        .query\
                        .filter(Maintenance.uid == str(maintenance_id))\
                        .first()
                    if _maintenance:
                        _maintenance.type = _type
                        db.session.commit()

                        return jsonify({"info": "Maintenance edited successfully!"}), 200

        return jsonify({"error": "Maintenance not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def deactivate(maintenance_id):

    try:

        if valid_uuid(maintenance_id):
            _maintenance = Maintenance\
                .query\
                .filter(Maintenance.uid == maintenance_id)\
                .first()
            if _maintenance:
                _maintenance.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Maintenance deactivated successfully!"}), 200

        return jsonify({"error": "Maintenance not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
