import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import GreenhouseMaintenanceIssue

from app.core import user
from app.core import maintenance
from app.core import customer
from app.core.maintenance import issues
from app.core.maintenance import greenhouses

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouse_issue_obj(greenhouse_issue=False, real_id=False):
    _greenhouse_issue = {}
    if greenhouse_issue:
        if real_id:
            _greenhouse_issue["real_id"] = greenhouse_issue.id

        _greenhouse_issue["id"] = str(greenhouse_issue.uid)
        _greenhouse_issue["issue"] = issues.fetch_by_id(greenhouse_issue.issue_id).get_json()
        _greenhouse_issue["greenhouse_maintenance"] = greenhouses.fetch_by_id(greenhouse_issue.greenhouse_maintenance_id).get_json()
        _greenhouse_issue["maintenance"] = maintenance.fetch_by_id(greenhouse_issue.maintenance_id).get_json()
        _greenhouse_issue["customer"] = customer.fetch_by_id(greenhouse_issue.customer_id).get_json()
        _greenhouse_issue["created_by"] = creator_detail(greenhouse_issue.creator_id)
        _greenhouse_issue["status"] = status_name(greenhouse_issue.status)
        _greenhouse_issue["created"] = greenhouse_issue.created_at.strftime('%d %B %Y')
        _greenhouse_issue["modified"] = greenhouse_issue.modified_at

    return _greenhouse_issue

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        greenhouse_issue = db\
            .session\
            .query(GreenhouseMaintenanceIssue)\
            .filter(GreenhouseMaintenanceIssue.uid == uid)\
            .filter(GreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_issue:
            response = greenhouse_issue_obj(greenhouse_issue, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        greenhouse_issue = db\
            .session\
            .query(GreenhouseMaintenanceIssue)\
            .filter(GreenhouseMaintenanceIssue.id == id)\
            .filter(GreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_issue:
            response = greenhouse_issue_obj(greenhouse_issue)

    return jsonify(response)

def fetch_all():
    response = []

    _greenhouse_issue = db\
        .session\
        .query(GreenhouseMaintenanceIssue)\
        .filter(GreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
        .all()

    for _sh in _greenhouse_issue:
        response.append(greenhouse_issue_obj(_sh))

    return jsonify(response)

def fetch_by_greenhouse_maintenance(id=0):
    response = {}
    if id:
        greenhouse_issue = db\
            .session\
            .query(GreenhouseMaintenanceIssue)\
            .filter(GreenhouseMaintenanceIssue.greenhouse_maintenance_id == id)\
            .filter(GreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_issue:
            response = greenhouse_issue_obj(greenhouse_issue)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        greenhouse_issue = db\
            .session\
            .query(GreenhouseMaintenanceIssue)\
            .filter(GreenhouseMaintenanceIssue.maintenance_id == id)\
            .filter(GreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_issue:
            response = greenhouse_issue_obj(greenhouse_issue)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        greenhouse_issue = db\
            .session\
            .query(GreenhouseMaintenanceIssue)\
            .filter(GreenhouseMaintenanceIssue.customer_id == id)\
            .filter(GreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_issue:
            response = greenhouse_issue_obj(greenhouse_issue)

    return jsonify(response)


def fetch_by_customer_all(id=0):
    response = []
    if id:
        greenhouse_issue = db\
            .session\
            .query(GreenhouseMaintenanceIssue)\
            .filter(GreenhouseMaintenanceIssue.customer_id == id)\
            .filter(GreenhouseMaintenanceIssue.status > config.STATUS_DELETED)\
            .all()
        for sc_i in greenhouse_issue:
            response.append(greenhouse_issue_obj(sc_i))

    return jsonify(response)


def add_new(greenhouse_issue_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_issue_data)
        if data:

            _notes = None
            if 'notes' in data and data['notes']:
                _notes = data['notes']

            _issue_id = None
            if valid_uuid(data['issue_id']):
                _issue_id = issues.fetch_one(data['issue_id'], True).get_json()['real_id']

            _greenhouse_maintenance_id = None
            if 'greenhouse_maintenance_id' in data and data['greenhouse_maintenance_id']:
                _greenhouse_maintenance_id = data['greenhouse_maintenance_id']

            # _greenhouse_maintenance_id = None
            # if valid_uuid(data['greenhouse_maintenance_id']):
            #     _greenhouse_maintenance_id = greenhouses.fetch_one(data['greenhouse_maintenance_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            # _customer_id = None
            # if valid_uuid(data['customer_id']):
            #     _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']
            _customer_id  = None
            if 'customer_id' in data and data['customer_id']:
                _customer_id = data['customer_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _issue_id and _greenhouse_maintenance_id and _maintenance_id and _customer_id and _creator:
                new_greenhouse_issue = GreenhouseMaintenanceIssue(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    issue_id = _issue_id,
                    greenhouse_maintenance_id = _greenhouse_maintenance_id,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_greenhouse_issue)
                db.session.commit()
                if new_greenhouse_issue:
                    if return_obj:
                        return jsonify(fetch_one(new_greenhouse_issue.uid)), 200

                    return jsonify({"info": "Greenhouse maintenance issue added successfully!"}), 200

        return jsonify({"error": "Greenhouse maintenance issue not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(greenhouse_issue_id, greenhouse_issue_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_issue_data)
        if data:

            if valid_uuid(greenhouse_issue_id):

                _issue_id = None
                if valid_uuid(data['issue_id']):
                    _issue_id = issues.fetch_one(data['issue_id'], True).get_json()['real_id']
                
                _greenhouse_maintenance_id = None
                if 'greenhouse_maintenance_id' in data and data['greenhouse_maintenance_id']:
                 _greenhouse_maintenance_id = data['greenhouse_maintenance_id']

                # _greenhouse_maintenance_id = None
                # if valid_uuid(data['greenhouse_maintenance_id']):
                #     _greenhouse_maintenance_id = maintenance.fetch_one(data['greenhouse_maintenance_id'], True).get_json()['real_id']

                if _issue_id and _greenhouse_maintenance_id:
                    _greenhouse_issue = GreenhouseMaintenanceIssue\
                        .query\
                        .filter(GreenhouseMaintenanceIssue.uid == str(greenhouse_issue_id))\
                        .first()
                    if _greenhouse_issue:
                        _greenhouse_issue.issue_id = _issue_id
                        _greenhouse_issue.greenhouse_maintenance_id = _greenhouse_maintenance_id
                        db.session.commit()

                        return jsonify({"info": "Greenhouse maintenance issue edited successfully!"}), 200

        return jsonify({"error": "Greenhouse maintenance issue not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(greenhouse_issue_id):

    try:
        if valid_uuid(greenhouse_issue_id) :
            _greenhouse_issue = GreenhouseMaintenanceIssue\
                .query\
                .filter(GreenhouseMaintenanceIssue.uid == greenhouse_issue_id)\
                .first()
            if _greenhouse_issue:
                _greenhouse_issue.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Greenhouse maintenance issue deactivated successfully!"}), 200

        return jsonify({"error": "Greenhouse maintenance issue not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
