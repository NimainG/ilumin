import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Commodity

from app.core import user
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def commodity_obj(commodity=False, real_id=False):
    _commodity = {}
    if commodity:
        if real_id:
            _commodity["real_id"] = commodity.id

        _commodity["id"] = str(commodity.uid)
        _commodity["name"] = commodity.name
        _commodity["description"] = commodity.description
        _commodity["customer"] = customer.fetch_by_id(commodity.customer_id).get_json()
        _commodity["created_by"] = creator_detail(commodity.creator_id)
        _commodity["status"] = status_name(commodity.status)
        _commodity["created"] = commodity.created_at.strftime('%d %B %Y') 
        _commodity["modified"] = commodity.modified_at

    return _commodity

def fetch_all():
    response = []

    commodities = db\
        .session\
        .query(Commodity)\
        .filter(Commodity.status > config.STATUS_DELETED)\
        .all()

    for commodity in commodities:
        response.append(commodity_obj(commodity))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        commodity = db\
            .session\
            .query(Commodity)\
            .filter(Commodity.uid == uid)\
            .filter(Commodity.status > config.STATUS_DELETED)\
            .first()
        if commodity:
            response = commodity_obj(commodity, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        commodity = db\
            .session\
            .query(Commodity)\
            .filter(Commodity.id == id)\
            .filter(Commodity.status > config.STATUS_DELETED)\
            .first()
        if commodity:
            response = commodity_obj(commodity)

    return jsonify(response)

def fetch_by_customer(customer_id=None):
    response = []

    commodities = db\
        .session\
        .query(Commodity)\
        .filter(Commodity.customer_id == customer_id)\
        .filter(Commodity.status > config.STATUS_DELETED)\
        .all()

    for commodity in commodities:
        response.append(commodity_obj(commodity))

    return jsonify(response)

def add_new(commodity_data=None, return_obj=False):

    try:
        data = json.loads(commodity_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _description = None
            if 'description' in data:
                _description = data['description']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _creator_id:
                new_commodity = Commodity(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    descr = _description,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_commodity)
                db.session.commit()
                if new_commodity:
                    if return_obj:
                        return jsonify(fetch_one(new_commodity.uid)), 200

                    return jsonify({"info": "Commodity added successfully!"}), 200

        return jsonify({"error": "Commodity not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(commodity_id, commodity_data=None, return_obj=False):

    try:
        data = json.loads(commodity_data)
        if data:

            if valid_uuid(commodity_id):

                _commodity = None
                if 'commodity' in data:
                    _commodity = data['commodity']

                _supply_id = None
                if valid_uuid(data['supply_id']):
                    _supply_id = supply.fetch_one(data['supply_id'], True).get_json()['real_id']

                if _commodity and _supply_id:
                    _commodity = Commodity\
                        .query\
                        .filter(Commodity.uid == str(commodity_id))\
                        .first()
                    if _commodity:
                        _commodity.commodity = _commodity
                        _commodity.supply_id = _supply_id
                        db.session.commit()

                        return jsonify({"info": "Commodity edited successfully!"}), 200

        return jsonify({"error": "Commodity not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(commodity_id):

    try:

        if valid_uuid(commodity_id) :
            _commodity = Commodity\
                .query\
                .filter(Commodity.uid == commodity_id)\
                .first()
            if _commodity:
                _commodity.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Commodity deactivated successfully!"}), 200

        return jsonify({"error": "Commodity not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
