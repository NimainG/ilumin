import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Partner, IghUser, PartnerUser

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def get_igh_contact(id):
    _contact = {}

    if id:
        _igh_contact == IghUser\
            .query\
            .filter(IghUser.id == id)\
            .first()
        if _igh_contact:
            _contact = {
                "employee_id": _igh_contact.employee_id,
                "user": person_info(_igh_contact.user_id)
            }

    return jsonify(_contact)

def get_partner_contact(id):
    _contact = {}

    if id:
        _partner_contact == PartnerUser\
            .query\
            .filter(PartnerUser.id == id)\
            .first()
        if _partner_contact:
            _contact = {
                "employee_id": _partner_contact.employee_id,
                "user": person_info(_partner_contact.user_id)
            }

    return jsonify(_contact)


def partner_obj(partner=False, real_id=False):
    _partner = {}
    if partner:
        if real_id:
            _partner["real_id"] = partner.id

        _partner["id"] = str(partner.uid)
        _partner["name"] = partner.name
        _partner["notes"] = partner.notes
        _partner["igh_contact"] = get_igh_contact(partner.igh_contact_id).get_json()
        _partner["contact_person"] = get_partner_contact(partner.contact_person_id).get_json()
        _partner["created_by"] = creator_detail(partner.creator_id)
        _partner["status"] = status_name(partner.status)
        _partner["created"] = partner.created_at
        _partner["modified"] = partner.modified_at

    return _partner

def fetch_all():
    response = []

    partners = db\
        .session\
        .query(Partner)\
        .filter(Partner.status > config.STATUS_DELETED)\
        .all()

    for partner in partners:
        response.append(partner_obj(partner))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        partner = db\
            .session\
            .query(Partner)\
            .filter(Partner.uid == uid)\
            .filter(Partner.status > config.STATUS_DELETED)\
            .first()
        if partner:
            response = partner_obj(partner, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        partner = db\
            .session\
            .query(Partner)\
            .filter(Partner.id == id)\
            .filter(Partner.status > config.STATUS_DELETED)\
            .first()
        if partner:
            response = partner_obj(partner)

    return jsonify(response)

def add_new(partner_data=None, return_obj=False):

    try:
        data = json.loads(partner_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _igh_contact_id = None
            if valid_uuid(data['igh_contact_id']):
                _igh_contact_id = user.igh.fetch_one(data['igh_contact_id'], True).get_json()['real_id']

            _contact_person_id = None
            if valid_uuid(data['contact_person_id']):
                _contact_person_id = user.partners.fetch_one(data['contact_person_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _igh_contact_id and _contact_person_id and _creator_id:
                new_partner = Partner(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    notes = _notes,
                    igh_contact_id = _igh_contact_id,
                    contact_person_id = _contact_person_id,
                    creator_id = _creator_id
                )
                db.session.add(new_partner)
                db.session.commit()
                if new_partner:
                    if return_obj:
                        return jsonify(fetch_one(new_partner.uid)), 200

                    return jsonify({"info": "Partner added successfully!"}), 200

        return jsonify({"error": "Partner not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(partner_id, partner_data=None, return_obj=False):

    try:
        data = json.loads(partner_data)
        if data:

            if valid_uuid(partner_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                _igh_contact_id = None
                if valid_uuid(data['igh_contact_id']):
                    _igh_contact_id = user.igh.fetch_one(data['igh_contact_id'], True).get_json()['real_id']

                _contact_person_id = None
                if valid_uuid(data['contact_person_id']):
                    _contact_person_id = user.partners.fetch_one(data['contact_person_id'], True).get_json()['real_id']

                _partner = Partner\
                    .query\
                    .filter(Partner.uid == str(partner_id))\
                    .first()
                if _partner:
                    _partner.name = _name
                    _partner.notes = _notes
                    _partner.igh_contact_id = _igh_contact_id
                    _partner.contact_person_id = _contact_person_id
                    db.session.commit()

                    return jsonify({"info": "Partner edited successfully!"}), 200

        return jsonify({"error": "Partner not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(partner_id):

    try:

        if valid_uuid(partner_id) :
            _partner = Partner\
                .query\
                .filter(Partner.uid == partner_id)\
                .first()
            if _partner:
                _partner.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Partner deactivated successfully!"}), 200

        return jsonify({"error": "Partner not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
