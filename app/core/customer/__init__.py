import os
import json
from urllib import response
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import CustomerFarm, db
from app.models import Customer, IghUser, CustomerUser
from app.core.customer import farms
from app.core import farm

from app.core import user
from app.core.user import igh
from app.core.user import customers
from app.core.customer import farms as ownfarms
from sqlalchemy import extract
load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def get_igh_contact(id):
    _contact = {}

    if id:
        _igh_contact = IghUser\
            .query\
            .filter(IghUser.id == id)\
            .first()
        if _igh_contact:
            _contact = {
                "employee_id": _igh_contact.employee_id,
                "user": person_info(_igh_contact.user_id).get_json()
            }

    return jsonify(_contact)

def get_customer_contact(id):
    _contact = {}

    if id:
        _customer_contact = CustomerUser\
            .query\
            .filter(CustomerUser.id == id)\
            .first()
        if _customer_contact:
            _contact = {
                "employee_id": _customer_contact.employee_id,
                "user": person_info(_customer_contact.user_id).get_json()
            }

    return jsonify(_contact)




def customer_obj(customer=False, real_id=False):
    _customer = {}
    if customer:
        if real_id:
            _customer["real_id"] = customer.id

        _customer["id"] = str(customer.uid)
        _customer["name"] = customer.name
        _customer["main_id"] = customer.id
        _customer["billing_address"] = customer.billing_address
        _customer["shipping_address"] = customer.shipping_address
        _customer["igh_contact"] = get_igh_contact(customer.igh_contact_id).get_json()
        
        # _customer["all_farms"] = ownfarms.fetch_by_customer(177).get_json()
        # _customer['farms'] = farms.fetch_all().get_json()
        # _customer["contact_person"] = get_customer_contact(customer.contact_person_id).get_json()
        _customer["contact_person_id"] = user.fetch_by_id(customer.contact_person_id).get_json()
        _customer["created_by"] = creator_detail(customer.creator_id)
        _customer["status"] = status_name(customer.status)
        _customer["created"] = customer.created_at.strftime('%d %B %Y')
        _customer["modified"] = customer.modified_at

    return _customer

def fetch_all():
    response = []

    customerd = db\
        .session\
        .query(Customer)\
        .filter(Customer.status > config.STATUS_DELETED)\
        .order_by(Customer.id.desc())\
        .all()

    for customel in customerd:
        response.append(customer_obj(customel))

    return jsonify(response)


def fetch_by_month(_data_date):
    response = []

    _data = db\
            .session\
            .query(Customer)\
            .filter(Customer.created_at >= _data_date)\
            .filter(Customer.status == config.STATUS_ACTIVE)\
            .all()
    for customel in _data:
        response.append(customer_obj(customel))

    return jsonify(response)

def fetch_by_fiveyears(_data_date):
    response = []

    _data = db\
            .session\
            .query(Customer)\
            .filter(Customer.created_at <= _data_date)\
            .filter(Customer.status > config.STATUS_DELETED)\
            .all()
    for customel in _data:
        response.append(customer_obj(customel))

    return jsonify(response)

def fetch_by_dates(_start_date,_end_date):
    response = []

    _data = db\
            .session\
            .query(Customer)\
            .filter(Customer.created_at >= _end_date)\
            .filter(Customer.created_at <= _start_date)\
            .filter(Customer.status > config.STATUS_DELETED)\
            .all()
    for customel in _data:
        response.append(customer_obj(customel))

    return jsonify(response)




def fetch_all1(page):
    response = []
    page_limit = config.PAGE_LIMIT
    customer = db.session\
        .query(Customer)\
        .order_by(Customer.id.desc())\
        .limit(page_limit)\
        .offset((page-1)*page_limit)\
        .all()
        
    for cstmr in customer:
        response.append(customer_obj(cstmr))

    return jsonify(response)


def fetch_by_farms(cls, all_farms):
    return CustomerFarm.query.join(all_farms).filter(all_farms.id).all()




def fetch_inorder():
    response = []

    customers = db\
        .session\
        .query(Customer)\
        .filter(Customer.status > config.STATUS_DELETED)\
        .order_by(Customer.id.desc()).all()

    for customer in customers:
        response.append(customer_obj(customer))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer = db\
            .session\
            .query(Customer)\
            .filter(Customer.uid == uid)\
            .filter(Customer.status > config.STATUS_DELETED)\
            .first()
        if customer:
            response = customer_obj(customer, real_id)

    return jsonify(response)

def fetch_by_user(user_id=0):
    response = {}
    if user_id:
        customer_user = db\
            .session\
            .query(Customer)\
            .filter(Customer.contact_person_id == user_id)\
            .filter(Customer.status > config.STATUS_DELETED)\
            .first()
        if customer_user:
            response = customer_obj(customer_user)

    return jsonify(response)





   

def fetch_by_id(id=0,real_id=False):
    response = {}
    if id:
        customer = db\
            .session\
            .query(Customer)\
            .filter(Customer.id == id)\
            .filter(Customer.status > config.STATUS_DELETED)\
            .first()
        if customer:
            response = customer_obj(customer,real_id)

    return jsonify(response)

def add_new(customer_data=None, return_obj=False):

    try:
        data = json.loads(customer_data)
        if data:

            if 'name' in data and 'creator_id' in data:

                _name = data['name']
                _billing_address = data['billing_address']
                _shipping_address = data['shipping_address']
                _igh_contact = None
                _creator = None
                _contact_person_id = None

                if valid_uuid(data['igh_contact_id']):
                    _igh_contact = igh.fetch_one(data['igh_contact_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']
                if valid_uuid(data['contact_person_id']):
                    _contact_person_id = user.fetch_one(data['contact_person_id'], True).get_json()['real_id']
                if _name and _creator:
                    new_customer = Customer(
                        uid = uuid.uuid4(),
                        status = config.STATUS_INACTIVE,
                        name = _name,
                        billing_address = _billing_address,
                        shipping_address = _shipping_address,
                        igh_contact_id = _igh_contact,
                        creator_id = _creator,
                        contact_person_id = _contact_person_id
                    )
                    db.session.add(new_customer)
                    db.session.commit()
                    if new_customer:
                        if return_obj:
                            return jsonify(fetch_one(new_customer.uid)), 200

                        return jsonify({"info": "Customer added successfully!"}), 200

        return jsonify({"error": "Customer not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(customer_id,customer_data=None, return_obj=False):

    try:
        data = json.loads(customer_data)
        if data:

            if valid_uuid(customer_id):

                _billing_address = None
                _shipping_address = None
                _name = None
                _billing_address = data['billing_address']
                _shipping_address = data['shipping_address']
                _name = data['name']
               
                _igh_contact = None

                if valid_uuid(data['igh_contact_id']):
                    _igh_contact = igh.fetch_one(data['igh_contact_id'], True).get_json()['real_id']
                    
                # if valid_uuid(data['farm_id']):
                #     _farm_id = farm.fetch_one(data['farm_id'], True).get_json()['real_id']

                
                _customer = Customer\
                        .query\
                        .filter(Customer.uid == str(customer_id))\
                        .first()
                if _customer:
                        _customer.name = _name
                        _customer.billing_address = _billing_address
                        _customer.shipping_address = _shipping_address
                        _customer.igh_contact_id = _igh_contact
                      
                        db.session.commit()

                        return jsonify({"info": "Customer edited successfully!"}), 200

        return jsonify({"error": "Customer not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_id):

    try:

        if valid_uuid(customer_id) :
            _customer = Customer\
                .query\
                .filter(Customer.uid == customer_id)\
                .first()
            if _customer:
                _customer.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer deactivated successfully!"}), 200

        return jsonify({"error": "Customer not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(customer_id):

    try:

        if valid_uuid(customer_id) :
            _customer = Customer\
                .query\
                .filter(Customer.uid == customer_id)\
                .first()
            if _customer:
                _customer.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "Customer Activated successfully!"}), 200

        return jsonify({"error": "Customer not Activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
