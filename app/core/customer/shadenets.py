# customer_shadenet_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerShadenet

from app.core import customer
from app.core import shadenet
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def customer_shadenet_obj(customer_shadenet=False, real_id=False):
    _customer_shadenet = {}
    if customer_shadenet:
        if real_id:
            _customer_shadenet["real_id"] = customer_shadenet.id

        _customer_shadenet["id"] = str(customer_shadenet.uid)
        _customer_shadenet["shadenet"] = shadenet.fetch_by_id(customer_shadenet.shadenet_id).get_json()
        _customer_shadenet["customer"] = customer.fetch_by_id(customer_shadenet.customer_id).get_json()
        _customer_shadenet["created_by"] = creator_detail(customer_shadenet.creator_id)
        _customer_shadenet["status"] = status_name(customer_shadenet.status)
        _customer_shadenet["created"] = customer_shadenet.created_at.strftime('%d %B %Y')
        _customer_shadenet["modified"] = customer_shadenet.modified_at

    return _customer_shadenet

def fetch_all():
    response = []

    customer_shadenets = db\
        .session\
        .query(CustomerShadenet)\
        .filter(CustomerShadenet.status > config.STATUS_DELETED)\
        .all()

    for customer_shadenet in customer_shadenets:
        response.append(customer_shadenet_obj(customer_shadenet))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer_shadenet = db\
            .session\
            .query(CustomerShadenet)\
            .filter(CustomerShadenet.uid == uid)\
            .filter(CustomerShadenet.status > config.STATUS_DELETED)\
            .first()
        if customer_shadenet:
            response = customer_shadenet_obj(customer_shadenet, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        customer_shadenet = db\
            .session\
            .query(CustomerShadenet)\
            .filter(CustomerShadenet.id == id)\
            .filter(CustomerShadenet.status > config.STATUS_DELETED)\
            .first()
        if customer_shadenet:
            response = customer_shadenet_obj(customer_shadenet)

    return jsonify(response)


def fetch_by_customer(customer_id):
    response = []
    if customer_id:
        customer_shadenets = db\
            .session\
            .query(CustomerShadenet)\
            .filter(CustomerShadenet.customer_id == customer_id)\
            .filter(CustomerShadenet.status > config.STATUS_DELETED)\
            .all()
        for customer_shadenet in customer_shadenets:
            response.append(customer_shadenet_obj(customer_shadenet))

    return jsonify(response)

def add_new(customer_shadenet_data=None, return_obj=False):

    try:
        data = json.loads(customer_shadenet_data)
        if data:

            _shadenet_id = None
            if valid_uuid(data['shadenet_id']):
                _shadenet_id = shadenet.fetch_one(data['shadenet_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shadenet_id and _customer_id and _creator_id:
                new_customer_shadenet = CustomerShadenet(
                    uid = uuid.uuid4(),
                    status = config.STATUS_INACTIVE,
                    shadenet_id = _shadenet_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_customer_shadenet)
                db.session.commit()
                if new_customer_shadenet:
                    if return_obj:
                        return jsonify(fetch_one(new_customer_shadenet.uid)), 200

                    return jsonify({"info": "Customer shadenet added successfully!"}), 200

        return jsonify({"error": "Customer shadenet not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_shadenet_id):

    try:
        if valid_uuid(customer_shadenet_id) :
            _customer_shadenet = CustomerShadenet\
                .query\
                .filter(CustomerShadenet.uid == customer_shadenet_id)\
                .first()
            if _customer_shadenet:
                _customer_shadenet.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify("Customer shadenet deactivated successfully!"), 200

        return jsonify({"error": "Customer shadenet not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(customer_shadenet_id):

    try:
        if valid_uuid(customer_shadenet_id) :
            _customer_shadenet = CustomerShadenet\
                .query\
                .filter(CustomerShadenet.uid == customer_shadenet_id)\
                .first()
            if _customer_shadenet:
                _customer_shadenet.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "Customer shadenet activated successfully!"}), 200

        return jsonify({"error": "Customer shadenet not activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def delete(customer_shadenet_id):

    try:
        if valid_uuid(customer_shadenet_id) :
            _customer_shadenet = CustomerShadenet\
                .query\
                .filter(CustomerShadenet.uid == customer_shadenet_id)\
                .first()
            if _customer_shadenet:
                _customer_shadenet.status = config.STATUS_DELETED
                db.session.commit()

                return jsonify({"info": "Customer shadenet deleted successfully!"}), 200

        return jsonify({"error": "Customer shadenet not deleted, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
