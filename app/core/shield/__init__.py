import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv
from datetime import datetime
from app.config import activateConfig
from app.core.customer import shields

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Shield

from app.core import greenhouse, user
from app.core.shield import types as shield_types
from app.core.shield import spears as shield_spears
from app.core.data import shielddata

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def shield_obj(shield=False, real_id=False):
    _shield = {}
    if shield:
        if real_id:
            _shield["real_id"] = shield.id

        _shield["id"] = str(shield.uid)
        _shield["serial"] = shielddata.fetch_by_boron(shield.serial).get_json()
        _shield["mac_address"] = shield.mac_address
        _shield["assigned"] = shield.assigned
        _shield["greenhouse_id"] = greenhouse.fetch_by_green_ref(shield.greenhouse_id).get_json()
        _shield["type"] = shield_types.fetch_by_id(shield.type_id).get_json()
        _shield["spears"] = shield_spears.fetch_by_shield_id(shield.id).get_json()
        _shield["created_by"] = creator_detail(shield.creator_id)
        _shield["status"] = status_name(shield.status)
        _shield["created"] = shield.created_at.strftime('%d %B %Y')
        _shield["modified"] = shield.modified_at
    return _shield

def fetch_all():
    response = []

    shields = db\
        .session\
        .query(Shield)\
        .filter(Shield.status > config.STATUS_DELETED)\
        .all()

    for shield in shields:
        response.append(shield_obj(shield))

    return jsonify(response)

def fetch_all1(page):
    response = []
    page_limit = config.PAGE_LIMIT
    divc = db.session\
        .query(Shield)\
        .limit(page_limit)\
        .offset((page-1)*page_limit)
        
    for d in divc :
        response.append(shield_obj(d))

    return jsonify(response)


def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.uid == uid)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        _shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.id == id)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .first()
        if _shield:
            response = shield_obj(_shield)

    return jsonify(response)

def fetch_by_greenhouse(greenhouse_id):
    response = {}
    if greenhouse_id:
        shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.greenhouse_id == greenhouse_id)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)



def fetch_by_greenhouses(greenhouse_id):
    response = []
    if greenhouse_id:
        shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.greenhouse_id == greenhouse_id)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .all()
        for d in shield :
         response.append(shield_obj(d))

   
        # if shield:
        #     response = shield_obj(shield)

    return jsonify(response)


def fetch_by_serial(serial):
    response = {}
    if serial:
        shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.serial == serial)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)

def fetch_by_type_id(type_id=0):
    response = {}
    if type_id:
        shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.type_id == type_id)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)

def add_new(shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:

            if 'serial' in data and 'mac_address' in data and 'type_id' \
            in data and 'creator_id' in data:

                _serial = data['serial']
                _mac_address = data['mac_address']

                _shield_type = None
                _creator = None
                _greenhouse_id = None

                _greenhouse_id = data['greenhouse_id']
                # if valid_uuid(data['boron_id']):
                #     _boron_id = shield_types.fetch_one(data['boron_id'], True).get_json()['real_id']


                if valid_uuid(data['type_id']):
                    _shield_type = shield_types.fetch_one(data['type_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _serial and _mac_address:
                    new_shield = Shield(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        serial = _serial,
                        greenhouse_id = _greenhouse_id,
                        assigned = config.NOT_ASSIGNED,
                        mac_address = _mac_address,
                        type_id = _shield_type,
                        creator_id = _creator
                    )
                    db.session.add(new_shield)
                    db.session.commit()
                    if new_shield:
                        if return_obj:
                            return jsonify(fetch_one(new_shield.uid)), 200

                        return jsonify({"info": "Shield added successfully!"}), 200

        return jsonify({"error": "Shield not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(shield_id, shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:

            if valid_uuid(shield_id):

                _serial = data['serial']
                _mac_address = data['mac_address']
                _assigned = data['assigned']

                # _shield_type = None

                # if valid_uuid(data['shield_type_id']):
                #     _shield_type = shield_types.fetch_one(data['shield_type_id'], True).get_json()['real_id']

               
                _shield = Shield\
                        .query\
                        .filter(Shield.uid == str(shield_id))\
                        .first()
                if _shield:
                        _shield.serial = _serial
                        _shield.mac_address = _mac_address
                        # _shield.type_id = _shield_type
                        _shield.assigned = _assigned
                        db.session.commit()

                        return jsonify({"info": "Shield edited successfully!"}), 200

        return jsonify({"error": "Shield not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(shield_id):

    try:

        if valid_uuid(shield_id):
            _shield= Shield\
                .query\
                .filter(Shield.uid == shield_id)\
                .first()
            if _shield:
                _shield.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "Shadenet activated successfully!"}), 200

        return jsonify({"error": "Shadenet not activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406


def deactivate(shield_id):

    try:

        if valid_uuid(shield_id) :
            _shield = Shield\
                .query\
                .filter(Shield.uid == shield_id)\
                .first()
            if _shield:
                _shield.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shield deactivated successfully!"}), 200

        return jsonify({"error": "Shield not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
