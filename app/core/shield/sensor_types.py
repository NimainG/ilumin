import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import SensorType

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def sensor_type_obj(sensor_type=False, real_id=False):
    _sensor_type = {}
    if sensor_type:
        if real_id:
            _sensor_type["real_id"] = sensor_type.id

        _sensor_type["id"] = str(sensor_type.uid)
        _sensor_type["name"] = sensor_type.name
        _sensor_type["description"] = sensor_type.description
        _sensor_type["info_link"] = sensor_type.info_link
        _sensor_type["created_by"] = creator_detail(sensor_type.creator_id)
        _sensor_type["status"] = status_name(sensor_type.status)
        _sensor_type["created"] = sensor_type.created_at
        _sensor_type["modified"] = sensor_type.modified_at

    return _sensor_type

def fetch_all():
    response = []

    sensor_types = db\
        .session\
        .query(SensorType)\
        .filter(SensorType.status > config.STATUS_DELETED)\
        .all()

    for sensor_type in sensor_types:
        response.append(sensor_type_obj(sensor_type))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        sensor_type = db\
            .session\
            .query(SensorType)\
            .filter(SensorType.uid == uid)\
            .filter(SensorType.status > config.STATUS_DELETED)\
            .first()
        if sensor_type:
            response = sensor_type_obj(sensor_type, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        sensor_type = db\
            .session\
            .query(SensorType)\
            .filter(SensorType.id == id)\
            .filter(SensorType.status > config.STATUS_DELETED)\
            .first()
        if sensor_type:
            response = sensor_type_obj(sensor_type)

    return jsonify(response)

def add_new(sensor_type_data=None, return_obj=False):

    try:
        data = json.loads(sensor_type_data)
        if data:

            if 'name' in data and 'creator_id' in data:

                _name = data['name']
                _description = data['description'] if 'description' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _name and _creator:
                    new_sensor_type = SensorType(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        description = _description,
                        info_link = _info_link,
                        creator_id = _creator
                    )
                    db.session.add(new_sensor_type)
                    db.session.commit()
                    if new_sensor_type:
                        if return_obj:
                            return jsonify(fetch_one(new_sensor_type.uid)), 200

                        return jsonify({"info": "Sensor type added successfully!"}), 200

        return jsonify({"error": "Sensor type not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(sensor_type_id, sensor_type_data=None, return_obj=False):

    try:
        data = json.loads(sensor_type_data)
        if data:

            if valid_uuid(sensor_type_id) and  'name' in data:

                _name = data['name']
                _description = data['description'] if 'description' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                if _name:
                    _sensor_type = SensorType\
                        .query\
                        .filter(SensorType.uid == sensor_type_id)\
                        .first()
                    if _sensor_type:
                        _sensor_type.name = _name
                        _sensor_type.description = _description
                        _sensor_type.info_link = _info_link
                        db.session.commit()

                        return jsonify({"info": "Sensor type edited successfully!"}), 200

        return jsonify({"error": "Sensor type not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(sensor_type_id):

    try:

        if valid_uuid(sensor_type_id) :
            _sensor_type = SensorType\
                .query\
                .filter(SensorType.uid == sensor_type_id)\
                .first()
            if _sensor_type:
                _sensor_type.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Sensor type deactivated successfully!"}), 200

        return jsonify({"error": "Sensor type not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
