import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Module

from app.core import user
from app.core import media

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def realm_to_name(realm_id=None):
    _realm_name = ""

    if realm_id == config.USER_IGH:
        _realm_name = "IGH"

    if realm_id == config.USER_CUSTOMER:
        _realm_name = "Customer"

    if realm_id == config.USER_PARTNER:
        _realm_name = "Partner"

    return _realm_name

def module_obj(module=False, real_id=False):
    _module = {}
    if module:
        if real_id:
            _module["real_id"] = module.id

        _module["id"] = str(module.uid)
        _module["name"] = module.name
        _module["realm"] = realm_to_name(module.user_realm)
        _module["icon"] = media.fetch_by_id(module.icon_media_id).get_json()
        _module["created_by"] = creator_detail(module.creator_id)
        _module["status"] = status_name(module.status)
        _module["created"] = module.created_at
        _module["modified"] = module.modified_at

    return _module

def fetch_all():
    response = []

    modules = db\
        .session\
        .query(Module)\
        .filter(Module.status > config.STATUS_DELETED)\
        .all()

    for module in modules:
        response.append(module_obj(module))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        module = db\
            .session\
            .query(Module)\
            .filter(Module.uid == uid)\
            .filter(Module.status > config.STATUS_DELETED)\
            .first()
        if module:
            response = module_obj(module, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        module = db\
            .session\
            .query(Module)\
            .filter(Module.id == id)\
            .filter(Module.status > config.STATUS_DELETED)\
            .first()
        if module:
            response = module_obj(module)

    return jsonify(response)

def fetch_by_user_realm(user_realm_id=0, real_id=False):
    response = []
    if user_realm_id:
        modules = db\
            .session\
            .query(Module)\
            .filter(Module.user_realm == user_realm_id)\
            .filter(Module.status > config.STATUS_DELETED)\
            .all()
        for module in modules:
            response.append(module_obj(module, real_id))

    return jsonify(response)

def fetch_by_name_realm(name=None, user_realm_id=0):
    response = {}
    if name and user_realm_id:
        module = db\
            .session\
            .query(Module)\
            .filter(Module.name == name)\
            .filter(Module.user_realm == user_realm_id)\
            .filter(Module.status > config.STATUS_DELETED)\
            .first()
        if module:
            response = module_obj(module)

    return jsonify(response)

def add_new(module_data=None, return_obj=False):

    try:
        data = json.loads(module_data)
        if data:

            if 'name' in data and 'user_realm' in data and 'creator_id' in data:

                _name = data['name']
                _user_realm = data['user_realm']
                _icon_media_id = None

                if 'icon_media_id' in data and valid_uuid(data['icon_media_id']):
                    _icon_media = media\
                        .fetch_one(data['icon_media_id'], True)\
                        .get_json()
                    if _icon_media:
                        _icon_media_id = _icon_media["real_id"]

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator_obj = user.fetch_one(data['creator_id'], True).get_json()
                    if _creator_obj:
                        _creator = _creator_obj["real_id"]

                if _name and _user_realm and _creator:
                    new_module = Module(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        user_realm = _user_realm,
                        icon_media_id = _icon_media_id,
                        creator_id = _creator
                    )
                    db.session.add(new_module)
                    db.session.commit()
                    if new_module:
                        if return_obj:
                            return fetch_one(new_module.uid), 200

                        return jsonify({"info": "Module added successfully!"}), 200

        return jsonify({"error": "Module not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(module_id, module_data=None, return_obj=False):

    try:
        data = json.loads(module_data)
        if data:

            if valid_uuid(module_id) and 'name' in data and 'user_realm' in data:

                _name = data['name']
                _user_realm = data['user_realm']

                _icon_media_id = None
                if 'icon_media_id' in data and valid_uuid(data['icon_media_id']):
                    _icon_media = media\
                        .fetch_one(data['icon_media_id'], True)\
                        .get_json()
                    if _icon_media:
                        _icon_media_id = _icon_media["real_id"]

                if _name and _user_realm:
                    _module = Module\
                        .query\
                        .filter(Module.uid == module_id)\
                        .first()
                    if _module:
                        _module.name = _name
                        _module.user_realm = _user_realm
                        _module.icon_media_id = _icon_media_id
                        db.session.commit()

                        return jsonify({"info": "Module edited successfully!"}), 200

        return jsonify({"error": "Module not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(module_id):

    try:

        if valid_uuid(module_id) :
            _module = Module\
                .query\
                .filter(Module.uid == module_id)\
                .first()
            if _module:
                _module.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Module deactivated successfully!"}), 200

        return jsonify({"error": "Module not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
