import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv
from app.core.customer import shields

from app.config import activateConfig


from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import DeviceDataBatch

from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def data_source_name(source_id):
    _source = ""

    if source_id == config.DATA_CSV:
        _source = "CSV Upload"

    if source_id == config.DATA_AT:
        _source = "AfricasTalking"

    if source_id == config.DATA_G_SHEET:
        _source = "Google Sheet"

    if source_id == config.DATA_MANUAL:
        _source = "Manual"

    if source_id == config.DATA_MQTT:
        _source = "MQTT"

    return _source


def device_data_obj(data=False, real_id=False):
    _data = {}
    if data:
        if real_id:
            _data["real_id"] = data.id

        _data["id"] = str(data.uid)
        _data["batch"] = data.batch
        # _data["boron_id"] = data.boron_id
        _data["status"] = status_name(data.status)
        _data["created"] = data.created_at

    return _data

def fetch_all():
    response = []

    shield_data = db\
        .session\
        .query(DeviceDataBatch)\
        .filter(DeviceDataBatch.status > config.STATUS_DELETED)\
        .all()

    for sh in shield_data:
        response.append(device_data_obj(sh))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield_data = db\
            .session\
            .query(DeviceDataBatch)\
            .filter(DeviceDataBatch.uid == uid)\
            .filter(DeviceDataBatch.status > config.STATUS_DELETED)\
            .first()
        if shield_data:
            response = device_data_obj(shield_data, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shield_data = db\
            .session\
            .query(DeviceDataBatch)\
            .filter(DeviceDataBatch.id == id)\
            .filter(DeviceDataBatch.status > config.STATUS_DELETED)\
            .first()
        if shield_data:
            response = device_data_obj(shield_data)

    return jsonify(response)





def add_new(shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:
            if 'batch' in data: 
            

                _batch = data['batch']
                _data_source = data['data_source']
                _boron_id = data['boron_id']
                

                if _batch and _data_source:
                    new_d_data = DeviceDataBatch(
                        uid = uuid.uuid4(),
                        boron_id = _boron_id,
                        status = config.STATUS_ACTIVE,
                        batch = _batch,
                        
                    )
                    db.session.add(new_d_data)
                    db.session.commit()
                    if new_d_data:
                        if return_obj:
                            return jsonify(fetch_one(new_d_data.uid)), 200

                        return jsonify({"info": "Shield data added successfully!"}), 200

        return jsonify({"error": "Shield data not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
