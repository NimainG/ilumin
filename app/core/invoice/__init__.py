import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Invoice

from app.core import user
from app.core import customer
from app.core.invoice import payments
from app.core.invoice import particulars
from app.core.invoice import credit_notes

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def get_next_invoice_serial():
    _serial = config.INVOICE_SERIAL_START

    _latest_serial = db\
        .session\
        .query(Invoice)\
        .filter(Invoice.status > config.STATUS_DELETED)\
        .order_by(Invoice.serial.desc())\
        .first()
    if _latest_serial:
        _serial = _latest_serial.serial + 1

    return _serial

def payment_status_name(payment_status):
    _payment_status_name = ""
    if payment_status == config.PAYMENT_STATUS_PENDING:
        _payment_status_name = "Pending"

    if payment_status == config.PAYMENT_STATUS_PROCESSING:
        _payment_status_name = "Processing"

    if payment_status == config.PAYMENT_STATUS_FAILED:
        _payment_status_name = "Failed"

    if payment_status == config.PAYMENT_STATUS_SUCCESS:
        _payment_status_name = "Success"

    return payment_status_name


def invoice_obj(invoice=False, real_id=False):
    _invoice = {}
    if invoice:
        if real_id:
            _invoice["real_id"] = invoice.id

        _invoice["id"] = str(invoice.uid)
        _invoice["serial"] = invoice.serial
        _invoice["gross"] = invoice.gross
        _invoice["tax"] = invoice.tax
        _invoice["deductions"] = invoice.deductions
        _invoice["net"] = invoice.net
        _invoice["fully_paid"] = invoice.fully_paid
        _invoice["customer"] = customer.fetch_by_id(invoice.customer_id).get_json()
        _invoice["particulars"] = particulars.fetch_by_invoice_id(invoice.id).get_json()
        _invoice["payments"] = payments.fetch_by_invoice_id(invoice.id).get_json()
        _invoice["credit_notes"] = credit_notes.fetch_by_invoice_id(invoice.id).get_json()
        _invoice["created_by"] = creator_detail(invoice.creator_id)
        _invoice["status"] = status_name(invoice.status)
        _invoice["created"] = invoice.created_at
        _invoice["modified"] = invoice.modified_at

    return _invoice

def fetch_all():
    response = []

    invoices = db\
        .session\
        .query(Invoice)\
        .filter(Invoice.status > config.STATUS_DELETED)\
        .all()

    for invoice in invoices:
        response.append(invoice_obj(invoice))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        invoice = db\
            .session\
            .query(Invoice)\
            .filter(Invoice.uid == uid)\
            .filter(Invoice.status > config.STATUS_DELETED)\
            .first()
        if invoice:
            response = invoice_obj(invoice, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        invoice = db\
            .session\
            .query(Invoice)\
            .filter(Invoice.id == id)\
            .filter(Invoice.status > config.STATUS_DELETED)\
            .first()
        if invoice:
            response = invoice_obj(invoice)

    return jsonify(response)

def fetch_by_serial(serial=0):
    response = {}
    if serial:
        invoice = db\
            .session\
            .query(Invoice)\
            .filter(Invoice.serial == serial)\
            .filter(Invoice.status > config.STATUS_DELETED)\
            .first()
        if invoice:
            response = invoice_obj(invoice)

    return jsonify(response)

def fetch_by_customer_id(customer_id=0):
    response = {}
    if customer_id:
        invoice = db\
            .session\
            .query(Invoice)\
            .filter(Invoice.customer_id == customer_id)\
            .filter(Invoice.status > config.STATUS_DELETED)\
            .first()
        if invoice:
            response = invoice_obj(invoice)

    return jsonify(response)

def add_new(invoice_data=None, return_obj=False):

    try:
        data = json.loads(invoice_data)
        if data:

            if 'gross' in data and 'tax' in data and 'net' in data and \
                'customer_id' in data and 'creator_id' in data:

                _gross = data['gross']
                _tax = data['tax']
                _net = data['net']
                _fully_paid = data['fully_paid']
                _customer_id = data['customer_id']
                _deductions = data['deductions']  if 'deductions' in data else None

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _gross and _tax and _net and _customer_id and _creator:
                    new_invoice = Invoice(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        serial = get_next_invoice_serial(),
                        gross = _gross,
                        tax = _tax,
                        deductions = _deductions,
                        net = _net,
                        fully_paid = _fully_paid,
                        customer_id = _customer_id,
                        creator_id = _creator
                    )
                    db.session.add(new_invoice)
                    db.session.commit()
                    if new_invoice:
                        if return_obj:
                            return jsonify(fetch_one(new_invoice.uid)), 200

                        return jsonify({"info": "Invoice added successfully!"}), 200

        return jsonify({"error": "Invoice not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(invoice_id, invoice_data=None, return_obj=False):

    try:
        data = json.loads(invoice_data)
        if data:

            if valid_uuid(invoice_id) and 'gross' in data and 'tax' in data and 'net' in data:

                _gross = data['gross']
                _tax = data['tax']
                _net = data['net']
                _deductions = data['deductions']  if 'deductions' in data else None

                if _name:
                    _invoice = Invoice\
                        .query\
                        .filter(Invoice.uid == str(invoice_id))\
                        .first()
                    if _invoice:
                        _invoice.gross = _gross
                        _invoice.tax = _tax
                        _invoice.deductions = _deductions
                        _invoice.net = _net
                        db.session.commit()

                        return jsonify({"info": "Invoice edited successfully!"}), 200

        return jsonify({"error": "Invoice not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(invoice_id):

    try:

        if valid_uuid(invoice_id) :
            _invoice = Invoice\
                .query\
                .filter(Invoice.uid == invoice_id)\
                .first()
            if _invoice:
                _invoice.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Invoice deactivated successfully!"}), 200

        return jsonify({"error": "Invoice not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
