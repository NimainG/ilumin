import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import InvoiceParticular

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def particular_obj(particular=False, real_id=False):
    _particular = {}
    if particular:
        if real_id:
            _particular["real_id"] = particular.id

        _particular["id"] = str(particular.uid)
        _particular["item"] = particular.item
        _particular["pax"] = particular.pax
        _particular["amount"] = particular.amount
        _particular["discount"] = particular.discount
        _particular["total"] = particular.total
        _particular["created_by"] = creator_detail(particular.creator_id)
        _particular["status"] = status_name(particular.status)
        _particular["created"] = particular.created_at
        _particular["modified"] = particular.modified_at

    return _particular

def fetch_all():
    response = []

    particulars = db\
        .session\
        .query(InvoiceParticular)\
        .filter(InvoiceParticular.status > config.STATUS_DELETED)\
        .all()

    for particular in particulars:
        response.append(particular_obj(particular))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        particular = db\
            .session\
            .query(InvoiceParticular)\
            .filter(InvoiceParticular.uid == uid)\
            .filter(InvoiceParticular.status > config.STATUS_DELETED)\
            .first()
        if particular:
            response = particular_obj(particular, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        particular = db\
            .session\
            .query(InvoiceParticular)\
            .filter(InvoiceParticular.id == id)\
            .filter(InvoiceParticular.status > config.STATUS_DELETED)\
            .first()
        if particular:
            response = particular_obj(particular)

    return jsonify(response)

def fetch_by_invoice_id(invoice_id=0):
    response = {}
    if invoice_id:
        particular = db\
            .session\
            .query(InvoiceParticular)\
            .filter(InvoiceParticular.invoice_id == invoice_id)\
            .filter(InvoiceParticular.status > config.STATUS_DELETED)\
            .first()
        if particular:
            response = particular_obj(particular)

    return jsonify(response)

def add_new(particular_data=None, return_obj=False):

    try:
        data = json.loads(particular_data)
        if data:

            if 'item' in data and 'pax' in data and 'amount' in data and \
                'total' in data and 'invoice_id' in data and 'creator_id' in data:

                _item = data['item']
                _pax = data['pax']
                _amount = data['amount']
                _total = data['total']
                _invoice_id = data['invoice_id']
                _discount = data['discount'] if 'discount' in data else None

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _item and _pax and _amount and _total and _creator:
                    new_particular = InvoiceParticular(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        item = _item,
                        pax = _pax,
                        amount = _amount,
                        discount = _discount,
                        total = _total,
                        invoice_id = _invoice_id,
                        creator_id = _creator
                    )
                    db.session.add(new_particular)
                    db.session.commit()
                    if new_particular:
                        if return_obj:
                            return jsonify(fetch_one(new_particular.uid)), 200

                        return jsonify({"info": "Particular added successfully!"}), 200

        return jsonify({"error": "Particular not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(particular_id, particular_data=None, return_obj=False):

    try:
        data = json.loads(particular_data)
        if data:

            if valid_uuid(particular_id) and 'item' in data and 'pax' in data \
                and 'amount' in data and 'total' in data:

                _item = data['item']
                _pax = data['pax']
                _amount = data['amount']
                _total = data['total']
                _discount = data['discount'] if 'discount' in data else None

                if _item and _pax and _amount and _total:
                    _particular = InvoiceParticular\
                        .query\
                        .filter(InvoiceParticular.uid == particular_id)\
                        .first()
                    if _particular:
                        _particular.item = _item
                        _particular.pax = _pax
                        _particular.amount = _amount
                        _particular.discount = _discount
                        _particular.total = _total
                        db.session.commit()

                        return jsonify({"info": "Particular edited successfully!"}), 200

        return jsonify({"error": "Particular not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(particular_id):

    try:

        if valid_uuid(particular_id) :
            _particular = InvoiceParticular\
                .query\
                .filter(InvoiceParticular.uid == particular_id)\
                .first()
            if _particular:
                _particular.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Particular deactivated successfully!"}), 200

        return jsonify({"error": "Particular not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
