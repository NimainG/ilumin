import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Alert, SystemAlert, ShieldAlert, UserAlert, AlertConfig

from app.core import shield
from app.core.shield import sensor_types
from app.core import user
from app.core import customer as customer_user


load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def alert_info(id, type):
    _alert_info = {}

    if type == config.ALERT_SYSTEM:
        _detail = SystemAlert\
            .query\
            .filter(SystemAlert.alert_id == id)\
            .first()
        if _detail:
            _alert_info = {
                "name": _detail.name,
                "detail": _detail.detail,
                "trigger": _detail.trigger
            }

    if type == config.ALERT_SHIELD:
        _detail = ShieldAlert\
            .query\
            .filter(ShieldAlert.alert_id == id)\
            .first()
        if _detail:
            _alert_info = {
                "name": _detail.name,
                "current_read": _detail.current_read,
                "ideal_read": _detail.ideal_read,
                "shield": shield.fetch_by_id(_detail.shield_id).get_json(),
                "sensor": sensor_types.fetch_by_id(_detail.sensor_type_id).get_json(),
            }

    if type == config.ALERT_USER:
        _detail = UserAlert\
            .query\
            .filter(UserAlert.alert_id == id)\
            .first()
        if _detail:
            _alert_info = {
                "name": _detail.name,
                "detail": _detail.detail,
                "trigger": _detail.trigger,
                "action_text": _detail.action_text,
                "action_link": _detail.action_link
            }

    return jsonify(_alert_info)

def alert_type_name(id):
    type_name = ""
    if id:
        if id == config.ALERT_SYSTEM:
            type_name = "System"

        if id == config.ALERT_SHIELD:
            type_name = "Shield"

        if id == config.ALERT_USER:
            type_name = "User"

    return type_name

def alert_severity(id):
    severity_name = ""
    if id :
        if id == config.ALERT_INFORMATIONAL:
            severity_name = "Informational"

        if id == config.ALERT_WARNING:
            severity_name = "Warning"

        if id == config.ALERT_MINOR:
            severity_name = "Minor"

        if id == config.ALERT_MAJOR:
            severity_name = "Major"

        if id == config.ALERT_CRITICAL:
            severity_name = "Critical"

        if id == config.ALERT_FATAL:
            severity_name = "Fatal"

    return severity_name

def alert_mode_name(id):
    mode_name = ""
    if id:
        if id == config.NOTIFICATION_MODE_PROFILE:
            mode_name = "Profile"

        if id == config.NOTIFICATION_MODE_EMAIL:
            mode_name = "Email"

        if id == config.NOTIFICATION_MODE_SMS:
            mode_name = "SMS"

        if id == config.NOTIFICATION_MODE_ALL:
            mode_name = "Profile, Email and SMS"

    return mode_name


def alert_obj(alert=False, real_id=False):
    _alert = {}
    if alert:
        if real_id:
            _alert["real_id"] = alert.id

        _alert["id"] = str(alert.uid)
        _alert["type"] = alert_type_name(alert.type)
        _alert["severity"] = alert_severity(alert.severity)
        _alert["customer_id"] = customer_user.fetch_by_id(alert.customer_id, True).get_json()
        _alert["info"] = alert_info(alert.id,alert.type).get_json()
        _alert["created_by"] = creator_detail(alert.creator_id)
        _alert["status"] = status_name(alert.status)
        _alert["created_time"] = alert.created_at.strftime('%HH:%mm')
        _alert["created"] = alert.created_at

    return _alert

def fetch_all():
    response = []

    alerts = db\
        .session\
        .query(Alert)\
        .order_by(Alert.id.desc())\
        .filter(Alert.status > config.STATUS_DELETED)\
        .all()

    for alert in alerts:
        response.append(alert_obj(alert))

    return jsonify(response)



def fetch_all1(page):
    response = []
    page_limit = config.PAGE_LIMIT

    alerts = db\
        .session\
        .query(Alert)\
        .order_by(Alert.id.desc())\
        .limit(page_limit)\
        .offset((page-1)*page_limit)\
        .all()

    for alert in alerts:
        response.append(alert_obj(alert))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        alert = db\
            .session\
            .query(Alert)\
            .filter(Alert.uid == uid)\
            .filter(Alert.status > config.STATUS_DELETED)\
            .first()
        if alert:
            response = alert_obj(alert, real_id)

    return jsonify(response)

def fetch_by_customer(customer_id,page):
    page_limit = config.PAGE_LIMIT
    response = []
    if customer_id:
        customer_alerts = db\
            .session\
            .query(Alert)\
            .filter(Alert.customer_id == customer_id)\
            .filter(Alert.status > config.STATUS_DELETED)\
            .limit(page_limit)\
            .offset((page-1)*page_limit)\
            .all()
        for customer_alrt in customer_alerts:
            response.append(alert_obj(customer_alrt))

    return jsonify(response)


def fetch_by_customer_all(customer_id,type):
    page_limit = config.PAGE_LIMIT
    response = []
    if customer_id:
        customer_alerts = db\
            .session\
            .query(Alert)\
            .filter(Alert.customer_id == customer_id)\
            .filter(Alert.type == type)\
            .filter(Alert.status > config.STATUS_DELETED)\
            .all()
        for customer_alrt in customer_alerts:
            response.append(alert_obj(customer_alrt))

    return jsonify(response)

def fetch_by_type_all(type):
    response = []
    if type:
        alert = db\
            .session\
            .query(Alert)\
            .filter(Alert.type == type)\
            .filter(Alert.status > config.STATUS_DELETED)\
            .all()
        for customer_alrt in alert:
            response.append(alert_obj(customer_alrt))

    return jsonify(response)




def fetch_by_type(type):
    response = {}
    if type:
        alert = db\
            .session\
            .query(Alert)\
            .filter(Alert.type == type)\
            .filter(Alert.status > config.STATUS_DELETED)\
            .first()
        if alert:
            response = alert_obj(alert)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        alert = db\
            .session\
            .query(Alert)\
            .filter(Alert.id == id)\
            .filter(Alert.status > config.STATUS_DELETED)\
            .first()
        if alert:
            response = alert_obj(alert)

    return jsonify(response)

def add_new(alert_data=None, return_obj=False):

    try:
        data = json.loads(alert_data)
        if data:

            if 'type' in data and 'severity' in data :

                _type = data['type']
                _severity = data['severity']
                _customer = data['customer_id']
                
                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                # if valid_uuid(data['customer_id']):
                #     _customer = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

                if _type and _severity and _creator:
                    new_alert = Alert(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        type = _type,
                        severity = _severity,
                        customer_id = _customer,
                        creator_id = _creator
                    )
                    db.session.add(new_alert)
                    db.session.commit()
                    if new_alert:
                        if _type == config.ALERT_SYSTEM:
                            _name = data['name'] if 'name' in data else None
                            _detail = data['detail'] if 'detail' in data else None
                            _trigger = data['trigger'] if 'trigger' in data else None

                            if _name and _detail and _trigger:
                                new_system_alert = SystemAlert(
                                    uid = uuid.uuid4(),
                                    status = config.STATUS_ACTIVE,
                                    name = _name,
                                    detail = _detail,
                                    trigger = _trigger,
                                    alert_id = new_alert.id
                                )
                                db.session.add(new_system_alert)
                                db.session.commit()

                        if _type == config.ALERT_SHIELD:
                            _name = data['name'] if 'name' in data else None
                            _current_read = data['current_read'] if 'current_read' in data else None
                            _ideal_read = data['ideal_read'] if 'ideal_read' in data else None
                            _shield_id = data['shield_id'] if 'shield_id' in data else None
                            _sensor_type_id = data['sensor_type_id'] if 'sensor_type_id' in data else None

                            if _name and _current_read and _ideal_read and \
                                _shield_id and _sensor_type_id and _trigger:
                                new_shield_alert = ShieldAlert(
                                    uid = uuid.uuid4(),
                                    status = config.STATUS_ACTIVE,
                                    name = _name,
                                    current_read = _current_read,
                                    ideal_read = _ideal_read,
                                    shield_id = _shield_id,
                                    sensor_type_id = _sensor_type_id,
                                    customer_id = _customer,
                                    alert_id = new_alert.id
                                )
                                db.session.add(new_shield_alert)
                                db.session.commit()

                        if _type == config.ALERT_USER:
                            _name = data['name'] if 'name' in data else None
                            _detail = data['detail'] if 'detail' in data else None
                            _trigger = data['trigger'] if 'trigger' in data else None
                            _action_text = data['action_text'] if 'action_text' in data else None
                            _action_link = data['action_link'] if 'action_link' in data else None

                            if _name:
                                new_user_alert = UserAlert(
                                    uid = uuid.uuid4(),
                                    status = config.STATUS_ACTIVE,
                                    name = _name,
                                    detail = _detail,
                                    trigger = _trigger,
                                    action_text = _action_text,
                                    action_link = _action_link,
                                    alert_id = new_alert.id,
                                    customer_id = _customer,
                                    creator_id = _creator
                                )
                                db.session.add(new_user_alert)
                                db.session.commit()

                        if return_obj:
                            return jsonify(fetch_one(new_alert.uid)), 200

                        return jsonify({"info": "Alert added successfully!"}), 200

        return jsonify({"error": "Alert not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def set_alert_config(user_id, mode, creator):

    _config = {}

    _user = None
    _creator = creator
    _mode = mode

    try:

        if valid_uuid(user_id):
            _user = user.fetch_one(user_id, True).get_json()

        if _mode and _user:
            _exists = AlertConfig\
                .query\
                .filter(AlertConfig.user_id == _user['real_id'])\
                .first()
            if _exists:
                _exists.preferred_mode = _mode
                db.session.commit()

                _config = fetch_alert_config(_user['real_id']).get_json()
            else:
                new_alert_config = AlertConfig(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    user_id = _user["real_id"],
                    preferred_mode = mode,
                    creator_id = _creator
                )
                db.session.add(new_alert_config)
                db.session.commit()
                if new_alert_config:
                    _config = fetch_alert_config(_user['real_id']).get_json()

        return jsonify(_config), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def fetch_alert_config(user_id=None):
    _config = {}
    _user = user_id

    if _user:
        _alert_config = AlertConfig\
            .query\
            .filter(AlertConfig.user_id == _user)\
            .first()
        if _alert_config:
            _config = {
                "code": _alert_config.preferred_mode,
                "mode": alert_mode_name(_alert_config.preferred_mode)
            }

    return jsonify(_config), 200
